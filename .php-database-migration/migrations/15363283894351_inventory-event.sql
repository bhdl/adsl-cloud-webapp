-- // InventoryEvent
-- Migration SQL that makes the change goes here.

CREATE TABLE `inventory_event` (
  `inventoryEventId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `siteId` INT(11) UNSIGNED NOT NULL,
  `startTime` DATETIME NOT NULL,
  `endTime` DATETIME NOT NULL,
  PRIMARY KEY (`inventoryEventId`, `siteId`),
  KEY `siteId` (`siteId`),
  CONSTRAINT `inventory_event_ibfk_1` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE='utf8_general_ci' COMMENT='Leltározási események tábla';

INSERT INTO `inventory_event` VALUES
(1,1,'2018-09-06 10:24:00','2018-09-07 10:24:00'),
(2,1,'2018-09-06 10:24:00','2018-09-07 10:24:00');

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.inventoryEvent', 'admin.inventoryEvent');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryEvent', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryEvent', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryEvent', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryEvent', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryEvent', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryEvent', 'modify');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryEvent', 'view');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryEvent', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryEvent', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryEvent', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryEvent', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryEvent', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryEvent', 'modify');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryEvent', 'view');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryEvent', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryEvent', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryEvent', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryEvent', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryEvent', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryEvent', 'modify');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryEvent', 'view');


-- @UNDO
-- SQL to undo the change goes here.

ALTER table inventory_event drop foreign key inventory_event_ibfk_1;

DROP TABLE `inventory_event`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.inventoryEvent';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.inventoryEvent';
DELETE FROM `acl_resource` WHERE `resourceId` = 'admin.inventoryEvent';