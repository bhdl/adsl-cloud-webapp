-- // Realtime Map
-- Migration SQL that makes the change goes here.

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.realtimeMap', 'admin.realtimeMap');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.realtimeMap', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.realtimeMap', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.realtimeMap', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.realtimeMap', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.realtimeMap', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.realtimeMap', 'modify');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.realtimeMap', 'view');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.realtimeMap', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.realtimeMap', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.realtimeMap', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.realtimeMap', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.realtimeMap', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.realtimeMap', 'modify');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.realtimeMap', 'view');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.realtimeMap', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.realtimeMap', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.realtimeMap', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.realtimeMap', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.realtimeMap', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.realtimeMap', 'modify');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.realtimeMap', 'view');


ALTER TABLE `site`
	ADD COLUMN `projection` VARCHAR(255) NULL DEFAULT NULL AFTER `address`,
	ADD COLUMN `topLatitude` DOUBLE NULL DEFAULT NULL AFTER `projection`,
	ADD COLUMN `bottomLatitude` DOUBLE NULL DEFAULT NULL AFTER `topLatitude`,
	ADD COLUMN `rightLongitude` DOUBLE NULL DEFAULT NULL AFTER `bottomLatitude`,
	ADD COLUMN `leftLongitude` DOUBLE NULL DEFAULT NULL AFTER `rightLongitude`,
	ADD COLUMN `mapSvg` TEXT NULL DEFAULT NULL AFTER `leftLongitude`;

UPDATE `site` SET
  `projection` = 'mercator',
  `topLatitude` = 47.693444,
  `bottomLatitude` = 47.690512,
  `rightLongitude` = 19.296237,
  `leftLongitude` = 19.291946,
  `mapSvg` = 'g: { rect: [ { x: \'72.9\', y: \'69.3\', class: \'st0\', width: \'132.2\', height: \'70.5\', }, { x: \'169.5\', y: \'204\', class: \'st0\', width: \'132.2\', height: \'70.5\', }, ], }',
  `createdAt` = '2018-09-24 00:00:00',
  `updatedAt` = '2018-09-24 00:00:00';

-- @UNDO
-- SQL to undo the change goes here.

ALTER TABLE `site`
	DROP COLUMN `projection`,
	DROP COLUMN `leftLongitude`,
	DROP COLUMN `bottomLatitude`,
	DROP COLUMN `rightLongitude`,
	DROP COLUMN `topLatitude`,
	DROP COLUMN `mapSvg`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.realtimeMap';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.realtimeMap';
DELETE FROM `acl_resource` WHERE `resourceId` = 'admin.realtimeMap';