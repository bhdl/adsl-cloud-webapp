-- // product_position
-- Migration SQL that makes the change goes here.
CREATE TABLE `product_position_event` (
	`productPositionEventId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`positionEventData` JSON NULL DEFAULT NULL,
	`start` TIMESTAMP NULL DEFAULT NULL,
	`end` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`productPositionEventId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

CREATE TABLE `product_position` (
	`productPositionId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`productPositionEventId` INT(11) UNSIGNED NULL DEFAULT NULL,
	`productId` INT(11) UNSIGNED NULL DEFAULT NULL,
	`currentX` FLOAT UNSIGNED NOT NULL,
	`currentY` FLOAT UNSIGNED NOT NULL,
	`currentZ` FLOAT UNSIGNED NOT NULL,
	`newX` FLOAT UNSIGNED NULL DEFAULT NULL,
	`newY` FLOAT UNSIGNED NULL DEFAULT NULL,
	`newZ` FLOAT UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`productPositionId`),
	INDEX `product_position_fk1` (`productPositionEventId`),
	INDEX `product_position_fk2` (`productId`),
	CONSTRAINT `product_position_fk1` FOREIGN KEY (`productPositionEventId`) REFERENCES `product_position_event` (`productPositionEventId`) ON UPDATE CASCADE,
	CONSTRAINT `product_position_fk2` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`) ON UPDATE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.productPositioning', 'admin.productPositioning');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productPositioning', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productPositioning', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productPositioning', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productPositioning', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productPositioning', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productPositioning', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productPositioning', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productPositioning', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productPositioning', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productPositioning', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productPositioning', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productPositioning', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productPositioning', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productPositioning', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productPositioning', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productPositioning', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productPositioning', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productPositioning', 'modify');

-- @UNDO
-- SQL to undo the change goes here.

DROP TABLE product_position;
DROP TABLE product_position_event;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.productPositioning';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.productPositioning';
DELETE FROM `acl_resource` WHERE `resourceId` = 'admin.productPositioning';
