-- // inventory-snapshot
-- Migration SQL that makes the change goes here.

CREATE TABLE `inventory_snapshot` (
	`inventorySnapshotId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`inventoryEventId` INT(11) UNSIGNED NOT NULL,
    `productId` INT(11) UNSIGNED NOT NULL,
    `productInstanceId` INT(11) UNSIGNED NOT NULL,
    `productStatusId` INT(11) UNSIGNED NOT NULL,
    `rfid` VARCHAR(255) NOT NULL,
	`snapshotTime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updatedAt` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`deletedAt` DATETIME NULL DEFAULT NULL,
	`deleted` ENUM('yes','no') NOT NULL DEFAULT 'no',
	PRIMARY KEY (`inventorySnapshotId`),
	INDEX `inventory_snapshot_fk1` (`inventoryEventId`),
	INDEX `inventory_snapshot_fk2` (`productId`),
    INDEX `inventory_snapshot_fk3` (`productInstanceId`),
	INDEX `inventory_snapshot_fk4` (`productStatusId`),
	CONSTRAINT `inventory_snapshot_fk1` FOREIGN KEY (`inventoryEventId`) REFERENCES `inventory_event` (`inventoryEventId`) ON UPDATE CASCADE,
	CONSTRAINT `inventory_snapshot_fk2` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`) ON UPDATE CASCADE,
    CONSTRAINT `inventory_snapshot_fk3` FOREIGN KEY (`productInstanceId`) REFERENCES `product_instance` (`productInstanceId`) ON UPDATE CASCADE,
	CONSTRAINT `inventory_snapshot_fk4` FOREIGN KEY (`productStatusId`) REFERENCES `product_status` (`productStatusId`) ON UPDATE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.inventorySnapshot', 'admin.inventorySnapshot');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventorySnapshot', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventorySnapshot', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventorySnapshot', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventorySnapshot', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventorySnapshot', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventorySnapshot', 'modify');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventorySnapshot', 'view');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventorySnapshot', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventorySnapshot', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventorySnapshot', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventorySnapshot', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventorySnapshot', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventorySnapshot', 'modify');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventorySnapshot', 'view');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventorySnapshot', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventorySnapshot', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventorySnapshot', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventorySnapshot', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventorySnapshot', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventorySnapshot', 'modify');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventorySnapshot', 'view');

-- @UNDO
-- SQL to undo the change goes here.

DROP TABLE `inventory_snapshot`;
DROP TABLE `product_status`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.inventorySnapshot';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.inventorySnapshot';
DELETE FROM `acl_resource` WHERE `resourceId` = 'admin.inventorySnapshot';