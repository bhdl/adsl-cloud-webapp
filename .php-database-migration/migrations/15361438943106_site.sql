-- // Site
-- Migration SQL that makes the change goes here.

CREATE TABLE `site` (
  `siteId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `partnerId` INT(11) UNSIGNED NOT NULL,
  `gatewayId` VARCHAR(255) NOT NULL,
  `address` VARCHAR(512) NOT NULL,
  PRIMARY KEY (`siteId`, `partnerId`),
  KEY `partnerId` (`partnerId`),
  CONSTRAINT `site_ibfk_1` FOREIGN KEY (`partnerId`) REFERENCES `partner` (`partnerId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE='utf8_general_ci' COMMENT='Telephelyek tábla';

INSERT INTO `site` VALUES
(1,1,'gw-01','telephely 1'),
(2,1,'gw-02','telephely 2'),
(3,1,'srth-1','Test Site Address');

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.site', 'admin.site');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'modify');


-- @UNDO
-- SQL to undo the change goes here.

ALTER table site drop foreign key site_ibfk_1;

DROP TABLE `site`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.site';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.site';
DELETE FROM `acl_resource` WHERE `resourceId` = 'admin.site';