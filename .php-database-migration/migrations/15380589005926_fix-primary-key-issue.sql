-- // fix-primary-key-issue
-- Migration SQL that makes the change goes here.
ALTER TABLE `product`
	DROP FOREIGN KEY `product_fk_1`;
ALTER TABLE `inventory_snapshot`
	DROP FOREIGN KEY `inventory_snapshot_fk2`;
ALTER TABLE `product_instance`
	DROP FOREIGN KEY `product_instance_fk1`;

ALTER TABLE `product` MODIFY `productId` INT NOT NULL;
ALTER TABLE `product` DROP PRIMARY KEY;
ALTER TABLE `product` MODIFY `productId` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT;

ALTER TABLE `product`
	ADD CONSTRAINT `product_fk_1` FOREIGN KEY (`partnerId`) REFERENCES `partner` (`partnerId`) ON UPDATE CASCADE;
ALTER TABLE `inventory_snapshot`
	ADD CONSTRAINT `inventory_snapshot_fk2` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`) ON UPDATE CASCADE;
ALTER TABLE `product_instance`
	ADD CONSTRAINT `product_instance_fk1` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`) ON UPDATE CASCADE;

ALTER TABLE `inventory_event`
	DROP FOREIGN KEY `inventory_event_fk_1`;
ALTER TABLE `product_instance`
	DROP FOREIGN KEY `product_instance_fk2`;
ALTER TABLE `tag`
	DROP FOREIGN KEY `tag_fk_1`;

ALTER TABLE `site` MODIFY `siteId` INT NOT NULL;
ALTER TABLE `site` DROP PRIMARY KEY;
ALTER TABLE `site` MODIFY `siteId` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT;

ALTER TABLE `inventory_event`
	ADD CONSTRAINT `inventory_event_fk_1` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE;
ALTER TABLE `product_instance`
	ADD CONSTRAINT `product_instance_fk2` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE;

ALTER TABLE `tag` MODIFY `tagId` INT NOT NULL;
ALTER TABLE `tag` DROP PRIMARY KEY;
ALTER TABLE `tag` MODIFY `tagId` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT;

ALTER TABLE `tag`
	ADD CONSTRAINT `tag_fk_1` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE;

-- @UNDO
-- SQL to undo the change goes here.

ALTER TABLE `product`
	DROP FOREIGN KEY `product_fk_1`;
ALTER TABLE `inventory_snapshot`
	DROP FOREIGN KEY `inventory_snapshot_fk2`;
ALTER TABLE `product_instance`
	DROP FOREIGN KEY `product_instance_fk1`;

ALTER TABLE `product` MODIFY `productId` INT NOT NULL;
ALTER TABLE `product` DROP PRIMARY KEY;
ALTER TABLE `product` MODIFY `productId` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT;
ALTER TABLE `product` MODIFY `partnerId` INT UNSIGNED NOT NULL PRIMARY KEY;

ALTER TABLE `product`
	ADD CONSTRAINT `product_fk_1` FOREIGN KEY (`partnerId`) REFERENCES `partner` (`partnerId`) ON UPDATE CASCADE;
ALTER TABLE `inventory_snapshot`
	ADD CONSTRAINT `inventory_snapshot_fk2` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`) ON UPDATE CASCADE;
ALTER TABLE `product_instance`
	ADD CONSTRAINT `product_instance_fk1` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`) ON UPDATE CASCADE;

ALTER TABLE `inventory_event`
	DROP FOREIGN KEY `inventory_event_fk_1`;
ALTER TABLE `product_instance`
	DROP FOREIGN KEY `product_instance_fk2`;
ALTER TABLE `tag`
	DROP FOREIGN KEY `tag_fk_1`;

ALTER TABLE `site` MODIFY `siteId` INT NOT NULL;
ALTER TABLE `site` DROP PRIMARY KEY;
ALTER TABLE `site` MODIFY `siteId` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT;
ALTER TABLE `site` MODIFY `partnerId` INT UNSIGNED NOT NULL PRIMARY KEY;

ALTER TABLE `inventory_event`
	ADD CONSTRAINT `inventory_event_fk_1` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE;
ALTER TABLE `product_instance`
	ADD CONSTRAINT `product_instance_fk2` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE;

ALTER TABLE `tag`
	DROP FOREIGN KEY `tag_fk_1`;

ALTER TABLE `tag` MODIFY `tagId` INT NOT NULL;
ALTER TABLE `tag` DROP PRIMARY KEY;
ALTER TABLE `tag` MODIFY `tagId` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT;
ALTER TABLE `tag` MODIFY `siteId` INT UNSIGNED NOT NULL PRIMARY KEY;

ALTER TABLE `tag`
	ADD CONSTRAINT `tag_fk_1` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE;