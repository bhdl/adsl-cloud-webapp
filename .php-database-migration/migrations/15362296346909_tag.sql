-- // Tag
-- Migration SQL that makes the change goes here.
CREATE TABLE `tag` (
  `tagId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `siteId` int(11) UNSIGNED NOT NULL,
  `tagToken` varchar(255) NOT NULL,
  `clientId` varchar(255),
  `type` enum('forklift','person','drone') NOT NULL DEFAULT 'forklift',
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`tagId`,`siteId`),
  KEY `fk_tag_1_idx` (`siteId`),
  CONSTRAINT `fk_tag_1` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tag tábla';

INSERT INTO `tag` (`tagId`, `siteId`, `tagToken`, `clientId` , `type`, `name`, `description`) VALUES (1, 1, 'tagToken1', 'exClient1' , 'forklift', 'First', 'Indoor');
INSERT INTO `tag` (`tagId`, `siteId`, `tagToken`, `clientId` , `type`, `name`, `description`) VALUES (2, 1, 'tagToken2', 'exClient2' , 'person', 'Joe', 'Indoor');
INSERT INTO `tag` (`tagId`, `siteId`, `tagToken`, `clientId` , `type`, `name`, `description`) VALUES (3, 1, 'tagToken3', NULL , 'drone', 'Test', 'Outdoor');

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.tag', 'admin.tag');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.tag', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.tag', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.tag', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.tag', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.tag', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.tag', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.tag', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.tag', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.tag', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.tag', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.tag', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.tag', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.tag', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.tag', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.tag', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.tag', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.tag', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.tag', 'modify');

-- @UNDO
-- SQL to undo the change goes here.

DROP TABLE `tag`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.tag';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.tag';
DELETE FROM `acl_resource` WHERE `resourceId` = 'admin.tag';