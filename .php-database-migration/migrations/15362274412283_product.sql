-- // Product
-- Migration SQL that makes the change goes here.

CREATE TABLE `product` (
  `productId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `partnerId` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` MEDIUMTEXT,
  PRIMARY KEY (`productId`, `partnerId`),
  KEY `partnerId` (`partnerId`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`partnerId`) REFERENCES `partner` (`partnerId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE='utf8_general_ci' COMMENT='Termékek tábla';

INSERT INTO `product` VALUES
(1,1,'Product1','Long description'),
(2,1,'Product2','Another description');

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.product', 'admin.product');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'modify');

-- @UNDO
-- SQL to undo the change goes here.

ALTER table product drop foreign key product_ibfk_1;

DROP TABLE `product`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.product';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.product';
DELETE FROM `acl_resource` WHERE `resourceId` = 'admin.product';