-- // inventory status
-- Migration SQL that makes the change goes here.

CREATE TABLE `inventory_status` (
  `inventoryStatusId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `inventoryEventId` INT(11) UNSIGNED NOT NULL,
  `rfid` VARCHAR(255) NOT NULL,
  `x` FLOAT NOT NULL,
  `y` FLOAT NOT NULL,
  `z` FLOAT NOT NULL,
  `accuracy` FLOAT NOT NULL,
  `latitude` FLOAT NOT NULL,
  `longitude` FLOAT NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updatedAt` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`deletedAt` DATETIME NULL,
	`deleted` ENUM('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`inventoryStatusId`, `inventoryEventId`),
  KEY `inventoryEventId` (`inventoryEventId`),
  CONSTRAINT `inventory_status_ibfk_1` FOREIGN KEY (`inventoryEventId`) REFERENCES `inventory_event` (`inventoryEventId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE='utf8_general_ci' COMMENT='Leltározási státusz tábla';

INSERT INTO `inventory_status` (`inventoryEventId`,`rfid`,`x`,`y`,`z`,`accuracy`,`latitude`,`longitude`) VALUES
(1,'randomrfid001',1.5,1.5,1.5,0.8,30.63,67.1150),
(1,'randomrfid002',1.5,1.5,1.5,0.8,30.63,67.1150);

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.inventoryStatus', 'admin.inventoryStatus');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryStatus', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryStatus', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryStatus', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryStatus', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryStatus', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.inventoryStatus', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryStatus', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryStatus', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryStatus', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryStatus', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryStatus', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.inventoryStatus', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryStatus', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryStatus', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryStatus', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryStatus', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryStatus', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.inventoryStatus', 'modify');

-- @UNDO
-- SQL to undo the change goes here.

ALTER table inventory_status drop foreign key inventory_status_ibfk_1;

DROP TABLE `inventory_status`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.inventoryStatus';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.inventoryStatus';
DELETE FROM `acl_resource` WHERE `resourceId` = 'admin.inventoryStatus';