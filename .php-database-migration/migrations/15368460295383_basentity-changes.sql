-- // basentity_changes
-- Migration SQL that makes the change goes here.

ALTER TABLE `inventory_event`
	ADD COLUMN `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `endTime`,
	ADD COLUMN `updatedAt` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `createdAt`,
	ADD COLUMN `deletedAt` DATETIME NULL AFTER `updatedAt`,
	ADD COLUMN `deleted` ENUM('yes','no') NOT NULL DEFAULT 'no' AFTER `deletedAt`;

ALTER TABLE `partner`
	ADD COLUMN `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ADD COLUMN `updatedAt` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `createdAt`,
	ADD COLUMN `deletedAt` DATETIME NULL AFTER `updatedAt`,
	ADD COLUMN `deleted` ENUM('yes','no') NOT NULL DEFAULT 'no' AFTER `deletedAt`;

ALTER TABLE `product`
	ADD COLUMN `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ADD COLUMN `updatedAt` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `createdAt`,
	ADD COLUMN `deletedAt` DATETIME NULL AFTER `updatedAt`,
	ADD COLUMN `deleted` ENUM('yes','no') NOT NULL DEFAULT 'no' AFTER `deletedAt`;

ALTER TABLE `site`
	ADD COLUMN `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ADD COLUMN `updatedAt` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `createdAt`,
	ADD COLUMN `deletedAt` DATETIME NULL AFTER `updatedAt`,
	ADD COLUMN `deleted` ENUM('yes','no') NOT NULL DEFAULT 'no' AFTER `deletedAt`;

ALTER TABLE `tag`
	ADD COLUMN `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ADD COLUMN `updatedAt` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `createdAt`,
	ADD COLUMN `deletedAt` DATETIME NULL AFTER `updatedAt`,
	ADD COLUMN `deleted` ENUM('yes','no') NOT NULL DEFAULT 'no' AFTER `deletedAt`;

ALTER TABLE `site`
	DROP FOREIGN KEY `site_ibfk_1`;

ALTER TABLE `product`
	DROP FOREIGN KEY `product_ibfk_1`;

ALTER TABLE `tag`
	DROP FOREIGN KEY `fk_tag_1`;

ALTER TABLE `inventory_event`
	DROP FOREIGN KEY `inventory_event_ibfk_1`;

ALTER TABLE `partner`
	CHANGE COLUMN `partnerId` `partnerId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST;

ALTER TABLE `product`
	ALTER `partnerId` DROP DEFAULT;
ALTER TABLE `product`
	CHANGE COLUMN `productId` `productId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `partnerId` `partnerId` INT(11) UNSIGNED NOT NULL AFTER `productId`;

ALTER TABLE `site`
	ALTER `partnerId` DROP DEFAULT;
ALTER TABLE `site`
	CHANGE COLUMN `siteId` `siteId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `partnerId` `partnerId` INT(11) UNSIGNED NOT NULL AFTER `siteId`;

ALTER TABLE `tag`
	ALTER `siteId` DROP DEFAULT;
ALTER TABLE `tag`
	CHANGE COLUMN `tagId` `tagId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `siteId` `siteId` INT(11) UNSIGNED NOT NULL AFTER `tagId`;

ALTER TABLE `inventory_event`
	ALTER `siteId` DROP DEFAULT;
ALTER TABLE `inventory_event`
	CHANGE COLUMN `inventoryEventId` `inventoryEventId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `siteId` `siteId` INT(11) UNSIGNED NOT NULL AFTER `inventoryEventId`;

ALTER TABLE `inventory_event`
	ADD CONSTRAINT `inventory_event_fk_1` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE;

ALTER TABLE `tag`
	ADD CONSTRAINT `tag_fk_1` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE;

ALTER TABLE `site`
	ADD CONSTRAINT `site_fk_1` FOREIGN KEY (`partnerId`) REFERENCES `partner` (`partnerId`) ON UPDATE CASCADE;

ALTER TABLE `product`
	ADD CONSTRAINT `product_fk_1` FOREIGN KEY (`partnerId`) REFERENCES `partner` (`partnerId`) ON UPDATE CASCADE;


-- @UNDO
-- SQL to undo the change goes here.

ALTER TABLE `inventory_event`
	DROP COLUMN `createdAt`,
	DROP COLUMN `updatedAt`,
	DROP COLUMN `deletedAt`,
	DROP COLUMN `deleted`;

ALTER TABLE `partner`
	DROP COLUMN `createdAt`,
	DROP COLUMN `updatedAt`,
	DROP COLUMN `deletedAt`,
	DROP COLUMN `deleted`;

ALTER TABLE `product`
	DROP COLUMN `createdAt`,
	DROP COLUMN `updatedAt`,
	DROP COLUMN `deletedAt`,
	DROP COLUMN `deleted`;

ALTER TABLE `site`
	DROP COLUMN `createdAt`,
	DROP COLUMN `updatedAt`,
	DROP COLUMN `deletedAt`,
	DROP COLUMN `deleted`;

ALTER TABLE `tag`
	DROP COLUMN `createdAt`,
	DROP COLUMN `updatedAt`,
	DROP COLUMN `deletedAt`,
	DROP COLUMN `deleted`;

ALTER TABLE `site`
	DROP FOREIGN KEY `site_ibfk_1`;

ALTER TABLE `product`
	DROP FOREIGN KEY `product_ibfk_1`;

ALTER TABLE `tag`
	DROP FOREIGN KEY `fk_tag_1`;

ALTER TABLE `inventory_event`
	DROP FOREIGN KEY `inventory_event_ibfk_1`;

ALTER TABLE `inventory_status`
	DROP FOREIGN KEY `inventory_status_ibfk_1`;


ALTER TABLE `inventory_status`
	ADD CONSTRAINT `inventory_status_ibfk_1` FOREIGN KEY (`inventoryEventId`) REFERENCES `inventory_event` (`inventoryEventId`) ON UPDATE CASCADE;

ALTER TABLE `inventory_event`
	ADD CONSTRAINT `inventory_event_ibfk_1` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE;

ALTER TABLE `tag`
	ADD CONSTRAINT `fk_tag_1` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE;

ALTER TABLE `site`
	ADD CONSTRAINT `site_ibfk_1` FOREIGN KEY (`partnerId`) REFERENCES `partner` (`partnerId`) ON UPDATE CASCADE;

ALTER TABLE `product`
	ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`partnerId`) REFERENCES `partner` (`partnerId`) ON UPDATE CASCADE;
