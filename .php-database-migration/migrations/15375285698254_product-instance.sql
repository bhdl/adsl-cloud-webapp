-- // Product Instance
-- Migration SQL that makes the change goes here.

CREATE TABLE `product_status` (
	`productStatusId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`productStatusId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

INSERT INTO `product_status` (`productStatusId`, `name`) VALUES (1, 'Új');
INSERT INTO `product_status` (`productStatusId`, `name`) VALUES (2, 'Használt');
INSERT INTO `product_status` (`productStatusId`, `name`) VALUES (3, 'Garanciális');

CREATE TABLE `product_instance` (
	`productInstanceId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`productStatusId` INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`productId` INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`siteId` INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`rfid` VARCHAR(255) NOT NULL DEFAULT '0',
	`createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updatedAt` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`deletedAt` DATETIME NULL DEFAULT NULL,
	`deleted` ENUM('yes','no') NOT NULL DEFAULT 'no',
	PRIMARY KEY (`productInstanceId`),
	INDEX `product_instance_fk1` (`productId`),
	INDEX `product_instance_fk2` (`siteId`),
	INDEX `product_instance_fk3` (`productStatusId`),
	CONSTRAINT `product_instance_fk1` FOREIGN KEY (`productId`) REFERENCES `product` (`productId`) ON UPDATE CASCADE,
	CONSTRAINT `product_instance_fk2` FOREIGN KEY (`siteId`) REFERENCES `site` (`siteId`) ON UPDATE CASCADE,
	CONSTRAINT `product_instance_fk3` FOREIGN KEY (`productStatusId`) REFERENCES `product_status` (`productStatusId`) ON UPDATE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.productInstance', 'admin.productInstance');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productInstance', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productInstance', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productInstance', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productInstance', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productInstance', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productInstance', 'modify');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.productInstance', 'view');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productInstance', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productInstance', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productInstance', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productInstance', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productInstance', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productInstance', 'modify');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.productInstance', 'view');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productInstance', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productInstance', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productInstance', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productInstance', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productInstance', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productInstance', 'modify');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.productInstance', 'view');

-- @UNDO
-- SQL to undo the change goes here.

DROP TABLE `product_instance`;
DROP TABLE `product_status`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.productInstance';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.productInstance';
DELETE FROM `acl_resource` WHERE `resourceId` = 'admin.productInstance';