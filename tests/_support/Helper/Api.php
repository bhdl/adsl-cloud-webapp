<?php
namespace AppTest\Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Api extends \Codeception\Module
{
  protected $version = 'v1';
  protected $pageNumber = 'page';
  protected $dataPerPage = 'number';
  protected $urlFront = 'rest';

  public function getFrontOfPath() {
    return '/'.$this->urlFront.'/'.$this->version.'/';
  }

  public function getPaginationUrl($page = 1, $dataNumberOfPage = 10) {
    return $this->pageNumber.'='.$page.'&'.$this->dataPerPage.'='.$dataNumberOfPage;
  }

}
