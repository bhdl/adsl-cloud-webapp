<?php

namespace AppTest\Filter;

use App\Filter\Boolean;

/**
 * Class BooleanTest
 * @package AppTest
 */
class BooleanTest
    extends \Codeception\Test\Unit
{
    /**
     * @var Boolean
     */
    protected $filter;

    /**
     * _before
     */
    protected function _before()
    {
        $this->filter = new Boolean();
    }

    /**
     * _after
     */
    protected function _after()
    {
    }

    /**
     * @dataProvider booleanDataProvider
     *
     * @param $expected
     * @param $value
     */
    public function testBoolean($expected, $value)
    {
        $this->assertEquals(
            $expected
            , $this->filter->filter($value)
        );
    }

    /**
     * @return array
     */
    public function booleanDataProvider()
    {
        return [
            [true, 'true']
            , [false, 'false']
            , [true, 1]
            , [false, 0]
            , [true, '1']
            , [false, '0']
        ];
    }
}