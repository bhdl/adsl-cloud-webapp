<?php

namespace AppTest\Filter;

use App\Filter\NullString;

/**
 * Class NullStringTest
 * @package AppTest
 */
class NullStringTest
    extends \Codeception\Test\Unit
{
    /**
     * @dataProvider nullStringWithTypeAllDataProvider
     *
     * @param $expected
     * @param $value
     */
    public function testNullStringWithTypeAll($expected, $value)
    {
        $filter = new NullString();

        $this->assertEquals(
            $expected
            , $filter->filter($value)
        );
    }

    /**
     * @return array
     */
    public function nullStringWithTypeAllDataProvider()
    {
        return [
            [null, '0']
            , [null, '']
            , [null, []]
            , [null, 0]
            , [null, false]
            , [null, 'null']
            , [null, 'NULL']
        ];
    }

    /**
     * @dataProvider nullStringWithTypeStringAndNullStringDataProvider
     *
     * @param $expected
     * @param $value
     */
    public function testNullStringWithTypeStringAndNullString($expected, $value)
    {
        $filter = new NullString([
            NullString::STRING
            , NullString::NULL_STRING
        ]);

        $this->assertEquals(
            $expected
            , $filter->filter($value)
        );
    }

    /**
     * @return array
     */
    public function nullStringWithTypeStringAndNullStringDataProvider()
    {
        return [
            [null, '']
            , [null, 'null']
            , [null, 'NULL']
        ];
    }
}