<?php

namespace AppTest\Filter;

use App\Filter\StringClean;

/**
 * Class StringCleanTest
 * @package AppTest
 */
class StringCleanTest
    extends \Codeception\Test\Unit
{
    /**
     * @var StringClean
     */
    protected $filter;

    /**
     * _before
     */
    protected function _before()
    {
        $this->filter = new StringClean();
    }


    /**
     * @dataProvider stringCleanDataProvider
     *
     * @param $expected
     * @param $value
     */
    public function testStringClean($expected, $value)
    {
        $this->assertEquals(
            $expected
            , $this->filter->filter($value)
        );
    }

    /**
     * @return array
     */
    public function stringCleanDataProvider()
    {
        return [
            ["Árvíztűrő tükörfúrógép", "Árvíztűrő tükörfúrógép\r\n"]
            , ["Árvíztűrő tükörfúrógép", "\rÁrvíztűrő tükörfúrógép\r\n"]
            , ["Árvíztűrő tükörfúrógép", "\n\rÁrvíztűrő tükörfúrógép\r\n"]
            , ["Árvíztűrő tükörfúrógép", "Árvíztűrő tükörfúrógép"]
        ];
    }
}