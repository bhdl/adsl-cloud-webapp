<?php

namespace AppTest\Model\Behavior;

use Phalcon\Di;

/**
 * Class LogModelCollectorTest
 * @package AppTest\Model\Behavior
 */
class LogModelCollectorTest
    extends \Codeception\Test\Unit
{
    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    /**
     * _before
     */
    protected function _before()
    {
    }

    /**
     * _after
     */
    protected function _after()
    {
    }

    /**
     * testCreateModel
     */
    public function testCreateModel()
    {
        $db = Di::getDefault()->get('db');

        try {
            $db->beginLogged();

            $group            = new \App\Model\Group();
            $group->groupName = 'Unit Test';
            $group->guest     = \App\Model\Group::NO;
            $group->active    = \App\Model\Group::YES;

            $group->create();
            
            $models = \App\Model\Behavior\LogModelCollector::getModels();
            
            if (!is_array($models) || count($models) != 1) {
                $this->tester->fail('Nem található a Group model.');
            }

            $this->isValidCollectedModelStructure($models[0]);

            $data = $group->toArray(false);

            $this->tester->assertEquals('App\Model\Group', $models[0]['model']);
            $this->tester->assertEquals($data, $models[0]['data']);
            $this->tester->assertEquals([], $models[0]['snapShotData']);
            $this->tester->assertEquals(\Phalcon\Mvc\Model::OP_CREATE, $models[0]['operation']);
            $this->tester->assertEquals(array_keys($data), $models[0]['changed']);

            $db->rollbackLogged();

        } catch (\Phalcon\Exception $e) {
            $db->rollbackLogged();
        }
    }

    /**
     * testUpdateModel
     */
    public function testUpdateModel()
    {
        $db = Di::getDefault()->get('db');

        try {
            $db->beginLogged();

            $group            = new \App\Model\Group();
            $group->groupName = 'Unit Test';
            $group->guest     = \App\Model\Group::NO;
            $group->active    = \App\Model\Group::YES;
            $group->create();

            \App\Model\Behavior\LogModelCollector::clearModels();

            $snapShotData = $group->toArray(false);

            $group->groupName = 'Unit Test2';
            $group->active    = \App\Model\Group::NO;
            $group->update();

            $models = \App\Model\Behavior\LogModelCollector::getModels();

            if (!is_array($models) || count($models) != 1) {
                $this->tester->fail('Nem található a Group model.');
            }

            $this->isValidCollectedModelStructure($models[0]);

            $data = $group->toArray(false);

            $this->tester->assertEquals('App\Model\Group', $models[0]['model']);
            $this->tester->assertEquals($data, $models[0]['data']);
            $this->tester->assertEquals($snapShotData, $models[0]['snapShotData']);
            $this->tester->assertEquals(\Phalcon\Mvc\Model::OP_UPDATE, $models[0]['operation']);
            $this->tester->assertEquals([
                'groupId', 'groupName', 'active'
            ], $models[0]['changed']);

            $db->rollbackLogged();

        } catch (\Phalcon\Exception $e) {
            $db->rollbackLogged();
        }
    }

    /**
     * testDeleteModel
     */
    public function testDeleteModel()
    {
        $db = Di::getDefault()->get('db');

        try {
            $db->beginLogged();

            $group            = new \App\Model\Group();
            $group->groupName = 'Unit Test';
            $group->guest     = \App\Model\Group::NO;
            $group->active    = \App\Model\Group::YES;
            $group->create();

            \App\Model\Behavior\LogModelCollector::clearModels();

            $snapShotData = $group->toArray(false);

            $group->delete();

            $models = \App\Model\Behavior\LogModelCollector::getModels();

            if (!is_array($models) || count($models) != 1) {
                $this->tester->fail('Nem található a Group model.');
            }

            $this->isValidCollectedModelStructure($models[0]);

            $data = $group->toArray(false);

            $this->tester->assertEquals('App\Model\Group', $models[0]['model']);
            $this->tester->assertEquals($data, $models[0]['data']);
            $this->tester->assertEquals($snapShotData, $models[0]['snapShotData']);
            $this->tester->assertEquals(\Phalcon\Mvc\Model::OP_DELETE, $models[0]['operation']);
            $this->tester->assertEquals(array_keys($data), $models[0]['changed']);

            $db->rollbackLogged();

        } catch (\Phalcon\Exception $e) {
            $db->rollbackLogged();
        }
    }

    /**
     * @param array $model
     */
    protected function isValidCollectedModelStructure(array $model)
    {
        $this->tester->assertArrayHasKey('model', $model);
        $this->tester->assertTrue(is_string($model['model']));

        $this->tester->assertArrayHasKey('data', $model);
        $this->tester->assertTrue(is_array($model['data']));

        $this->tester->assertArrayHasKey('snapShotData', $model);
        $this->tester->assertTrue(is_array($model['snapShotData']));

        $this->tester->assertArrayHasKey('operation', $model);
        $this->tester->assertTrue(is_numeric($model['operation']));
        $this->tester->assertTrue(in_array($model['operation'], [
            \Phalcon\Mvc\Model::OP_NONE
            , \Phalcon\Mvc\Model::OP_CREATE
            , \Phalcon\Mvc\Model::OP_DELETE
            , \Phalcon\Mvc\Model::OP_UPDATE
        ]));

        $this->tester->assertArrayHasKey('changed', $model);
        $this->tester->assertTrue(is_array($model['changed']));
    }
}