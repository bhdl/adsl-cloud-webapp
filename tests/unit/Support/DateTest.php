<?php
namespace AppTest\Support;
use App\Support\Date;

class DateTest
    extends \Codeception\Test\Unit
{
    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * testSecondsToString
     *
     * @dataProvider secondsToStringProvider
     *
     * @param $expected
     * @param $input
     */
    public function testSecondsToString($expected, $input)
    {
        $date = new Date();
        $this->assertEquals($expected, $date->secondsToString($input));
    }

    public function secondsToStringProvider()
    {
        return [
            ['', 0]
            , ['30 másodperc', 30]
            , ['1 perc', 60]
            , ['5 perc', 300]
            , ['1 óra', 3600]
            , ['1 óra, 1 perc', 3660]
            , ['1 nap, 1 másodperc', 86401]
            , ['1 hét', 604800]
            , ['1 hét, 1 nap, 1 óra, 1 perc, 1 másodperc', 694861]
        ];
    }
}
