<?php

namespace AppTest\Support;
use App\Support\Str;

/**
 * Class StrTest
 * @package AppTest
 */
class StrTest
    extends \Codeception\Test\Unit
{
    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    /**
     * _before
     */
    protected function _before()
    {
    }

    /**
     * _after
     */
    protected function _after()
    {
    }

    /**
     * testFirstUpper
     *
     * @dataProvider firstUpperProvider
     *
     * @param $expected
     * @param $input
     */
    public function testFirstUpper($expected, $input)
    {
        $this->assertEquals($expected, Str::firstUpper($input));
    }

    /**
     * testFirstLower
     *
     * @dataProvider firstLowerProvider
     *
     * @param $expected
     * @param $input
     */
    public function testFirstLower($expected, $input)
    {
        $this->assertEquals($expected, Str::firstLower($input));
    }

    /**
     * testShuffle
     *
     * @dataProvider shuffleProvider
     *
     * @param $originalString
     */
    public function testShuffle($originalString)
    {
        $shuffeledString = Str::shuffle($originalString);
        $this->assertTrue(StrTest::areContainsSameCharacters($originalString, $shuffeledString));
    }

    public function testRandomHashWithDefaultLength()
    {
        $randomHash = Str::randomHash();
        $this->assertEquals(8, strlen($randomHash));
    }

    public function testRandomHashContainsOnlyLettersAndNumbers()
    {
        $randomHash = Str::randomHash();
        $this->assertTrue(StrTest::isContainsOnlyLettersAndNumbers($randomHash));
    }

    public function testRandomHashWithPositiveLength()
    {
        $length = rand(2, 35);
        $randomHash = Str::randomHash($length);
        $this->assertEquals($length, strlen($randomHash));
    }

    public function testRandomHashWithNegativeLength()
    {
        $randomHash = Str::randomHash(rand(-30, -1));
        $this->assertEquals('', $randomHash);
    }

    public function firstUpperProvider() {
        return [
            ['Hello', 'hello']
            , ['Hello', 'Hello']
            , ['HELLO', 'hELLO']
            , ['', '']
            , ['A', 'a']
            , ['A', 'A']
            , ['First letter', 'first letter']
        ];
    }

    public function firstLowerProvider() {
        return [
            ['hello', 'hello']
            , ['hello', 'Hello']
            , ['hELLO', 'HELLO']
            , ['', '']
            , ['a', 'a']
            , ['a', 'A']
            , ['first Letter', 'First Letter']
        ];
    }

    public function shuffleProvider()
    {
        return [
            ['']
            , ['ddjsdsljfnvnj sfdjjsfjld']
            , ['ashsshhd']
            , ['aASDHNC aajdj 1234']
        ];
    }

    public static function isContainsOnlyLettersAndNumbers($string)
    {
        return ctype_alnum($string);
    }

    public static function areContainsSameCharacters($textFirst, $textSecond)
    {
        $charsFirst = preg_split('//', $textFirst, -1, PREG_SPLIT_NO_EMPTY);
        $charsSecond = preg_split('//', $textSecond, -1, PREG_SPLIT_NO_EMPTY);

        sort($charsFirst);
        sort($charsSecond);

        return implode('', $charsFirst) === implode('', $charsSecond);
    }
}