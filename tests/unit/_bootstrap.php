<?php

define('APP_PATH', dirname(dirname(dirname(__FILE__))));

require_once APP_PATH . '/vendor/autoload.php';

// Register DI services which are required for testing
$services = [
    'dotEnv'      => '\\App\\ServiceProvider\\Service\\DotEnv'
    , 'app'       => '\\App\\ServiceProvider\\Service\\App'
    , 'config'    => '\\App\\ServiceProvider\\Service\\Config'
    , 'loader'    => '\\App\\ServiceProvider\\Service\\Loader'
    , 'db'        => '\\App\\ServiceProvider\\Service\\Db'
    , 'auth'      => '\\App\\ServiceProvider\\Service\\Auth'
    , 'session'   => '\\App\\ServiceProvider\\Service\\Session'
    , 'filter'    => '\\App\\ServiceProvider\\Service\\Filter'
    , 'tag'       => '\\App\\ServiceProvider\\Service\\Admin\\Tag'
    , 'session'   => '\\App\\ServiceProvider\\Service\\Session'
    , 'logger'    => '\\App\\ServiceProvider\\Service\\Logger'
];

$bootstrap = new \App\Bootstrap();
$bootstrap->registerServices($services);

/**
 * @var \App\Model\User $user
 */
$user = $bootstrap->getDi()
    ->get('auth')
    ->getIdentity()
;

// Locale should be hungarian (hu_HU) al the cases, the test cases should align to this.
$user->setInterfaceLanguage(
    \App\Model\Language::findFirstByLocale('hu_HU')
);

// Register locale dependent services
$bootstrap->registerServices([
    'locale'      => '\\App\\ServiceProvider\\Service\\Locale'
    , 'translate' => '\\App\\ServiceProvider\\Service\\Translate'
]);