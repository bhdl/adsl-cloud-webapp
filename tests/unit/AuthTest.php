<?php
namespace AppTest;

/**
 * Class AuthTest
 * @package AppTest
 */
class AuthTest extends \Codeception\Test\Unit
{
    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * Guest user, if no login
     */
    public function testUserIsGuest()
    {
        $user = \Phalcon\Di::getDefault()->get('auth')->getIdentity();
        $this->assertEquals(true, $user->isGuest());
    }
}