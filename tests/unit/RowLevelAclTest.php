<?php
namespace AppTest;

use App\Model\User;
use App\Model\Job;
use PHPUnit\Framework\TestResult;

/**
 * Class RowLevelAclTest
 * @package AppTest
 */
class RowLevelAclTest
    extends \Codeception\Test\Unit
{
    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    /**
     * @var Job
     */
    protected $model;

    /**
     * _before
     */
    protected function _before()
    {
        $this->model = new Job();
        $this->model->jobType = Job::JOB_TYPE_EXPORT;
        $this->model->create();
    }

    /**
     * _after
     */
    protected function _after()
    {
        $this->model->delete();
    }

    /**
     * testFindWithEmptyArguments
     */
    public function testFindWithEmptyArguments()
    {
        $model = Job::find();
        $this->assertEquals(1, $model->count());
        $this->assertModel($model->getFirst());
    }

    /**
     * testFindFirstWithEmptyArguments
     */
    public function testFindFirstWithEmptyArguments()
    {
        $model = Job::findFirst();
        $this->assertModel($model);
    }

    /**
     * testFindWithNumericArgument
     */
    /*public function testFindWithNumericArgument()
    {
        $model = Job::find((int)$this->model->jobId);
        $this->assertEquals(1, $model->count());
        $this->assertModel($model->getFirst());
    }*/

    /**
     * testFindFirstWithNumericArgument
     */
    /*public function testFindFirstWithNumericArgument()
    {
        $model = Job::findFirst((int)$this->model->jobId);
        $this->assertModel($model->getFirst());
    }*/

    /**
     * testFindWithArrayArgumentsAndCondition
     */
    public function testFindWithArrayArgumentsAndCondition()
    {
        $model = Job::find([
            'conditions' => 'jobType = \'' . Job::JOB_TYPE_EXPORT . '\''
        ]);
        $this->assertEquals(1, $model->count());
        $this->assertModel($model->getFirst());
    }

    /**
     * testFindFirstWithArrayArgumentsAndCondition
     */
    public function testFindFirstWithArrayArgumentsAndCondition()
    {
        $model = Job::findFirst([
            'conditions' => 'jobType = \'' . Job::JOB_TYPE_EXPORT . '\''
        ]);
        $this->assertModel($model);
    }

    /**
     * testFindWithArrayArgumentsAndFirstKeyIsString
     */
    public function testFindWithArrayArgumentsAndFirstKeyIsString()
    {
        $model = Job::find([
            'jobType = \'' . Job::JOB_TYPE_EXPORT . '\''
            , 'order' => 'jobId ASC'
        ]);
        $this->assertEquals(1, $model->count());
        $this->assertModel($model->getFirst());
    }

    /**
     * testFindFirstWithArrayArgumentsAndFirstKeyIsString
     */
    public function testFindFirstWithArrayArgumentsAndFirstKeyIsString()
    {
        $model = Job::findFirst([
            'jobType = \'' . Job::JOB_TYPE_EXPORT . '\''
            , 'order' => 'jobId ASC'
        ]);
        $this->assertModel($model);
    }

    /**
     * testFindWithArrayArgumentsAndNoConditions
     */
    public function testFindWithArrayArgumentsAndNoConditions()
    {
        $model = Job::find([
            'order' => 'jobId ASC'
        ]);
        $this->assertEquals(1, $model->count());
        $this->assertModel($model->getFirst());
    }

    /**
     * testFindFirstWithArrayArgumentsAndNoConditions
     */
    public function testFindFirstWithArrayArgumentsAndNoConditions()
    {
        $model = Job::findFirst([
            'order' => 'jobId ASC'
        ]);
        $this->assertModel($model);
    }

    /**
     * testBuilder
     */
    /*public function testBuilderWithNoParams()
    {
        $b = new \App\Model\Builder\Job();
        $builder = $b->build();

        $resultSet = $builder->getQuery()->execute();

        $this->assertEquals(1, $resultSet->count());
        $this->assertModel(new Job($resultSet->getFirst()->toArray()));
    }*/

    /**
     * testBuilderWithParams
     */
    /*public function testBuilderWithParams()
    {
        $b = new \App\Model\Builder\VatPeriod();

        $builder = $b->build([
            'vatPeriodStartAtFrom' => '2000-01-01'
            , 'vatPeriodStartAtTo' => '2020-01-01'
        ]);

        $resultSet = $builder->getQuery()->execute();

        $this->assertEquals(1, $resultSet->count());
        $this->assertModel(new VatPeriod($resultSet->getFirst()->toArray()));
    }*/

    /**
     * @param VatGroup $model
     */
    protected function assertModel($model)
    {
        $this->assertEquals($this->model->jobId, $model->jobId);
        $this->assertEquals($this->getUser()->userId, $model->userId);
    }

    /**
     * @return User
     */
    protected function getUser()
    {
        return \Phalcon\Di::getDefault()->get('auth')->getIdentity();
    }
}