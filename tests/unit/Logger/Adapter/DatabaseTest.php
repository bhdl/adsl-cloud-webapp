<?php

namespace AppTest\Logger\Adapter;

use App\Model\Log;
use Phalcon\Di;
use Phalcon\Logger;
use Phalcon\Logger\Multiple;

/**
 * Class DatabaseTest
 * @package AppTest\Logger\Adapter
 */
class DatabaseTest
    extends \Codeception\Test\Unit
{
    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    /**
     * @var \App\Db\Adapter\MysqlLogged
     */
    protected $db;

    /**
     * @var Multiple
     */
    protected $logger;

    /**
     * @var \App\Model\User
     */
    protected $user;

    /**
     * _before
     */
    protected function _before()
    {
        $this->db     = Di::getDefault()->get('db');
        $this->logger = Di::getDefault()->get('logger');
        $this->user   = Di::getDefault()->get('auth')->getIdentity();
    }

    /**
     * _after
     */
    protected function _after()
    {
    }

    /**
     * testCreateModel
     */
    public function testCreateModel()
    {
        try {
            $this->db->beginLogged();

            $group            = new \App\Model\Group();
            $group->groupName = 'Unit Test';
            $group->guest     = \App\Model\Group::NO;
            $group->active    = \App\Model\Group::YES;
            $group->create();

            $message = 'A csoport létrehozása sikerült.';

            $this->logger->info($message);

            $arguments =
                'objectId = ' . $group->groupId
                . ' AND className = \'App\\\Model\\\Group\''
                . ' AND operation = ' . \App\Model\Group::OP_CREATE
            ;

            $logModel = \App\Model\LogModel::findFirst($arguments);
            $this->isValidLogModel($logModel);

            $log = $logModel->getLog();
            $this->isValidLog($message, $log);

            $map = [
                'groupId'     => [null, $group->groupId]
                , 'groupName' => [null, $group->groupName]
                , 'guest'     => [null, $group->guest]
                , 'active'    => [null, $group->active]
            ];

            $this->isValidLogModelAttributes($map, $logModel->getLogModelAttributes());

            $this->db->rollbackLogged();

        } catch (\Phalcon\Exception $e) {
            $this->db->rollbackLogged();
        }
    }

    /**
     * testUpdateModel
     */
    public function testUpdateModel()
    {
        try {
            $this->db->beginLogged();

            $group            = new \App\Model\Group();
            $group->groupName = 'Unit Test';
            $group->guest     = \App\Model\Group::NO;
            $group->active    = \App\Model\Group::YES;
            $group->create();

            $group->groupName = 'Unit Test2';
            $group->active    = \App\Model\Group::NO;
            $group->update();

            $message = 'A csoport módosítása sikerült.';

            $this->logger->info($message);

            $arguments =
                'objectId = ' . $group->groupId
                . ' AND className = \'App\\\Model\\\Group\''
                . ' AND operation = ' . \App\Model\Group::OP_UPDATE
            ;

            $logModel = \App\Model\LogModel::findFirst($arguments);
            $this->isValidLogModel($logModel);

            $log = $logModel->getLog();
            $this->isValidLog($message, $log);

            $map = [
                'groupName' => ['Unit Test', $group->groupName]
                , 'active'  => [\App\Model\Group::YES, $group->active]
            ];

            $this->isValidLogModelAttributes($map, $logModel->getLogModelAttributes());

            $this->db->rollbackLogged();

        } catch (\Phalcon\Exception $e) {
            $this->db->rollbackLogged();
        }
    }

    /**
     * testDeleteModel
     */
    public function testDeleteModel()
    {
        try {
            $this->db->beginLogged();

            $group            = new \App\Model\Group();
            $group->groupName = 'Unit Test';
            $group->guest     = \App\Model\Group::NO;
            $group->active    = \App\Model\Group::YES;
            $group->create();

            $group->delete();

            $message = 'A csoport törlése sikerült.';

            $this->logger->info($message);

            $arguments =
                'objectId = ' . $group->groupId
                . ' AND className = \'App\\\Model\\\Group\''
                . ' AND operation = ' . \App\Model\Group::OP_DELETE
            ;

            $logModel = \App\Model\LogModel::findFirst($arguments);
            $this->isValidLogModel($logModel);

            $log = $logModel->getLog();
            $this->isValidLog($message, $log);

            $this->tester->assertEmpty($logModel->getLogModelAttributes());

            $this->db->rollbackLogged();

        } catch (\Phalcon\Exception $e) {
            $this->db->rollbackLogged();
        }
    }

    /**
     * @param $message
     * @param $log
     */
    protected function isValidLog($message, $log)
    {
        $this->tester->assertInstanceOf(\App\Model\Log::class, $log);

        $this->tester->assertEquals($this->user->userId, $log->userId);
        $this->tester->assertEquals(Logger::INFO, $log->priority);
        $this->tester->assertEquals($message, $log->content);
        $this->tester->assertNull($log->exception);
    }

    /**
     * @param $logModel
     */
    protected function isValidLogModel($logModel)
    {
        $this->tester->assertInstanceOf(\App\Model\LogModel::class, $logModel);
    }

    /**
     * @param $map
     * @param $logModelAttributes
     */
    protected function isValidLogModelAttributes($map, $logModelAttributes)
    {
        $logModelAttributes = $logModelAttributes->toArray();

        $i = 0;
        foreach ($map as $attribute=>$values) {
            list($oldValue, $newValue) = $values;
            $this->tester->assertEquals($attribute, $logModelAttributes[$i]['attribute']);
            $this->tester->assertEquals($oldValue, $logModelAttributes[$i]['oldValue']);
            $this->tester->assertEquals($newValue, $logModelAttributes[$i]['newValue']);
            $i++;
        }
    }
}