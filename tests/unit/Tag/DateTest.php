<?php

namespace AppTest\Tag;
use Phalcon\Di;

/**
 * Class DateTest
 * @package AppTest
 */
class DateTest
    extends \Codeception\Test\Unit
{
    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    /**
     * _before
     */
    protected function _before()
    {
    }

    /**
     * _after
     */
    protected function _after()
    {
    }

    /**
     * testCurrency
     */
    public function testDate()
    {
        $tag = Di::getDefault()->get('tag');

        $this->assertEquals('2000-01-01', $tag->date('2000-01-01'));
    }
}