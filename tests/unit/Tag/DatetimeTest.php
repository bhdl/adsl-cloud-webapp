<?php

namespace AppTest\Tag;
use Phalcon\Di;

/**
 * Class DatetimeTest
 * @package AppTest
 */
class DatetimeTest
    extends \Codeception\Test\Unit
{
    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    /**
     * _before
     */
    protected function _before()
    {
    }

    /**
     * _after
     */
    protected function _after()
    {
    }

    /**
     * testDatetime
     */
    public function testDatetime()
    {
        $tag = Di::getDefault()->get('tag');

        $this->assertEquals('2000-01-01, 13:48:00', $tag->datetime('2000-01-01 13:48:00'));
    }
}