<?php

namespace AppTest\Tag;
use Phalcon\Di;

/**
 * Class DateIntervalTest
 * @package AppTest
 */
class DateIntervalTest
    extends \Codeception\Test\Unit
{
    public $tag;

    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    /**
     * _before
     */
    protected function _before()
    {
        $this->tag = Di::getDefault()->get('tag');
    }

    /**
     * _after
     */
    protected function _after()
    {
    }

    /**
     * testDateInterval
     *
     * @dataProvider dateIntervalProvider
     *
     * @param string $expected string representation of a date interval
     * @param array of stringDate $dateInterval [fromDate, toDate]
     */
    public function testDateInterval($expected, $dateInterval)
    {
        list($from, $to) = $dateInterval;
        $this->assertEquals($expected, $this->tag->dateInterval($from, $to));
    }

    public function dateIntervalProvider()
    {
        return [
            ['2000-01-01 - 2000-01-05', ['2000-01-01', '2000-01-05']]
            , ['2000-01-01', ['2000-01-01', '2000-01-01']]
        ];
    }
}