<?php

namespace AppTest\Tag;
use Phalcon\Di;

/**
 * Class CurrencyTest
 * @package AppTest
 */
class CurrencyTest
    extends \Codeception\Test\Unit
{
    public $tag;

    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    /**
     * _before
     */
    protected function _before()
    {
        $this->tag = Di::getDefault()->get('tag');
    }

    /**
     * _after
     */
    protected function _after()
    {
    }

    /**
     * testCurrency
     *
     * @dataProvider currencyProvider
     *
     * @param string $expected string representation of the given number and currency
     * @param int or float $number number representation of a currency
     */
    public function testCurrency($expected, $number)
    {
        $this->assertEquals($expected, $this->tag->currency($number, 'HUF'));
    }

    public function currencyProvider()
    {
        return [
            ['0 Ft', 0]
            , ['1 Ft', 1]
            , ['100 Ft', 100]
            , ['99,10 Ft', 99.1]
            , ['99,12 Ft', 99.12]
            , ['1 000 Ft', 1000]
        ];
    }
}