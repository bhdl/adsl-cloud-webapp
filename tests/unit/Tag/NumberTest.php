<?php

namespace AppTest\Tag;
use Phalcon\Di;

/**
 * Class NumberTest
 * @package AppTest
 */
class NumberTest
    extends \Codeception\Test\Unit
{
    public $tag;

    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    /**
     * _before
     */
    protected function _before()
    {
        $this->tag = Di::getDefault()->get('tag');
    }

    /**
     * _after
     */
    protected function _after()
    {
    }

    /**
     * testNumber
     *
     * @dataProvider numberProvider
     *
     * @param string $expected string representation of the given number
     * @param int or float $inputNumber simple number
     */
    public function testNumber($expected, $inputNumber)
    {
        $this->assertEquals($expected, $this->tag->number($inputNumber));
    }

    public function numberProvider()
    {
        return [
            ['0', 0]
            , ['1', 1]
            , ['100', 100]
            , ['99,1', 99.1]
            , ['99,12', 99.12]
            , ['0', 0.00]
            , ['1 000', 1000]
        ];
    }
}