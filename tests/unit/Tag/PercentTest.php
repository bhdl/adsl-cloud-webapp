<?php

namespace AppTest\Tag;
use Phalcon\Di;

/**
 * Class PercentTest
 * @package AppTest
 */
class PercentTest
    extends \Codeception\Test\Unit
{
    public $tag;

    /**
     * @var \AppTest\UnitTester
     */
    protected $tester;

    /**
     * _before
     */
    protected function _before()
    {
        $this->tag = Di::getDefault()->get('tag');
    }

    /**
     * _after
     */
    protected function _after()
    {
    }

    /**
     * testCurrency
     *
     * @dataProvider percentProvider
     */
    public function testPercent($expected, $input)
    {
        $this->assertEquals($expected, $this->tag->percent($input));
    }

    public function percentProvider()
    {
        return [
            ['0%', 0]
            , ['1%', 1]
            , ['100%', 100]
            , ['99,1%', 99.1]
            , ['99,12%', 99.12]
            , ['0%', 0.00]
        ];
    }
}