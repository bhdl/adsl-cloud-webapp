<?php
namespace AppTest\Partner;

use App\Model\Partner;
use AppTest\ApiTester;
use \Codeception\Util\Debug as Debug;
use Phalcon\Di;

class PartnerListCest
{
  protected $apiEndpoint = 'partner';

  protected $partner;

  protected $partnerResponseSchema = [
    'data' => [
      'partners' => 'array',
      'pagination' => [
        'count' => 'integer',
        'per_page' => 'integer',
        'current_page' => 'integer',
        'total_pages' => 'integer',
        'next_url' => 'string'
      ]
    ]
  ];

  protected $partnersSchema = [
    'partnerId' => 'integer',
    'name' => 'string',
  ];

  protected $responseData;

  public function _before(ApiTester $I)
  {
    $this->partner = new Partner();
    $this->partner->name = 'abc';
    $this->partner->create();
    $this->partner->refresh();     
    
    $I->haveHttpHeader('Content-Type', 'application/json');
  }

  public function _after(ApiTester $I)
  {
    if ($this->partner) {
        $this->partner->delete();
    }
  }

  // tests
    public function getAllPartnerJsonValid(ApiTester $I)
  {
    $I->wantTo('response is valid json against schema');
    $I->sendGET($I->getFrontOfPath().$this->apiEndpoint);
    $this->responseData = json_decode($I->grabResponse());
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseMatchesJsonType($this->partnerResponseSchema);
    foreach ($this->responseData->data->partners as $partner) {
      $I->assertEquals(property_exists($partner, 'partnerId'),true);
      $I->assertEquals(property_exists($partner, 'name'),true);
    }
  }

  public function getAllPartnerContainsPartner(ApiTester $I)
  {
    $I->wantTo('response contains the created partner');
    $url = $I->getFrontOfPath().$this->apiEndpoint.'/?'.$I->getPaginationUrl(1,Partner::count()+1);
    $I->sendGET($url);
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseContainsJson(array('partnerId' => $this->partner->partnerId, 
      'name' => $this->partner->name ));
  }

  public function getAllPartnerCountPartners(ApiTester $I)
  {
    $I->wantTo('response data/pagination/count tag equal partners count of database');
    $I->sendGET($I->getFrontOfPath().$this->apiEndpoint);
    $this->responseData = json_decode($I->grabResponse());
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->assertEquals($this->responseData->data->pagination->count, Partner::count());
  }

  public function getAllPartnerPaginationDataCorrelate(ApiTester $I){
    $I->wantTo('count / per_page = total_page in response data/pagination');
    $I->sendGET($I->getFrontOfPath().$this->apiEndpoint);
    $this->responseData = json_decode($I->grabResponse());
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $numberOfPages = floor($this->responseData->data->pagination->count / $this->responseData->data->pagination->per_page) + 1;
    $I->assertEquals($numberOfPages, $this->responseData->data->pagination->total_pages);
  }

  public function getAllPartnerPaginationDataEqualGivenData(ApiTester $I){
    $I->wantTo('per_page and current_page equals the given paramerets in response pagination');
    $url = $I->getFrontOfPath().$this->apiEndpoint.'/?'.$I->getPaginationUrl(1,5);
    $I->sendGET($url);
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $this->responseData = json_decode($I->grabResponse());
    $I->assertEquals($this->responseData->data->pagination->current_page, 1);
    $I->assertEquals($this->responseData->data->pagination->per_page, 5);
  }

  public function totalPageAndNextUrl(ApiTester $I){
    $I->wantTo('if total page <= current page then next url is empty');
    $numberOfPartner = Partner::count();
    $pageParameter = $numberOfPartner + 10;
    $url = $I->getFrontOfPath().$this->apiEndpoint.'/?'.$I->getPaginationUrl($pageParameter,1);
    $I->sendGET($url);
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $this->responseData = json_decode($I->grabResponse());
    $I->assertEquals($this->responseData->data->pagination->current_page, $pageParameter);
    $I->assertEquals($this->responseData->data->pagination->total_pages, $numberOfPartner);
    $I->assertEmpty($this->responseData->data->pagination->next_url);
  }
}
