<?php
namespace AppTest\Partner;

use App\Model\Partner;
use AppTest\ApiTester;
use \Codeception\Util\Debug as Debug;
use Phalcon\Di;

class CreatePartnerCest
{
  protected $apiEndpoint = 'partner';
  protected $partner;
  protected $responseData;

  public function _before(ApiTester $I)
  {  
    $I->haveHttpHeader('Content-Type', 'application/json');
  }

  public function _after(ApiTester $I)
  {
    if ($this->partner) {
      $this->partner->delete();
    }
  }

  // tests
  public function createPartner(ApiTester $I)
  {
    $I->wantTo('should be sucessful create partner');
    $url = $I->getFrontOfPath().$this->apiEndpoint;
    $I->sendPOST($url,[	'name' => 'TestPartnerName']);
    $I->seeResponseCodeIs(201);
    $I->seeResponseIsJson();
    $I->seeResponseMatchesJsonType(array('data' => 'array'));
    $I->seeResponseMatchesJsonType(array('partnerId' => 'integer', 
      'name' => 'string' ), '$.data[0]');
    $this->responseData = json_decode($I->grabResponse());
    $this->partner = Partner::findFirstByPartnerId($this->responseData->data[0]->partnerId);
    $I->assertNotEmpty($this->partner);
    $I->assertEquals($this->responseData->data[0]->partnerId, $this->partner->partnerId);
    $I->assertEquals($this->responseData->data[0]->name, $this->partner->name);
    $I->assertEquals($this->responseData->data[0]->name, 'TestPartnerName');
  }

  //hibás api javításra vár
  //azonos név
  public function missingName(ApiTester $I)
  {
    $I->wantTo('should be error when create partner with missing name');
    $url = $I->getFrontOfPath().$this->apiEndpoint;
    $I->sendPOST($url,[	'name' => '']);
    $I->seeResponseCodeIs(400);
    $I->seeResponseIsJson();
    $I->seeResponseMatchesJsonType(array('error' => ['code' => 'string', 
      'title' => 'string', 'message' => 'string']));
    $I->seeResponseContainsJson([
      'error' => [
        'code' => 'ERR_MISSING_ARGUMENT',
        'title' => 'Missing Argument',
        'message' => 'The name is required!'
      ]
    ]);
  }
}
