<?php
namespace AppTest\Partner;

use App\Model\Partner;
use AppTest\ApiTester;
use \Codeception\Util\Debug as Debug;
use Phalcon\Di;

class GetPartnerCest
{
  protected $apiEndpoint = 'partner';

  protected $partner;

  public function _before(ApiTester $I)
  {
    $this->partner = new Partner();
    $this->partner->name = 'abc';
    $this->partner->create();
    $this->partner->refresh();     
    
    $I->haveHttpHeader('Content-Type', 'application/json');
  }

  public function _after(ApiTester $I)
  {
    if ($this->partner) {
      $this->partner->delete();
    }
  }

  // tests
  public function getValidJsonPartner(ApiTester $I)
  {
    $I->wantTo('get valid json partner data when existing id is given');
    $I->sendGET($I->getFrontOfPath().$this->apiEndpoint.'/'.$this->partner->partnerId);
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseMatchesJsonType(array('data' => ['partnerId' => 'integer', 
    'name' => 'string' ]));
  }

  public function getPartner(ApiTester $I)
  {
    $I->wantTo('get partner data where existing id is given');
    $I->sendGET($I->getFrontOfPath().$this->apiEndpoint.'/'.$this->partner->partnerId);
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseContainsJson(array('data' => ['partnerId' => $this->partner->partnerId, 
    'name' => $this->partner->name ]));
  }

  public function getPartnerError(ApiTester $I)
  {
    $I->wantTo('get partner data when id is not exists');
    $maxId = Partner::maximum(
      [
          'column' => 'partnerId',
      ]
    );
    $maxId += 100;
    $I->sendGET($I->getFrontOfPath().$this->apiEndpoint.'/'.$maxId);
    $I->seeResponseCodeIs(401);
    $I->seeResponseIsJson();
    $I->seeResponseContainsJson([
      'error' => [
        'code' => 'ERR_RESOURCE_NOT_EXIST',
        'title' => 'Resource Not Exist',
        'message' => 'The requested tag does not exist!'
      ]
    ]);
  }
}
