DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `partnerId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `updatedBy` INT(11) NOT NULL,
  `updatedAt` DATETIME NOT NULL,
  `createdBy` INT(11) NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `deleted` ENUM('yes','no') NOT NULL DEFAULT 'no' COMMENT 'a partner logikailag törlve van-e',
  PRIMARY KEY (`partnerId`),
  UNIQUE KEY `name` (`name`) 
) ENGINE=INNODB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Partner tábla';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` VALUES
(1,'Partner1',1,'2018-09-06 10:24:00',1,'2018-09-06 10:24:00','no'),
(2,'Partner2',1,'2018-09-06 10:24:00',1,'2018-09-06 10:24:00','no');
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.partner', 'admin.partner');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.partner', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.partner', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.partner', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.partner', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.partner', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.partner', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.partner', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.partner', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.partner', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.partner', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.partner', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.partner', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.partner', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.partner', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.partner', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.partner', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.partner', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.partner', 'modify');

-- @UNDO
-- SQL to undo the change goes here.

DROP TABLE `partner`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.partner';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.partner';
DELETE FROM `acl_resource` WHERE resourceId = 'admin.partner';

-- Dumping structure for table adsl-webapp.product
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `productId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `partnerId` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` MEDIUMTEXT,
  `updatedBy` INT(11) NOT NULL,
  `updatedAt` DATETIME NOT NULL,
  `createdBy` INT(11) NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `deleted` ENUM('yes','no') NOT NULL DEFAULT 'no' COMMENT 'a termék logikailag törlve van-e',
  PRIMARY KEY (`productId`, `partnerId`),
  UNIQUE KEY `name` (`name`),
  KEY `partnerId` (`partnerId`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`partnerId`) REFERENCES `partner` (`partnerId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Termékek tábla';

INSERT INTO `product` VALUES
(1,1,'Product1','Long description',1,'2018-09-06 10:24:00',1,'2018-09-06 10:24:00','no'),
(2,1,'Product2','Another description',1,'2018-09-06 10:24:00',1,'2018-09-06 10:24:00','no');

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.product', 'admin.product');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.product', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.product', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.product', 'modify');

-- @UNDO
-- SQL to undo the change goes here.

DROP TABLE `product`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.product';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.product';
DELETE FROM `acl_resource` WHERE resourceId = 'admin.product';

-- Dumping structure for table adsl-webapp.site
DROP TABLE IF EXISTS `site`;
CREATE TABLE `site` (
  `siteId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `partnerId` INT(11) UNSIGNED NOT NULL,
  `gatewayId` VARCHAR(255) NOT NULL,
  `address` VARCHAR(512) NOT NULL,
  `updatedBy` INT(11) NOT NULL,
  `updatedAt` DATETIME NOT NULL,
  `createdBy` INT(11) NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `deleted` ENUM('yes','no') NOT NULL DEFAULT 'no' COMMENT 'a termék logikailag törlve van-e',
  PRIMARY KEY (`siteId`, `partnerId`),
  KEY `partnerId` (`partnerId`),
  CONSTRAINT `site_ibfk_1` FOREIGN KEY (`partnerId`) REFERENCES `partner` (`partnerId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Telephelyek tábla';

INSERT INTO `site` VALUES
(1,1,'gw-01','telephely 1',1,'2018-09-06 10:24:00',1,'2018-09-06 10:24:00','no'),
(2,1,'gw-02','telephely 1',1,'2018-09-06 10:24:00',1,'2018-09-06 10:24:00','no')

INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES ('admin.site', 'admin.site');

INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'ajax');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'create');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'dataTable');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'delete');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'index');
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES ('admin.site', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('admin', 'admin.site', 'modify');

INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'ajax');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'create');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'dataTable');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'delete');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'index');
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES ('arteries', 'admin.site', 'modify');

-- @UNDO
-- SQL to undo the change goes here.

DROP TABLE `site`;

DELETE FROM `acl_matrix` WHERE `resourceId` = 'admin.site';
DELETE FROM `acl_resource_has_action` WHERE `resourceId` = 'admin.site';
DELETE FROM `acl_resource` WHERE resourceId = 'admin.site';