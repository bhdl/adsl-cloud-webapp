DELETE FROM user_session;
DELETE FROM user_token;
DELETE FROM user_reset_password;
DELETE FROM navigation_history;
DELETE FROM log;
DELETE FROM job;
DELETE FROM data_table_state;
DELETE FROM translation;