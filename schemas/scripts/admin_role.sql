SET @targetRole = 'arteries';

delete from acl_matrix where roleId = @targetRole;

insert into `acl_matrix` (select @targetRole, resourceId, actionId from acl_resource_has_action);