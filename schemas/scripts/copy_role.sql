
SET @targetRole = 'adsl';
SET @sourceRole = 'arteries';

delete from acl_matrix where roleId = @targetRole;
insert into `acl_matrix` (select @targetRole, resourceId, actionId from acl_matrix where roleId = @sourceRole);