CREATE OR REPLACE VIEW view__log AS
SELECT
  l.logId
  , l.userId
  , l.name
  , l.priority
  , CASE (l.priority)
    WHEN 0 THEN 'emergency'
    WHEN 1 THEN 'critical'
    WHEN 2 THEN 'alert'
    WHEN 3 THEN 'error'
    WHEN 4 THEN 'warning'
    WHEN 5 THEN 'notice'
    WHEN 6 THEN 'info'
    WHEN 7 THEN 'debug'
    WHEN 8 THEN 'custom'
    ELSE 'special'
  END AS priorityName
  , l.createdAt
  , l.content
  , IF(LENGTH(l.content) > 100, CONCAT(SUBSTRING(l.content, 1, 100), ' ...'), l.content) contentShort
  , l.exception
  , u.name `user`
FROM
  log l
INNER JOIN
  view__user u
ON
  l.userId = u.userId