CREATE OR REPLACE VIEW view__page AS
SELECT
   p.*
  ,l.languageId
  ,l.pageName
  ,l.slug
  , l.content
  ,(SELECT MAX(position) FROM page WHERE category = p.category) = p.position as isMax
  ,(SELECT MIN(position) FROM page WHERE category = p.category) = p.position as isMin
FROM page as p
JOIN page_language as l
ON p.pageId = l.pageId
