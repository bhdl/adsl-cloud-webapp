CREATE OR REPLACE VIEW view__cron AS
    SELECT
        className,
        timing,
        status,
        lastRunAt,
        startedAt,
        timeout,
        active,
        IF(className = 'App\\Cron\\Job\\Monitor', 'yes', 'no') AS 'isMonitor',
        IF(((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(startedAt)) / 60) > timeout AND
           status = 'started', 'yes', 'no'
        ) AS 'isGarbage',
        CASE
        WHEN status = 'started' OR active = 'no' THEN 'no'
        WHEN lastRunAt IS NULL OR status IS NULL OR startedAt IS NULL THEN 'yes'
        WHEN ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(startedAt)) / 60) >= timing THEN 'yes'
        ELSE 'no'
        END AS 'isNeedToRun'
    FROM
        cron
