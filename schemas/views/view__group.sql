CREATE OR REPLACE VIEW view__group AS
SELECT
  g.*
  , (SELECT COUNT(*) FROM `user` u WHERE u.groupId = g.groupId) AS userCount
FROM
  `group` g