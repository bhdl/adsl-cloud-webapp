CREATE OR REPLACE VIEW view__user AS
SELECT
  u.*
  , g.groupName
  , g.guest
FROM
  `user` u
INNER JOIN
  `group` g
ON
  g.groupId = u.groupId
;