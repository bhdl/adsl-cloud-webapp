CREATE OR REPLACE VIEW view__translation AS
SELECT
  t.*
  , IF(t.key = t.value, 0, 1) changed
FROM
  translation t
;