-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.17 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Verzió:              9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping structure for tábla adsl.acl_action
DROP TABLE IF EXISTS `acl_action`;
CREATE TABLE IF NOT EXISTS `acl_action` (
  `actionId` VARCHAR(80) NOT NULL,
  `actionName` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`actionId`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Akciók';

-- Dumping data for table adsl.acl_action: ~48 rows (approximately)
/*!40000 ALTER TABLE `acl_action` DISABLE KEYS */;
INSERT INTO `acl_action` (`actionId`, `actionName`) VALUES
	('ajax', 'ajax'),
	('assignHasMany', 'assignHasMany'),
	('assignHasManyToMany', 'assignHasManyToMany'),
	('avatar', 'avatar'),
	('browse', 'browse'),
	('change', 'change'),
	('config', 'config'),
	('convert', 'convert'),
	('create', 'create'),
	('createHasMany', 'createHasMany'),
	('createHasManyToMany', 'createHasManyToMany'),
	('dataTable', 'dataTable'),
	('dataTableHasMany', 'dataTableHasMany'),
	('dataTableHasManyToMany', 'dataTableHasManyToMany'),
	('delete', 'delete'),
	('deleteHasMany', 'deleteHasMany'),
	('deleteHasManyToMany', 'deleteHasManyToMany'),
	('document', 'document'),
	('download', 'download'),
	('export', 'export'),
	('import', 'import'),
	('index', 'index'),
	('indexHasMany', 'indexHasMany'),
	('indexHasManyToMany', 'indexHasManyToMany'),
	('list', 'list'),
	('logout', 'logout'),
	('map', 'map'),
	('modify', 'modify'),
	('modifyHasMany', 'modifyHasMany'),
	('modifyHasManyToMany', 'modifyHasManyToMany'),
	('outdated', 'outdated'),
	('password', 'password'),
	('position', 'position'),
	('preview', 'preview'),
	('print', 'print'),
	('read', 'read'),
	('referralExamination', 'referralExamination'),
	('referralOther', 'referralOther'),
	('referralPayment', 'referralPayment'),
	('referralSample', 'referralSample'),
	('restore', 'restore'),
	('search', 'search'),
	('send', 'send'),
	('transaction', 'transaction'),
	('translate', 'translate'),
	('view', 'view'),
	('viewHasMany', 'viewHasMany'),
	('viewHasManyToMany', 'viewHasManyToMany');
/*!40000 ALTER TABLE `acl_action` ENABLE KEYS */;

-- Dumping structure for tábla adsl.acl_matrix
DROP TABLE IF EXISTS `acl_matrix`;
CREATE TABLE IF NOT EXISTS `acl_matrix` (
  `roleId` VARCHAR(80) NOT NULL,
  `resourceId` VARCHAR(80) NOT NULL,
  `actionId` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`roleId`,`resourceId`,`actionId`),
  KEY `resourceId` (`resourceId`) USING BTREE,
  KEY `actionId` (`actionId`) USING BTREE,
  CONSTRAINT `acl_matrix_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `acl_role` (`roleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acl_matrix_ibfk_2` FOREIGN KEY (`resourceId`) REFERENCES `acl_resource` (`resourceId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acl_matrix_ibfk_3` FOREIGN KEY (`actionId`) REFERENCES `acl_action` (`actionId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Jogosultsági mátrix';

-- Dumping data for table adsl.acl_matrix: ~248 rows (approximately)
/*!40000 ALTER TABLE `acl_matrix` DISABLE KEYS */;
INSERT INTO `acl_matrix` (`roleId`, `resourceId`, `actionId`) VALUES
	('admin', 'admin.acl', 'create'),
	('admin', 'admin.acl', 'dataTable'),
	('admin', 'admin.acl', 'delete'),
	('admin', 'admin.acl', 'index'),
	('admin', 'admin.acl', 'modify'),
	('arteries', 'admin.acl', 'create'),
	('arteries', 'admin.acl', 'dataTable'),
	('arteries', 'admin.acl', 'delete'),
	('arteries', 'admin.acl', 'index'),
	('arteries', 'admin.acl', 'modify'),
	('admin', 'admin.address', 'ajax'),
	('admin', 'admin.address', 'create'),
	('admin', 'admin.address', 'dataTable'),
	('admin', 'admin.address', 'index'),
	('arteries', 'admin.address', 'ajax'),
	('arteries', 'admin.address', 'create'),
	('arteries', 'admin.address', 'dataTable'),
	('arteries', 'admin.address', 'index'),
	('admin', 'admin.auth', 'change'),
	('admin', 'admin.auth', 'index'),
	('admin', 'admin.auth', 'logout'),
	('arteries', 'admin.auth', 'change'),
	('arteries', 'admin.auth', 'index'),
	('arteries', 'admin.auth', 'logout'),
	('guest', 'admin.auth', 'index'),
	('guest', 'admin.auth', 'logout'),
	('admin', 'admin.bnoCodes', 'ajax'),
	('admin', 'admin.bnoCodes', 'create'),
	('admin', 'admin.bnoCodes', 'dataTable'),
	('admin', 'admin.bnoCodes', 'delete'),
	('admin', 'admin.bnoCodes', 'index'),
	('admin', 'admin.bnoCodes', 'modify'),
	('admin', 'admin.bnoCodes', 'search'),
	('admin', 'admin.bnoCodes', 'send'),
	('admin', 'admin.bnoCodes', 'view'),
	('arteries', 'admin.bnoCodes', 'ajax'),
	('arteries', 'admin.bnoCodes', 'create'),
	('arteries', 'admin.bnoCodes', 'dataTable'),
	('arteries', 'admin.bnoCodes', 'delete'),
	('arteries', 'admin.bnoCodes', 'index'),
	('arteries', 'admin.bnoCodes', 'modify'),
	('arteries', 'admin.bnoCodes', 'search'),
	('arteries', 'admin.bnoCodes', 'send'),
	('arteries', 'admin.bnoCodes', 'view'),
	('admin', 'admin.browser', 'index'),
	('arteries', 'admin.browser', 'index'),
	('admin', 'admin.categories', 'ajax'),
	('admin', 'admin.categories', 'create'),
	('admin', 'admin.categories', 'index'),
	('admin', 'admin.categories', 'modify'),
	('admin', 'admin.categories', 'search'),
	('admin', 'admin.categories', 'send'),
	('admin', 'admin.categories', 'view'),
	('arteries', 'admin.categories', 'ajax'),
	('arteries', 'admin.categories', 'create'),
	('arteries', 'admin.categories', 'index'),
	('arteries', 'admin.categories', 'modify'),
	('arteries', 'admin.categories', 'search'),
	('arteries', 'admin.categories', 'send'),
	('arteries', 'admin.categories', 'view'),
	('arteries', 'admin.config', 'index'),
	('admin', 'admin.cron', 'dataTable'),
	('admin', 'admin.cron', 'index'),
	('arteries', 'admin.cron', 'dataTable'),
	('arteries', 'admin.cron', 'index'),
	('admin', 'admin.group', 'create'),
	('admin', 'admin.group', 'dataTable'),
	('admin', 'admin.group', 'delete'),
	('admin', 'admin.group', 'index'),
	('admin', 'admin.group', 'modify'),
	('arteries', 'admin.group', 'create'),
	('arteries', 'admin.group', 'dataTable'),
	('arteries', 'admin.group', 'delete'),
	('arteries', 'admin.group', 'index'),
	('arteries', 'admin.group', 'modify'),
	('admin', 'admin.index', 'ajax'),
	('admin', 'admin.index', 'index'),
	('admin', 'admin.index', 'view'),
	('arteries', 'admin.index', 'ajax'),
	('arteries', 'admin.index', 'index'),
	('arteries', 'admin.index', 'view'),
	('admin', 'admin.job', 'ajax'),
	('admin', 'admin.job', 'create'),
	('admin', 'admin.job', 'dataTable'),
	('admin', 'admin.job', 'delete'),
	('admin', 'admin.job', 'index'),
	('admin', 'admin.job', 'modify'),
	('admin', 'admin.job', 'search'),
	('admin', 'admin.job', 'send'),
	('admin', 'admin.job', 'view'),
	('arteries', 'admin.job', 'ajax'),
	('arteries', 'admin.job', 'create'),
	('arteries', 'admin.job', 'dataTable'),
	('arteries', 'admin.job', 'delete'),
	('arteries', 'admin.job', 'index'),
	('arteries', 'admin.job', 'modify'),
	('arteries', 'admin.job', 'search'),
	('arteries', 'admin.job', 'send'),
	('arteries', 'admin.job', 'view'),
	('admin', 'admin.language', 'change'),
	('admin', 'admin.language', 'create'),
	('admin', 'admin.language', 'dataTable'),
	('admin', 'admin.language', 'delete'),
	('admin', 'admin.language', 'index'),
	('admin', 'admin.language', 'modify'),
	('admin', 'admin.language', 'translate'),
	('arteries', 'admin.language', 'change'),
	('arteries', 'admin.language', 'create'),
	('arteries', 'admin.language', 'dataTable'),
	('arteries', 'admin.language', 'delete'),
	('arteries', 'admin.language', 'index'),
	('arteries', 'admin.language', 'modify'),
	('arteries', 'admin.language', 'translate'),
	('admin', 'admin.log', 'ajax'),
	('admin', 'admin.log', 'dataTable'),
	('admin', 'admin.log', 'index'),
	('admin', 'admin.log', 'search'),
	('admin', 'admin.log', 'view'),
	('arteries', 'admin.log', 'ajax'),
	('arteries', 'admin.log', 'dataTable'),
	('arteries', 'admin.log', 'index'),
	('arteries', 'admin.log', 'search'),
	('arteries', 'admin.log', 'view'),
	('admin', 'admin.page', 'ajax'),
	('admin', 'admin.page', 'create'),
	('admin', 'admin.page', 'dataTable'),
	('admin', 'admin.page', 'delete'),
	('admin', 'admin.page', 'index'),
	('admin', 'admin.page', 'modify'),
	('admin', 'admin.page', 'position'),
	('admin', 'admin.page', 'search'),
	('arteries', 'admin.page', 'ajax'),
	('arteries', 'admin.page', 'create'),
	('arteries', 'admin.page', 'dataTable'),
	('arteries', 'admin.page', 'delete'),
	('arteries', 'admin.page', 'index'),
	('arteries', 'admin.page', 'modify'),
	('arteries', 'admin.page', 'position'),
	('arteries', 'admin.page', 'search'),
	('admin', 'admin.password', 'change'),
	('admin', 'admin.password', 'index'),
	('admin', 'admin.password', 'outdated'),
	('arteries', 'admin.password', 'change'),
	('arteries', 'admin.password', 'index'),
	('arteries', 'admin.password', 'outdated'),
	('guest', 'admin.password', 'index'),
	('admin', 'admin.profile', 'ajax'),
	('admin', 'admin.profile', 'avatar'),
	('admin', 'admin.profile', 'config'),
	('admin', 'admin.profile', 'index'),
	('admin', 'admin.profile', 'password'),
	('arteries', 'admin.profile', 'ajax'),
	('arteries', 'admin.profile', 'avatar'),
	('arteries', 'admin.profile', 'config'),
	('arteries', 'admin.profile', 'index'),
	('arteries', 'admin.profile', 'password'),
	('admin', 'admin.request', 'ajax'),
	('admin', 'admin.request', 'assignHasManyToMany'),
	('admin', 'admin.request', 'create'),
	('admin', 'admin.request', 'createHasManyToMany'),
	('admin', 'admin.request', 'dataTable'),
	('admin', 'admin.request', 'dataTableHasManyToMany'),
	('admin', 'admin.request', 'delete'),
	('admin', 'admin.request', 'deleteHasManyToMany'),
	('admin', 'admin.request', 'export'),
	('admin', 'admin.request', 'index'),
	('admin', 'admin.request', 'indexHasManyToMany'),
	('admin', 'admin.request', 'modify'),
	('admin', 'admin.request', 'modifyHasManyToMany'),
	('admin', 'admin.request', 'view'),
	('arteries', 'admin.request', 'ajax'),
	('arteries', 'admin.request', 'assignHasManyToMany'),
	('arteries', 'admin.request', 'create'),
	('arteries', 'admin.request', 'createHasManyToMany'),
	('arteries', 'admin.request', 'dataTable'),
	('arteries', 'admin.request', 'dataTableHasManyToMany'),
	('arteries', 'admin.request', 'delete'),
	('arteries', 'admin.request', 'deleteHasManyToMany'),
	('arteries', 'admin.request', 'export'),
	('arteries', 'admin.request', 'index'),
	('arteries', 'admin.request', 'indexHasManyToMany'),
	('arteries', 'admin.request', 'modify'),
	('arteries', 'admin.request', 'modifyHasManyToMany'),
	('arteries', 'admin.request', 'view'),
	('admin', 'admin.settings', 'ajax'),
	('admin', 'admin.settings', 'create'),
	('admin', 'admin.settings', 'index'),
	('admin', 'admin.settings', 'modify'),
	('admin', 'admin.settings', 'search'),
	('admin', 'admin.settings', 'send'),
	('admin', 'admin.settings', 'view'),
	('arteries', 'admin.settings', 'ajax'),
	('arteries', 'admin.settings', 'create'),
	('arteries', 'admin.settings', 'index'),
	('arteries', 'admin.settings', 'modify'),
	('arteries', 'admin.settings', 'search'),
	('arteries', 'admin.settings', 'send'),
	('arteries', 'admin.settings', 'view'),
	('admin', 'admin.specialization', 'ajax'),
	('admin', 'admin.specialization', 'create'),
	('admin', 'admin.specialization', 'dataTable'),
	('admin', 'admin.specialization', 'delete'),
	('admin', 'admin.specialization', 'index'),
	('admin', 'admin.specialization', 'modify'),
	('admin', 'admin.specialization', 'search'),
	('admin', 'admin.specialization', 'send'),
	('admin', 'admin.specialization', 'view'),
	('arteries', 'admin.specialization', 'ajax'),
	('arteries', 'admin.specialization', 'create'),
	('arteries', 'admin.specialization', 'dataTable'),
	('arteries', 'admin.specialization', 'delete'),
	('arteries', 'admin.specialization', 'index'),
	('arteries', 'admin.specialization', 'modify'),
	('arteries', 'admin.specialization', 'search'),
	('arteries', 'admin.specialization', 'send'),
	('arteries', 'admin.specialization', 'view'),
	('admin', 'admin.user', 'ajax'),
	('admin', 'admin.user', 'create'),
	('admin', 'admin.user', 'dataTable'),
	('admin', 'admin.user', 'delete'),
	('admin', 'admin.user', 'export'),
	('admin', 'admin.user', 'index'),
	('admin', 'admin.user', 'modify'),
	('admin', 'admin.user', 'search'),
	('admin', 'admin.user', 'send'),
	('arteries', 'admin.user', 'ajax'),
	('arteries', 'admin.user', 'create'),
	('arteries', 'admin.user', 'dataTable'),
	('arteries', 'admin.user', 'delete'),
	('arteries', 'admin.user', 'export'),
	('arteries', 'admin.user', 'index'),
	('arteries', 'admin.user', 'modify'),
	('arteries', 'admin.user', 'search'),
	('arteries', 'admin.user', 'send');
/*!40000 ALTER TABLE `acl_matrix` ENABLE KEYS */;

-- Dumping structure for tábla adsl.acl_resource
DROP TABLE IF EXISTS `acl_resource`;
CREATE TABLE IF NOT EXISTS `acl_resource` (
  `resourceId` VARCHAR(80) NOT NULL,
  `resourceName` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`resourceId`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Erőforrások';

-- Dumping data for table adsl.acl_resource: ~21 rows (approximately)
/*!40000 ALTER TABLE `acl_resource` DISABLE KEYS */;
INSERT INTO `acl_resource` (`resourceId`, `resourceName`) VALUES
	('admin.acl', 'admin.acl'),
	('admin.address', 'admin.address'),
	('admin.auth', 'admin.auth'),
	('admin.bnoCodes', 'admin.bnoCodes'),
	('admin.browser', 'admin.browser'),
	('admin.categories', 'admin.categories'),
	('admin.config', 'admin.config'),
	('admin.cron', 'admin.cron'),
	('admin.group', 'admin.group'),
	('admin.index', 'admin.index'),
	('admin.job', 'admin.job'),
	('admin.language', 'admin.language'),
	('admin.log', 'admin.log'),
	('admin.page', 'admin.page'),
	('admin.password', 'admin.password'),
	('admin.profile', 'admin.profile'),
	('admin.request', 'admin.request'),
	('admin.settings', 'admin.settings'),
	('admin.specialization', 'admin.specialization'),
	('admin.user', 'admin.user');
/*!40000 ALTER TABLE `acl_resource` ENABLE KEYS */;

-- Dumping structure for tábla adsl.acl_resource_has_action
DROP TABLE IF EXISTS `acl_resource_has_action`;
CREATE TABLE IF NOT EXISTS `acl_resource_has_action` (
  `resourceId` VARCHAR(80) NOT NULL,
  `actionId` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`resourceId`,`actionId`),
  KEY `actionId` (`actionId`),
  CONSTRAINT `acl_resource_has_action_ibfk_1` FOREIGN KEY (`resourceId`) REFERENCES `acl_resource` (`resourceId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acl_resource_has_action_ibfk_2` FOREIGN KEY (`actionId`) REFERENCES `acl_action` (`actionId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Resource és action kapcsolatok';

-- Dumping data for table adsl.acl_resource_has_action: ~127 rows (approximately)
/*!40000 ALTER TABLE `acl_resource_has_action` DISABLE KEYS */;
INSERT INTO `acl_resource_has_action` (`resourceId`, `actionId`) VALUES
	('admin.address', 'ajax'),
	('admin.bnoCodes', 'ajax'),
	('admin.categories', 'ajax'),
	('admin.index', 'ajax'),
	('admin.job', 'ajax'),
	('admin.log', 'ajax'),
	('admin.page', 'ajax'),
	('admin.profile', 'ajax'),
	('admin.request', 'ajax'),
	('admin.settings', 'ajax'),
	('admin.specialization', 'ajax'),
	('admin.user', 'ajax'),
	('admin.request', 'assignHasManyToMany'),
	('admin.profile', 'avatar'),
	('admin.auth', 'change'),
	('admin.language', 'change'),
	('admin.password', 'change'),
	('admin.profile', 'config'),
	('admin.acl', 'create'),
	('admin.address', 'create'),
	('admin.bnoCodes', 'create'),
	('admin.categories', 'create'),
	('admin.group', 'create'),
	('admin.job', 'create'),
	('admin.language', 'create'),
	('admin.page', 'create'),
	('admin.request', 'create'),
	('admin.settings', 'create'),
	('admin.specialization', 'create'),
	('admin.user', 'create'),
	('admin.request', 'createHasManyToMany'),
	('admin.acl', 'dataTable'),
	('admin.address', 'dataTable'),
	('admin.bnoCodes', 'dataTable'),
	('admin.cron', 'dataTable'),
	('admin.group', 'dataTable'),
	('admin.job', 'dataTable'),
	('admin.language', 'dataTable'),
	('admin.log', 'dataTable'),
	('admin.page', 'dataTable'),
	('admin.request', 'dataTable'),
	('admin.specialization', 'dataTable'),
	('admin.user', 'dataTable'),
	('admin.request', 'dataTableHasManyToMany'),
	('admin.acl', 'delete'),
	('admin.bnoCodes', 'delete'),
	('admin.categories', 'delete'),
	('admin.group', 'delete'),
	('admin.job', 'delete'),
	('admin.language', 'delete'),
	('admin.page', 'delete'),
	('admin.request', 'delete'),
	('admin.settings', 'delete'),
	('admin.specialization', 'delete'),
	('admin.user', 'delete'),
	('admin.request', 'deleteHasManyToMany'),
	('admin.request', 'export'),
	('admin.user', 'export'),
	('admin.acl', 'index'),
	('admin.address', 'index'),
	('admin.auth', 'index'),
	('admin.bnoCodes', 'index'),
	('admin.browser', 'index'),
	('admin.categories', 'index'),
	('admin.config', 'index'),
	('admin.cron', 'index'),
	('admin.group', 'index'),
	('admin.index', 'index'),
	('admin.job', 'index'),
	('admin.language', 'index'),
	('admin.log', 'index'),
	('admin.page', 'index'),
	('admin.password', 'index'),
	('admin.profile', 'index'),
	('admin.request', 'index'),
	('admin.settings', 'index'),
	('admin.specialization', 'index'),
	('admin.user', 'index'),
	('admin.request', 'indexHasManyToMany'),
	('admin.auth', 'logout'),
	('admin.acl', 'modify'),
	('admin.bnoCodes', 'modify'),
	('admin.categories', 'modify'),
	('admin.group', 'modify'),
	('admin.job', 'modify'),
	('admin.language', 'modify'),
	('admin.page', 'modify'),
	('admin.request', 'modify'),
	('admin.settings', 'modify'),
	('admin.specialization', 'modify'),
	('admin.user', 'modify'),
	('admin.request', 'modifyHasManyToMany'),
	('admin.password', 'outdated'),
	('admin.profile', 'password'),
	('admin.page', 'position'),
	('admin.bnoCodes', 'search'),
	('admin.categories', 'search'),
	('admin.job', 'search'),
	('admin.log', 'search'),
	('admin.page', 'search'),
	('admin.settings', 'search'),
	('admin.specialization', 'search'),
	('admin.user', 'search'),
	('admin.bnoCodes', 'send'),
	('admin.categories', 'send'),
	('admin.job', 'send'),
	('admin.settings', 'send'),
	('admin.specialization', 'send'),
	('admin.user', 'send'),
	('admin.language', 'translate'),
	('admin.bnoCodes', 'view'),
	('admin.categories', 'view'),
	('admin.index', 'view'),
	('admin.job', 'view'),
	('admin.log', 'view'),
	('admin.request', 'view'),
	('admin.settings', 'view'),
	('admin.specialization', 'view');
/*!40000 ALTER TABLE `acl_resource_has_action` ENABLE KEYS */;

-- Dumping structure for tábla adsl.acl_role
DROP TABLE IF EXISTS `acl_role`;
CREATE TABLE IF NOT EXISTS `acl_role` (
  `roleId` VARCHAR(80) NOT NULL,
  `roleName` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`roleId`),
  UNIQUE KEY `roleName` (`roleName`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Szerepkörök';

-- Dumping data for table adsl.acl_role: ~3 rows (approximately)
/*!40000 ALTER TABLE `acl_role` DISABLE KEYS */;
INSERT INTO `acl_role` (`roleId`, `roleName`) VALUES
	('admin', 'Adminisztrátor'),
	('arteries', 'Arteries Studio Ltd.'),
	('guest', 'Guest'),
	('user', 'User');
/*!40000 ALTER TABLE `acl_role` ENABLE KEYS */;

-- Dumping structure for tábla adsl.config
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `configId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `outDatedPasswordDay` INT(10) UNSIGNED NOT NULL COMMENT 'Hány nap után járjon le a jelszó',
  `languageId` INT(10) UNSIGNED NOT NULL COMMENT 'Alapértelmezett nyelv',
  `currencyId` INT(10) UNSIGNED NOT NULL COMMENT 'Alapértelmezett pénznem',
  `contactEmail` VARCHAR(200) DEFAULT NULL,
  `contactName` VARCHAR(200) NOT NULL COMMENT 'Kapcsolat menüpontos formban megadott adatok címzettje',
  `contactAddress` VARCHAR(200) NOT NULL COMMENT 'Cím',
  `contactPhone` VARCHAR(50) NOT NULL COMMENT 'Telefonszám',
  `testEmail` VARCHAR(200) DEFAULT NULL COMMENT 'Teszteléshez e-mail cím',
  `testName` VARCHAR(100) DEFAULT NULL COMMENT 'Teszteléshez név',
  PRIMARY KEY (`configId`),
  KEY `languageId` (`languageId`),
  KEY `currencyId` (`currencyId`),
  CONSTRAINT `config_ibfk_1` FOREIGN KEY (`languageId`) REFERENCES `language` (`languageId`) ON UPDATE CASCADE,
  CONSTRAINT `config_ibfk_2` FOREIGN KEY (`currencyId`) REFERENCES `currency` (`currencyId`) ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Beállítások';

-- Dumping data for table adsl.config: ~1 rows (approximately)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`configId`, `outDatedPasswordDay`, `languageId`, `currencyId`, `contactEmail`, `contactName`, `contactAddress`, `contactPhone`, `testEmail`, `testName`) VALUES
	(1, 90, 1, 1, '', '', '', '', NULL, NULL);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Dumping structure for tábla adsl.cron
DROP TABLE IF EXISTS `cron`;
CREATE TABLE IF NOT EXISTS `cron` (
  `className` VARCHAR(50) NOT NULL,
  `timing` MEDIUMINT(9) UNSIGNED NOT NULL COMMENT 'perc',
  `status` ENUM('started','finished') DEFAULT NULL,
  `lastRunAt` DATETIME DEFAULT NULL,
  `startedAt` DATETIME DEFAULT NULL,
  `timeout` SMALLINT(6) UNSIGNED NOT NULL COMMENT 'perc',
  `active` ENUM('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`className`),
  KEY `active` (`active`),
  KEY `status` (`status`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Utemezett feladatok';

-- Dumping data for table adsl.cron: ~2 rows (approximately)
/*!40000 ALTER TABLE `cron` DISABLE KEYS */;
INSERT INTO `cron` (`className`, `timing`, `status`, `lastRunAt`, `startedAt`, `timeout`, `active`) VALUES
	('App\\Cron\\Job\\Cleanup', 60, 'finished', '2018-04-26 16:42:02', '2018-04-26 16:42:02', 5, 'yes'),
	('App\\Cron\\Job\\Monitor', 0, 'finished', '2018-04-26 16:47:02', '2018-04-26 16:47:02', 1, 'yes');
/*!40000 ALTER TABLE `cron` ENABLE KEYS */;

-- Dumping structure for tábla adsl.data_table_state
DROP TABLE IF EXISTS `data_table_state`;
CREATE TABLE IF NOT EXISTS `data_table_state` (
  `dataTableStateId` VARCHAR(80) NOT NULL,
  `userId` INT(10) UNSIGNED NOT NULL,
  `data` TEXT NOT NULL,
  PRIMARY KEY (`userId`,`dataTableStateId`),
  CONSTRAINT `data_table_state_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Data Table komponens mentett beállításai';

-- Dumping data for table adsl.data_table_state: ~11 rows (approximately)
/*!40000 ALTER TABLE `data_table_state` DISABLE KEYS */;
INSERT INTO `data_table_state` (`dataTableStateId`, `userId`, `data`) VALUES
	('dataTableAcl', 9, '{"time":1533289158286,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2]}'),
	('dataTableCron', 9, '{"time":1533289167304,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3,4,5,6,7,8,9]}'),
	('dataTableGroup', 9, '{"time":1533289154323,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3]}'),
	('dataTableJobRole', 9, '{"time":1534330346920,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3,4]}'),
	('dataTableLanguage', 9, '{"time":1533292724437,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3,4,5,6]}'),
	('dataTableLog', 9, '{"time":1533289160710,"start":0,"length":10,"order":[[3,"desc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3,4]}'),
	('dataTablePage', 9, '{"time":1533289174086,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3,4,5,6]}'),
	('dataTableProduct', 9, '{"time":1534416353421,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3,4]}'),
	('dataTableProductHealthCarePoint', 9, '{"time":1534254454019,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3,4,5]}'),
	('dataTableSpecialization', 9, '{"time":1534254451257,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3,4]}'),
	('dataTableTranslate', 9, '{"time":1533292729989,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2]}'),
	('dataTableUser', 9, '{"time":1534323907247,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3,4,5,6]}'),
	('dataTableUser', 12, '{"time":1533301158930,"start":0,"length":10,"order":[[0,"asc"]],"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true},"columns":[{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":false,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}},{"visible":true,"search":{"search":"","smart":true,"regex":false,"caseInsensitive":true}}],"ColReorder":[0,1,2,3,4,5]}');
/*!40000 ALTER TABLE `data_table_state` ENABLE KEYS */;

-- Dumping structure for tábla adsl.group
DROP TABLE IF EXISTS `group`;
CREATE TABLE IF NOT EXISTS `group` (
  `groupId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `groupName` VARCHAR(80) NOT NULL,
  `guest` ENUM('yes','no') NOT NULL DEFAULT 'no' COMMENT 'Vendég',
  `active` ENUM('yes','no') NOT NULL DEFAULT 'yes' COMMENT 'Aktív',
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `groupName` (`groupName`),
  KEY `active` (`active`),
  KEY `guest` (`guest`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Felhasználói csoportok';

-- Dumping data for table adsl.group: ~4 rows (approximately)
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` (`groupId`, `groupName`, `guest`, `active`) VALUES
	(1, 'Guest', 'yes', 'yes'),
	(2, 'Arteries', 'no', 'yes'),
	(3, 'Adminisztrátor', 'no', 'yes'),
	(4, 'User', 'no', 'yes');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;

-- Dumping structure for tábla adsl.group_has_acl_role
DROP TABLE IF EXISTS `group_has_acl_role`;
CREATE TABLE IF NOT EXISTS `group_has_acl_role` (
  `groupId` INT(10) UNSIGNED NOT NULL,
  `roleId` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`groupId`,`roleId`),
  KEY `roleId` (`roleId`) USING BTREE,
  CONSTRAINT `group_has_acl_role_ibfk_1` FOREIGN KEY (`groupId`) REFERENCES `group` (`groupId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `group_has_acl_role_ibfk_2` FOREIGN KEY (`roleId`) REFERENCES `acl_role` (`roleId`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Felhasználói csoportok és szerepköreik';

-- Dumping data for table adsl.group_has_acl_role: ~3 rows (approximately)
/*!40000 ALTER TABLE `group_has_acl_role` DISABLE KEYS */;
INSERT INTO `group_has_acl_role` (`groupId`, `roleId`) VALUES
	(3, 'admin'),
	(2, 'arteries'),
	(1, 'guest'),
	(4, 'user');
/*!40000 ALTER TABLE `group_has_acl_role` ENABLE KEYS */;

-- Dumping structure for tábla adsl.language
DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `languageId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `locale` VARCHAR(5) NOT NULL,
  `iso` CHAR(2) NOT NULL,
  `languageName` VARCHAR(255) NOT NULL,
  `dateFormat` VARCHAR(20) NOT NULL COMMENT 'dátum formátum, pl. Y.m.d.',
  `dateRegexPattern` VARCHAR(80) NOT NULL COMMENT 'dátum formátumot felismerő regex minta',
  `dateInputMask` VARCHAR(40) NOT NULL COMMENT 'dátum beviteli mezőknél használt maszk',
  `interface` ENUM('yes','no') NOT NULL DEFAULT 'yes' COMMENT 'Nyevválasztóban megjelenik',
  `timeFormat` VARCHAR(20) NOT NULL COMMENT 'idő formátum, pl. H:i',
  `timeRegexPattern` VARCHAR(80) NOT NULL COMMENT 'idő formátumot felismerő regex minta',
  `timeInputMask` VARCHAR(40) NOT NULL COMMENT 'idő beviteli mezőknél használt maszk',
  PRIMARY KEY (`languageId`),
  UNIQUE KEY `iso` (`iso`),
  UNIQUE KEY `locale` (`locale`),
  KEY `interface` (`interface`)
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Nyelvek';

-- Dumping data for table adsl.language: ~2 rows (approximately)
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` (`languageId`, `locale`, `iso`, `languageName`, `dateFormat`, `dateRegexPattern`, `dateInputMask`, `interface`, `timeFormat`, `timeRegexPattern`, `timeInputMask`) VALUES
	(1, 'en_GB', 'en', 'English', 'd/m/Y', '/^[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}$/', '{{99}}/{{99}}/{{9999}}', 'yes', 'H:i:s', '/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/', '{{99}}:{{99}}.');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;

-- Dumping structure for tábla adsl.log
DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `logId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` INT(10) UNSIGNED DEFAULT NULL COMMENT 'Felhasználó',
  `name` VARCHAR(32) DEFAULT NULL COMMENT 'Log azonosító (qs)',
  `priority` INT(3) NOT NULL COMMENT 'Súlyosság, lásd phalcon dokumentáció',
  `content` TEXT COMMENT 'Üzenet',
  `exception` TEXT COMMENT 'Ha kivétel történt, stack trace',
  `createdAt` DATETIME NOT NULL COMMENT 'Időpont',
  PRIMARY KEY (`logId`),
  KEY `type` (`priority`) USING BTREE,
  KEY `log_ibfk_1` (`userId`) USING BTREE,
  CONSTRAINT `log_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=853 DEFAULT CHARSET=utf8 COMMENT='Napló';


-- Dumping structure for tábla adsl.log_model
DROP TABLE IF EXISTS `log_model`;
CREATE TABLE IF NOT EXISTS `log_model` (
  `logModelId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `logId` INT(10) UNSIGNED NOT NULL,
  `objectId` VARCHAR(80) NOT NULL COMMENT 'Model entitás azonosító',
  `operation` INT(10) UNSIGNED NOT NULL COMMENT 'Művelet (lásd phalcon model)',
  `className` VARCHAR(50) NOT NULL COMMENT 'Model osztály',
  PRIMARY KEY (`logModelId`),
  KEY `objectId` (`objectId`),
  KEY `operation` (`operation`),
  KEY `logId` (`logId`),
  CONSTRAINT `log_model_ibfk_1` FOREIGN KEY (`logId`) REFERENCES `log` (`logId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=641 DEFAULT CHARSET=utf8 COMMENT='Model változások';


-- Dumping structure for tábla adsl.log_model_attribute
DROP TABLE IF EXISTS `log_model_attribute`;
CREATE TABLE IF NOT EXISTS `log_model_attribute` (
  `logModelAttributeId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `logModelId` INT(10) UNSIGNED NOT NULL,
  `attribute` VARCHAR(80) NOT NULL COMMENT 'model attribútum',
  `oldValue` TEXT COMMENT 'régi érték',
  `newValue` TEXT COMMENT 'új érték',
  PRIMARY KEY (`logModelAttributeId`),
  KEY `logModelId` (`logModelId`),
  CONSTRAINT `log_model_attribute_ibfk_1` FOREIGN KEY (`logModelId`) REFERENCES `log_model` (`logModelId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=1796 DEFAULT CHARSET=utf8 COMMENT='Modellek attribútum szintű változásai';


-- Dumping structure for tábla adsl.migration
DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `id` DECIMAL(20,0) DEFAULT NULL,
  `applied_at` VARCHAR(25) DEFAULT NULL,
  `version` VARCHAR(25) DEFAULT NULL,
  `description` VARCHAR(255) DEFAULT NULL
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- Dumping data for table adsl.migration: ~0 rows (approximately)
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;

-- Dumping structure for tábla adsl.navigation_history
DROP TABLE IF EXISTS `navigation_history`;
CREATE TABLE IF NOT EXISTS `navigation_history` (
  `navigationHistoryId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` INT(10) UNSIGNED NOT NULL,
  `type` ENUM('user') NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`navigationHistoryId`),
  KEY `userId` (`userId`),
  KEY `type` (`type`),
  CONSTRAINT `navigation_history_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Utoljára megtekintett oldalak';


-- Dumping structure for tábla adsl.owner
DROP TABLE IF EXISTS `owner`;
CREATE TABLE IF NOT EXISTS `owner` (
  `ownerId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `lastName` VARCHAR(60) NOT NULL COMMENT 'Vezetéknév',
  `firstName` VARCHAR(60) NOT NULL COMMENT 'Keresztnév',
  `ownerType` ENUM('naturalPerson','legalEntity') NOT NULL COMMENT 'Típus (természetes vagy jogi személy)',
  PRIMARY KEY (`ownerId`),
  KEY `customerType` (`ownerType`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Tulajdonosok';

-- Dumping data for table adsl.owner: ~0 rows (approximately)
/*!40000 ALTER TABLE `owner` DISABLE KEYS */;
/*!40000 ALTER TABLE `owner` ENABLE KEYS */;

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `pageId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parentId` INT(11) UNSIGNED DEFAULT NULL,
  `position` INT(11) UNSIGNED NOT NULL COMMENT 'Megjelenítési sorrend',
  `active` ENUM('yes','no') NOT NULL,
  `slideshow` ENUM('yes','no') NOT NULL DEFAULT 'no' COMMENT 'szerepel-e az oldalon slideshow',
  `map` ENUM('yes','no') NOT NULL DEFAULT 'no' COMMENT 'szerepel-e az oldalon térkép',
  PRIMARY KEY (`pageId`),
  KEY `active` (`active`),
  KEY `page_ibfk_1` (`parentId`),
  CONSTRAINT `page_ibfk_1` FOREIGN KEY (`parentId`) REFERENCES `page` (`pageId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Microsite lapok (mini CMS)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,NULL,1,'yes','yes','yes'),
(2,NULL,1,'yes','yes','no');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_language`
--

DROP TABLE IF EXISTS `page_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_language` (
  `pageId` INT(11) UNSIGNED NOT NULL,
  `languageId` INT(11) UNSIGNED NOT NULL,
  `pageName` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(255) NOT NULL,
  `content` TEXT NOT NULL,
  PRIMARY KEY (`pageId`,`languageId`) USING BTREE,
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `pageName` (`pageName`),
  KEY `languageId` (`languageId`),
  CONSTRAINT `page_language_ibfk_1` FOREIGN KEY (`pageId`) REFERENCES `page` (`pageId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `page_language_ibfk_2` FOREIGN KEY (`languageId`) REFERENCES `language` (`languageId`) ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='Microsite oldal nyelvi mutációk';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_language`
--

LOCK TABLES `page_language` WRITE;
/*!40000 ALTER TABLE `page_language` DISABLE KEYS */;
INSERT INTO `page_language` VALUES (1,1,'Kezdőlap','kezdolap','kezdolap tartalom'),
(2,1,'Adatkezelés','adatkezeles','adatkezeles');
/*!40000 ALTER TABLE `page_language` ENABLE KEYS */;
UNLOCK TABLES;


-- Dumping structure for tábla adsl.translation
DROP TABLE IF EXISTS `translation`;
CREATE TABLE IF NOT EXISTS `translation` (
  `translationId` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `languageId` INT(10) UNSIGNED NOT NULL,
  `key` VARCHAR(512) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `value` VARCHAR(512) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`translationId`),
  UNIQUE KEY `languageId_2` (`languageId`,`key`),
  KEY `languageId` (`languageId`),
  CONSTRAINT `translation_ibfk_1` FOREIGN KEY (`languageId`) REFERENCES `language` (`languageId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

-- Dumping data for table adsl.translation: ~56 rows (approximately)
/*!40000 ALTER TABLE `translation` DISABLE KEYS */;
INSERT INTO `translation` (`translationId`, `languageId`, `key`, `value`) VALUES
	(1, 1, 'Users', 'Users'),
	(2, 1, ' mező kitöltése kötelező!', 'Input is required'),
	(3, 1, 'Érvénytelen e-mail cím!', 'E-mail address is invalid.'),
	(4, 1, 'Csoport', 'Group'),
	(5, 1, 'Név', 'Name'),
	(6, 1, 'Jelszó', 'Password'),
	(7, 1, 'Jelszó ismétlése', 'Password again'),
	(8, 1, 'Új jelszó megadása első belépésnél', 'Must enter a new password at the first login'),
	(9, 1, 'Aktív', 'Active'),
	(10, 1, 'Jóváhagyva', 'Approved'),
	(11, 1, 'Mentés', 'Save'),
	(12, 1, 'Mégse', 'Cancel'),
	(13, 1, 'igen', 'yes'),
	(14, 1, 'nem', 'no'),
	(15, 1, 'Fordítás', 'Translate'),
	(16, 1, 'Kérem, válasszon!', 'Please, choose!'),
	(17, 1, 'Vissza a listára', 'Back to the list'),
	(18, 1, 'A jelszónak tartalmaznia kell kis és nagybetűt valamint számot és minimum 8 karakter hosszúnak kell lennie!', 'The password must contain lower case letter and upper case letter and numeric character and must be at least 8 characters!'),
	(19, 1, 'Felhasználók', 'Users'),
	(20, 1, 'Lista', 'List'),
	(21, 1, 'Tesztrendszer', 'Test environment'),
	(22, 1, 'Keresés', 'Search'),
	(23, 1, 'Alaphelyzet', 'Reset'),
	(24, 1, 'Létrehozás', 'Create'),
	(25, 1, 'E-mail cím', 'Email address'),
	(26, 1, 'Új keresési feltétel ...', 'New search conditions ...'),
	(27, 1, 'Találat oldalanként', 'Items per page'),
	(28, 1, 'Belépési adatok újraküldése', 'Resend login data'),
	(29, 1, 'Módosítás', 'Modify'),
	(34, 1, 'Regisztráció ideje', 'Registration date'),
	(35, 1, 'Utolsó belépés ideje', 'Date of last login'),
	(36, 1, 'Törlés', 'Delete'),
	(37, 1, 'Utolsó belépés', 'Last login'),
	(38, 1, 'Összesen', 'All'),
	(39, 1, 'Előző', 'Previous'),
	(40, 1, 'Következő', 'Next'),
	(41, 1, 'Nincs találat.', 'Not found anything.'),
	(42, 1, 'Összesen: _TOTAL_ találat. Találatok: _START_ - _END_.', 'All: _TOTAL_ item. Items: _START_ - _END_.'),
	(43, 1, 'Első', 'First'),
	(44, 1, 'Last', 'Utolsó'),
	(45, 1, 'A(z) "%field%" mező kitöltése kötelező, nem lehet üres!', 'The "%field%" value must be set. This attribute can\'t be empty!'),
	(46, 1, 'Sorrend', 'Position'),
	(47, 1, 'Kiemelt', 'Highlighted');
/*!40000 ALTER TABLE `translation` ENABLE KEYS */;

-- Dumping structure for tábla adsl.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `userId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `groupId` INT(10) UNSIGNED NOT NULL COMMENT 'Szerepkör, Felhasználói csoport',
  `languageId` INT(10) UNSIGNED DEFAULT NULL COMMENT 'Alapértelmezett nyelv',
  `name` VARCHAR(80) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(80) NOT NULL,
  `active` ENUM('yes','no') NOT NULL DEFAULT 'yes' COMMENT 'Fiók státusz,Aktív',
  `approve` ENUM('yes','no') NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Regisztráció dátuma',
  `lastLogin` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Utolsó belépés ideje',
  `deleted` ENUM('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`) USING BTREE,
  KEY `groupId` (`groupId`) USING BTREE,
  KEY `active` (`active`),
  KEY `languageId` (`languageId`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`groupId`) REFERENCES `group` (`groupId`) ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`languageId`) REFERENCES `language` (`languageId`) ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COMMENT='Felhasználók';

-- Dumping data for table adsl.user: ~8 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`userId`, `groupId`, `languageId`, `name`, `email`, `password`, `active`, `approve`, `createdAt`, `lastLogin`, `deleted`) VALUES
	(5, 1, NULL, 'Guest Account', 'guest@localhost.localdomain', 'N/A', 'yes', 'yes', '2018-09-24 00:00:00', '2018-09-24 00:00:00', 'no'),
	(6, 2, NULL, 'CLI', 'cli@localhost.localdomain', 'N/A', 'yes', 'yes', '2018-09-24 00:00:00', '2018-09-24 00:00:00', 'no'),
	(9, 3, NULL, 'User1', 'user1@arteries.hu', '$2y$12$OGptaG9nRmpyTFhhY2lWTuWsAwetUw1ihcnYeBdXrQ0lbg5hxprni', 'yes', 'yes', '2018-09-24 00:00:00', '2018-09-24 00:00:00', 'no'),
	(12, 2, NULL, 'User2', 'user2@arteries.hu', '$2y$12$OGptaG9nRmpyTFhhY2lWTuWsAwetUw1ihcnYeBdXrQ0lbg5hxprni', 'yes', 'yes', '2018-09-24 00:00:00', '2018-09-24 00:00:00', 'no'),
	(15, 4, NULL, 'User3', 'user3@arteries.hu', '$2y$12$OGptaG9nRmpyTFhhY2lWTuWsAwetUw1ihcnYeBdXrQ0lbg5hxprni', 'yes', 'yes', '2018-09-24 00:00:00', '2018-09-24 00:00:00', 'no'),
	(53, 4, NULL, 'Teszt Elek', 'tesztelek@arteries.hu', '$2y$12$OWdnRVFaRW45Q3pqRUZ4R.4UxaCDsfAV4JxOcIYAWWxiQ1ONG6afq', 'yes', 'yes', '2018-09-24 00:00:00', '2018-09-24 00:00:00', 'no'),
	(54, 4, NULL, 'Teszt Elek', 'tesztelek+3@arteries.hu', '$2y$12$L3cvY1NKblRBSmVieWdOeO4nB6/3Oa613ciNYvcvRs4sI8UP.5Xai', 'yes', 'yes', '2018-09-24 00:00:00', '2018-09-24 00:00:00', 'no'),
	(55, 4, NULL, 'Teszt Elek', 'tesztelek+4@arteries.hu', '$2y$12$bnlkUjhGcUlkTXdDeXBDd.UvHpGTe8CjCDW3xkCWjy7RwOX6QIpty', 'yes', 'yes', '2018-09-24 00:00:00', '2018-09-24 00:00:00', 'no');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for tábla adsl.user_password
DROP TABLE IF EXISTS `user_password`;
CREATE TABLE IF NOT EXISTS `user_password` (
  `userPasswordId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` INT(10) UNSIGNED NOT NULL,
  `password` VARCHAR(80) NOT NULL,
  `modifiedAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userPasswordId`),
  KEY `userId` (`userId`),
  CONSTRAINT `user_password_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='Felhasználók korábbi jelszavai';

-- Dumping data for table adsl.user_password: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_password` DISABLE KEYS */;
INSERT INTO `user_password` (`userPasswordId`, `userId`, `password`, `modifiedAt`) VALUES
	(5, 9, '$2y$12$Q0xpTm9VMFdyRXJDQjkzdeiBvvrk5lgsvimTTxDzgIdYUXlS9TYci', '2018-09-24 00:00:00'),
	(9, 12, '$2y$12$eURMZElQaGxocmFiRkFSS.VoU4/vb0lTkLbqqrj/CpuC2nRnq.gi.', '2018-09-24 00:00:00'),
	(31, 53, '$2y$12$OWdnRVFaRW45Q3pqRUZ4R.4UxaCDsfAV4JxOcIYAWWxiQ1ONG6afq', '2018-09-24 00:00:00'),
	(32, 54, '$2y$12$L3cvY1NKblRBSmVieWdOeO4nB6/3Oa613ciNYvcvRs4sI8UP.5Xai', '2018-09-24 00:00:00'),
	(33, 55, '$2y$12$bnlkUjhGcUlkTXdDeXBDd.UvHpGTe8CjCDW3xkCWjy7RwOX6QIpty', '2018-09-24 00:00:00');
/*!40000 ALTER TABLE `user_password` ENABLE KEYS */;

-- Dumping structure for tábla adsl.user_reset_password
DROP TABLE IF EXISTS `user_reset_password`;
CREATE TABLE IF NOT EXISTS `user_reset_password` (
  `userResetPasswordId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` INT(10) UNSIGNED NOT NULL,
  `code` VARCHAR(48) NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedAt` DATETIME DEFAULT NULL,
  `reset` ENUM('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`userResetPasswordId`),
  KEY `usersId` (`userId`) USING BTREE,
  KEY `code` (`code`) USING BTREE,
  KEY `reset` (`reset`),
  CONSTRAINT `user_reset_password_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table adsl.user_reset_password: ~0 rows (approximately)

-- Dumping structure for tábla adsl.user_session
DROP TABLE IF EXISTS `user_session`;
CREATE TABLE IF NOT EXISTS `user_session` (
  `userSessionId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` INT(10) UNSIGNED NOT NULL,
  `token` VARCHAR(64) NOT NULL,
  `data` BLOB NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedAt` DATETIME DEFAULT NULL,
  PRIMARY KEY (`userSessionId`),
  UNIQUE KEY `token` (`token`),
  KEY `userId` (`userId`),
  CONSTRAINT `user_session_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Dumping data for table adsl.user_session: ~11 rows (approximately)

-- Dumping structure for tábla adsl.user_token
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE IF NOT EXISTS `user_token` (
  `userTokenId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` INT(10) UNSIGNED NOT NULL,
  `token` VARCHAR(30) NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expirationAt` DATETIME DEFAULT NULL,
  PRIMARY KEY (`userTokenId`),
  UNIQUE KEY `token` (`token`),
  KEY `userId` (`userId`),
  CONSTRAINT `user_token_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table adsl.user_token: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_token` DISABLE KEYS */;
INSERT INTO `user_token` (`userTokenId`, `userId`, `token`, `createdAt`, `expirationAt`) VALUES
	(4, 53, 'X2Na2xs6q8hiEIGAipqbJMNmIo9wdd', '2018-09-24 00:00:00', NULL),
	(5, 54, 'mxILKgqxpskVDFK9lM2g1VFnJw8ETT', '2018-09-24 00:00:00', NULL),
	(6, 55, 'PGiTQzN4LGQntkQWRZxLFDQ66KBeXB', '2018-09-24 00:00:00', NULL);
/*!40000 ALTER TABLE `user_token` ENABLE KEYS */;

-- Dumping structure for nézet adsl.view__cron
DROP VIEW IF EXISTS `view__cron`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view__cron` (
	`className` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
	`timing` MEDIUMINT(9) UNSIGNED NOT NULL COMMENT 'perc',
	`status` ENUM('started','finished') NULL COLLATE 'utf8_general_ci',
	`lastRunAt` DATETIME NULL,
	`startedAt` DATETIME NULL,
	`timeout` SMALLINT(6) UNSIGNED NOT NULL COMMENT 'perc',
	`active` ENUM('yes','no') NOT NULL COLLATE 'utf8_general_ci',
	`isMonitor` VARCHAR(3) NOT NULL COLLATE 'utf8mb4_general_ci',
	`isGarbage` VARCHAR(3) NOT NULL COLLATE 'utf8mb4_general_ci',
	`isNeedToRun` VARCHAR(3) NOT NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MYISAM;

-- Dumping structure for nézet adsl.view__group
DROP VIEW IF EXISTS `view__group`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view__group` (
	`groupId` INT(10) UNSIGNED NOT NULL,
	`groupName` VARCHAR(80) NOT NULL COLLATE 'utf8_general_ci',
	`guest` ENUM('yes','no') NOT NULL COMMENT 'Vendég' COLLATE 'utf8_general_ci',
	`active` ENUM('yes','no') NOT NULL COMMENT 'Aktív' COLLATE 'utf8_general_ci',
	`userCount` BIGINT(21) NULL
) ENGINE=MYISAM;

-- Dumping structure for nézet adsl.view__log
DROP VIEW IF EXISTS `view__log`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view__log` (
	`logId` INT(10) UNSIGNED NOT NULL,
	`userId` INT(10) UNSIGNED NULL COMMENT 'Felhasználó',
	`name` VARCHAR(32) NULL COMMENT 'Log azonosító (qs)' COLLATE 'utf8_general_ci',
	`priority` INT(3) NOT NULL COMMENT 'Súlyosság, lásd phalcon dokumentáció',
	`priorityName` VARCHAR(9) NOT NULL COLLATE 'utf8mb4_general_ci',
	`createdAt` DATETIME NOT NULL COMMENT 'Időpont',
	`content` TEXT NULL COMMENT 'Üzenet' COLLATE 'utf8_general_ci',
	`contentShort` MEDIUMTEXT NULL COLLATE 'utf8_general_ci',
	`exception` TEXT NULL COMMENT 'Ha kivétel történt, stack trace' COLLATE 'utf8_general_ci',
	`user` VARCHAR(80) NOT NULL COLLATE 'utf8_general_ci'
) ENGINE=MYISAM;

-- Dumping structure for nézet adsl.view__translation
DROP VIEW IF EXISTS `view__translation`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view__translation` (
	`translationId` INT(11) UNSIGNED NOT NULL,
	`languageId` INT(10) UNSIGNED NOT NULL,
	`key` VARCHAR(512) NOT NULL COLLATE 'utf8_bin',
	`value` VARCHAR(512) NOT NULL COLLATE 'utf8_bin',
	`changed` INT(1) NOT NULL
) ENGINE=MYISAM;

-- Dumping structure for nézet adsl.view__user
DROP VIEW IF EXISTS `view__user`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view__user` (
	`userId` INT(10) UNSIGNED NOT NULL,
	`groupId` INT(10) UNSIGNED NOT NULL COMMENT 'Szerepkör, Felhasználói csoport',
	`languageId` INT(10) UNSIGNED NULL COMMENT 'Alapértelmezett nyelv',
	`name` VARCHAR(80) NOT NULL COLLATE 'utf8_general_ci',
	`email` VARCHAR(255) NOT NULL COLLATE 'utf8_general_ci',
	`password` VARCHAR(80) NOT NULL COLLATE 'utf8_general_ci',
	`active` ENUM('yes','no') NOT NULL COMMENT 'Fiók státusz,Aktív' COLLATE 'utf8_general_ci',
	`approve` ENUM('yes','no') NOT NULL COLLATE 'utf8_general_ci',
	`createdAt` DATETIME NOT NULL COMMENT 'Regisztráció dátuma',
	`lastLogin` DATETIME NOT NULL COMMENT 'Utolsó belépés ideje',
	`deleted` ENUM('yes','no') NOT NULL COLLATE 'utf8_general_ci',
	`groupName` VARCHAR(80) NOT NULL COLLATE 'utf8_general_ci',
	`guest` ENUM('yes','no') NOT NULL COMMENT 'Vendég' COLLATE 'utf8_general_ci'
) ENGINE=MYISAM;

-- Dumping structure for nézet adsl.view__cron
DROP VIEW IF EXISTS `view__cron`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view__cron`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view__cron` AS SELECT `cron`.`className` AS `className`,`cron`.`timing` AS `timing`,`cron`.`status` AS `status`,`cron`.`lastRunAt` AS `lastRunAt`,`cron`.`startedAt` AS `startedAt`,`cron`.`timeout` AS `timeout`,`cron`.`active` AS `active`,IF((`cron`.`className` = 'App\\Cron\\Job\\Monitor'),'yes','no') AS `isMonitor`,IF(((((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`cron`.`startedAt`)) / 60) > `cron`.`timeout`) AND (`cron`.`status` = 'started')),'yes','no') AS `isGarbage`,(CASE WHEN ((`cron`.`status` = 'started') OR (`cron`.`active` = 'no')) THEN 'no' WHEN (ISNULL(`cron`.`lastRunAt`) OR ISNULL(`cron`.`status`) OR ISNULL(`cron`.`startedAt`)) THEN 'yes' WHEN (((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`cron`.`startedAt`)) / 60) >= `cron`.`timing`) THEN 'yes' ELSE 'no' END) AS `isNeedToRun` FROM `cron`;

-- Dumping structure for nézet adsl.view__group
DROP VIEW IF EXISTS `view__group`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view__group`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view__group` AS SELECT `g`.`groupId` AS `groupId`,`g`.`groupName` AS `groupName`,`g`.`guest` AS `guest`,`g`.`active` AS `active`,(SELECT COUNT(0) FROM `user` `u` WHERE (`u`.`groupId` = `g`.`groupId`)) AS `userCount` FROM `group` `g`;

-- Dumping structure for nézet adsl.view__log
DROP VIEW IF EXISTS `view__log`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view__log`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view__log` AS SELECT `l`.`logId` AS `logId`,`l`.`userId` AS `userId`,`l`.`name` AS `name`,`l`.`priority` AS `priority`,(CASE `l`.`priority` WHEN 0 THEN 'emergency' WHEN 1 THEN 'critical' WHEN 2 THEN 'alert' WHEN 3 THEN 'error' WHEN 4 THEN 'warning' WHEN 5 THEN 'notice' WHEN 6 THEN 'info' WHEN 7 THEN 'debug' WHEN 8 THEN 'custom' ELSE 'special' END) AS `priorityName`,`l`.`createdAt` AS `createdAt`,`l`.`content` AS `content`,IF((LENGTH(`l`.`content`) > 100),CONCAT(SUBSTR(`l`.`content`,1,100),' ...'),`l`.`content`) AS `contentShort`,`l`.`exception` AS `exception`,`u`.`name` AS `user` FROM (`log` `l` JOIN `view__user` `u` ON((`l`.`userId` = `u`.`userId`)));

-- Dumping structure for nézet adsl.view__translation
DROP VIEW IF EXISTS `view__translation`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view__translation`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view__translation` AS SELECT `t`.`translationId` AS `translationId`,`t`.`languageId` AS `languageId`,`t`.`key` AS `key`,`t`.`value` AS `value`,IF((`t`.`key` = `t`.`value`),0,1) AS `changed` FROM `translation` `t`;

-- Dumping structure for nézet adsl.view__user
DROP VIEW IF EXISTS `view__user`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view__user`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view__user` AS SELECT `u`.`userId` AS `userId`,`u`.`groupId` AS `groupId`,`u`.`languageId` AS `languageId`,`u`.`name` AS `name`,`u`.`email` AS `email`,`u`.`password` AS `password`,`u`.`active` AS `active`,`u`.`approve` AS `approve`,`u`.`createdAt` AS `createdAt`,`u`.`lastLogin` AS `lastLogin`,`u`.`deleted` AS `deleted`,`g`.`groupName` AS `groupName`,`g`.`guest` AS `guest` FROM (`user` `u` JOIN `group` `g` ON((`g`.`groupId` = `u`.`groupId`)));

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


CREATE VIEW `view__page` AS SELECT `p`.`pageId` AS `pageId`,`p`.`position` AS `position`,`p`.`active` AS `active`,`p`.`slideshow` AS `slideshow`,`p`.`map` AS `map`,`p`.`parentId` AS `parentId`,`l`.`languageId` AS `languageId`,`l`.`pageName` AS `pageName`,`l`.`slug` AS `slug`,`l`.`content` AS `content`,((SELECT MAX(`page`.`position`) FROM `page`) = `p`.`position`) AS `isMax`,((SELECT MIN(`page`.`position`) FROM `page`) = `p`.`position`) AS `isMin` FROM (`page` `p` JOIN `page_language` `l` ON((`p`.`pageId` = `l`.`pageId`)))
