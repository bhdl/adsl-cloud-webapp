**Fejlesztőkörnyezet beállításának lépései (UNIX, Windows):**

* GIT repository clone

* .env fájl készítése az .env.example alapján "local" APP_ENV értékkel

* Docker telepítése

* Windows esetén Docker Quickstart Terminal elindítása (ne használd a sima cmd-t), továbbá VirtualBox esetén, a projekt mappa megosztása a docker default gépével, /adsl-webapp néven.

* Windowson docker machine újraindítása, hogy elérhető legyen az új könyvtár:
```
docker-machine stop
docker-machine start
```

* Docker VM elindítása a docker/local könyvtárban (egy szinten a docker-compose.yml-el):

A docker/local mappában található docker-compose-linux.yml vagy docker-compose-win.yml másolása docker-compose.yml néven

`docker-compose up`

Docker Toolbox esetén:
`docker-compose -f docker-compose-win.yml up -d`

* MySQL adatbázis létrehozása: `adsl`, 

* /schemas/initial_schema.sql importálása (utf8_general_ci) karakterkódolással. 

* PHPMyAdmin felület a http://localhost:8080-on érhető el, "root" felhasználó névvel és jelszóval.

* Host gép hosts fájlban "project.local" felvétele, ami a 127.0.0.1-re mutat (Windows esetén 192.168.99.100). 

* Belépés a virtuális gépbe: 

`docker exec -ti adsl_webapp bash`

* Projekt könyvtárban (/var/www/adsl-webapp) composer csomagok letöltése:

`composer update`

* Írásjog a www-data számára a munkakönyvtárakra (csak fejlesztői munkaállomáson adható ki ilyen formában a parancs):

`chmod -R 0777 workspace`

`chmod -R 0777 public/workspace`

* Saját felhasználó létrehozása az "user" táblában.

* .env file létrehozása example alapján

* config/base.php létrehozása example alapján
majd az adatbázis adatok beállítása

* config/local.php létrehozása example alapján
majd 'baseUri' => 'https://adsl.local' beállítása

* config mappában található többi example fájl másolása example nélküli néven

* Böngészőben https://adsl.local URL-en az oldal elérhető lesz.

**SQL migration beállítása**

A composer-el a migration tool automatikusan feltelepül.

* Környezet hozzáadása, majd inicializálás (local)

./vendor/bin/migrate migrate:addenv yml

a default formátum yml de meglehet adni neki json vagy php formátumot.

Enter lenyomása után adjuk meg a file nevét. pl.: local

ha ez hibára fut, akkor a .php-database-migration mappában local.yml.example másolása example nélküli néven, majd az új fájlban adatbázis adatok beállítása 

./vendor/bin/migrate migrate:init local

./vendor/bin/migrate migrate:up local

További információ: 
https://packagist.org/packages/php-database-migration/php-database-migration

**LESS-hez gulp telepítése (opcionális, ha akarsz a frontenden is dolgozni)**

Ezt nem szükséges a virtuális gépben elvégezni, ha a host gépen is van már NPM. Globálisan (sudo jog szükséges lehet):

`npm install -g gulp-cli`

Majd a projekt gyökérkönyvtárban (package.json alapján):

`npm install`

Ha minden rendben megy, az alábbi parancs kiadásával a gulp watcher elindul:

`gulp`