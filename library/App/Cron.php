<?php

namespace App;

use Phalcon\Exception;
use Phalcon\Mvc\User\Component;
use App\Model\Cron as CronModel;

/**
 * Class Cron
 * @package App
 */
final class Cron extends Component
{
	/**
	 * @var array
	 */
	protected $jobs = [];

	/**
	 * Cron constructor.
	 */
	public function  __construct()
	{
		$this->jobs = $this->getNeedToRunJobs();
	}

	/**
     * Összes futtatandó feladat instance elkészítése
     *
	 * @return array
	 */
	public function getNeedToRunJobs()
	{
		$jobs = [];

		foreach (CronModel::getNeedToRunJobs() as $job) {
			$jobs[] = $this->getJob($job->className);
		}

		return $jobs;
	}

    /**
     * Job instance létrehozása
     *
     * @param string $className
     * @return \Phalcon\Mvc\Model
     * @throws Exception
     */
	public function getJob($className)
	{
        $job = $className::findFirst(
            'className = \'' . addslashes($className) . '\''
        );

        if (!is_object($job)) {
            throw new Exception(
                $className
                . ' nem sikerült az aktív'
                . ' rekord létrehozása.'
            );
        }

        $job->setLogger($this->logger);

		return $job;
	}

	/**
     * Csak egy feladat futtatása, className alapján
     *
	 * @param string $className
	 * @return Cron
	 * @throws Exception
	 */
	public function runJob($className)
	{
        return $this->runJobs([
            $this->getJob($className)
        ]);
	}

	/**
     * Összes várakozó feladat futtatása
     *
     * @param array $jobs
	 * @return Cron
	 */
	public function runJobs($jobs = [])
	{
	    if ($jobs == null) {
	        $jobs = $this->jobs;
        }

        /**
         * @var \App\Model\Cron $job
         */
        foreach ($jobs as $job) {
		    try {
		        // napló
                if (!$job->isMonitor()) {
                    $this->logger->debug($job->className . ' job indítása.');
                }

                // elindítottnak jelöljük a jobot
				$job->setStarted();

                // indítjuk a jobot
                $job->run();

                // befejezettnek jelöljük a jobot
				$job->setFinished();

                // napló
                if (!$job->isMonitor()) {
                    $this->logger->debug($job->className . ' job lefutott.');
                }
		    } catch (Exception $e) {
		        $this->logger->error(
                    $e->getMessage() . PHP_EOL . $e->getTraceAsString()
				);
		    }
		}

		return $this;
	}
}