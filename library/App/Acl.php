<?php

namespace App;
use App\Model\AbstractModel;
use App\Model\AclMatrix;
use App\Model\AclRole;
use App\Model\Doctor;
use App\Model\Praxis;
use App\Model\User;
use Phalcon\Acl\Resource;
use Phalcon\Acl\Role;
use Phalcon\Exception;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\User\Component;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use App\Model\AclResourceHasAction;
use App\Model\Interfaces\Acl as AclInterface;

/**
 * Class Acl
 * @package App
 */
class Acl
    extends Component
{
    /**
     * The ACL Object
     *
     * @var \Phalcon\Acl\Adapter\Memory
     */
    private $acl;

    /**
     * @var string
     */
    const CACHE_KEY = 'acl';

    /**
     * @var array
     */
    protected $controllerCache;

    /**
     * @var array
     */
    protected $resourceHasActionCache;

    /**
     * Elvégzi a szükséges műveleteket, hogy az ACL javascript oldalon
     * is futtatható legyen.
     *
     * @return void
     * @throws Exception
     */
    public function buildJsAcl()
    {
        $jsArray = [];
        $results = AclResourceHasAction::find();

        foreach ($results as $result) {
            if (!array_key_exists($result->resourceId, $jsArray)) {
                // ha még nem létezik, létrehozzuk
                $jsArray[$result->resourceId] = [];
            }

            $jsArray[$result->resourceId][$result->actionId] = $this->isAllowedByIdentity(
                $result->resourceId
                , $result->actionId
            );
        }

        // JS onload script
        $js = '$(document).ready(function(){
            Acl.setAcl(' . json_encode($jsArray, JSON_NUMERIC_CHECK) . ');
        });';

        $this->assets->addInlineJs($js);
    }

    /**
     * Megvizsgálja, hogy az adott controller (resource) támogatja-e
     * az action-t
     *
     * TODO: a függvény még nincs kész, ha egy resource nem controller
     * vagy az action nem controller action akkor nem működik!
     *
     * @param string $resourceId
     * @param string $actionId
     * @return bool
     */
    public function resourceHasAction($resourceId, $actionId)
    {
        if (!$this->resourceHasActionCache)
        {
            $resourceHasActions =
                AclResourceHasAction::find();

            foreach($resourceHasActions as $resourceHasAction)
            {
                $this->resourceHasActionCache[$resourceHasAction->resourceId][]
                    = $resourceHasAction->actionId;
            }
        }

        return (in_array($actionId, $this->resourceHasActionCache[$resourceId]));
    }

    /**
     * @param $resourceId
     * @return bool|Controller
     */
    protected function getController($resourceId)
    {
        $controllerClass = ucfirst($resourceId) . 'Controller';

        if (array_key_exists($controllerClass, $this->controllerCache)) {
            // cache hit
            return $this->controllerCache[$controllerClass];
        }

        if (!class_exists($controllerClass)) {
            // nincs ilyen class
            return null;
        }

        // példányosítjuk
        $controller = new $controllerClass;

        // cache-be mentjük
        $this->controllerCache[$controllerClass] = $controller;

        return $controller;
    }

    /**
     * Rebuilds the access list into a file
     *
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function rebuild()
    {
        $acl = new AclMemory();

        $acl->setDefaultAction(\Phalcon\Acl::DENY);

        // Szerepkörök
        $roles = AclRole::find();
        foreach ($roles as $role) {
            $acl->addRole(new Role($role->roleId));
        }

        // Jogosultságok (resource + action)
        $array = [];
        foreach (AclResourceHasAction::find() as $resourceHasAcl) {
            $array[$resourceHasAcl->resourceId][] = $resourceHasAcl->actionId;
        }

        foreach ($array as $resourceId=>$actions) {
            $acl->addResource(new Resource($resourceId), $actions);
        }

        // Szerepkör és jogosultság összekapcsolása
        $matrix = AclMatrix::find();
        foreach ($matrix as $m) {
            $acl->allow($m->roleId, $m->resourceId, $m->actionId);
        }

        return $acl;
    }

    /**
     * Egyszerű jogosultságvizsgálat
     *
     * @param $roleId
     * @param $resourceId
     * @param $actionId
     * @return bool
     */
    public function isAllowed($roleId, $resourceId, $actionId)
    {
        return $this->getAcl()->isAllowed(
            $roleId
            , $resourceId
            , $actionId
        );
    }

    /**
     * A belépett felhasználó összes szerepkörével nézi a jogosultságot
     *
     * @param $resourceId
     * @param $actionId
     * @param User|null $user
     * @return bool
     * @throws Exception
     */
    public function isAllowedByIdentity($resourceId, $actionId, $user = null)
    {
        if ($user == null) {
            $user = $this->auth->getIdentity();
        }

        $where = [];
        /** Ha doktor joggal van, és az adott rendelőben nem rendelő admin,
         *  de egyébként rendelő admin szerepköre is van,
         *  akkor el kell venni tőle a jogot
         */
        if ($user && $user->isDoctor()) {
            /** @var Doctor $doctor */
            $doctor = $user->getDoctor();

            /** @var Praxis $praxis */
            $praxis = $doctor->getInterfacePraxis();

            if (!$doctor->isPraxisAdmin($praxis->praxisId)) {
                $where = ['roleId != "' . AclRole::ROLE_ID_PRAXIS_ADMIN . '"'];
            }
        }

        $groupHasAclRoles = $user->group->getGroupHasAclRoles($where);

        if (count($groupHasAclRoles) == 0) {
            throw new Exception(sprintf(
                'A felhasználói csoporthoz ("%s") nincs szerepkör kapcsolva.'
                , $user->group->groupName
            ));
        }

        foreach ($groupHasAclRoles as $relation) {
            // ha valamelyik szerepkörnél engedélyezett
            if ($this->getAcl()->isAllowed(
                $relation->roleId
                , $resourceId
                , $actionId
            )) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string        $resourceId
     * @param string        $actionId
     * @param AbstractModel $model
     * @param null|User     $user
     * @return boolean
     * @throws Exception
     */
    public function isAllowedModelByIdentity($resourceId, $actionId, $model, $user = null)
    {
        $result = $this->isAllowedByIdentity($resourceId, $actionId, $user);

        if (!$result) {
            return $result;
        }

        if (!($model instanceof AclInterface)) {
            throw new Exception(
                sprintf('A %s modellnek az AclInterface-t implementálnia kell.', get_class($model))
            );
        }

        return $model->isAllowedByIdentity($resourceId, $actionId, $user);
    }

    /**
     * @param string    $resourceId
     * @param string    $actionId
     * @param null|User $user
     * @return Acl
     */
    public function denyByIdentity($resourceId, $actionId, $user = null)
    {
        if ($user == null) {
            $user = $this->auth->getIdentity();
        }

        foreach ($user->getGroup()->getGroupHasAclRoles() as $groupHasAclRole) {
            $this->getAcl()->deny($groupHasAclRole->roleId, $resourceId, $actionId);
        }

        return $this;
    }

    /**
     * Returns the ACL list
     *
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function getAcl()
    {
        // Check if the ACL is already created
        if ($this->acl != null) {
            return $this->acl;
        }

        //ACL cache engedelyezve van-e
        if ($this->aclCache != null) {
            $this->acl = $this->aclCache->get(self::CACHE_KEY);
        }

        //ha nincs cacheben/lejart vagy nincs engedelyezve a cache
        if($this->acl == null) {
            // ujraepitjuk az acl-t
            $this->acl = $this->rebuild();
            if ($this->aclCache != null) {
                $this->aclCache->save(
                    self::CACHE_KEY
                    , $this->acl
                );
            }
        }

        return $this->acl;
    }
}