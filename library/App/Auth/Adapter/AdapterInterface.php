<?php

namespace App\Auth\Adapter;

use App\Model\User;

/**
 * Auth Interfész
 * @package App\Auth\Adapter
 */
interface AdapterInterface
{
    /**
     * @return User
     */
    public function login();
}