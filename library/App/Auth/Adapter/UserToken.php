<?php

namespace App\Auth\Adapter;

use Phalcon\Exception;
use App\Model\UserToken as UserTokenModel;
use App\Model\User as UserModel;

/**
 * Class UserToken
 * @package App\Auth\Adapter
 */
class UserToken
    extends AbstractAdapter
{
    /**
     * @var string
     */
    protected $token;

    /**
     * @return static
     * @throws Exception
     */
    public function login()
    {
        $tokenModel = UserTokenModel::findFirstByToken($this->token);

        // nincs ilyen token
        if (!$tokenModel) {
            throw new Exception('A megadott token nem létezik.');
        }

        // lejárat vizsgálata
        $expirationAt = $tokenModel->expirationAt;
        if ($expirationAt != null) {
            if($tokenModel->expirationAt < date('Y-m-d H:i:s')) {
                throw new Exception('A megadott token érvényességi ideje lejárt. Kérjük, készítsen újat.');
            }
        }

        // létezik-e ilyen felhasználó
        $userModel = UserModel::findFirst($tokenModel->userId);
        if (!($userModel)) {
            throw new Exception('Nem található a megadott tokenhez felhasználó!');
        }

        $messages = $userModel->checkStatus();
        if (count($messages)) {
            throw new Exception(implode(' ', $messages));
        }

        // ha minden ok
        return $userModel;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return UserToken
     */
    public function setToken(string $token): UserToken
    {
        $this->token = $token;
        return $this;
    }
}