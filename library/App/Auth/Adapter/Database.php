<?php

namespace App\Auth\Adapter;

use Phalcon\Exception;
use App\Model\User;

/**
 * Class Database
 * @package App\Auth\Adapter
 */
class Database
    extends AbstractAdapter
{
    /**
     * @var int
     */
    protected $userId;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $model;

    /**
     * @return User
     * @throws Exception
     */
    public function login()
    {
        $modelClass = $this->model;
        $model = new $modelClass();

        if (is_numeric($this->userId)) {
            $user = $model->findFirst($this->userId);
            if ($user == false) {
                throw new Exception('A megadott azonosítóval nem létezik felhasználó!');
            }
        } else {
            $user = $model->findFirstByEmail($this->email);
            if ($user == false || !$this->security->checkHash($this->password, $user->password)) {
                throw new Exception('A megadott e-mail cím és jelszó páros hibás!');
            }
        }

        // flagek ellenőrzése
        $messages = $user->checkStatus();
        if (count($messages)) {
            throw new Exception(implode(' ', $messages));
        }

        return $user;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Database
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Database
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return Database
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }
}