<?php

namespace App\Auth\Adapter;

use App\Model\Config;
use App\Model\User;
use Phalcon\Exception;
use Phalcon\Mvc\User\Component;

/**
 * Class AbstractAdapter
 * @package App\Auth\Adapter
 */
class AbstractAdapter
    extends Component
    implements AdapterInterface
{
    /**
     * @return mixed
     */
    public function login()
    {
    }
}