<?php

namespace App;

use App\ServiceProvider\AbstractServiceProvider;
use Phalcon\Di\FactoryDefault;
use Phalcon\Exception;

/**
 * Class Bootstrap
 * @package App
 */
class Bootstrap
{
    /**
     * @var FactoryDefault
     */
    protected $di;

    /**
     * Minimum service requirements for modular Phalcon behaviour.
     *
     * Rest of the services are defined by the modules own bootstraps.
     *
     * @return array
     */
    public function getDefaultServices()
    {
        //TODO áthelyezni
        /*require_once '../vendor/sentry/sentry/lib/Raven/Autoloader.php';
        \Raven_Autoloader::register();
        $client = new \Raven_Client('https://53e7029a39a44bab904468acfe44019e:460e73e79435480cbd4d803418c1eb04@sentry.io/1271700');
        $error_handler = new \Raven_ErrorHandler($client);
        $error_handler->registerExceptionHandler();
        $error_handler->registerErrorHandler();
        $error_handler->registerShutdownFunction();*/
        //

        return [
            'dotEnv'         => '\\App\\ServiceProvider\\Service\\DotEnv'
            , 'app'          => '\\App\\ServiceProvider\\Service\\App'
            , 'config'       => '\\App\\ServiceProvider\\Service\\Config'
            , 'loader'       => '\\App\\ServiceProvider\\Service\\Loader'
            , 'errorHandler' => '\\App\\ServiceProvider\\Service\\ErrorHandler'
            , 'router'       => '\\App\\ServiceProvider\\Service\\Router'
        ];
    }

    /**
     * @param bool $sendResponse
     * @return mixed
     * @throws Exception
     */
    public function run($sendResponse = true)
    {
        $this->registerServices();

        $response = $this->di->get('app')->handle();

        if (!$sendResponse) {
            return $response;
        }

        $response->send();
    }

    /**
     * @param array $services
     * @return $this
     * @throws Exception
     */
    public function registerServices($services = [])
    {
        foreach ($services ?: $this->getDefaultServices() as $serviceName=>$className) {
            $this->registerService($serviceName, $className);

        }

        return $this;
    }

    /**
     * @param $serviceName
     * @param $className
     * @throws Exception
     * @return Bootstrap
     */
    public function registerService($serviceName, $className)
    {
        if ($this->di == null) {
            $this->di = new FactoryDefault();
            $this->di->setShared('bootstrap', $this);
        }

        if (!class_exists($className)) {
            throw new Exception('Unknown service provider: ' . $className);
        }

        // initialize the service
        $serviceProvider = new $className();
        if (!($serviceProvider instanceof AbstractServiceProvider)) {
            throw new Exception(
                'This class must be an instance of AbstractServiceProvider' . $className
            );
        }
        $serviceProvider->setDi($this->di);
        $service = $serviceProvider->getService();

        if (is_object($service)) {
            // valid resource return value
            $this->di->setShared($serviceName, $service);
        }

        return $this;
    }

    /**
     * @return FactoryDefault
     */
    public function getDi(): FactoryDefault
    {
        return $this->di;
    }

    /**
     * @param FactoryDefault $di
     * @return Bootstrap
     */
    public function setDi(FactoryDefault $di): Bootstrap
    {
        $this->di = $di;
        return $this;
    }
}
