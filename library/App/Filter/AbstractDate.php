<?php

namespace App\Filter;

/**
 * Class AbstractDate
 * @package App\Filter
 */
abstract class AbstractDate
    implements FilterInterface
{
    /**
     * @var string
     */
    protected $format;

    /**
     * @param string $value formátum "Y-m-d" vagy "Y-m-d H:i:s"
     * @return string
     */
    public function filter($value)
    {
        return strftime(
            $this->format
            , strtotime($value)
        );
    }
}