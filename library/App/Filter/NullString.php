<?php

namespace App\Filter;

use Phalcon\Filter\Exception;

/**
 * Class NullString
 * @package App\Filter
 */
class NullString
    implements FilterInterface
{
    const BOOLEAN       = 1;
    const INTEGER       = 2;
    const EMPTY_ARRAY   = 4;
    const STRING        = 8;
    const ZERO          = 16;
    const NULL_STRING   = 32;
    const ALL           = 63;

    /**
     * @var array
     */
    private $constants = array(
        self::BOOLEAN       => 'boolean',
        self::INTEGER       => 'integer',
        self::EMPTY_ARRAY   => 'array',
        self::STRING        => 'string',
        self::ZERO          => 'zero',
        self::NULL_STRING   => 'nullString',
        self::ALL           => 'all'
    );

    /**
     * Internal type to detect
     * @var integer
     */
    private $type = self::ALL;

    /**
     * NullString constructor.
     * @param null $options
     * @throws Exception
     */
    public function __construct($options = null)
    {
        if (!is_array($options)) {
            $options = func_get_args();
            $temp    = array();

            if (!empty($options)) {
                $temp = array_shift($options);
            }

            $options = $temp;
        } else if (is_array($options) && array_key_exists('type', $options)) {
            $options = $options['type'];
        }

        if (!empty($options)) {
            $this->setType($options);
        }
    }

    /**
     * Returns null representation of $value, if value is empty and matches
     * types that should be considered null.
     * @param  string $value
     * @return null
     */
    public function filter($value)
    {
        $type = $this->getType();

        // NULL_STRING ('null', 'NULL')
        if ($type >= self::NULL_STRING) {
            $type -= self::NULL_STRING;
            if (is_string($value) && mb_strtolower($value) == 'null') {
                return null;
            }
        }

        // STRING ZERO ('0')
        if ($type >= self::ZERO) {
            $type -= self::ZERO;
            if (is_string($value) && ($value == '0')) {
                return null;
            }
        }

        // STRING ('')
        if ($type >= self::STRING) {
            $type -= self::STRING;
            if (is_string($value) && ($value == '')) {
                return null;
            }
        }

        // EMPTY_ARRAY (array())
        if ($type >= self::EMPTY_ARRAY) {
            $type -= self::EMPTY_ARRAY;
            if (is_array($value) && ($value == array())) {
                return null;
            }
        }

        // INTEGER (0)
        if ($type >= self::INTEGER) {
            $type -= self::INTEGER;
            if (is_int($value) && ($value == 0)) {
                return null;
            }
        }

        // BOOLEAN (false)
        if ($type >= self::BOOLEAN) {
            $type -= self::BOOLEAN;
            if (is_bool($value) && ($value == false)) {
                return null;
            }
        }

        return $value;
    }

    /**
     * Returns the set null types
     * @return integer
     */
    private function getType()
    {
        return $this->type;
    }

    /**
     * Set the null types
     * @param  integer|array $type
     * @throws Exception
     */
    private function setType($type = null)
    {
        if (is_array($type)) {
            $detected = 0;
            foreach($type as $value) {
                if (is_int($value)) {
                    $detected += $value;
                } else if (in_array($value, $this->constants)) {
                    $detected += array_search($value, $this->constants);
                }
            }

            $type = $detected;
        } else if (is_string($type)) {
            if (in_array($type, $this->constants)) {
                $type = array_search($type, $this->constants);
            }
        }

        if (!is_int($type) || ($type < 0) || ($type > self::ALL)) {
            throw new Exception('Unknown type');
        }

        $this->type = $type;
    }

}