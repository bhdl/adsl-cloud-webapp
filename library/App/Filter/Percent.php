<?php

namespace App\Filter;

use App\Traits\InterfaceLanguage;

/**
 * Class Percent
 * @package App\Filter
 */
class Percent
{
    use InterfaceLanguage;

    /**
     * @param $value
     * @return string
     */
    public function filter($value): string
    {
        $locale = $this->getInterfaceLanguage()->locale;

        $formatter = new \NumberFormatter(
            $locale
            , \NumberFormatter::DECIMAL
        );

        $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, 8);

        return $formatter->format($value) . '%';
    }
}