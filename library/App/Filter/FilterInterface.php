<?php

namespace App\Filter;

/**
 * Interface FilterInterface
 * @package App\Filter
 */
interface FilterInterface
{
    /**
     * @param string $value
     * @return mixed
     */
    public function filter($value);
}