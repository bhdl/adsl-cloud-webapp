<?php

namespace App\Filter;

/**
 * Class StringClean
 * @package App\Filter
 */
class StringClean
    implements FilterInterface
{
	/**
	 * Cserék: miről
	 * @var array
	 */
    private $from;

	/**
	 * Cserék: mire
	 * @var array
	 */
    private $to;

    /**
     * StringCleanFilter constructor.
     * @param string $invalidChars
     */
    public function __construct($invalidChars = '')
    {
        $this->setInvalidChars($invalidChars);
    }

    /**
     * Sztring levágása
     * @param mixed $value
     * @return string
     */
    public function filter($value)
    {
        $value = trim($value);

        $value = preg_replace("/\r?\n|\r/", '', $value);

        return preg_replace(
            $this->from,
            $this->to,
            mb_convert_encoding($value, 'UTF-8', mb_detect_encoding($value, 'UTF-8, ISO-8859-2', true))
        );
    }

    /**
     * Eltávolítandó karakterek beállítása
     * @param $invalidChars
     */
    private function setInvalidChars($invalidChars)
    {
    	$from = array('/\n\r|\r\n|\r/');
    	$to = array("\n");

    	if (mb_strlen($invalidChars))
    	{
    		$chars = array();
    		preg_match_all('/./u', $invalidChars, $chars);
    		$from[] = '/[' . implode('', array_map('preg_quote', $chars[0])) . ']/';
    		$to[] = '';
    	}

    	$from[] = '/([^A-Za-z0-9])\1+/';
    	$to[] = '$1';

    	$this->from = $from;
    	$this->to = $to;
    }

}