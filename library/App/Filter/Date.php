<?php

namespace App\Filter;

/**
 * Class Date
 * @package App\Filter
 */
class Date extends AbstractDate
{
    /**
     * @var string
     */
    protected $format = '%x';
}