<?php

namespace App\Filter;

use Phalcon\Filter\Exception;

/**
 * Class TextLimit
 * @package App\Filter
 */
class TextLimit
    implements FilterInterface
{
    /**
     * Maximális karakterszám
     * @var int
     */
    private $limit;

    /**
     * Egyedi kimeneti formátum, amibe a vágott szöveg bekerül
     *
     * %value% és %origValue% paraméter használható
     * @var string
     */
    private $format;

    /**
     * TextLimit constructor.
     * @param $limit
     * @param string $format
     * @throws Exception
     */
    public function __construct($limit, $format = '%value%')
    {
        $this->setLimit($limit);
        $this->setFormat($format);
    }

    /**
     * Sztring levágása
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        if (mb_strlen($value) > $this->limit) {
            $filtered = mb_substr($value, 0, $this->limit);
            return str_replace(
                array('%value%', '%origValue%')
                , array($filtered, $value)
                , $this->format
            );
        }

        return $value;
    }

    /**
     * Limit setter
     * @param $limit
     * @throws Exception
     */
    private function setLimit($limit)
    {
        if (!is_int($limit)) {
            throw new Exception('A limit csak szám lehet.');
        }

        $this->limit = $limit;
    }

    /**
     * Format setter
     * @param $format
     */
    private function setFormat($format)
    {
        $this->format = $format;
    }
}