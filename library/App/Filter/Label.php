<?php

namespace App\Filter;

/**
 * Class Label
 * @package App\Filter
 */
class Label
    implements FilterInterface
{
    /**
     * @param $value
     * @return string
     */
    public function filter($value)
    {
        $transliterator = \Transliterator::create('Any-Latin; Latin-ASCII');
        $string = $transliterator->transliterate(
            mb_convert_encoding(htmlspecialchars_decode($value), 'UTF-8', 'auto')
        );

        $string = trim(strtolower($string));

        return $string;
    }
}