<?php

namespace App\Filter;

use Phalcon\Filter\Exception;

/**
 * Class Boolean
 * @package App\Filter
 */
class Boolean
    implements FilterInterface
{
    /**
     * @var array
     */
    protected $map = [
        'true'    => true
        , 'false' => false
        , '0'     => false
        , '1'     => true
        , 'yes'   => true
        , 'no'    => false
    ];

    /**
     * @param string $value
     * @return bool
     * @throws Exception
     */
	public function filter($value)
	{
		if (!array_key_exists($value, $this->map)) {
			throw new Exception(
				"A(z) {$value} érték nem feleltethető meg bool típusnak."
			);
		}

		return $this->map[$value];
	}

    /**
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * @param array $map
     * @return Boolean
     */
    public function setMap(array $map): Boolean
    {
        $this->map = $map;
        return $this;
    }
}