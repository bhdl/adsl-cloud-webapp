<?php

namespace App\Filter;

/**
 * Class DateInterval
 * @package App\Filter
 */
class DateInterval
    implements FilterInterface
{
    /**
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        $dates = unserialize($value);

        $dateFrom = $dates[0];
        $dateTo   = $dates[1];

        if (is_string($dateFrom)) {
            $dateFrom = strtotime($dateFrom);
        }

        if (is_string($dateTo)) {
            $dateTo = strtotime($dateTo);
        }

        $filtered = strftime('%x', $dateFrom);

        if (strcmp(date('Ymd', $dateFrom), date('Ymd', $dateTo)) !== 0) {
            return $filtered . ' - ' . strftime('%x', $dateTo);
        }

        return $filtered;
    }
}