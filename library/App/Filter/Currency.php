<?php

namespace App\Filter;

use App\Traits\InterfaceLanguage;

/**
 * Class Currency
 * @package App\Filter
 */
class Currency
    implements FilterInterface
{
    use InterfaceLanguage;

    /**
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        list($v, $currencyCode) = unserialize($value);

        // 0-k törlése
        $v += 0;

        $locale = $this->getInterfaceLanguage()->locale;

        $formatter = new \NumberFormatter(
            $locale
            , \NumberFormatter::CURRENCY
        );

        if ($v == (int)$v) {
            $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, 0);
        }

//        $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, 8);

        return $formatter->formatCurrency($v, $currencyCode);
    }
}