<?php

namespace App\Filter;

/**
 * Class Datetime
 * @package App\Filter
 */
class Datetime extends AbstractDate
{
    /**
     * @var string
     */
    protected $format = '%x, %X';
}