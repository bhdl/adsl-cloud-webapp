<?php

namespace App\Filter;

use App\Traits\InterfaceLanguage;

/**
 * Class Number
 * @package App\Filter
 */
class Number
    implements FilterInterface
{
    use InterfaceLanguage;

    /**
     * @param $value
     * @return string
     */
    public function filter($value)
    {
        $locale = $this->getInterfaceLanguage()->locale;

        $formatter = new \NumberFormatter(
            $locale
            , \NumberFormatter::DECIMAL
        );

        $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, 8);

        return $formatter->format($value);
    }
}