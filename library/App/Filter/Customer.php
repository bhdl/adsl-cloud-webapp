<?php

namespace App\Filter;

use Phalcon\Mvc\User\Component;

/**
 * Class Customer
 * @package App\Filter
 */
class Customer
    extends Component
{
    /**
     * @param array $values
     * @return mixed
     */
    public function filter($values)
    {
        foreach ($values as &$v) {
            if (is_array($v)) {
                $v = $this->filter($v);
            }

            $v = $this->clean($v);
        }

        return $values;
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function clean($value)
    {
        return $this->filter->sanitize($value, ['striptags', 'trim']);
    }
}