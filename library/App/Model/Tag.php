<?php

namespace App\Model;

use App\Model\Traits\HasLanguages;
use App\Traits\InterfaceLanguage;
use App\Model\Traits\SoftDelete;
use Phalcon\Exception;

/**
 * Class Log
 * @package App\Model
 */
class Tag
    extends AbstractLogModel
{
    use SoftDelete;

    /**
     * @var integer
     */
    public $tagId;

    /**
     * @var integer
     */
    public $type = self::FORKLIFT;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var int
     */
    public $siteId;

    /**
     * @var string
     */
    public $tagToken;

    /**
     * @const string
     */
    const FORKLIFT = 'forklift';

    /**
     * @const string
     */
    const PERSON = 'person';

    /**
     * @const string
     */
    const DRONE = 'drone';

    /**
     * @var array
     */
    public static $types = [
        self::FORKLIFT      => 'forklift'
        , self::PERSON      => 'person'
        , self::DRONE       => 'drone'
    ];

    /**
     * @var datetime
     */
    public $createdAt;

    /**
     * @var datetime
     */
    public $updatedAt;

    /**
     * @var datetime
     */
    public $deletedAt;

    /**
     * @var string
     */
    public $deleted = self::NO;

    /**
     * @var array
     */
    public static $deletedTypes = [
        self::YES  => 'igen'
        , self::NO => 'nem'
    ];

    /**
     * @param string $key
     * @return string
     * @throws Exception
     */
    public static function getTypeValue($key)
    {
        if (!array_key_exists($key, static::$types)) {
            throw new Exception($this->translate->t('Nincs ilyen kulcs a boolean értékek között:') . $key);
        }

        return static::$types[$key];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return self::$types;
    }

    /**
     * @return array
     */
    public static function getDeletedTypes()
    {
        return self::$deletedTypes;
    }

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->initializeSoftDelete();

        $this->skipAttributes([
            "createdAt"
            , "updatedAt"
        ]);

	    $this->belongsTo('siteId', __NAMESPACE__ . '\Site', 'siteId', array(
            'alias' => 'sites',
            'reusable' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}