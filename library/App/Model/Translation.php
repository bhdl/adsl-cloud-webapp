<?php

namespace App\Model;

/**
 * Class Translation
 * @package App\Model
 */
class Translation
    extends AbstractModel
{
    /**
     * @var int
     */
    public $translationId;

    /**
     * @var integer
     */
    public $languageId;

    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $value;

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('languageId', __NAMESPACE__ . '\Language', 'languageId', array(
            'alias'      => 'language'
            , 'reusable' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->key;
    }
}