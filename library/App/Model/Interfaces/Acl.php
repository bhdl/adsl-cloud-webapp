<?php

namespace App\Model\Interfaces;

/**
 * Interface Acl
 * @package App\Model\Interfaces
 */
interface Acl
{
    /**
     * @param string $resourceId
     * @param string $actionId
     * @param null $user
     * @return boolean
     */
    public function isAllowedByIdentity($resourceId, $actionId, $user = null);
}