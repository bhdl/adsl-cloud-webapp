<?php

namespace App\Model;

use App\Support\Str;

/**
 * Class UserResetPassword
 * @package App\Model
 */
class UserResetPassword
    extends AbstractModel
{
    /**
     * @var integer
     */
    public $userResetPasswordId;

    /**
     * @var integer
     */
    public $userId;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $createdAt;

    /**
     * @var string
     */
    public $modifiedAt;

    /**
     * @var string
     */
    public $reset;

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        // Generate a random confirmation code
        $this->code = preg_replace('/[^a-zA-Z0-9]/', '', Str::randomHash(30));

        // Set status to non-confirmed
        $this->reset = self::NO;
    }

    /**
     * Sets the timestamp before update the confirmation
     */
    public function beforeValidationOnUpdate()
    {
        // Timestamp the confirmaton
        $this->modifiedAt = date('Y-m-d H:i:s');

        // Set status to confirmed
        $this->reset = self::YES;
    }

    /**
     *
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('userId', __NAMESPACE__ . '\User', 'userId', array(
            'alias' => 'user'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->userResetPasswordId;
    }
}
