<?php

namespace App\Model\Behavior;

use Phalcon\Di;
use Phalcon\Exception;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;

/**
 * Class Readonly
 * @package App\Model\Behavior
 */
class Readonly
    extends Behavior
    implements BehaviorInterface
{
    /**
     * @param string $type
     * @param \Phalcon\Mvc\ModelInterface $model
     * @throws Exception
     */
    public function notify($type, \Phalcon\Mvc\ModelInterface $model)
    {
        switch ($type) {
            case "beforeDelete":
            case "beforeUpdate":

                if ($model->isReadonly()) {

                    $translate = Di::getDefault()->get('translate');

                    throw new Exception(
                        $translate->_('A művelet nem hajtható végre a rekordon, mert az írásvédett.')
                    );
                }

                break;

            default: break;
        }
    }
}