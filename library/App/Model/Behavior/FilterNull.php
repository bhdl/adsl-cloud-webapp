<?php

namespace App\Model\Behavior;

use App\Filter\NullString;
use Phalcon\Di;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\Model\MetaData;

/**
 * Class FilterNull
 * @package App\Model\Behavior
 */
class FilterNull
    extends Behavior
    implements BehaviorInterface
{
    /**
     * @param string $type
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    public function notify($type, \Phalcon\Mvc\ModelInterface $model)
    {
        switch ($type) {
            case "beforeSave":
                $this->filterNull($model);
                break;
            default:
                break;
        }
    }

    /**
     * NULL típusú mezők esetén a 0, '', stb. értéket NULL-ra állítjuk.
     *
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    protected function filterNull(\Phalcon\Mvc\ModelInterface $model)
    {
        /**
         * @var $modelsMetaData MetaData
         */
        $modelsMetaData = $model->getModelsMetaData();

        // 0, '', '0', stb. értékek átalakítása NULL-ra
        $nulls = array_diff(
            $modelsMetaData->getAttributes($model)
            , $modelsMetaData->getNotNullAttributes($model)
        );

        $filter = new NullString([
            'type' => [NullString::STRING, NullString::NULL_STRING]
        ]);

        foreach ($nulls as $null) {
            if ($filter->filter($model->$null) === null) {
                $model->$null = null;
            }
        }
    }
}