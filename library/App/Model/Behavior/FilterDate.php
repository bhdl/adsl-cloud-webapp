<?php

namespace App\Model\Behavior;

use App\Support\Date;
use Phalcon\Di;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\Model\MetaData;

/**
 * Class FilterDate
 * @package App\Model\Behavior
 */
class FilterDate
    extends Behavior
    implements BehaviorInterface
{
    /**
     * @param string $type
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    public function notify($type, \Phalcon\Mvc\ModelInterface $model)
    {
        switch ($type) {
            case "beforeValidation":
                $this->filterDate($model);
                break;
            default:
                break;
        }
    }

    /**
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    protected function filterDate(\Phalcon\Mvc\ModelInterface $model)
    {
        /**
         * @var $modelsMetaData MetaData
         */
        $modelsMetaData = $model->getModelsMetaData();

        $dataTypes = $modelsMetaData->getDataTypes($model);

        foreach ($dataTypes as $attribute=>$type) {

            if ($type == \Phalcon\Db\Column::TYPE_DATE) {
                $date = Date::dateFromLocalized($model->$attribute);
                if ($date) {
                    $model->$attribute = $date->format('Y-m-d');
                }
            } else if($type == \Phalcon\Db\Column::TYPE_DATETIME) {
                $date = Date::dateTimeFromLocalized($model->$attribute);
                if ($date) {
                    $model->$attribute = $date->format('Y-m-d H:i:s');
                }
            }
        }
    }
}