<?php

namespace App\Model\Behavior;

use Phalcon\Di;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;

/**
 * Class RowLevelAcl
 * @package App\Model\Behavior
 */
class RowLevelAcl
    extends Behavior
    implements BehaviorInterface
{
    /**
     * @param string $type
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    public function notify($type, \Phalcon\Mvc\ModelInterface $model)
    {
        switch ($type) {
            case "beforeValidation":
                $traits = class_uses($model);
                if (
                    is_array($traits)
                    && array_key_exists('App\Model\Traits\RowLevelAcl', $traits)
                    && !is_numeric($model->{$model::getRowLevelAclUserAttribute()})
                    && $model::isRowLevelAclEnabled()
                ) {

                    $di = Di::getDefault();

                    // a belépett felhasználó id-ját elmentjük a modellbe
                    $model->{$model::getRowLevelAclUserAttribute()} = $di->get('auth')
                        ->getIdentity()
                        ->userId
                    ;
                }

                break;

            default:
                break;
        }
    }
}