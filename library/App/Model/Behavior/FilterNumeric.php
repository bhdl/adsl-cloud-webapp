<?php

namespace App\Model\Behavior;

use App\Support\Numeric;
use Phalcon\Di;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
use Phalcon\Mvc\Model\MetaData;

/**
 * Class FilterNumeric
 * @package App\Model\Behavior
 */
class FilterNumeric
    extends Behavior
    implements BehaviorInterface
{
    /**
     * @param string $type
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    public function notify($type, \Phalcon\Mvc\ModelInterface $model)
    {
        switch ($type) {
            case "beforeValidation":
                $this->filterNumeric($model);
                break;
            default:
                break;
        }
    }

    /**
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    protected function filterNumeric(\Phalcon\Mvc\ModelInterface $model)
    {
        /**
         * @var $modelsMetaData MetaData
         */
        $modelsMetaData = $model->getModelsMetaData();

        // decimal, float értékek esetén "," => "." csere
        $numeric = array_keys($modelsMetaData->getDataTypesNumeric($model) ?: []);

        foreach ($numeric as $n) {
            $model->$n = Numeric::numericFromLocalized($model->$n);
        }
    }
}