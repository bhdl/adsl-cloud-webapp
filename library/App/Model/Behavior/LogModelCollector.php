<?php

namespace App\Model\Behavior;

use App\Model\AbstractLogModel;
use Phalcon\Exception;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;

/**
 * A LogModelCollector összeszedi a megváltozott (létrehozás, módosítás) modelleket
 * ami bármelyik másik komponens (pl. logger) le tud kérdezni.
 *
 * A LogModelCollector-t a MysqlLogged adapter kapcsolgatja ki-be.
 *
 * Class LogModelCollector
 * @package App\Model\Behavior
 */
class LogModelCollector
    extends Behavior
    implements BehaviorInterface
{
    /**
     * @var bool
     */
    protected static $enabled = false;

    /**
     * Model collection, ami a megváltozott modelleket tartalmazza
     *
     * [[model:string, data:[]]]
     *
     * @var array
     */
    protected static $models = [];

    /**
     * @param string $type
     * @param \Phalcon\Mvc\ModelInterface $model
     */
    public function notify($type, \Phalcon\Mvc\ModelInterface $model)
    {
        if (
            !static::isEnabled()
            || !($model instanceof AbstractLogModel)
        ) {
            // nincs engedélyezve
            return;
        }

        switch ($type) {
            case "beforeSave":
            case "beforeDelete":
                // módosítás, törlés
                static::addModel($model);
                break;

            case "afterCreate":
                // létrehozás, pk visszaírása (beforeCreate-nél még nincs, viszont az állapot kell)
                foreach (static::$models as &$m) {
                    if (strcmp($m['model'], get_class($model)) !== 0) {
                        // nem ezeket a droidokat keressük
                        continue;
                    }

                    $primaryKeys = $model
                        ->getModelsMetaData()
                        ->getPrimaryKeyAttributes($model)
                    ;

                    foreach ($primaryKeys as $pk) {
                        $m['data'][$pk] = $model->$pk;
                    }
                }

                break;

            default: break;
        }
    }

    /**
     * @param $model
     */
    public static function addModel(\Phalcon\Mvc\ModelInterface $model)
    {
        // létrehozás esetén összes mező
        $changedFields = $model->getModelsMetaData()->getAttributes($model);
        if ($model->getOperationMade() == Model::OP_UPDATE) {
            // módosítás esetén csak a releváns mezők
            try {
                $changedFields = $model->getChangedFields();
            } catch (Exception $e) {
            }
        }

        static::$models[] = [
            // a modell ami változott (csak név)
            'model'          => get_class($model)
            // a modell aktuális állapota
            , 'data'         => $model->toArray(false) ?: []
            // a modell korábbi állapota
            , 'snapShotData' => $model->getSnapshotData() ?: []
            // az elvégzett művelet típusa
            , 'operation'    => $model->getOperationMade()
            // milyen mezők változtak meg?
            , 'changed'      => $changedFields ?: []
        ];
    }

    /**
     * @return array
     */
    public static function getModels()
    {
        return static::$models;
    }

    /**
     * clearModels
     */
    public static function clearModels()
    {
        static::$models = [];
    }

    /**
     * @return boolean
     */
    public static function isEnabled(): bool
    {
        return self::$enabled;
    }

    /**
     * @param boolean $enabled
     */
    public static function setEnabled(bool $enabled)
    {
        self::$enabled = $enabled;
    }
}