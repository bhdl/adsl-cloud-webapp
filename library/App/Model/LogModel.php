<?php

namespace App\Model;

use Phalcon\Exception;

/**
 * Class LogModel
 * @package App\Model
 */
class LogModel
    extends AbstractModel
{
    /**
     * @const integer
     */
    const CREATE = 1;

    /**
     * @const integer
     */
    const MODIFY = 2;

    /**
     * @const integer
     */
    const DELETE = 3;

    /**
     * @var int
     */
    public $logModelId;

    /**
     * @var int
     */
    public $logId;

    /**
     * @var string
     */
    public $objectId;

    /**
     * @var
     */
    public $operation;

    /**
     * @var string
     */
    public $className;

    /**
     * @var array
     */
    protected static $operations = [
        self::CREATE     => 'Létrehozás'
        , self::MODIFY   => 'Módosítás'
        , self::DELETE   => 'Törlés'
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('logId', __NAMESPACE__ . '\Log', 'logId', array(
            'alias'    => 'log',
            'reusable' => true
        ));

        $this->hasMany('logModelId', __NAMESPACE__ . '\LogModelAttribute', 'logModelId', array(
            'alias' => 'logModelAttributes'
            , 'foreignKey' => array(
                'message'  => 'A rekord nem törölhető, mert már tartozik hozzá egyéb adat.'
            )
        ));
    }

    /**
     * @return array
     */
    public static function getOperations() : array
    {
        return self::$operations;
    }

    /**
     * @param int $operation
     * @return string
     * @throws Exception
     */
    public static function getOperationValue(int $operation) : string
    {
        if (!array_key_exists($operation, static::$operations)) {
            throw new Exception('Nincs ilyen művelet a operation értékek között: ' . $operation);
        }

        return static::$operations[$operation];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->logModelId;
    }
}