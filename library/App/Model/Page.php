<?php

namespace App\Model;

use App\Model\Traits\HasLanguages;
use App\Traits\InterfaceLanguage;
use Phalcon\Exception;

/**
 * Class Log
 * @package App\Model
 */
class Page
    extends AbstractLogModel
{
    use HasLanguages, InterfaceLanguage;
    /**
     * @var integer
     */
    public $pageId;

    /**
     * @var integer
     */
    public $parentId;

    /**
     * @var integer
     */
    public $position;

    /**
     * @var string
     */
    public $active;

    /**
     * @var string
     */
    public $slideshow;

    /**
     * @var string
     */
    public $map;

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->hasMany('pageId', __NAMESPACE__ . '\PageLanguage', 'pageId', array(
            'alias' => 'pageLanguages'
        ));
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return strcmp($this->active, self::YES) === 0;
    }

    /**
     * beforeValidation
     */
    public function beforeValidation()
    {
        if(!$this->position) {
            $maxPosition = Page::maximum(["column" => "position", "conditions" => "parentId = $this->parentId" ]);
            if ($maxPosition) {
                $this->position = intval($maxPosition) + 1;
            } else {
                $this->position = 1;
            }
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getPageLanguages([
            'languageId = ' . $this->getInterfaceLanguage()->languageId
        ])->getFirst()->pageName;
    }
}