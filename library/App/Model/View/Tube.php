<?php

namespace App\Model\View;

use App\Model\Config;
use App\Traits\InterfaceLanguage;
use NumberFormatter;
use Phalcon\Mvc\Model\Resultset;

/**
 * Class Tube
 * @package App\Model\View
 */
class Tube
    extends \App\Model\Tube
{
    /**
     * @return string
     */
    public function getSource()
    {
        return 'view__tube';
    }
}