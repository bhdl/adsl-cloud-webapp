<?php

namespace App\Model\View;

/**
 * Class Unit
 * @package App\Model\View
 */
class Unit
    extends \App\Model\View\AbstractViewModel
{
    /**
     * @return string
     */
    public function getSource()
    {
        return 'view__unit';
    }
}