<?php

namespace App\Model\View;

/**
 * Class Group
 * @package App\Model\View
 */
class Group
    extends AbstractViewModel
{
    /**
     * @return string
     */
    public function getSource()
    {
        return 'view__group';
    }
}