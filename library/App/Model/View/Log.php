<?php

namespace App\Model\View;

/**
 * Class Log
 * @package App\Model\View
 */
class Log
    extends AbstractViewModel
{
    /**
     * @return string
     */
    public function getSource()
    {
        return 'view__log';
    }
}