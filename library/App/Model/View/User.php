<?php

namespace App\Model\View;

/**
 * Class User
 * @package App\Model\View
 */
class User
    extends AbstractViewModel
{
    /**
     * @return string
     */
    public function getSource()
    {
        return 'view__user';
    }
}