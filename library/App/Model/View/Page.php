<?php

namespace App\Model\View;

/**
 * Class Page
 * @package App\Model\View
 */
class Page
    extends AbstractViewModel
{
    /**
     * @return string
     */
    public function getSource()
    {
        return 'view__page';
    }

    /**
     * returns the child pages of the page
     * @param \App\Model\Language $language
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getChildPages(\App\Model\Language $language)
    {
        return self::find([
            "active = '" . \App\Model\AbstractModel::YES . "'"
            . " AND parentId = " . $this->pageId
            . " AND languageId = " . $language->languageId
            , "order" => "position ASC"
        ]);
    }
}