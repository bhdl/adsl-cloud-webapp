<?php

namespace App\Model\View;

use App\Model\Config;
use App\Traits\InterfaceLanguage;
use NumberFormatter;
use Phalcon\Mvc\Model\Resultset;

/**
 * Class TubeClassification
 * @package App\Model\View
 */
class TubeClassification
    extends \App\Model\TubeClassification
{
    /**
     * @return string
     */
    public function getSource()
    {
        return 'view__tube_classification';
    }
}
