<?php

namespace App\Model\View;

/**
 * Class Translation
 * @package App\Model\View
 */
class Translation
    extends AbstractViewModel
{
    /**
     * @return string
     */
    public function getSource()
    {
        return 'view__translation';
    }
}