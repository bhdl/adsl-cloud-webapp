<?php

namespace App\Model\View;

use App\Model\Traits\FindArgumentsInjector;
use Phalcon\Mvc\Model;

/**
 * Class AbstractViewModel
 * @package App\Model\View
 */
abstract class AbstractViewModel
    extends Model
{
    use FindArgumentsInjector;
}