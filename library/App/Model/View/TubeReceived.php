<?php

namespace App\Model\View;


/**
 * Class TubeReceived
 * @package App\Model\View
 */
class TubeReceived
    extends \App\Model\TubeClassification
{
    /**
     * @return string
     */
    public function getSource()
    {
        return 'view__tube_received';
    }
}
