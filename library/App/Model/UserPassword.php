<?php

namespace App\Model;


/**
 * Class UserPassword
 * @package App\Model
 */
class UserPassword
    extends AbstractModel
{
    /**
     * @var int
     */
    public $userPasswordId;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $modifiedAt;
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->userPasswordId;
    }
}