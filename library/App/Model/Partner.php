<?php

namespace App\Model;

use App\Model\Traits\SoftDelete;
use Phalcon\Mvc\Model\Relation;

class Partner extends AbstractLogModel
{
    use SoftDelete;

    /**
     * @var integer
     */
    public $partnerId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var datetime
     */
    public $createdAt;

    /**
     * @var datetime
     */
    public $updatedAt;

    /**
     * @var datetime
     */
    public $deletedAt;

    /**
     * @var string
     */
    public $deleted = self::NO;

    /**
     * @var array
     */
    public static $deletedTypes = [
        self::YES  => 'igen'
        , self::NO => 'nem'
    ];

    public function initialize()
    {
        parent::initialize();

        $this->initializeSoftDelete();

        $this->skipAttributes([
            "createdAt"
            , "updatedAt"
        ]);

        $this->hasMany('partnerId', __NAMESPACE__ . '\Product', 'partnerId', array(
            'alias' => 'products'
             , 'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
                'message' => 'Organization cannot be deleted because it has sites'
            ]
        ));

        $this->hasMany('partnerId', __NAMESPACE__ . '\Site', 'partnerId', array(
            'alias' => 'sites'
        ));

    }

    /**
     * @return array
     */
    public static function getDeletedTypes()
    {
        return self::$deletedTypes;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}