<?php

namespace App\Model;

use App\Model\Traits\HasLanguages;
use App\Traits\InterfaceLanguage;
use Phalcon\Exception;

/**
 * Class Log
 * @package App\Model
 */
class ProductPositionEvent
    extends AbstractLogModel
{
    use HasLanguages, InterfaceLanguage;
    /**
     * @var integer
     */
    public $productPositionEventId;

    /**
     * @var string
     */
    public $positionEventData;

    /**
     * @var timestamp
     */
    public $start;

    /**
     * @var timestamp
     */
    public $end;

    /**
     * @return string
     */
    public function getName()
    {
        return 'N\A';
    }
}
