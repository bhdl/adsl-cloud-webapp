<?php

namespace App\Model\Traits;

/**
 * Trait FindArgumentsInjector
 * @package App\Model\Traits
 */
trait FindArgumentsInjector
{
    /**
     * Phalcon Model find() jellegű függvények argumentum listájába helyez a paramétereknek
     * alapján további SQL feltételeket. RowLevelAcl és Flags traitek használják.
     *
     * @param null $arguments
     * @param \Closure $callable
     * @return array|null
     */
    protected static function injectCallableToFindArguments(
        $arguments = null
        , \Closure $callable
    ) {
        $c = $callable();

        if (!is_string($c)) {
            return $arguments;
        }

        if ($arguments == null) {
            // üres argumentum lista
            $arguments = [
                'conditions' => $c
            ];
        } else if (is_numeric($arguments)) {
            $model = new static();
            $key   = $model->getPrimaryKeyAttribute();
            // elsődleges kulcs
            $arguments = static::injectConditionToConditions(
                "{$key} = '{$arguments}'"
                , $c
            );
        } else if (is_string($arguments)) {
            // string szűrőfeltétel
            $arguments = static::injectConditionToConditions(
                $arguments
                , $c
            );
        } else if (is_array($arguments) && array_key_exists('conditions', $arguments)) {
            // van 'conditions' az argumentum listában
            $arguments['conditions'] = static::injectConditionToConditions(
                $arguments['conditions']
                , $c
            );
        } else if (is_array($arguments) && array_key_exists(0, $arguments)) {
            // egyszerű string az első paraméter az argumentum listában (van más paraméter is)
            $arguments[0] = static::injectConditionToConditions(
                $arguments[0]
                , $c
            );
        } else if (is_array($arguments) && !array_key_exists('conditions', $arguments)) {
            // array, de nincs conditions viszont van más (order, group, limit)
            $arguments['conditions'] = static::injectConditionToConditions(
                '1 = 1'
                , $c
            );
        }

        return $arguments;
    }

    /**
     * @param $conditions
     * @param $condition
     * @return string
     */
    protected static function injectConditionToConditions($conditions, $condition)
    {
        return sprintf(
            '(%s) AND (%s)'
            , $condition
            , $conditions
        );
    }
}