<?php

namespace App\Model\Traits;

use App\Model\User;
use Phalcon\Di;
use Phalcon\Exception;

/**
 * Trait RowLevelAcl
 * @package App\Model\Traits
 */
trait RowLevelAcl
{
    /**
     * @var string
     */
    protected static $rowLevelAclUserAttribute = 'userId';

    /**
     * @var bool
     */
    protected static $rowLevelAclEnabled = true;

    /**
     * @param null $arguments
     * @return mixed
     */
    public static function find($arguments = null)
    {
        if (self::isRowLevelAclEnabled($arguments)) {
            $arguments = static::injectCallableToFindArguments(
                $arguments
                , self::getRowLevelAclFindCallable()
            );
        }

        return parent::find($arguments);
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public static function findFirst($arguments = null)
    {
        if (self::isRowLevelAclEnabled($arguments)) {
            $arguments = static::injectCallableToFindArguments(
                $arguments
                , self::getRowLevelAclFindCallable()
            );
        }

        return parent::findFirst($arguments);
    }

    /**
     * @return \Closure
     */
    protected static function getRowLevelAclFindCallable()
    {
        $method = 'getRowLevelAclFindCallableCustom';

        if (method_exists(__CLASS__, $method)) {
            return self::$method();
        }

        return self::getRowLevelAclFindCallableDefault();
    }

    /**
     * @return \Closure
     */
    protected static function getRowLevelAclFindCallableDefault()
    {
        return function() {
            /**
             * @var User $user
             */
            $user = Di::getDefault()
                ->get('auth')
                ->getIdentity()
            ;

            return
                self::getRowLevelAclUserAttribute()
                . ' = ' . $user->userId
            ;
        };
    }

    /**
     * @param User $user
     * @return bool
     * @throws Exception
     */
    /*public function isRowLevelAclOwner(User $user)
    {
        if (!property_exists($this, self::getRowLevelAclUserAttribute())) {
            throw new Exception(sprintf(
                'Nem létezik ilyen attribútum a(z) "%s" modellben: %s'
                , get_class($this)
                , self::getRowLevelAclUserAttribute()
            ));
        }

        // megegyezik
        if ($this->{self::getRowLevelAclUserAttribute()} == $user->userId) {
            return true;
        }

        // nem egyezik meg
        return false;
    }*/

    /**
     * @return string
     */
    public static function getRowLevelAclUserAttribute(): string
    {
        return self::$rowLevelAclUserAttribute;
    }

    /**
     * @param string $rowLevelAclUserAttribute
     */
    public static function setRowLevelAclUserAttribute(string $rowLevelAclUserAttribute)
    {
        self::$rowLevelAclUserAttribute = $rowLevelAclUserAttribute;
    }

    /**
     * @param array|null $arguments find or findFirst arguments (optional)
     * @return bool
     */
    public static function isRowLevelAclEnabled($arguments = null)
    {
        if (is_array($arguments)
            && array_key_exists('rowLevelAclEnabled', $arguments)
        ) {
            return $arguments['rowLevelAclEnabled'];
        }

        return self::$rowLevelAclEnabled;
    }

    /**
     * @param bool $rowLevelAclEnabled
     */
    public static function setRowLevelAclEnabled(bool $rowLevelAclEnabled)
    {
        self::$rowLevelAclEnabled = $rowLevelAclEnabled;
    }
}