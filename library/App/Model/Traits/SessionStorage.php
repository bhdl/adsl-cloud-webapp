<?php

namespace App\Model\Traits;

/**
 * Trait SessionStorage
 * @package App\Model\Traits
 */
trait SessionStorage
{
    /**
     * Tetszőleges változó beállítása a session scope-ba
     *
     * @param string $key
     * @param string $value
     * @return SessionStorage
     */
    protected function setSessionVar($key, $value)
    {
        $session = $this->getSession();

        $vars = $session->get($this->getSessionKey());

        if (!is_array($vars)) {
            $vars = [];
        }

        $vars[$key] = $value;

        $session->set($this->getSessionKey(), $vars);

        return $this;
    }

    /**
     * Session scope változó kiolvasása
     *
     * @param string $key
     * @return mixed|null
     */
    protected function getSessionVar($key)
    {
        $session = $this->getSession();

        $vars = $session->get($this->getSessionKey());

        if (!is_array($vars) || !array_key_exists($key, $vars)) {
            return null;
        }

        return $vars[$key];
    }

    /**
     * Egyedi session beállítások
     *
     * @return string
     */
    protected function getSessionKey()
    {
        return sprintf(
            'session_storage_%s_%s'
            , mb_strtolower($this->getModelName())
            , $this->getPrimaryKey()
        );
    }

    /**
     * Session Bag
     *
     * @return \Phalcon\Session\Bag
     */
    protected function getSession()
    {
        return $this->getDI()->get('session');
    }
}