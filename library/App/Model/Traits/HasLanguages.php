<?php

namespace App\Model\Traits;
use App\Model\Language;
use Phalcon\Exception;

/**
 * Trait FindArgumentsInjector
 * @package App\Model\Traits
 */
trait HasLanguages
{
    /**
     * @return string
     */
    protected function getLanguageRelationAlias()
    {
        return lcfirst($this->getModelName()) . 'Languages';
    }

    /**
     * @return string
     */
    protected function getLanguageRelationGetter()
    {
        return 'get' . ucfirst($this->getLanguageRelationAlias());
    }

    /**
     * @param array|string|null $arguments
     * @return \Phalcon\Mvc\Model\Resultset\Simple
     */
    public function getLanguages($arguments = null)
    {
        return $this->{$this->getLanguageRelationGetter()}($arguments);
    }

    /**
     * @return AbstractModel
     * @throws Exception
     */
    public function deleteLanguages()
    {
        $languages = $this->{$this->getLanguageRelationGetter()}();

        if ($languages) {
            $result = $languages->delete();
            if (!$result) {
                throw new Exception('A nyelvi mutációk modelljeinek törlése nem sikerült.');
            }
        }

        return $this;
    }

    /**
     * @param Form $form
     * @return AbstractModel
     */
    public function createLanguages($form)
    {
        $languages = Language::findInterfaceLanguages();

        /**
         * @var Language $language
         */
        foreach ($languages as $language) {

            $modelLanguageClass = '\\App\\Model\\' . $this->getModelName() . 'Language';

            /**
             * @var AbstractModel
             */
            $modelLanguage = new $modelLanguageClass();
            $modelLanguage->{$this->getPrimaryKeyAttribute()} = $this->{$this->getPrimaryKeyAttribute()};
            $modelLanguage->languageId                    = $language->languageId;

            $metaData = $modelLanguage->getModelsMetaData();
            $attributes = $metaData->getAttributes($modelLanguage);
            foreach ($attributes as $attribute) {
                try {
                    $element = $form->get($attribute . '_' . $language->languageId);
                    if ($element) {
                        $modelLanguage->$attribute = $element->getValue();
                    }
                } catch (Exception $e) {
                }
            }

            $modelLanguage->create();
        }

        return $this;
    }
}