<?php

namespace App\Model\Traits;

use App\Model\AbstractModel;
use Phalcon\Exception;
use Phalcon\Mvc\Model\Behavior\SoftDelete as PhalconSoftDelete;


/**
 * Trait SoftDelete
 * @package App\Model\Traits
 */
trait SoftDelete
{

    private $deletedConnectionModels = [];

    /**
     *
     */
    public function initializeSoftDelete()
    {
        $this->addBehavior(
            new PhalconSoftDelete(
                [
                    'field' => 'deleted',
                    'value' => self::YES
                ]
            )
        );

        $this->addBehavior(
            new PhalconSoftDelete(
                [
                    'field' => 'deletedAt',
                    'value' => new \Phalcon\Db\RawValue("now()")
                ]
            )
        );
    }

    /**
     *
     */
    public function beforeDelete()
    {
        $hasMany = $this->di->getDefault()->getModelsManager()->getHasMany($this);
        $hasOne = $this->di->getDefault()->getModelsManager()->getHasOne($this);

        if(is_array($hasMany)) {
            foreach ($hasMany as $related) {
                $className = $related->getReferencedModel();

                if (in_array($className, $this->deletedConnectionModels)) {
                    continue;
                }

                $className::find([
                    $this->getPrimaryKeyAttribute() . " = " . $this->getPrimaryKey()
                ])->delete();

                $this->deletedConnectionModels[] = $className;
            }
        }

        if(is_array($hasOne)) {
            foreach ($hasOne as $related) {
                $className = $related->getReferencedModel();
                $className::find([
                    $this->getPrimaryKeyAttribute() . " = " . $this->getPrimaryKey()
                ])->delete();
            }
        }
    }

    /**
     *
     */
    public function afterDelete()
    {
        $this->deletedConnectionModels = [];
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public static function find($arguments = null)
    {
        $arguments = static::injectCallableToFindArguments(
            $arguments
          , function() {
                return "deleted = '".AbstractModel::NO."'";
            }
        );



        return parent::find($arguments);
    }

    /**
     * @param null $arguments
     * @return mixed
     */
    public static function findFirst($arguments = null)
    {
        $arguments = static::injectCallableToFindArguments(
            $arguments
            , function() {
                return "deleted = '".AbstractModel::NO."'";
            }
        );

        return parent::findFirst($arguments);
    }
}