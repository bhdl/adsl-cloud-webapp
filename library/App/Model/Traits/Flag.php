<?php

namespace App\Model\Traits;

use App\Support\HashMap;
use Phalcon\Exception;

/**
 * Trait Flag
 * @package App\Model\Traits
 */
trait Flag
{
    /**
     * @var string
     */
    public $flags;

    /**
     * Flag definíciók (flag => megnevezés). Használd a FLAG_XYZ konstansokat.
     * @var array
     */
    protected static $flagValues;

    /**
     * Ezek a flag csoportok. Egy-egy csoporton belül nem lehet több flaget tenni a számlára.
     * Használd a FLAG_XYZ konstansokat.
     *
     * @var array
     */
    protected static $flagGroups;

    /**
     * Számla keresése címke alapján
     *
     * @param array|string $include
     * @param array|string $exclude
     * @param array|string|null $arguments
     * @return Flag
     */
    public static function findByFlags($include = null, $exclude = null, $arguments = null)
    {
        if (is_string($include)) {
            $include = [$include];
        }

        if (HashMap::isFilled($include)) {
            $arguments = static::prepareFindArguments(
                $arguments
                , self::getFlagFindConditionsCallable($include, true)
            );
        }

        if (is_string($exclude)) {
            $exclude = [$exclude];
        }

        if (HashMap::isFilled($exclude)) {
            $arguments = static::prepareFindArguments(
                $arguments
                , self::getFlagFindConditionsCallable($exclude, false)
            );
        }

        return static::find($arguments);
    }

    /**
     * @return array
     */
    public static function getFlagValues()
    {
        return static::$flagValues;
    }

    /**
     * @param string $flag
     * @return string
     * @throws Exception
     */
    public static function getFlagValue(string $flag) : string
    {
        if (!array_key_exists($flag, static::$flagValues)) {
            throw new Exception('Nincs ilyen számla címke az értékek között: ' . $flag);
        }

        return static::$flagValues[$flag];
    }

    /**
     * @return array|null
     */
    public function getFlagsAsArray()
    {
        if ($this->flags == null) {
            return [];
        }

        return explode(',', $this->flags);
    }

    /**
     * @param string $flag
     * @return bool
     */
    public function hasFlag($flag)
    {
        return in_array($flag, $this->getFlagsAsArray());
    }

    /**
     * @param array $flags
     * @param bool  $strict
     * @return bool
     */
    public function hasFlags($flags, $strict = false)
    {
        $count = 0;

        foreach ($flags as $flag) {
            if ($this->hasFlag($flag)) {
                $count++;
            }
        }

        return count($flags) == $count ? true : (!$strict && $count > 0 ? true : false);
    }

    /**
     * @param string $flag
     * @return Flag
     */
    public function addFlag($flag)
    {
        $flags = $this->getFlagsAsArray();

        foreach (static::$flagGroups ?: [] as $groupFlags) {
            if (in_array($flag, $groupFlags)) {
                // ez az a csoport
                if (count($flags) > 0) {
                    $flags = array_diff($flags, $groupFlags);
                }
            }
        }

        $flags[] = $flag;

        $this->flags = implode(',', array_unique($flags));

        return $this;
    }

    /**
     * @param array $flags
     * @return Flag
     */
    public function addFlags($flags = [])
    {
        foreach ($flags as $flag) {
            $this->addFlag($flag);
        }

        return $this;
    }

    /**
     * @param string $flag
     * @return Flag
     */
    public function clearFlag($flag)
    {
        $flags = $this->getFlagsAsArray();

        if (!in_array($flag, $flags)) {
            return $this;
        }

        $flags = array_flip($flags);

        unset($flags[$flag]);

        $this->flags = implode(',', array_unique(array_flip($flags))) ?: null;

        return $this;
    }

    /**
     * @param array $flags
     * @return Flag
     */
    public function clearFlags($flags = [])
    {
        foreach ($flags as $flag) {
            $this->clearFlag($flag);
        }

        return $this;
    }

    /**
     * @param array   $flags
     * @param boolean $include
     * @return callable
     */
    public static function getFlagFindConditionsCallable($flags = [], $include = false)
    {
        $callable = function() use ($flags, $include) {
            $sql = [];
            foreach ($flags as $flag) {
                $sql[] = 'FIND_IN_SET(\'' . $flag . '\', flags) ' . ($include ? '>' : '=') . ' 0';
            }
            return implode(' AND ', $sql);
        };

        return $callable;
    }
}