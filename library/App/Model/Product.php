<?php

namespace App\Model;

use App\Model\Traits\HasLanguages;
use App\Traits\InterfaceLanguage;
use Phalcon\Exception;
use Phalcon\Mvc\Model\Relation;
use App\Model\Traits\SoftDelete;

class Product extends AbstractLogModel
{
    use SoftDelete;

    /**
     * @var integer
     */
    public $productId;

    /**
     * @var integer
     */
    public $partnerId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var datetime
     */
    public $createdAt;

    /**
     * @var datetime
     */
    public $updatedAt;

    /**
     * @var datetime
     */
    public $deletedAt;

    /**
     * @var string
     */
    public $deleted = self::NO;

    /**
     * @var array
     */
    public static $deletedTypes = [
        self::YES  => 'igen'
        , self::NO => 'nem'
    ];

    /**
     * @var string
     */
    public $description;

    public function initialize()
    {
        parent::initialize();

        $this->initializeSoftDelete();

        $this->skipAttributes([
            "createdAt"
            , "updatedAt"
        ]);

        $this->belongsTo('partnerId', __NAMESPACE__ . '\Partner', 'partnerId', array(
            'alias' => 'partners',
            'reusable' => true
        ));
    }

    /**
     * @return array
     */
    public static function getDeletedTypes()
    {
        return self::$deletedTypes;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}