<?php

namespace App\Model;

/**
 * Class AclMatrix
 * @package App\Model
 */
class AclMatrix extends AbstractLogModel
{
    /**
     * @var string
     */
    public $roleId;

    /**
     * @var string
     */
    public $resourceId;

    /**
     * @var string
     */
    public $actionId;

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('roleId', __NAMESPACE__ . '\AclRole', 'roleId', array(
            'alias' => 'role'
            , 'reusable' => true
        ));

        $this->belongsTo('actionId', __NAMESPACE__ . '\AclAction', 'actionId', array(
            'alias' => 'action'
            , 'reusable' => true
        ));

        $this->belongsTo('resourceId', __NAMESPACE__ . '\AclResource', 'resourceId', array(
            'alias' => 'resource'
            , 'reusable' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'N/A';
    }
}