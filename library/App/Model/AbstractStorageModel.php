<?php

namespace App\Model;
use Phalcon\Exception;

/**
 * Class AbstractStorageModel
 * @package App\Model
 */
abstract class AbstractStorageModel
    extends AbstractLogModel
{
    /**
     * Ez lesz a fájl neve, amivel elmenti a rekordot. Gyakorlatban pl. az elsődleges kulcsa a modellnek
     *
     * @return int|string
     */
    abstract public function getStorageId();

    /**
     * Az elérési út, amin belül a fájlokat elmentjük
     *
     * @return string
     */
    abstract public function getStoragePath();

    /**
     * A fájl neve, amit letöltéskor használ
     *
     * @return string
     */
    abstract public function getFileName();

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->getStorage() . DIRECTORY_SEPARATOR . $this->getStorageId();
    }

    /**
     * @param string $separator
     * @return string
     */
    protected function getStorage($separator = DIRECTORY_SEPARATOR)
    {
        $storage = implode($separator, [
            rtrim($this->getStoragePath(), '\\/')
            , floor($this->getStorageId() / 1000) // minden ezredik új mappába kerül
        ]);

        return $storage;
    }

    /**
     * @param array $options
     * @throws Exception
     */
    public function download($options = [])
    {
        $timeLimit = isset($options['timeLimit']) ? $options['timeLimit'] : 300;
        $bufferSize = isset($options['bufferSize']) ? $options['bufferSize'] : 2048;

        // lassu halozat es hosszu file eseteben elerhetjuk a default 30 mp-et
        // emiatt kikapcsoljuk
        set_time_limit($timeLimit);

        if (($fd = fopen($this->getPath(), 'r')) === false) {
            // nem sikerult megnyitni, mert pl. nincs ott a dokumentum ...
            throw new Exception('Sajnáljuk, de ez az állomány nem található a szerveren.');
        };

        $this->sendHeaders([
            // mentes ablakot doja fel, azaz letolthetove valik
            'forceFileDownload' => true,
            // ha automatikusan meg akarjuk nyittatni alkalmazassal, akkor false kell legyen
        ]);

        while (!feof($fd)) {
            $outputBuffer = fread($fd, $bufferSize);
            echo $outputBuffer;
        }

        fclose($fd);

        // ez itt veget er, nehogy a framework meg valami mast kikuldjon a vegen..
        exit;
    }

    /**
     * A mimeType es az opciok felhasznalasaval kikuldi a bongeszonek szukseges headereket.
     *
     * @param array $options Ket boolean opcio van: forceFileDownload es forceFileOpen.
     *                        A ketto egyszerre nem lehet true, mert ertelmetlen.
     *                        A bongeszo ebben az esetben eldonti, hogy mi legyen.
     *
     */
    protected function sendHeaders($options = [])
    {
        $mimeType = isset($options['mimeType']) ? $options['mimeType'] : 'application/octet-stream';

        // Opciok vizsgalata
        $forceFileDownloadKeyword = '';
        $forceFileDownload = false; // nem menti automatikusan file-ba a dokumentumot

        if (isset($options['forceFileDownload']) && $options['forceFileDownload'] === true) {
            $forceFileDownload = true;
            $forceFileDownloadKeyword = ' attachment;';
        }

        $forceFileOpen = true; // megnyitas programban
        // defaultban a file megnyitasat kerjuk, de a bongesz donti el, hogy mit fog tenni...
        if (isset($options['forceFileOpen']) && $options['forceFileOpen'] === false) {
            $forceFileOpen = false;
        }

        header('Content-type: ' . $mimeType);
        header('Content-Disposition:' . $forceFileDownloadKeyword . ' filename="' . $this->getFileName() . '"');
        header("Content-length: " . filesize($this->getPath()));

        if ($forceFileOpen && !$forceFileDownload) {
            // file megnyitas bongeszo pluginben vagy tarsitott programban
            header("Cache-control: private");
        }
    }
}