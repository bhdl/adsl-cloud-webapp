<?php

namespace App\Model;

use App\Model\Traits\RowLevelAcl;

/**
 * Class NavigationHistory
 * @package App\Model
 */
class NavigationHistory
    extends AbstractModel
{
    use RowLevelAcl;

    /**
     * @var int
     */
    public $navigationHistoryId;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $createdAt;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    const TYPE_USER = 'user';

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->addBehavior(new \App\Model\Behavior\RowLevelAcl);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->navigationHistoryId;
    }
}