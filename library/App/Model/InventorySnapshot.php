<?php

namespace App\Model;

class InventorySnapshot extends AbstractLogModel
{
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}