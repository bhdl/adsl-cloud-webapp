<?php

namespace App\Model;

use App\Model\Behavior\FilterNull;
use App\Model\Behavior\FilterNumeric;
use App\Model\Behavior\FilterDate;
use App\Model\Traits\FindArgumentsInjector;
use Phalcon\Exception;
use Phalcon\Mvc\Model;

/**
 * Class AbstractModel
 * @package App\Model
 */
abstract class AbstractModel
    extends Model
{
    use FindArgumentsInjector;

    /**
     * @const string
     */
    const YES = 'yes';

    /**
     * @const string
     */
    const NO = 'no';

    /**
     * @var array
     */
    protected static $booleans = [
        self::YES  => 'igen'
        , self::NO => 'nem'
    ];

    /**
     * @return string
     */
    abstract public function getName();

    /**
     * Model üzenetek nyelvesítése
     *
     * @param null $filter
     * @return \Phalcon\Mvc\Model\MessageInterface[]
     */
    public function getMessages($filter = null)
    {
        $messages = parent::getMessages($filter);

        $translate = $this->getDI()->get('translate');

        foreach ($messages as $message) {

            switch ($message->getType()) {

                case 'InvalidCreateAttempt':
                    $key = 'Sajnáljuk, de a létrehozás nem sikerült, mert már létezik ilyen rekord.'; break;
                case 'InvalidUpdateAttempt':
                    $key = 'Sajnáljuk, de a módosítás nem sikerült.'; break;
                case 'PresenceOf':
                    $key = 'A(z) "%field%" mező kitöltése kötelező, nem lehet üres!'; break;
                case 'ConstraintViolation':
                    $key = $message->getMessage() ?: 'A rekord nem törölhető, mert már tartozik hozzá egyéb adat.'; break;
                case 'Numeric':
                    $key = 'A mezőnek számot kell tartalmaznia!'; break;
                default:
                    $key = sprintf('Ismeretlen hiba (%field%, %model%, %type%)'); break;
            }

            $tMessage = $translate->_(
                $key
                , [
                    'field'   => $message->getField()
                    , 'model' => $message->getModel()
                    , 'type'  => $message->getType()
                ]
            );

            $message->setMessage($tMessage);
        }

        return $messages;
    }

    /**
     * @param string $separator
     * @return string
     */
    public function getMessagesAsString($separator = ' ')
    {
        $string = [];

        foreach ($this->getMessages() as $message) {
            $string[] = $message->__toString();
        }

        return implode($separator, $string);
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->{$this->getPrimaryKeyAttribute()};
    }

    /**
     * CRUD tábla elsödleges kulcsát adja vissza
     * @return mixed
     */
    public function getPrimaryKeyAttribute()
    {
        $metadata = $this->getModelsMetaData();

        $metadata = $metadata->getPrimaryKeyAttributes($this);

        $primaryKey = array_shift($metadata);

        return $primaryKey;
    }

    /**
     * Visszaadja a model nevét
     * @return string
     */
    public function getModelName(): string
    {
        return array_pop(explode('\\', get_class($this)));
    }

    /**
     * Model lemásolása
     */
    public function clone()
    {
        $className = get_called_class();

        // Model clone
        $model = new $className(
            $this->toArray()
        );

        $metaData = $this->getModelsMetaData();

        $primaryKeys = $metaData->getPrimaryKeyAttributes($this);
        if (is_array($primaryKeys)) {
            // kompozit kulcsok esetén több mező is lehet
            foreach ($primaryKeys as $primaryKey) {
                // Elsődleges mező null értékre rakása
                $model->{$primaryKey} = null;
            }
        }

        return $model;
    }

    /**
     * initialize
     */
    public function initialize()
    {
        $this->addBehavior(new FilterNumeric());
        $this->addBehavior(new FilterNull());
        $this->addBehavior(new FilterDate());
    }

    /**
     * @return array
     */
    public static function getBooleans()
    {
        return self::$booleans;
    }

    /**
     * @param string $key
     * @return string
     * @throws Exception
     */
    public static function getBooleanValue($key)
    {
        if (!array_key_exists($key, static::$booleans)) {
            throw new Exception($this->translate->t('Nincs ilyen kulcs a boolean értékek között:') . $key);
        }

        return static::$booleans[$key];
    }

    /**
     * @return bool
     */
    public function isSaved()
    {
        return $this->{$this->getPrimaryKeyAttribute()} !== null;
    }

    /**
     * @param boolean $useName
     * @return array
     */
    public function toArray($useName = true)
    {
        $array = parent::toArray();

        if (!$useName) {
            return $array;
        }

        if (!array_key_exists('name', $array)) {
            $array['name'] = $this->getName();
        }

        return $array;
    }

    /**
     * @param $attributes
     * @return array
     */
    public function toArrayWithAttributes(array $attributes)
    {
        $array = parent::toArray();

        if(empty($attributes)) {
            return $array;
        }

        foreach ($array as $key => $column) {
            if(!in_array($key, $attributes)) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    /**
     * @param $attributes
     * @return array
     */
    public function toArrayWithoutAttributes(array $attributes)
    {
        $array = parent::toArray();

        if(empty($attributes)) {
            return $array;
        }

        foreach ($array as $key => $column) {
            if(in_array($key, $attributes)) {
                unset($array[$key]);
            }
        }

        return $array;
    }
}