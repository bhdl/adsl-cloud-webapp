<?php

namespace App\Model;

use App\Model\Traits\Flag;
use App\Model\Interfaces\Acl as AclInterface;
use App\Model\Traits\RowLevelAcl;
use Phalcon\Di;


/**
 * Class Referral
 * @package App\Model
 */
class Referral
    extends AbstractLogModel
    implements AclInterface
{
    use Flag, RowLevelAcl;

    /**
     * @var int
     */
    public $referralId;

    /**
     * @var int
     */
    public $praxisId;

    /**
     * @var int
     */
    public $doctorId;

    /**
     * @var int
     */
    public $patientId;

    /**
     * @var int
     */
    public $courierId;

    /**
     * @var int
     */
    public $addressId;

    /**
     * @var int
     */
    public $currencyId;

    /**
     * @var int
     */
    public $paymentMethodId;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $praxisSenderNumber;

    /**
     * @var int
     */
    public $dailyNumber;

    /**
     * @var float
     */
    public $net;

    /**
     * @var float
     */
    public $gross;

    /**
     * @var float
     */
    public $vat;

    /**
     * @var string
     */
    public $arrivalDateAt;

    /**
     * @var string
     */
    public $clinical;

    /**
     * @var string
     */
    public $therapy;

    /**
     * @var string
     */
    public $medicalHistory;

    /**
     * @var string
     */
    public $comment;

    /**
     * @var string
     */
    public $note;

    /**
     * @var string
     */
    public $courierPickupLocation;

    /**
     * @var string
     */
    public $courierPickupInterval;

    /**
     * @var string
     */
    public $courierComment;

    /**
     * @var string
     */
    public $vaccinationName;

    /**
     * @var string
     */
    public $vaccinationNumber;

    /**
     * @var string
     */
    public $vaccinationLastDateAt;

    /**
     * @var string
     */
    public $vaccinationChipReadingDateAt;

    /**
     * @var string
     */
    public $vaccinationReasonCountry;

    /**
     * @var string
     */
    public $examintaionReason;

    /**
     * @var string
     */
    public $lockup;

    /**
     * @var string
     */
    public $urgent;

    /**
     * @var string
     */
    public $fromDoctor;

    /**
     * @var string
     */
    public $referralType;

    /**
     * @const string
     */
    const REFERRAL_TYPE_UNIVERSAL = 'universal';

    /**
     * @const string
     */
    const REFERRAL_TYPE_EXPORT = 'export';

    /**
     * @const string
     */
    const REFERRAL_TYPE_RABIES_REQUESTING = 'rabiesRequesting';

    /**
     * @var array
     */
    protected static $referralTypes = [
        self::REFERRAL_TYPE_UNIVERSAL  => 'Általános'
        , self::REFERRAL_TYPE_RABIES_REQUESTING => 'Veszettségkérő'
        , self::REFERRAL_TYPE_EXPORT=> 'Export'
    ];

    /**
     * @const string
     */
    const EXAMINATION_REASON_UNITED_KINGDOM = 'unitedKingdom';

    /**
     * @const string
     */
    const EXAMINATION_REASON_EU = 'eu';

    /**
     * @const string
     */
    const EXAMINATION_REASON_IRELAND = 'ireland';

    /**
     * @const string
     */
    const EXAMINATION_REASON_MALTA = 'malta';

    /**
     * @const string
     */
    const EXAMINATION_REASON_SWEDEN = 'sweden';

    /**
     * @const string
     */
    const EXAMINATION_REASON_OTHER_COUNTRY = 'otherCountry';

    /**
     * @const string
     */
    const EXAMINATION_REASON_INFORMATIVE_EXAMINATION = 'informativeExamination';

    /**
     * @var array
     */
    protected static $examintaionReasons = [
        self::EXAMINATION_REASON_UNITED_KINGDOM  => 'Egyesült Királyság'
        , self::EXAMINATION_REASON_EU => 'EU'
        , self::EXAMINATION_REASON_IRELAND => 'Írország'
        , self::EXAMINATION_REASON_MALTA => 'Málta'
        , self::EXAMINATION_REASON_SWEDEN => 'Svédország'
        , self::EXAMINATION_REASON_OTHER_COUNTRY => 'Egyéb ország'
        , self::EXAMINATION_REASON_INFORMATIVE_EXAMINATION => 'Tájékoztató vizsgálat'
    ];

    /**
     * @var string
     */
    public $discount;

    /**
     * @var string
     */
    public $flag;

    /**
     * @var string
     */
    const FLAG_SENT = 'sent';

    /**
     * @var string
     */
    const FLAG_ARRIVED = 'arrived';

    /**
     * @var string
     */
    const FLAG_AMENDED = 'amended';

    /**
     * @var string
     */
    const FLAG_PARTIAL_RESULT = 'partialResult';

    /**
     * @var string
     */
    const FLAG_FINAL_RESULT = 'finalResult';

    /**
     * @var string
     */
    const FLAG_CLOSED = 'closed';

    /**
     * @var string
     */
    const FLAG_RE_OPENED = 'reOpened';

    /**
     * @var string
     */
    const FLAG_INVOICED = 'invoiced';

    /**
     * @var string
     */
    const FLAG_NO_PAYMENT = 'noPayment';

    /**
     * @var string
     */
    const FLAG_PAID_IN_FULL = 'paidInFull';

    /**
     * @var array
     */
    protected static $flagValues = [
        // aktuális állapot
        self::FLAG_SENT             => 'Beküldött'
        , self::FLAG_ARRIVED        => 'Érkeztetve'
        , self::FLAG_AMENDED        => 'Módosítva'
        , self::FLAG_RE_OPENED      => 'Újra nyitott'
        , self::FLAG_CLOSED         => 'Lezárt'
        // pénzügyi állapot
        , self::FLAG_NO_PAYMENT     => 'Nincs fizetve'
        , self::FLAG_PAID_IN_FULL   => 'Fizetve'
        // eredmény
        , self::FLAG_PARTIAL_RESULT => 'Részeredmény'
        , self::FLAG_FINAL_RESULT   => 'Végleges eredmény'
        // egyéb
        , self::FLAG_INVOICED       => 'Számlázva'

    ];

    /**
     * @var array
     */
    protected static $flagGroups = [
        [
            self::FLAG_SENT
            , self::FLAG_ARRIVED
            , self::FLAG_AMENDED
            , self::FLAG_RE_OPENED
            , self::FLAG_CLOSED
        ]
        , [
            self::FLAG_NO_PAYMENT
            , self::FLAG_PAID_IN_FULL
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        // rendelő
        $this->belongsTo('praxisId', Praxis::class, 'praxisId', array(
            'alias' => 'praxis'
        , 'reusable'  => true
        ));

        // orvos
        $this->belongsTo('doctorId', Doctor::class, 'doctorId', array(
            'alias' => 'doctor'
        , 'reusable'  => true
        ));

        // páciens
        $this->belongsTo('patientId', Patient::class, 'patientId', array(
            'alias' => 'patient'
        , 'reusable'  => true
        ));

        // futár
        $this->belongsTo('courierId', Courier::class, 'courierId', array(
            'alias' => 'courier'
        , 'reusable'  => true
        ));

        // kiválasztott owner cím
        $this->belongsTo('addressId', Address::class, 'addressId', array(
            'alias' => 'address'
        , 'reusable'  => true
        ));

        // kiválasztott owner cím
        $this->belongsTo('currencyId', Currency::class, 'currencyId', array(
            'alias' => 'currency'
        , 'reusable'  => true
        ));

        // fizetési mód
        $this->belongsTo('paymentMethodId', PaymentMethod::class, 'paymentMethodId', array(
            'alias' => 'paymentMethod'
        , 'reusable'  => true
        ));

        // dokumentumok
        $this->hasMany('referralId', Document::class, 'referralId', [
            'alias'      => 'documents'
            , 'reusable' => true
        ]);

        // beutaló kérések és minták
        $this->hasMany('referralId', ReferralHasReferralSample::class, 'referralId', [
            'alias'      => 'referralHasReferralSamples'
            , 'reusable' => true
        ]);

        // csőforgalmi minősítések
        $this->hasMany('referralId', TubeReceived::class, 'referralId', [
            'alias'      => 'tubesReceived'
            , 'reusable' => true
        ]);

        // minták és kérések
        $this->hasMany('referralId', ReferralHasReferralSample::class, 'referralId', [
            'alias'      => 'referralHasReferralSamples'
            , 'reusable' => true
        ]);

    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getPatient() ? $this->getPatient()->getName() : 'N/A';
    }

    /**
     * @return array
     */
    public static function getReferralTypes() : array
    {
        return self::$referralTypes;
    }

    /**
     * @param string $referralType
     * @return string
     * @throws \Exception
     */
    public static function getReferralTypeValue(string $referralType) : string
    {
        if (!array_key_exists($referralType, static::$referralTypes)) {
            throw new \Exception('Nincs ilyen típus az értékek között: ' . $referralType);
        }

        return static::$referralTypes[$referralType];
    }

    /**
     * @return bool
     */
    public function isReferralTypeUniversal()
    {
        return strcmp($this->referralType, self::REFERRAL_TYPE_UNIVERSAL) === 0;
    }

    /**
     * @return bool
     */
    public function isReferralTypeExport()
    {
        return strcmp($this->referralType, self::REFERRAL_TYPE_EXPORT) === 0;
    }

    /**
     * @return bool
     */
    public function isReferralTypeRabiesrequesting()
    {
        return strcmp($this->referralType, self::REFERRAL_TYPE_RABIES_REQUESTING) === 0;
    }

    /**
     * @return bool
     */
    public function isLockup()
    {
        return strcmp($this->lockup, self::YES) === 0;
    }

    /**
     * @return bool
     */
    public function isUrgent()
    {
        return strcmp($this->urgent, self::YES) === 0;
    }

    /**
     * @return bool
     */
    public function isFromDoctor()
    {
        return strcmp($this->fromDoctor, self::YES) === 0;
    }

    /**
     * @return array
     */
    public static function getExaminationReasons() : array
    {
        return self::$examintaionReasons;
    }

    /**
     * @param string $examinationReason
     * @return string
     * @throws \Exception
     */
    public static function getExaminationReasonValue(string $examinationReason) : string
    {
        if (!array_key_exists($examinationReason, static::$examintaionReasons)) {
            throw new \Exception('Nincs ilyen vizsgálati ok az értékek között: ' . $examinationReason);
        }

        return static::$examintaionReasons[$examinationReason];
    }

    /**
     * @return bool
     */
    public function isExaminationReasonUnitedKingdom()
    {
        return strcmp($this->examintaionReason, self::EXAMINATION_REASON_UNITED_KINGDOM) === 0;
    }

    /**
     * @return bool
     */
    public function isExaminationReasonEU()
    {
        return strcmp($this->examintaionReason, self::EXAMINATION_REASON_EU) === 0;
    }

    /**
     * @return bool
     */
    public function isExaminationReasonIreland()
    {
        return strcmp($this->examintaionReason, self::EXAMINATION_REASON_IRELAND) === 0;
    }

    /**
     * @return bool
     */
    public function isExaminationReasonMalta()
    {
        return strcmp($this->examintaionReason, self::EXAMINATION_REASON_MALTA) === 0;
    }

    /**
     * @return bool
     */
    public function isExaminationReasonSweden()
    {
        return strcmp($this->examintaionReason, self::EXAMINATION_REASON_SWEDEN) === 0;
    }

    /**
     * @return bool
     */
    public function isExaminationReasonOtherCountry()
    {
        return strcmp($this->examintaionReason, self::EXAMINATION_REASON_OTHER_COUNTRY) === 0;
    }

    /**
     * @return bool
     */
    public function isExaminationReasonInformativeExamination()
    {
        return strcmp($this->examintaionReason, self::EXAMINATION_REASON_INFORMATIVE_EXAMINATION) === 0;
    }


    /**
     * @return array
     * @throws \Exception
     */
    public static function getGroupedFlagValues()
    {
        return [
            'Típus szerint' => [
                self::FLAG_SENT            => self::getFlagValue(self::FLAG_SENT)
                , self::FLAG_ARRIVED       => self::getFlagValue(self::FLAG_ARRIVED)
                , self::FLAG_AMENDED       => self::getFlagValue(self::FLAG_AMENDED)
                , self::FLAG_RE_OPENED     => self::getFlagValue(self::FLAG_RE_OPENED)
                , self::FLAG_CLOSED        => self::getFlagValue(self::FLAG_CLOSED)
            ]
            , 'Befizetés szerint' => [
                self::FLAG_NO_PAYMENT      => self::getFlagValue(self::FLAG_NO_PAYMENT)
                , self::FLAG_PAID_IN_FULL  => self::getFlagValue(self::FLAG_PAID_IN_FULL)
            ]
            , 'Eredmény' => [
                self::FLAG_PARTIAL_RESULT  => self::getFlagValue(self::FLAG_PARTIAL_RESULT)
                , self::FLAG_FINAL_RESULT  => self::getFlagValue(self::FLAG_FINAL_RESULT)

            ]
            , 'Egyéb' => [
                self::FLAG_INVOICED        => self::getFlagValue(self::FLAG_INVOICED)
            ]
        ];
    }

    /**
     * @param string $resourceId
     * @param string $actionId
     * @param null $user
     * @return boolean
     */
    public function isAllowedByIdentity($resourceId, $actionId, $user = null)
    {
        // resourceId itt nem érdekes, csak az action
        switch ($actionId) {
            case AclAction::ACTION_DELETE:
                return $this->hasFlags([self::FLAG_SENT], false);
                break;
            case AclAction::ACTION_MODIFY:
                return $this->hasFlags([self::FLAG_SENT, self::FLAG_RE_OPENED], false);
                break;

            default:
                return true;
                break;
        }

        return false;
    }

    /**
     * Tartoznak-e a beutalóhoz dokumentumok?
     *
     * @return bool
     */
    public function hasDocuments()
    {
        return $this->documents->count() > 0;
    }

    /**
     * Az orvos csak azokat a beutalókat látja, amik a rendelőihez és ezen belül is az éppen aktív rendelőhöz
     * tartoznak
     *
     * @return \Closure
     */
    protected static function getRowLevelAclFindCallableCustom()
    {
        return function(){

            $user = Di::getDefault()
                ->get('auth')
                ->getIdentity()
            ;

            if (!$user->isDoctor()) {
                return;
            }

            $doctor = $user->getDoctor();

            $praxis = $doctor->getInterfacePraxis();

            return  'praxisId IN('
                    . 'SELECT phd.praxisId FROM App\Model\PraxisHasDoctor phd WHERE phd.doctorId = \'' . $user->getDoctor()->doctorId . '\''
                . ')'
                ;
        };
    }

    /**
     * @param int|string|null $identifierType
     * @throws \Exception
     */
    public function getDailyNumber($dryRun = true, $identifierType = null)
    {
        try {
            if (empty($this->dailyNumber)) {
                // generátor lekészítése
                $generator = new \App\Generator\Identifier\Referral($this);
                if (is_numeric($identifierType)) {
                    // pattern id van megadva
                    $generator->setIdentifierPattern(
                        \App\Model\IdentifierPattern::findFirst($identifierType) ?: null
                    );
                }

                $this->arrivalDateAt = date('Y-m-d');
                $this->dailyNumber = $generator->generate(['dryRun' => $dryRun]);

                if (!$dryRun) {
                    $this->update();
                }
            }

        } catch (\Exception $e) {
        }
    }

    public function getExaminationsHasRequests()
    {
        return ExaminationHasRequest::find([
            'requestId IN (
                SELECT DISTINCT rhrs.requestId FROM App\Model\ReferralHasReferralSample rhrs WHERE rhrs.referralId = ' . $this->referralId . ')'
            , 'columns' => 'DISTINCT examinationId'
        ]);
    }

    public function getPraxisSenderNumber()
    {
        return $this->praxisSenderNumber ?: ' - ';
    }

    /**
     * deleteAllTubesReceive - az entitáshoz tartozó összes csőforgalmi minősítés törlése
     */
    public function deleteAllTubesReceive()
    {
        /** @var TubeReceived $tubeReceived */
        foreach ($this->getTubesReceived() as $tubeReceived) {
            $tubeReceived->delete();
        }
    }

    /**
     * getSelectedItems - szűkített kérés/panel lista
     * @return array
     */
    public function getSelectedItems()
    {
        $items = [];
        foreach ($this->getReferralHasReferralSamples() as $referralHasReferralSample) {
            if ($referralHasReferralSample->panelId) {
                $items[$referralHasReferralSample->panelId] = $referralHasReferralSample->getPanel();
            } else {
                $items[$referralHasReferralSample->requestId] = $referralHasReferralSample->getRequest();
            }
        }
        return $items;
    }

    /**
     * getSelectedReferralSamples - szűkített minta lista
     * @return array
     */
    public function getSelectedReferralSamples()
    {
        $selectedReferralSamples = [];
        foreach ($this->getReferralHasReferralSamples() as $referralHasReferralSample) {
            $selectedReferralSamples[$referralHasReferralSample->referralSampleId] = $referralHasReferralSample;
        }
        return $selectedReferralSamples;
    }
}
