<?php

namespace App\Model;

class Group
    extends AbstractLogModel
{
    /**
     * @var integer
     */
    public $groupId;

    /**
     * @var string
     */
    public $groupName;

    /**
     * @var string
     */
    public $guest;

    /**
     * @var string
     */
    public $active;

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->hasMany('groupId', __NAMESPACE__ . '\User', 'groupId', array(
            'alias' => 'users'
            , 'foreignKey' => array(
                'message'  => 'A rekord nem törölhető, mert már tartozik hozzá egyéb adat.'
            )
        ));

        $this->hasMany('groupId', __NAMESPACE__ . '\GroupHasAclRole', 'groupId', array(
            'alias' => 'groupHasAclRoles'
        ));
    }

    /**
     * Vendég csoport megkeresése
     *
     * @return \Phalcon\Mvc\Model
     * @throws Exception
     */
    public static function findGuest()
    {
        $group = Group::findFirst('guest = \'yes\'');
        if (!is_object($group)) {
            throw new Exception('Nincs létrehozva vendég felhasználói csoport.');
        }

        return $group;
    }

    /**
     * Vendég csoport-e?
     * @return bool
     */
    public function isGuest()
    {
        return strcmp(self::YES, $this->guest) === 0;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return strcmp(self::YES, $this->active) === 0;
    }

    /**
     * @param $roleId
     * @return GroupHasAclRole
     */
    public function addGroupHasAclRole($roleId)
    {
        $model = new GroupHasAclRole();

        $model->roleId  = $roleId;
        $model->groupId = $this->groupId;

        $model->save();

        return $model;
    }

    /**
     * @param $roles
     * @return $this
     */
    public function addGroupHasAclRoles($roles)
    {
        foreach ($roles as $role) {
            $this->addGroupHasAclRole($role);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearGroupHasAclRole()
    {
        foreach($this->getGroupHasAclRoles() as $model){
            $model->delete();
        }

        return $this;
    }

    /**
     * @param AclRole|int $role
     * @return bool
     */
    public function hasAclRole($role)
    {
        $roleId = $role;
        if ($role instanceof AclRole) {
            $roleId = $role->roleId;
        }

        return GroupHasAclRole::findFirst(
            'groupId = ' . $this->groupId
            . ' AND roleId = \'' . $roleId . '\''
        ) !== false;
    }

    /**
     * @return bool
     */
    public function isAclRoleDoctor()
    {
        return $this->hasAclRole(AclRole::ROLE_ID_DOCTOR);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->groupName;
    }

    /**
     * @return array|null
     */
    public function getRoleIds()
    {
        $roleIds = [];
        foreach($this->getGroupHasAclRoles() as $model){
            $roleIds[] = $model->roleId;
        }
        if (empty($roleIds)) {
            return [];
        }

        return $roleIds;
    }

}