<?php

namespace App\Model;

use App\Support\Str;

/**
 * Class UserToken
 * @package App\Model
 */
class UserToken
    extends AbstractLogModel
{
    /**
     * @var integer
     */
    public $userTokenId;

    /**
     * @var integer
     */
    public $userId;

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    public $createdAt;

    /**
     * @var string
     */
    public $expirationAt;
    
    /**
     * beforeValidationOnCreate
     */
    public function beforeValidationOnCreate()
    {
        $this->token = preg_replace(
            '/[^a-zA-Z0-9]/'
            , ''
            , Str::randomHash(30)
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'N/A';
    }
}