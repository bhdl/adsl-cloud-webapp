<?php

namespace App\Model;

use Phalcon\Db\RawValue;

/**
 * Class Cron
 * @package App\Model
 */
class Cron
    extends AbstractModel
{
    /**
     * @var string
     */
    public $className;

    /**
     * @var integer
     */
    public $timing;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $lastRunAt;

    /**
     * @var string
     */
    public $startedAt;

    /**
     * @var integer
     */
    public $timeout;

    /**
     * @var string
     */
    public $active;

    /**
     * @var int
     */
    public $isMonitor;

    /**
     * @var int
     */
    public $isGarbage;

    /**
     * @var int
     */
    public $isNeedToRun;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var string
     */
    const STATUS_STARTED  = 'started';

    /**
     * @var string
     */
    const STATUS_FINISHED = 'finished';

    /**
     * @var array
     */
    protected static $statuses = [
        self::STATUS_FINISHED  => 'Befejeződött'
        , self::STATUS_STARTED => 'Fut'
    ];

    /**
     * @return string
     */
    public function getSource()
    {
        return 'view__cron';
    }

    /**
     * Elinditottnak jeloli meg a jobot
     * @return void
     */
    public function setStarted()
    {
        $this->_updateFlags(self::STATUS_STARTED, new RawValue('NOW()'), null);
    }

    /**
     * Befejezettre allitja a jobot
     * @return void
     */
    public function setFinished()
    {
        $this->_updateFlags(self::STATUS_FINISHED, null, new RawValue('NOW()'));
    }

    /**
     * @param $status
     * @param $started
     * @param $lastRun
     */
    protected function _updateFlags($status, $started, $lastRun)
    {
        $fields = array('status');
        $values = array($status);

        if ($started) {
            $fields[] = 'startedAt';
            $values[] = $started;
        }

        if ($lastRun) {
            $fields[] = 'lastRunAt';
            $values[] = $lastRun;
        }

        $this->getWriteConnection()->update(
            'cron'
            , $fields
            , $values
            , 'className = \'' . addslashes($this->className) . '\''
        );
    }

    /**
     * @return bool
     */
    public function isStarted()
    {
        return strcmp($this->status, self::STATUS_STARTED) === 0;
    }

    /**
     * @return bool
     */
    public function isFinished()
    {
        return strcmp($this->status, self::STATUS_FINISHED) === 0;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return strcmp($this->active, self::YES) === 0;
    }

    /**
     * @return bool
     */
    public function isMonitor()
    {
        return strcmp($this->isMonitor, self::YES) === 0;
    }

    /**
     * @return bool
     */
    public function isGarbage()
    {
        return strcmp($this->isGarbage, self::YES) === 0;
    }

    /**
     * @return bool
     */
    public function isNeedToRun()
    {
        return strcmp($this->isNeedToRun, self::YES) === 0;
    }

    /**
     * reseteli a job-ot
     */
    public function reset()
    {
        $this->status = null;
        $this->startedAt = null;
        $this->lastRunAt = null;

        $this->save();
    }

    /**
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function getGarbageJobs()
    {
        return self::find(array(
            'isGarbage = \'yes\''
        ));
    }

    /**
     * @param int $limit
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public static function getNeedToRunJobs($limit = 2)
    {
        return self::find(array(
            'isNeedToRun = \'yes\''
            , 'order' => 'isMonitor DESC'
            , 'limit' => $limit
        ));
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $key
     * @return string
     * @throws Exception
     */
    public static function getStatusValue($key)
    {
        if (!array_key_exists($key, static::$statuses)) {
            throw new Exception('Nincs ilyen kulcs a státusz értékek között: ' . $key);
        }

        return static::$statuses[$key];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->className;
    }
}