<?php

namespace App\Model;

use App\Model\Traits\HasLanguages;
use App\Traits\InterfaceLanguage;
use Phalcon\Exception;

/**
 * Class Log
 * @package App\Model
 */
class ProductPosition
    extends AbstractLogModel
{
    use HasLanguages, InterfaceLanguage;
    /**
     * @var integer
     */
    public $productPositionId;

    /**
     * @var integer
     */
    public $productPositionEventId;

    /**
     * @var integer
     */
    public $productId;

    /**
     * @var integer
     */
    public $currentX;

    /**
     * @var integer
     */
    public $currentY;

    /**
     * @var integer
     */
    public $currentZ;

    /**
     * @var integer
     */
    public $newX;

    /**
     * @var integer
     */
    public $newY;

    /**
     * @var integer
     */
    public $newZ;

    /**
     * @return string
     */
    public function getName()
    {
        return 'N\A';
    }
}
