<?php

namespace App\Model;

/**
 * Class LogModelAttribute
 * @package App\Model
 */
class LogModelAttribute extends AbstractModel
{
    /**
     * @var int
     */
    public $logModelAttributeId;

    /**
     * @var int
     */
    public $logModelId;

    /**
     * @var string
     */
    public $attribute;

    /**
     * @var string
     */
    public $oldValue;

    /**
     * @var string
     */
    public $newValue;

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('logModelId', __NAMESPACE__ . '\LogModel', 'logModelId', array(
            'alias'      => 'logModel'
            , 'reusable' => true
        ));
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->logModelAttributeId;
    }
}