<?php

namespace App\Model;

use App\Model\Behavior\LogModelCollector;

/**
 * Class AbstractLogModel
 * @package App\Model
 */
abstract class AbstractLogModel
    extends AbstractModel
{
    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        // mezőszintű változáskövetés
        $this->keepSnapshots(true);

        // logmodelcollector viselkedés hozzáadása
        $this->addBehavior(
            new LogModelCollector()
        );
    }
}