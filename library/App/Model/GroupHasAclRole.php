<?php

namespace App\Model;

/**
 * Class GroupHasAclRole
 * @package App\Model
 */
class GroupHasAclRole
    extends AbstractLogModel
{
    /**
     * @var integer
     */
    public $groupId;

    /**
     * @var string
     */
    public $roleId;
    
    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('groupId', __NAMESPACE__ . '\Group', 'groupId', array(
            'alias' => 'group'
            , 'reusable' => true
        ));

        $this->belongsTo('roleId', __NAMESPACE__ . '\AclRole', 'roleId', array(
            'alias' => 'role'
            , 'reusable' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'N/A';
    }
}