<?php

namespace App\Model;

/**
 * Class AclResource
 * @package App\Model
 */
class AclResource extends AbstractLogModel
{
    /**
     * @var string
     */
    public $resourceId;

    /**
     * @var string
     */
    public $resourceName;
    
    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->hasMany('resourceId', __NAMESPACE__ . '\AclMatrix', 'resourceId', array(
            'alias' => 'matrix'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->resourceName;
    }
}