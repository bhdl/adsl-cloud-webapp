<?php

namespace App\Model\Builder;

/**
 * Class Cron
 * @package App\Model\Builder
 */
class Cron
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'className'
                , 'timing'
                , 'startedAt'
                , 'lastRunAt'
                , 'timeout'
                , 'status'
                , 'active'
                , 'isMonitor'
                , 'isGarbage'
                , 'isNeedToRun'
            ])
            ->addFrom('App\Model\Cron')
        ;

        return $builder;
    }
}