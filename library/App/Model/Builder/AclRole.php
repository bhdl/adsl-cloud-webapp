<?php

namespace App\Model\Builder;

/**
 * Class AclRole
 * @package App\Model\Builder
 */
class AclRole
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns(['roleId', 'roleName'])
            ->addFrom('App\Model\AclRole')
        ;

        return $builder;
    }
}