<?php

namespace App\Model\Builder\Traits;

use Phalcon\Di;
use Phalcon\Mvc\Model\Query\Builder;

/**
 * Trait RowLevelAcl
 * @package App\Model\Builder\Traits
 */
trait RowLevelAcl
{
    /**
     * @var string
     */
    protected static $rowLevelAclUserAttribute = 'userId';

    /**
     * @var bool
     */
    protected static $rowLevelAclEnabled = true;

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    public function createBuilder()
    {
        /**
         * @var Builder $builder
         */
        $builder = parent::createBuilder();

        if (self::isRowLevelAclEnabled()) {

            self::injectCallableToBuilder(
                $builder
                , self::getRowLevelAclBuilderCallable()
            );

        }

        return $builder;
    }

    /**
     * @return \Closure
     */
    protected static function getRowLevelAclBuilderCallable()
    {
        $method = 'getRowLevelAclBuilderCallableCustom';

        if (method_exists(__CLASS__, $method)) {
            return self::$method();
        }

        return self::getRowLevelAclBuilderCallableDefault();
    }

    /**
     * Az alapértelmezett callback, ha csak egyszerű userId attribútumunk van.
     *
     * @return \Closure
     */
    protected static function getRowLevelAclBuilderCallableDefault()
    {
        return function(Builder $builder) {
            /**
             * @var User $user
             */
            $user = Di::getDefault()
                ->get('auth')
                ->getIdentity()
            ;

            $builder->andWhere(
                self::getRowLevelAclUserAttribute()
                . ' = ' . $user->userId
            );
        };
    }

    /**
     * @return string
     */
    public static function getRowLevelAclUserAttribute(): string
    {
        return self::$rowLevelAclUserAttribute;
    }

    /**
     * @param string $rowLevelAclUserAttribute
     */
    public static function setRowLevelAclUserAttribute(string $rowLevelAclUserAttribute)
    {
        self::$rowLevelAclUserAttribute = $rowLevelAclUserAttribute;
    }

    /**
     * @return bool
     */
    public static function isRowLevelAclEnabled()
    {
        return self::$rowLevelAclEnabled;
    }

    /**
     * @param bool $rowLevelAclEnabled
     */
    public static function setRowLevelAclEnabled(bool $rowLevelAclEnabled)
    {
        self::$rowLevelAclEnabled = $rowLevelAclEnabled;
    }
}