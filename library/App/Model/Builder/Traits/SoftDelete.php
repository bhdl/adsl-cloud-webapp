<?php

namespace App\Model\Builder\Traits;

use App\Model\AbstractModel;
use Phalcon\Di;
use Phalcon\Mvc\Model\Query\Builder;

/**
 * Trait SoftDelete
 * @package App\Model\Builder\Traits
 */
trait SoftDelete
{
    /**
     * This trait inject the soft delete functionality to your builder.
     * The result is that the querys not going to select the deleted elements.
     * How to use:
     * - Add the top of your model builder file this two lines:
     * use App\Model\Builder\Traits\BuilderInjector;
     * use App\Model\Builder\Traits\SoftDelete;
     * - Add this two lines to the model builder class
     * use SoftDelete, BuilderInjector;
     *
     * In your builder when you create the builder and you add the addFrom option you must define the alias as modelTable
     * For example:
     * $builder->columns(['productId', 'partnerId'])->addFrom('App\Model\Product', 'modelTable')
     */

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    public function createBuilder()
    {
        /**
         * @var Builder $builder
         */
        $builder = parent::createBuilder();

        self::injectCallableToBuilder(
            $builder
            , self::getSoftDeleteBuilderCallable()
        );

        return $builder;
    }

    /**
     * @return \Closure
     */
    protected static function getSoftDeleteBuilderCallable()
    {
        return function(Builder $builder) {

            $builder->andWhere(
                'modelTable.deleted = \''.AbstractModel::NO.'\''
            );
        };
    }
}