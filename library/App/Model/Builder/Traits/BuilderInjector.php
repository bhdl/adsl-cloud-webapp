<?php

namespace App\Model\Builder\Traits;

use Phalcon\Mvc\Model\Query\Builder;

/**
 * Trait BuilderInjector
 * @package App\Model\Builder\Traits
 */
trait BuilderInjector
{
    /**
     * @param Builder $builder
     * @param callable $callable
     */
    protected static function injectCallableToBuilder(
        Builder $builder
        , callable $callable
    ) {
        $callable($builder);
    }
}