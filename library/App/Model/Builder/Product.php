<?php

namespace App\Model\Builder;

use App\Model\Builder\Traits\BuilderInjector;
use App\Model\Builder\Traits\SoftDelete;

/**
 * Class Product
 * @package App\Model\Builder
 */
class Product
    extends AbstractBuilder
{
    use SoftDelete, BuilderInjector;

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'productId'
                , 'partnerId'
                , 'name'
                , 'description'
            ])
            ->addFrom('App\Model\Product', 'modelTable')
        ;

        // equals
        $this->buildEquals(['productId', 'partnerId']);

        // like
        $this->buildLikes(['name', 'description']);

        return $builder;
    }
}