<?php

namespace App\Model\Builder;

/**
 * Class InventoryStatus
 * @package App\Model\Builder
 */
class InventoryStatus
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'inventoryStatusId'
                , 'inventoryEventId'
                , 'rfid'
                , 'x'
                , 'y'
                , 'z'
                , 'accuracy'
                , 'latitude'
                , 'longitude'
            ])
            ->addFrom('App\Model\InventoryStatus', 'is')
        ;

        // equals
        $this->buildEquals(['inventoryStatusId', 'inventoryEventId']);

        // like
        $this->buildLikes(['rfid']);

        return $builder;
    }
}