<?php

namespace App\Model\Builder;

use App\Model\Builder\Traits\BuilderInjector;
use App\Model\Builder\Traits\SoftDelete;

/**
 * Class InventoryEvent
 * @package App\Model\Builder
 */
class InventoryEvent
    extends AbstractBuilder
{

    use SoftDelete, BuilderInjector;

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'inventoryEventId'
                , 'modelTable.siteId'
                , 'address'
                , 'startTime'
                , 'endTime'
            ])
            ->addFrom('App\Model\InventoryEvent', 'modelTable')
        ;

        $builder->innerJoin('App\Model\Site', 's.siteId = modelTable.siteId', 's');

        // equals
        $this->buildEquals(['inventoryEventId', 'siteId']);

        // like
        $this->buildLikes(['startTime', 'endTime']);

        return $builder;
    }
}