<?php

namespace App\Model\Builder;

/**
 * Class Doctor
 * @package App\Model\Builder
 */
class Doctor
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'doctorId'
                , 'languageId'
                , 'userId'
                , 'defaultPraxisId'
                , 'stamp'
                , 'lastName'
                , 'middleName'
                , 'firstName'
                , 'title'
            ])
            ->addFrom('App\Model\Doctor', 'd')
        ;

        if (
            $this->hasParam('relationModel') && $this->getParam('relationModel')
            && $this->hasParam('primaryKeyAttribute') && $this->getParam('primaryKeyAttribute')
            && $this->hasParam('modelId') && $this->getParam('modelId')
        ) {
            $builder->andWhere(
                'd.doctorId NOT IN (
                    SELECT dhx.doctorId FROM ' . $this->getParam('relationModel') . ' dhx 
                        WHERE  dhx.' . $this->getParam('primaryKeyAttribute') . '= '.$this->getParam('modelId').'
                )'
            );
        }

        if ($this->hasParam('praxisId')) {
            // adott praxis orvosai
            $builder->andWhere(
                'EXISTS(SELECT phd.doctorId FROM App\Model\PraxisHasDoctor phd WHERE phd.doctorId = d.doctorId AND phd.praxisId = :praxisId:)'
                , [
                    'praxisId' => $this->getParam('praxisId')
                ]
            );
        }

        $this->buildEquals('doctorId');

        if ($this->hasParam('autoComplete')) {

            $value = '%' . $this->getParam('autoComplete') . '%';

            $builder->andWhere(
                'lastName LIKE :ac1: OR firstName LIKE :ac2: OR middleName LIKE :ac3: OR stamp LIKE :ac4:'
                , [
                    'ac1'   =>  $value
                    , 'ac2' =>  $value
                    , 'ac3' =>  $value
                    , 'ac4' =>  $value
                ]
            );
        }


        $this->buildIn('doctorId', 'ids');

        return $builder;
    }
}