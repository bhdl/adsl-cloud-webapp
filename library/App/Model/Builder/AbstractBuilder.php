<?php

namespace App\Model\Builder;

use App\Model\Builder\Traits\BuilderInjector;
use App\Support\Date;
use App\Support\HashMap;
use App\Support\Numeric;
use Phalcon\Exception;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\User\Component;

/**
 * Class AbstractBuilder
 * @package App\Model\Builder
 */
abstract class AbstractBuilder
    extends Component
{
    use BuilderInjector;

    /**
     * @var string
     */
    const COMPARISON_LIKE = 'LIKE';

    /**
     * @var string
     */
    const COMPARISON_EQUAL = '=';

    /**
     * @var Builder
     */
    protected $builder;

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    abstract public function build();

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    protected function createBuilder()
    {
        if ($this->builder == null) {
            $this->builder = $this->getDi()->get('modelsManager')->createBuilder();
        }

        return $this->builder;
    }

    /**
     * @param array $options
     * @throws Exception
     */
    protected function buildEquals($options = [])
    {
        $this->buildMultiValues(self::COMPARISON_EQUAL, $options);
    }

    /**
     * @param string      $attribute
     * @param string|null $param
     */
    protected function buildEqual($attribute, $param = null)
    {
        $this->buildMultiValue(self::COMPARISON_EQUAL, $attribute, $param);
    }

    /**
     * @param array $options
     * @throws Exception
     */
    protected function buildLikes($options = [])
    {
        $this->buildMultiValues(self::COMPARISON_LIKE, $options);
    }

    /**
     * @param string      $attribute
     * @param null|string $param
     */
    protected function buildLike($attribute, $param = null)
    {
        $this->buildMultiValue(self::COMPARISON_LIKE, $attribute, $param);
    }

    /**
     * @param array $options
     * @throws Exception
     */
    protected function buildNumericIntervals($options = [])
    {
        foreach ($options as $k) {

            if (!is_array($k) || (count($k) < 2 || count($k) > 3)) {
                throw new Exception(
                    'A vizsgálati feltételben 2-3 elemet várok (attribútum, tól|ig).'
                );
            }

            $this->buildNumericInterval(
                $k[0]
                , $k[1] ?: null
                , $k[2] ?: null
            );
        }
    }

    /**
     * @param null $attribute
     * @param null $fromParam
     * @param null $toParam
     */
    protected function buildNumericInterval($attribute = null, $fromParam = null, $toParam = null)
    {
        if ($this->hasParam($fromParam)) {
            $this->builder->andWhere($attribute . ' >= :' . $fromParam . ':', [
                $fromParam => $this->getNumericParam($fromParam)
            ]);
        }

        if ($this->hasParam($toParam)) {
            $this->builder->andWhere($attribute . ' <= :' . $toParam . ':', [
                $toParam => $this->getNumericParam($toParam)
            ]);
        }
    }


    /**
     * @param array $options
     * @throws Exception
     */
    protected function buildDateIntervals($options = [])
    {
        foreach ($options as $k) {

            if (!is_array($k) || (count($k) < 2 || count($k) > 3)) {
                throw new Exception(
                    'A vizsgálati feltételben 2-3 elemet várok (attribútum, tól|ig).'
                );
            }

            $this->buildDateInterval(
                $k[0]
                , $k[1] ?: null
                , $k[2] ?: null
            );
        }
    }

    /**
     * @param null|string $attribute
     * @param null|string $fromParam
     * @param null|string $toParam
     */
    protected function buildDateInterval($attribute = null, $fromParam = null, $toParam = null)
    {
        if ($this->hasParam($fromParam)) {
            $this->builder->andWhere($attribute . ' >= :' . $fromParam . ':', [
                $fromParam => $this->getDateParam($fromParam)
            ]);
        }

        if ($this->hasParam($toParam)) {
            $this->builder->andWhere($attribute . ' <= :' . $toParam . ':', [
                $toParam => $this->getDateParam($toParam)
            ]);
        }
    }

    /**
     * @param array $options
     * @throws Exception
     */
    protected function buildDateTimeIntervals($options = [])
    {
        foreach ($options as $k) {

            if (!is_array($k) || (count($k) < 2 || count($k) > 3)) {
                throw new Exception(
                    'A vizsgálati feltételben 2-3 elemet várok (attribútum, tól|ig).'
                );
            }

            $this->buildDateTimeInterval(
                $k[0]
                , $k[1] ?: null
                , $k[2] ?: null
            );
        }
    }

    /**
     * @param null|string $attribute
     * @param null|string $fromParam
     * @param null|string $toParam
     */
    protected function buildDateTimeInterval($attribute = null, $fromParam = null, $toParam = null)
    {
        if ($this->hasParam($fromParam)) {
            $this->builder->andWhere($attribute . ' >= :' . $fromParam . ':', [
                $fromParam => $this->getDateParam($fromParam) . ' 00:00:00'
            ]);
        }

        if ($this->hasParam($toParam)) {
            $this->builder->andWhere($attribute . ' <= :' . $toParam . ':', [
                $toParam => $this->getDateParam($toParam) . ' 23:59:59'
            ]);
        }
    }

    /**
     * @param array $options
     * @throws Exception
     */
    protected function buildIns($options = [])
    {
        foreach ($options as $k) {

            if (is_array($k)) {
                if (count($k) < 1 || count($k) > 2) {
                    throw new Exception(
                        'Tömb típusú vizsgálati feltétel esetén 1|2 elemet várok (attribútum, paraméter).'
                    );
                }

                $this->buildIn(
                    $k[0]
                    , $k[1] ?: null
                );

                continue;
            }

            if (is_string($k)) {
                $this->buildIn($k);
            }
        }
    }

    /**
     * @param string      $attribute
     * @param string|null $param
     */
    protected function buildIn($attribute, $param = null)
    {
        $param = $param ?: $attribute;

        if ($this->hasParam($param)) {
            $ids = $this->getParam($param);

            if (is_string($ids)) {
                $ids = explode(',', $ids);
            }

            $ids = array_filter($ids);

            if (HashMap::isFilled($ids)) {
                $this->builder->inWhere($attribute, $ids);
            }
        }
    }

    /**
     * @param string $comparison
     * @param array $options
     * @throws Exception
     */
    protected function buildMultiValues($comparison, $options)
    {
        foreach ($options as $k) {
            if (is_array($k)) {
                if (count($k) < 1 || count($k) > 2) {
                    throw new Exception(
                        'Tömb típusú vizsgálati feltétel esetén 1|2 elemet várok (attribútum, paraméter).'
                    );
                }
                $this->buildMultiValue($comparison, $k[0], $k[1]);
                continue;
            }

            if (is_string($k)) {
                $this->buildMultiValue($comparison, $k);
            }
        }
    }

    /**
     * @param $comparison
     * @param $attribute
     * @param null $param
     */
    protected function buildMultiValue($comparison, $attribute, $param = null)
    {
        $param = $param ?: $attribute;

        $v = function($comparison, $value) {
            if ($comparison == AbstractBuilder::COMPARISON_LIKE) {
                return "%{$value}%";
            }

            return $value;
        };

        if ($this->hasParam($param)) {

            $values = $this->getParam($param);

            if (!is_array($values)) {
                $this->builder->andWhere(
                    "{$attribute} {$comparison} :{$param}:"
                    , [
                        $param => $v($comparison, $values)
                    ]
                );

                return;
            }

            if (is_array($values)) {

                $conditions = [];
                $bindParams = [];

                foreach ($values as $i=>$value) {

                    if (empty($value)) {
                        continue;
                    }

                    $paramKey     = $param . '_' . $i;
                    $conditions[] = "{$attribute} {$comparison} :{$paramKey}:";
                    $bindParams[$paramKey] = $v($comparison, $value);
                }

                if (HashMap::isFilled($conditions)) {
                    $this->builder->andWhere(
                        implode(' OR ', $conditions)
                        , $bindParams
                    );
                }
            }
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasParam($key)
    {
        return array_key_exists($key, $this->params) && !empty($this->params[$key]);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getParam($key)
    {
        return $this->params[$key];
    }

    /**
     * Dátum mező esetén a beviteli érték lokalizált
     *
     * @param string $key
     * @return string
     */
    public function getDateParam($key)
    {
        $dateTime = Date::dateFromLocalized(
            $this->getParam($key)
        );

        return $dateTime ? $dateTime->format('Y-m-d') : '1970-01-01';
    }

    /**
     * @param string $key
     * @return float
     */
    public function getNumericParam($key)
    {
        return Numeric::numericFromLocalized(
            $this->getParam($key)
        );
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return AbstractBuilder
     */
    public function setParam($key, $value)
    {
        $this->params[$key] = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     * @return AbstractBuilder
     */
    public function setParams(array $params)
    {
        $this->params = $params;

        return $this;
    }
}