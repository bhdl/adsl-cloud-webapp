<?php

namespace App\Model\Builder;

/**
 * Class User
 * @package App\Model\Builder
 */
class User
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'userId'
                , 'groupId'
                , 'name'
                , 'email'
                , 'active'
                , 'groupName'
                , 'name'
            ])
            ->addFrom('App\Model\View\User', 'u')
        ;

        // equals
        $this->buildEquals(['groupId', 'active']);

        // like
        $this->buildLikes(['name', 'email']);

        // in
        $this->buildIn('userId', 'ids');

        // guest kizárva
        $builder->andWhere(
            'u.groupId NOT IN(SELECT g.groupId FROM App\Model\Group g WHERE g.guest = \'yes\')'
        );

        return $builder;
    }
}