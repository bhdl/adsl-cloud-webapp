<?php

namespace App\Model\Builder;

/**
 * Class Availability
 * @package App\Model\Builder
 */
class Availability
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'availabilityId'
                , 'companyId'
                , 'praxisId'
                , 'doctorId'
                , 'availabilityType'
                , 'value'
                , 'comment'
                , 'primary'
            ])
            ->addFrom('App\Model\Availability')
        ;

        $this->buildEquals(['companyId', 'praxisId', 'doctorId']);

        return $builder;
    }
}