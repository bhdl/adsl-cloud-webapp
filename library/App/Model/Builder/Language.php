<?php

namespace App\Model\Builder;

/**
 * Class Language
 * @package App\Model\Builder
 */
class Language
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'languageId'
                , 'languageName'
                , 'locale'
                , 'iso'
                , 'interface'
            ])
            ->addFrom('App\Model\Language')
        ;

        return $builder;
    }
}