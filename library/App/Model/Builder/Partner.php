<?php

namespace App\Model\Builder;

use App\Model\Builder\Traits\BuilderInjector;
use App\Model\Builder\Traits\SoftDelete;

/**
 * Class Partner
 * @package App\Model\Builder
 */
class Partner
    extends AbstractBuilder
{
    use SoftDelete, BuilderInjector;

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'partnerId'
                , 'name'
            ])
            ->addFrom('App\Model\Partner', 'modelTable')
        ;

        // equals
        $this->buildEquals(['partnerId']);

        // like
        $this->buildLikes(['name']);

        return $builder;
    }
}