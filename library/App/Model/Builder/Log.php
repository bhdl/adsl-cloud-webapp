<?php

namespace App\Model\Builder;

/**
 * Class Log
 * @package App\Model\Builder
 */
class Log
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'logId'
                , 'user'
                , 'priority'
                , 'priorityName'
                , 'content'
                , 'contentShort'
                , 'exception'
                , 'createdAt'
            ])
            ->addFrom('App\Model\View\Log')
        ;

        $this->buildIns(['userId', 'priority']);

        $this->buildLikes(['content']);

        $this->buildDateTimeIntervals([
            ['createdAt', 'from', 'to']
        ]);

        return $builder;
    }
}