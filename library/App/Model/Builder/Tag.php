<?php

namespace App\Model\Builder;

use App\Model\Builder\Traits\BuilderInjector;
use App\Model\Builder\Traits\SoftDelete;

/**
 * Class Tag
 * @package App\Model\Builder
 */
class Tag
    extends AbstractBuilder
{

    use SoftDelete, BuilderInjector;

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'tagId'
                , 'type'
                , 'name'
                , 'description'
            ])
            ->addFrom('App\Model\Tag', 'modelTable')
        ;

        // equals
        $this->buildEquals(['tagId', 'type']);

        // like
        $this->buildLikes(['name', 'description']);

        return $builder;
    }
}