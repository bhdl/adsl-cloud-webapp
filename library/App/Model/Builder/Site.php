<?php

namespace App\Model\Builder;

use App\Model\Builder\Traits\BuilderInjector;
use App\Model\Builder\Traits\SoftDelete;

/**
 * Class Site
 * @package App\Model\Builder
 */
class Site
    extends AbstractBuilder
{

    use SoftDelete, BuilderInjector;

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'siteId'
                , 'partnerId'
                , 'gatewayId'
                , 'address'
            ])
            ->addFrom('App\Model\Site', 'modelTable')
        ;

        // equals
        $this->buildEquals(['siteId', 'partnerId']);

        // like
        $this->buildLikes(['gatewayId', 'address']);

        return $builder;
    }
}