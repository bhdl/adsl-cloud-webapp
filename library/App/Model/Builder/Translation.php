<?php

namespace App\Model\Builder;

/**
 * Class Translation
 * @package App\Model\Builder
 */
class Translation
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'translationId'
                , 'languageId'
                , 'key'
                , 'value'
                , 'changed'
            ])
            ->addFrom('App\Model\View\Translation')
        ;

        $this->buildEquals(['languageId']);

        return $builder;
    }
}