<?php

namespace App\Model\Builder;

/**
 * Class Address
 * @package App\Model\Builder
 */
class Address
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'addressId'
                , 'ownerId'
                , 'praxisId'
                , 'companyId'
                , 'addressName'
                , 'settlement'
                , 'zip'
                , 'country'
                , 'region'
                , 'line1'
                , 'line2'
                , 'addressTypes'
            ])
            ->addFrom('App\Model\Address')
        ;

        $this->buildEquals(['ownerId', 'praxisId', 'companyId']);

        if ($this->hasParam('autoComplete')) {


            $value = '%' . $this->getParam('autoComplete') . '%';

            $builder->andWhere(
                'addressName LIKE :ac1: OR settlement LIKE :ac2: OR zip LIKE :ac3: OR country LIKE :ac4: OR region LIKE :ac5: OR line1 LIKE :ac6: OR line2 LIKE :ac7:'
                , [
                    'ac1'   =>  $value
                    , 'ac2' =>  $value
                    , 'ac3' =>  $value
                    , 'ac4' =>  $value
                    , 'ac5' =>  $value
                    , 'ac6' =>  $value
                    , 'ac7' =>  $value
                ]
            );
        }


        return $builder;
    }
}