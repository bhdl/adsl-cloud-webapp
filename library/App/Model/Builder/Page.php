<?php

namespace App\Model\Builder;

use App\Model\Builder\Traits\BuilderInjector;
use App\Model\Builder\Traits\SoftDelete;

/**
 * Class Page
 * @package App\Model\Builder
 */
class Page
    extends AbstractBuilder
{
    use SoftDelete, BuilderInjector;

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     * @throws \Phalcon\Exception
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                "pageId",
                "parentId",
                "position",
                "active",
                "slideshow",
                "map",
                "languageId",
                "pageName",
                "slug",
                "content",
                "isMin",
                "isMax"
            ])
            ->addFrom('App\Model\View\Page', 'modelTable')
        ;

        $this->buildEquals(['active', 'slideshow', 'map', 'parentId', 'languageId']);

        $this->buildLikes(['pageName', 'slug']);

        return $builder;
    }
}