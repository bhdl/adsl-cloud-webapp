<?php

namespace App\Model\Builder;

use App\Model\AbstractModel;

/**
 * Class Group
 * @package App\Model\Builder
 */
class Group
    extends AbstractBuilder
{
    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    public function build()
    {
        $builder = $this->createBuilder();

        $builder
            ->columns([
                'groupId'
                , 'groupName'
                , 'active'
                , 'userCount'
            ])
            ->addFrom('App\Model\View\Group', 'g')
            ->where('g.guest = \'' . AbstractModel::NO . '\'')
        ;

        return $builder;
    }
}