<?php

namespace App\Model;

use Phalcon\Exception;
use App\Model\Traits\SoftDelete;

/**
 * Class Log
 * @package App\Model
 */
class ProductInstance
    extends AbstractLogModel
{
    use SoftDelete;

    /**
     * @var integer
     */
    public $productInstanceId;

    /**
     * @var integer
     */
    public $productId;

    /**
     * @var integer
     */
    public $siteId;

    /**
     * @var string
     */
    public $rfid;

    /**
     * @var string
     */
    public $status;

    /**
     * @var datetime
     */
    public $createdAt;

    /**
     * @var datetime
     */
    public $updatedAt;

    /**
     * @var datetime
     */
    public $deletedAt;

    /**
     * @var string
     */
    public $deleted = self::NO;

    /**
     * @var array
     */
    public static $deletedTypes = [
        self::YES  => 'igen'
        , self::NO => 'nem'
    ];
    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->initializeSoftDelete();

        $this->skipAttributes([
            "createdAt"
            , "updatedAt"
        ]);

        $this->belongsTo('productId', __NAMESPACE__ . '\Product', 'productId', array(
            'alias' => 'product'
        ));

        $this->belongsTo('siteId', __NAMESPACE__ . '\Product', 'productId', array(
            'alias' => 'site'
        ));
    }

    /**
     * @return array
     */
    public static function getDeletedTypes()
    {
        return self::$deletedTypes;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'N\A';
    }
}