<?php

namespace App\Model;

use App\Filter\Boolean;
use App\Model\Traits\RowLevelAcl;

/**
 * Class DataTableState
 * @package App\Model
 */
class DataTableState
    extends AbstractLogModel
{
    use RowLevelAcl;

    /**
     * @var integer
     */
    public $dataTableStateId;

    /**
     * @var integer
     */
    public $userId;

    /**
     * @var string
     */
    public $data;

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->addBehavior(new \App\Model\Behavior\RowLevelAcl);
    }

    /**
     * beforeSave
     */
    public function beforeSave()
    {
        if (is_array($this->data)) {

            $filter = new Boolean();
            $filter->setMap(['false' => false, 'true' => true]);

            array_walk_recursive($this->data, function(&$v, $k) use ($filter) {
                try {
                    $v = $filter->filter($v);
                } catch (\Phalcon\Filter\Exception $e) {
                }
            });

            $this->data = json_encode($this->data, JSON_NUMERIC_CHECK);
        }
    }

    /**
     * afterFetch
     */
    public function afterFetch()
    {
        $this->data = json_decode($this->data);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->dataTableStateId;
    }
}