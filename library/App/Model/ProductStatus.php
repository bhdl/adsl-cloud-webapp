<?php

namespace App\Model;

use Phalcon\Exception;
use Phalcon\Mvc\Model\Relation;

class ProductStatus extends AbstractLogModel
{
    /**
     * @var integer
     */
    public $productStatusId;

    /**
     * @var string
     */
    public $name;

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('productStatusId', __NAMESPACE__ . '\ProductStatus', 'productStatusId', array(
            'alias' => 'productStatuses',
            'reusable' => true
        ));
    }

    /**
     * @return array
     */
    public static function getDeletedTypes()
    {
        return self::$deletedTypes;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}