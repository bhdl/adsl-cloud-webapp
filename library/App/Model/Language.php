<?php

namespace App\Model;

use Phalcon\Exception;
use Phalcon\Mvc\Model\Resultset;

/**
 * Class Language
 * @package App\Model
 */
class Language
    extends AbstractLogModel
{
    /**
     * @var integer
     */
    public $languageId;

    /**
     * @var string
     */
    public $iso;

    /**
     * @var string
     */
    public $locale;

    /**
     * @var string
     */
    public $languageName;

    /**
     * @var string
     */
    public $dateFormat;

    /**
     * @var string
     */
    public $dateRegexPattern;

    /**
     * @var string
     */
    public $dateInputMask;

    /**
     * @var string
     */
    public $interface;
    
    /**
     * @throws Exception
     */
    public function beforeDelete()
    {
        if ($this->isDefault()) {
            throw new Exception('Az alapértelmezett nyelv nem törölhető!');
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->languageName;
    }

    /**
     * @param int|null $excludeLanguageId
     * @return \Phalcon\Mvc\Model\Resultset\Simple
     */
    public static function findInterfaceLanguages($excludeLanguageId = null)
    {
        return self::find([
            'interface = \'' . self::YES . '\''
            . ($excludeLanguageId ? ' AND languageId <> ' . $excludeLanguageId : '')
            , 'order' => 'languageName'
        ]);
    }

    /**
     * @return Language
     */
    public static function findDefault()
    {
        return self::findFirst(
            Config::get('languageId')
        );
    }

    /**
     * @return Resultset
     */
    public static function findSecondary()
    {
        return parent::find([
            'languageId <> ' . Config::get('languageId')
            , 'order' => 'languageName'
        ]);
    }

    /**
     * @return bool
     */
    public function isDefault()
    {
        return $this->languageId == Config::get('languageId');
    }

    /**
     * @param null $request
     * @return string
     */
    public function getChangeUrl($request = null)
    {
        if ($request == null) {
            $request = $this->getDI()->get('request');
        }

        return '/admin/language/change/'
            . $this->languageId
            . '?redirectUrl='
            . base64_encode($request->getURI())
        ;
    }
}