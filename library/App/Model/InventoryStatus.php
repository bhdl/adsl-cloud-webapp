<?php

namespace App\Model;

class InventoryStatus extends AbstractLogModel
{
    /**
     * @var integer
     */
    public $inventoryStatusId;

    /**
     * @var integer
     */
    public $inventoryEventId;

    /**
     * @var string
     */
    public $rfid;

    /**
     * @var float
     */
    public $x;

    /**
     * @var float
     */
    public $y;

    /**
     * @var float
     */
    public $z;

    /**
     * @var float
     */
    public $accuracy;

    /**
     * @var float
     */
    public $latitude;

    /**
     * @var float
     */
    public $longitude;

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('inventoryEventId', __NAMESPACE__ . '\InventoryEvent', 'inventoryEventId', array(
            'alias' => 'inventoryevent',
            'reusable' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}