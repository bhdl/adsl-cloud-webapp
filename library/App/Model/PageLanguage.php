<?php

namespace App\Model;

use Phalcon\Exception;
use Phalcon\Mvc\Model\Relation;
use Phalcon\Utils\Slug;

/**
 * Class Log
 * @package App\Model
 */
class PageLanguage
    extends AbstractLogModel
{
    /**
     * @var integer
     */
    public $pageId;

    /**
     * @var integer
     */
    public $languageId;

    /**
     * @var string
     */
    public $pageName;

    /**
     * @var string
     */
    public $slug;

    /**
     * @var string
     */
    public $content;

    /**
     * beforeValidation
     */
    public function beforeValidation()
    {
        $this->slug = Slug::generate($this->pageName);
    }
    
    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo('pageId', __NAMESPACE__ . '\Page', 'pageId', array(
            'alias' => 'page'
        ));

        $this->belongsTo('languageId', __NAMESPACE__ . '\Language', 'languageId', [
            'alias' => 'language'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->pageName;
    }
}