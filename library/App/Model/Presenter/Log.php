<?php

namespace App\Model\Presenter;

use App\Model\Log as LogModel;

/**
 * Class Log
 * @package App\Model\Presenter
 */
class Log
    extends AbstractPresenter
{
    /**
     *
     */
    protected function init()
    {
        $this->addRule('datetime', 'createdAt');
        $this->addRule(function($value) {
            return $this->tag->label(
                null // TODO
                , $this->translate->_(LogModel::getPriorityValue($value))
            );
        }, 'priority');
    }
}