<?php

namespace App\Model\Presenter;

use App\Model\AbstractModel;
use App\Model\Config;
use App\Model\Language as LanguageModel;

/**
 * Class Language
 * @package App\Model\Presenter
 */
class Language
    extends AbstractPresenter
{
    /**
     *
     */
    protected function init()
    {
        $this->addRule('bool', 'interface');

        $this->addRule(function($value, $options){
            $key = $options['row']['languageId'] == $options['defaultLanguageId'] ? AbstractModel::YES : AbstractModel::NO;
            return $this->tag->label(
                $key
                , $this->translate->_(LanguageModel::getBooleanValue($key))
            );
        }, 'default', [
            'defaultLanguageId' => Config::get('languageId')
        ]);
    }
}