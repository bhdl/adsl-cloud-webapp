<?php

namespace App\Model\Presenter;


/**
 * Class Address
 * @package App\Model\Presenter
 */
class Address
    extends AbstractPresenter
{
    /**
     *
     */
    protected function init()
    {
        $this->addRule(function($value) {
            return \App\Model\Address::getAddressTypeValues(
                $value
            );
        }, 'addressTypes');

        $this->addRule(function($value, $options) {
            $address = self::getModel(
                $options['row']['addressId']
                , \App\Model\Address::class
            );
            return $address->getName();
        }, 'name');

    }
}