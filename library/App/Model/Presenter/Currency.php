<?php

namespace App\Model\Presenter;

use App\Model\AbstractModel;
use App\Model\Config;
use App\Model\Currency as CurrencyModel;

/**
 * Class Currency
 * @package App\Model\Presenter
 */
class Currency
    extends AbstractPresenter
{
    /**
     *
     */
    protected function init()
    {
        $this->addRule(function($value, $options){
            $key = $options['row']['currencyId'] == $options['defaultCurrencyId'] ? AbstractModel::YES : AbstractModel::NO;
            return $this->tag->label(
                $key
                , $this->translate->_(CurrencyModel::getBooleanValue($key))
            );
        }, 'default', [
            'defaultCurrencyId' => Config::get('currencyId')
        ]);
    }
}