<?php

namespace App\Model\Presenter;

use App\Model\Cron as CronModel;

/**
 * Class Cron
 * @package App\Model\Presenter
 */
class Cron
    extends AbstractPresenter
{
    /**
     *
     */
    protected function init()
    {
        $this->addRule('datetime', ['lastRunAt', 'startedAt']);
        $this->addRule('bool', ['active', 'isMonitor', 'isGarbage', 'isNeedToRun']);
        $this->addRule(function($value) {
            return $this->tag->label(
                $value
                , CronModel::getStatusValue($value)
            );
        }, 'status');
    }
}