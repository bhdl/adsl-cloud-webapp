<?php

namespace App\Model\Presenter;


/**
 * Class Availability
 * @package App\Model\Presenter
 */
class Availability
    extends AbstractPresenter
{
    /**
     *
     */
    protected function init()
    {
        $this->addRule(function($value) {
            return \App\Model\Availability::getAvailabilityTypeValue(
                $this->translate->_($value)
            );
        }, 'availabilityType');

        $this->addRule('bool', ['primary']);
    }
}