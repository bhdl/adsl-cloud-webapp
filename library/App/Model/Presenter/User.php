<?php

namespace App\Model\Presenter;

/**
 * Class User
 * @package App\Model\Presenter
 */
class User
    extends AbstractPresenter
{
    /**
     * init
     */
    protected function init()
    {
        $this->addRule('bool', ['active']);
    }
}