<?php

namespace App\Model\Presenter;

use App\Model\AbstractModel;
use Phalcon\Mvc\User\Component;

/**
 * Class AbstractPresenter
 * @package App\Model\Presenter
 */
abstract class AbstractPresenter
    extends Component
{
    /**
     * @var bool
     */
    protected $useHtmlMarkup = true;

    /**
     * @var bool
     */
    protected $preserve = false;

    /**
     * @var array
     */
    protected $rows = [];

    /**
     * @var array
     */
    protected $rules = [];

    /**
     * @var array
     */
    protected static $modelsCache = [];

    /**
     * AbstractPresenter constructor.
     * @param array|null $rows
     */
    public function __construct(array $rows = null)
    {
        if ($rows) {
            $this->setRows($rows);
        }

        $this->init();
    }

    abstract protected function init();

    /**
     * @return array|null
     */
    public function present()
    {
        if (!is_array($this->rows)) {
            return $this->rows;
        }

        return $this->applyRules($this->rows);
    }

    /**
     * @param $rows
     * @return array
     */
    protected function applyRules($rows)
    {
        $results = [];

        foreach ($rows as $row) {

            $r = $row;

            foreach ($this->rules as $options) {

                $options['row'] = $row;

                $r = $this->applyRule($r, $options);
            }

            $results[] = $r;
        }

        return $results;
    }

    /**
     * @param $row
     * @param $options
     * @return mixed
     */
    protected function applyRule($row, $options)
    {
        $attributes = $options['attributes'];
        $method     = $options['method'];
        unset($options['method'], $options['attributes']);

        if (!is_array($attributes)) {
            $attributes = [$attributes];
        }

        foreach ($attributes as $attribute) {
            if ($row[$attribute] != null || !array_key_exists($attribute, $row)) {
                $options['attribute'] = $attribute;

                $preserved = $row[$attribute];

                $row[$attribute] = is_string($method)
                    ? $this->$method($row[$attribute], $options)
                    : $method($row[$attribute], $options)
                ;

                if ($this->isPreserve()) {
                    $row['_preserved'][$attribute] = $preserved;
                }
            }
        }

        return $row;
    }

    /**
     * @param string $method
     * @param array $attributes
     * @param array|null $options
     * @return AbstractPresenter
     */
    public function addRule($method, $attributes, array $options = null)
    {
        if ($options == null) {
            $options = [];
        }

        $options['method']     = $method;
        $options['attributes'] = $attributes;

        $this->rules[] = $options;

        return $this;
    }

    /**
     * @param int    $id
     * @param string $class
     * @return AbstractModel
     */
    protected static function getModel($id, $class)
    {
        $cacheId = $class . '-' . $id;

        if (!array_key_exists($cacheId, self::$modelsCache[$cacheId])) {

            $arguments = $id;
            if (!is_numeric($id)) {
                // string típusú elsődleges kulcsok
                $m         = $class::findFirst();
                $attribute = $m->getPrimaryKeyAttribute();
                $arguments = "{$attribute} = '$id'";
            }

            self::$modelsCache[$cacheId] = $class::findFirst($arguments);
        }

        return self::$modelsCache[$cacheId];
    }

    /**
     * @param $value
     * @return mixed
     * @throws \Phalcon\Exception
     */
    protected function bool($value)
    {
        if ($this->isUseHtmlMarkup()) {
            return $this->tag->label(
                $value
                , $this->translate->_(AbstractModel::getBooleanValue($value))
            );
        }

        return $this->translate->_(
            AbstractModel::getBooleanValue($value)
        );
    }

    /**
     * @param $value
     * @param array $options
     * @return string
     */
    protected function flag($value, $options = [])
    {
        $model = $options['model'];

        $flags     = explode(',', $value);
        $assembled = [];
        if ($this->isUseHtmlMarkup()) {
            foreach ($flags as $flag) {
                $assembled[] = $this->tag->label(
                    $flag
                    , $this->translate->_($model::getFlagValue($flag))
                );
            }

            return implode('&nbsp;', $assembled);
        }

        foreach ($flags as $flag) {
            $assembled[] = $this->translate->_(
                $model::getFlagValue($flag)
            );
        }

        return implode(', ', $assembled);
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function date($value)
    {
        return $this->tag->date($value);
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function datetime($value)
    {
        return $this->tag->datetime($value);
    }

    /**
     * @param $value
     * @param $options
     * @return mixed
     */
    protected function currency($value, $options = [])
    {
        return $this->tag->currency($value, $options['row']['currencyCode']);
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function percent($value)
    {
        return $this->tag->percent($value);
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function number($value)
    {
        return $this->tag->number($value);
    }

    /**
     * @return bool
     */
    public function isUseHtmlMarkup(): bool
    {
        return $this->useHtmlMarkup;
    }

    /**
     * @param bool $useHtmlMarkup
     * @return AbstractPresenter
     */
    public function setUseHtmlMarkup(bool $useHtmlMarkup): AbstractPresenter
    {
        $this->useHtmlMarkup = $useHtmlMarkup;
        return $this;
    }

    /**
     * @return array
     */
    public function getRows(): array
    {
        return $this->rows;
    }

    /**
     * @param array $rows
     * @return AbstractPresenter
     */
    public function setRows(array $rows): AbstractPresenter
    {
        $this->rows = $rows;
        return $this;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * @param array $rules
     * @return AbstractPresenter
     */
    public function setRules(array $rules): AbstractPresenter
    {
        $this->rules = $rules;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPreserve(): bool
    {
        return $this->preserve;
    }

    /**
     * @param bool $preserve
     * @return AbstractPresenter
     */
    public function setPreserve(bool $preserve): AbstractPresenter
    {
        $this->preserve = $preserve;
        return $this;
    }
}