<?php

namespace App\Model\Presenter;

/**
 * Class Group
 * @package App\Model\Presenter
 */
class Group
    extends AbstractPresenter
{
    /**
     *
     */
    protected function init()
    {
        $this->addRule('bool', 'active');
    }
}