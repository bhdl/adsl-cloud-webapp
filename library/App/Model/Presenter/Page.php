<?php

namespace App\Model\Presenter;

use App\Model\Page as PageModel;

/**
 * Class Page
 * @package App\Model\Presenter
 */
class Page
    extends AbstractPresenter
{
    /**
     *
     */
    protected function init()
    {
        $this->addRule('bool', 'active');
        $this->addRule('bool', 'slideshow');
        $this->addRule('bool', 'map');
    }
}