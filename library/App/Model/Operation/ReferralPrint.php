<?php

namespace App\Model\Operation;

use Phalcon\Exception;

/**
 * Beutalón végezhető műveletek:
 *
 * - minden műveletet tranzakcióban kell futtatni!
 * - a művelet ha nem dob kivételt, akkor minden rendben van
 * - bizonyos esetekben van visszatérési érték, egyébként void
 *
 * Class ReferralPrint
 * @package App\Model\Operation
 */
class ReferralPrint
    extends AbstractOperation
{
    protected $template;

    /**
     * @param array $args
     * @throws Exception
     * @throws \Exception
     */
    public function print($args = [])
    {
        $inline = array_key_exists('inline', $args) && $args['inline'] == true;
        $flag   = array_key_exists('flag', $args) && $args['flag'] !== null;

        if ($flag) {
            // csak akkor kapja a flaget, ha nem előnézetben vagyunk
            $this->model->addFlag($args['flag']);
            $this->model->update();
        }

        $template = new \App\Export\Pdf\Template\ReferralPrint();
        $template->setReferral($this->model);
        $pdf = $template->getPdf();

        ob_end_clean();
        $pdf->send($template->getFileName(), $inline);
    }

}