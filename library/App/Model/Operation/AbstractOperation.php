<?php

namespace App\Model\Operation;
use Phalcon\Exception;
use Phalcon\Mvc\User\Component;

/**
 * Class AbstractOperation
 * @package App\Model\Operation
 */
abstract class AbstractOperation
    extends Component
{
    /**
     * @var \App\Model\AbstractModel
     */
    protected $model;

    /**
     * @const string
     */
    const OPERATION_DELETE = 'delete';

    /**
     * @const string
     */
    const OPERATION_MODIFY = 'modify';

    /**
     * @const string
     */
    const OPERATION_CREATE = 'create';

    /**
     * @const string
     */
    const OPERATION_SEND = 'send';

    /**
     * @const string
     */
    const OPERATION_RESTORE = 'restore';

    /**
     * @const string
     */
    const OPERATION_PRINT = 'print';

    /**
     * @const string
     */
    const OPERATION_CONVERT = 'convert';

    /**
     * Constructor.
     *
     * @param \App\Model\AbstractModel $model
     */
    public function __construct(\App\Model\AbstractModel $model = null)
    {
        if ($model) {
            $this->model = $model;
        }
    }

    /**
     * @param $operation
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    public function run($operation, $args = [])
    {
        if (!method_exists($this, $operation)) {
            throw new Exception(
                'Nem létező művelet (operation)' . $operation
            );
        }

        return $this->$operation($args);
    }

    /**
     * @return \App\Model\AbstractModel
     */
    public function getModel(): \App\Model\AbstractModel
    {
        return $this->model;
    }

    /**
     * @param \App\Model\AbstractModel $model
     * @return AbstractOperation
     */
    public function setModel(\App\Model\AbstractModel $model): AbstractOperation
    {
        $this->model = $model;
        return $this;
    }
}