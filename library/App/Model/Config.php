<?php

namespace App\Model;

/**
 * Class Config
 * @package App\Model
 */
class Config
    extends AbstractLogModel
{
    /**
     * @var int
     */
    public $configId;

    /**
     * @var int
     */
    public $outDatedPasswordDay;

    /**
     * @var int
     */
    public $languageId;

    /**
     * @var int
     */
    public $currencyId;

    /**
     * @var string
     */
    public $contactEmail;

    /**
     * @var string
     */
    public $contactName;

    /**
     * @var string
     */
    public $contactAddress;

    /**
     * @var string
     */
    public $contactPhone;

    /**
     * @var string
     */
    public $testEmail;

    /**
     * @var string
     */
    public $testName;

    /**
     * @return string
     */
    public function getName()
    {
        return 'N/A';
    }

    /**
     * Config érték lekérdezése (kulcs alapján)
     *
     * @param string $key
     * @return null|string
     */
    public static function get($key)
    {
        $model = static::findFirst();

        if ($model) {
            return $model->$key;
        }

        return null;
    }
}