<?php

namespace App\Model;

use Phalcon\Utils\Slug;

class AclRole
    extends AbstractLogModel
{
    /**
     * @var string
     */
    public $roleId;

    /**
     * @var string
     */
    public $roleName;

    /**
     * @var string
     */
    const ROLE_ID_DOCTOR = 'doctor';
    
    /**
     * @var string
     */
    const ROLE_ID_PRAXIS_ADMIN = 'praxisAdmin';

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->hasMany('roleId', __NAMESPACE__ . '\AclMatrix', 'roleId', array(
            'alias' => 'matrix'
        ));

        $this->hasMany('roleId', __NAMESPACE__ . '\GroupHasAclRole', 'roleId', array(
            'alias' => 'groupHasAclRoles',
            'foreignKey' => [
                'message'  => 'A rekord nem törölhető, mert már tartozik hozzá egyéb adat.'
            ]
        ));
    }

    /**
     * beforeValidationOnCreate
     */
    public function beforeValidationOnCreate()
    {
        if($this->roleId === null) {
            $this->roleId = Slug::generate($this->roleName);
        }
    }

    /**
     * @param string $resourceId
     * @param string $actionId
     * @return AclMatrix
     */
    public function addAclRole($resourceId,$actionId)
    {
        $model = new AclMatrix();

        $model->roleId = $this->roleId;
        $model->resourceId =  $resourceId;
        $model->actionId =  $actionId;

        $model->save();

        return $model;
    }

    /**
     * @param array $roles
     * @return AclRole
     */
    public function addAclRoles($roles)
    {
        foreach ($roles as $resourceId => $actions) {
            foreach($actions as $actionId => $value){
                $this->addAclRole($resourceId,$actionId);
            }
        }

        return $this;
    }

    /**
     * @return AclRole
     */
    public function clearAclMatrix()
    {
        foreach($this->matrix as $model){
            $model->delete();
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->roleName;
    }
}