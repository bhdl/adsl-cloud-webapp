<?php

namespace App\Model;

/**
 * Class AclAction
 * @package App\Model
 */
class AclAction extends AbstractLogModel
{
    /**
     * @var string
     */
    public $actionId;

    /**
     * @var string
     */
    public $actionName;

    /**
     * @var string
     */
    const ACTION_CREATE = 'create';

    /**
     * @var string
     */
    const ACTION_INDEX = 'index';

    /**
     * @var string
     */
    const ACTION_VIEW = 'view';

    /**
     * @var string
     */
    const ACTION_SEARCH = 'search';

    /**
     * @var string
     */
    const ACTION_AJAX = 'ajax';

    /**
     * @var string
     */
    const ACTION_DELETE = 'delete';

    /**
     * @var string
     */
    const ACTION_RESTORE = 'restore';

    /**
     * @var string
     */
    const ACTION_MODIFY = 'modify';

    /**
     * @var string
     */
    const ACTION_SEND = 'send';

    /**
     * @var string
     */
    const ACTION_PRINT = 'print';

    /**
     * @var string
     */
    const ACTION_PREVIEW = 'preview';

    /**
     * @var string
     */
    const ACTION_READ = 'read';

    /**
     * @var string
     */
    const ACTION_TRANSLATE = 'translate';

    /**
     * @var string
     */
    const ACTION_POSITION = 'position';

    /**
     * @var string
     */
    const ACTION_DOCUMENT = 'document';

    /**
     * @var string
     */
    const ACTION_CONVERT = 'convert';

    /**
     * @var string
     */
    const ACTION_TRANSACTION = 'transaction';

    /**
     * @var string
     */
    const ACTION_DOWNLOAD = 'download';

    /**
     * @var string
     */
    const ACTION_INDEX_HAS_MANY = 'indexHasMany';

    /**
     * @var string
     */
    const ACTION_DATA_TABLE_HAS_MANY = 'dataTableHasMany';

    /**
     * @var string
     */
    const ACTION_CREATE_HAS_MANY = 'createHasMany';

    /**
     * @var string
     */
    const ACTION_DELETE_HAS_MANY = 'deleteHasMany';

    /**
     * @var string
     */
    const ACTION_VIEW_HAS_MANY = 'viewHasMany';

    /**
     * @var string
     */
    const ACTION_MODIFY_HAS_MANY = 'modifyHasMany';

    /**
     * @var string
     */
    const ACTION_INDEX_HAS_MANY_TO_MANY = 'indexHasManyToMany';

    /**
     * @var string
     */
    const ACTION_DATA_TABLE_HAS_MANY_TO_MANY = 'dataTableHasManyToMany';

    /**
     * @var string
     */
    const ACTION_CREATE_HAS_MANY_TO_MANY = 'createHasManyToMany';

    /**
     * @var string
     */
    const ACTION_DELETE_HAS_MANY_TO_MANY = 'deleteHasManyToMany';

    /**
     * @var string
     */
    const ACTION_VIEW_HAS_MANY_TO_MANY = 'viewHasManyToMany';

    /**
     * @var string
     */
    const ACTION_MODIFY_HAS_MANY_TO_MANY = 'modifyHasManyToMany';

    /**
     * @var string
     */
    const ACTION_ASSIGN_HAS_MANY_TO_MANY = 'assignHasManyToMany';

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->hasMany('actionId', __NAMESPACE__ . '\AclMatrix', 'actionId', array(
            'alias' => 'matrix'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->actionName;
    }
}