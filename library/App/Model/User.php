<?php

namespace App\Model;

use App\Model\Traits\SessionStorage;
use App\Support\HashMap;
use Phalcon\Exception;

/**
 * Class User
 * @package App\Model
 */
class User
    extends AbstractLogModel
{
    use SessionStorage;

    /**
     * @var integer
     */
    public $userId;

    /**
     * @var integer
     */
    public $groupId;

    /**
     * @var integer
     */
    public $languageId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $active;

    /**
     * @var string
     */
    public $passwordChange = self::NO;

    /**
     * @var string
     */
    public $createdAt;

    /**
     * @var null|Language
     */
    protected $interfaceLanguage;

    /**
     * @const string
     */
    const IMAGE_TYPE_AVATAR = 'avatar';

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        // felhasználói csoport
        $this->belongsTo('groupId', __NAMESPACE__ . '\Group', 'groupId', array(
            'alias' => 'group'
            , 'reusable'  => true
        ));

        // naplózás kapcsolatok
        $this->hasMany('userId', __NAMESPACE__ . '\Log', 'userId', array(
            'alias' => 'logs'
        ));

        // token-nek
        $this->hasMany('userId', __NAMESPACE__ . '\UserToken', 'userId', array(
            'alias' => 'userTokens'
        ));

        // language
        $this->belongsTo('languageId', __NAMESPACE__ . '\Language', 'languageId', array(
            'alias' => 'language'
            , 'reusable'  => true
        ));

        // orvos
        $this->belongsTo('userId', Doctor::class, 'userId', array(
            'alias' => 'doctor'
            , 'reusable'  => true
        ));
    }

    /**
     * beforeCreate
     */
    public function beforeCreate()
    {
        // hash
        $this->password = $this->getPasswordHash($this->password);
    }

    /**
     * afterCreate
     */
    public function afterCreate()
    {
        // jelszót elmentjük a password táblába
        if ($this->passwordChange != self::YES) {
            $this->savePassword();
        }
    }

    /**
     * beforeUpdate
     * @throws Exception
     */
    public function beforeUpdate()
    {
        // Jelszó ellenörzés, legutolsó 3-mal
        if ($this->hasChanged('password')) {
            // plain jelszóval ellenőrzés
            if ($this->lastThreePasswordCheck($this->password)) {
                throw new Exception('A jelszó már fel volt használva előzőleg!');
            }

            // hash
            $this->password = $this->getPasswordHash($this->password);

            // log
            $this->savePassword();
        }
    }

    /**
     * @param string $password
     * @return string
     */
    protected function getPasswordHash($password)
    {
        return $this->getDI()->get('security')->hash($password);
    }

    /**
     * Jelszó mentése a user_password táblába
     */
    protected function savePassword()
    {
        $userPassword = new UserPassword();
        $userPassword->userId     = $this->userId;
        $userPassword->password   = $this->password;
        $userPassword->modifiedAt = date('Y-m-d H:i:s');
        $userPassword->save();
    }

    /**
     * Új jelszót ellenörzi, hogy ezt a felhasználó nem e használta a legutolsó 3 alkalommal
     * @param string $password plain text jelszó (nem hash)
     * @return bool
     */
    protected function lastThreePasswordCheck(string $password): bool
    {
        /*
         * Régi jelszavak lekérdezése (utolsó 3)
         */
        $oldPasswords = UserPassword::find([
            "conditions" => "userId = :userId:",
            "bind" => ['userId' => $this->userId],
            "order" => "modifiedAt DESC",
            "limit" => 3
        ]);

        if ($oldPasswords == null) {
            return false;
        }

        $security = $this->getDI()->get('security');

        /*
         * Új jelszó ellenörzése a legutolsó 3 jelszóval
         */

        foreach($oldPasswords as $oldPassword) {
            if($security->checkHash($password, $oldPassword->password)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        if (empty($password)) {
            return $this;
        }

        $this->password = $password;

        return $this;
    }

    /**
     * Vendég felhasználói fiók megkeresése
     *
     * @return bool|\Phalcon\Mvc\ModelInterface
     * @throws Exception
     * @throws \App\Model\Exception
     */
    public static function findGuest()
    {
        $group = Group::findGuest();

        $user = $group->getUsers()->getFirst();

        if (!is_object($user)) {
            throw new Exception('Nincs létrehozva vendég felhasználói fiók.');
        }

        return $user;
    }

    /**
     * @return boolean
     */
    public function isGuest()
    {
        return $this->getGroup()->isGuest();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return strcmp($this->active, User::YES) === 0;
    }

    /**
     * Ellenőrzi, hogy a felhasználó bejelentkezhet-e a rendszerbe.
     *
     * @return array
     */
    public function checkStatus()
    {
        $translate = $this->getDI()->get('translate');

        $messages = [];

        if(!$this->isActive()) {
            $messages[] = $translate->_('A felhasználói fiók nem aktív!');
        }

        if (!$this->getGroup()->isActive()) {
            $messages[] = $translate->_('A felhasználói csoport nem aktív!');
        }

        return $messages;
    }

    /**
     * Ellenörzi, hogy a felhasználó jelszava elavult e
     * @return bool
     */
    public function isPasswordOutdated(): bool
    {
        $isOutdated = false;

        /**
         * Csak abban az esetben vizsgáljuk, ha a config fájlban be van állítva az elévülési idő
         */
        $outDatedPasswordDay = Config::get('outDatedPasswordDay');
        if($outDatedPasswordDay > 0) {
            /**
             * @var \App\Model\UserPassword $lastchangePassword
             */
            $userPassword = UserPassword::findFirst([
                "conditions" => "userId = :userId:",
                "bind" => ['userId' => $this->userId],
                "order" => "modifiedAt DESC"
            ]);

            /*
             * Még nem változtatott ...
             */
            if ($userPassword == null) {
                $isOutdated = true;
            } else {
                $currentDate = new \DateTime();
                $date = new \DateTime($userPassword->modifiedAt);

                if($currentDate->diff($date)->days > $outDatedPasswordDay) {
                    $isOutdated = true;
                }
            }
        }

        return $isOutdated;
    }

    /**
     * Proxy
     *
     * @return bool
     * @see Group::isAclRoleDoctor()
     */
    public function isAclRoleDoctor()
    {
        return $this->getGroup()->isAclRoleDoctor();
    }

    /**
     * @return bool
     */
    public function isDoctor()
    {
        return $this->isAclRoleDoctor() && $this->getDoctor();
    }

    /**
     * Kép url lekérdezése
     *
     * @param string  $imageType
     * @param boolean $url
     * @param boolean $usePlaceHolder
     * @return string
     */
    public function getImage($imageType = self::IMAGE_TYPE_AVATAR, $url = true, $usePlaceHolder = false)
    {
        $config = $this->getDI()->get('config');
        $app    = $this->getDI()->get('app');

        $placeHolder = null;

        if ($usePlaceHolder) {
            $placeHolder = '/assets/images/placeholder.jpg';
            if (!$url) {
                $placeHolder = $app->getAppPath($placeHolder);
            }
        }

        $userHash = md5($this->userId);

        $path = $config->user->{$imageType}->path . $userHash . '.jpg';
        if (file_exists($path)) {
            if ($url) {
                return $app->getAppUrl($config->user->{$imageType}->url . $userHash . '.jpg?time=' . time());
            }
            return $path;
        }

        return $placeHolder;
    }

    /**
     * Profilkép url lekérdezése
     *
     * @param boolean $usePlaceHolder
     * @return string
     */
    public function getAvatarImageUrl($usePlaceHolder = true)
    {
        return $this->getImage(self::IMAGE_TYPE_AVATAR, true, $usePlaceHolder);
    }

    /**
     * @param boolean $usePlaceHolder
     * @return string
     */
    public function getAvatarImagePath($usePlaceHolder = false)
    {
        return $this->getImage(self::IMAGE_TYPE_AVATAR, false, $usePlaceHolder);
    }

    /**
     * @return Language
     */
    public function getInterfaceLanguage()
    {
        if ($this->interfaceLanguage != null) {
            return $this->interfaceLanguage;
        }

        $language = null;

        // elsőleges a munkamenet (session)
        $languageId = $this->getSessionVar('languageId');
        if (is_numeric($languageId)) {
            $language = Language::findFirst($languageId);
        }

        // másodlagos a felhasználó alapértelmezett nyelve (user tábla)
        if ($language == null) {
            $language = $this->getLanguage();
        }

        if ($language == null) {
            // nincs beállítva az user táblában, adjuk az alapértelmezettet
            $language = Language::findDefault();
        }

        $this->interfaceLanguage = $language;

        return $this->interfaceLanguage;
    }

    /**
     * @param \App\Model\Language $language
     * @return User
     */
    public function setInterfaceLanguage($language)
    {
        return $this->setSessionVar('languageId', $language->languageId);
    }

    /**
     * @return boolean|UserToken
     */
    public function getActiveUserToken()
    {
        return $this
            ->getUserTokens('expirationAt IS NULL OR expirationAt < \'' . date('Y-m-d') . '\'')
            ->getFirst()
        ;
    }
}
