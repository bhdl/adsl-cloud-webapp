<?php

namespace App\Model;

use Phalcon\Exception;

/**
 * Class Log
 * @package App\Model
 */
class Log
    extends AbstractModel
{
    /**
     * @const integer
     */
    const EMERGENCY = 0;

    /**
     * @const integer
     */
    const CRITICAL = 1;

    /**
     * @const integer
     */
    const ALERT = 2;

    /**
     * @const integer
     */
    const ERROR = 3;

    /**
     * @const integer
     */
    const WARNING = 4;

    /**
     * @const integer
     */
    const NOTICE = 5;

    /**
     * @const integer
     */
    const INFO = 6;

    /**
     * @const integer
     */
    const DEBUG = 7;

    /**
     * @const integer
     */
    const CUSTOM = 8;

    /**
     * @const integer
     */
    const SPECIAL = 9;

    /**
     * @var integer
     */
    public $logId;

    /**
     * @var integer
     */
    public $userId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var integer
     */
    public $priority;

    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $exception;

    /**
     * @var string
     */
    public $createdAt;

    /**
     * @var array
     */
    protected static $priorities = [
        self::EMERGENCY     => 'Hiba - vészelyzet'
        , self::CRITICAL    => 'Hiba - kritikus'
        , self::ALERT       => 'Hiba - súlyos'
        , self::ERROR       => 'Hiba - normál'
        , self::WARNING     => 'Figyelmeztetés'
        , self::NOTICE      => 'Értesítés'
        , self::INFO        => 'Információ'
        , self::DEBUG       => 'Hibakeresés'
        , self::CUSTOM      => 'Beállítás'
        , self::SPECIAL     => 'Speciális'
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->hasMany('logId', __NAMESPACE__ . '\LogModel', 'logId', array(
            'alias' => 'logModels'
            , 'foreignKey' => array(
                'message'  => 'A rekord nem törölhető, mert már tartozik hozzá egyéb adat.'
            )
        ));

        $this->belongsTo('userId', __NAMESPACE__ . '\User', 'userId', array(
            'alias'      => 'user'
            , 'reusable' => true
        ));
    }

    /**
     * @return array
     */
    public static function getPriorities() : array
    {
        return self::$priorities;
    }

    /**
     * @param int $priority
     * @return string
     * @throws Exception
     */
    public static function getPriorityValue(int $priority) : string
    {
        if (!array_key_exists($priority, static::$priorities)) {
            throw new Exception('Nincs ilyen priorítás a priority értékek között: ' . $priority);
        }

        return static::$priorities[$priority];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->logId;
    }
}