<?php

namespace App\Support;

/**
 * Class Str
 * @package App\Support
 */
final class Str
{
	/**
	 * Split the given string by the given delimiters
	 *
	 * @param string $string
	 * @param array $delimiters
	 * @return array
	 */
	public static function chunkData($string, array $delimiters)
	{

		return preg_split(
			'/(' . implode('|', array_map('preg_quote', $delimiters)) . ')/',
			$string,
			null,
			PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY
		);
	}

	/**
	 * Transform the first character of the given string to uppercase
	 *
	 * @param string $string
	 * @return string
	 */
	public static function firstUpper($string)
	{
		return mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
	}

    /**
     * Transform the first character of the given string to lowercase
     *
     * @param $string
     * @return string
     */
	public static function firstLower($string)
    {
        return mb_strtolower(mb_substr($string, 0, 1)) . mb_substr($string, 1);
    }

	/**
	 * Transform line endings to \n character.
	 *
	 * @param string $s
	 * @return string
	 */
	public static function rowLineBreakStandardization($s)
	{
		return preg_replace('/\r\n|\n\r|\r/', "\n", $s);
	}

	/**
	 * Add '/' character to the end of path string
	 *
	 * @param string $path
	 * @return string
	 */
	public static function pathString($path)
	{
		return rtrim($path, '/') . '/';
	}

	/**
	 * Remove accent characters (UTF-8)
	 *
	 * @param string $s
	 * @return string
	 */
	public static function removeAccent($s)
	{
		return str_replace(
			array('ű', 'Ű', 'ő', 'Ő'),
			array('u', 'U', 'o', 'O'),
			preg_replace(
				'~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i',
				'$1',
				htmlentities($s, ENT_QUOTES, 'UTF-8')
			)
		);
	}

	/**
	 * If the given string is encoded in ISO-8859-2, then convert it to UTF-8
	 *
	 * @param string $s
	 * @return string
	 */
	public static function removeLatin2($s)
	{
		return mb_detect_encoding($s, 'UTF-8, ISO-8859-2', true) == 'ISO-8859-2'
            ? mb_convert_encoding($s, 'UTF-8', 'ISO-8859-2') : $s
        ;
	}

	/**
	 * Transform underscore formatted string to camelCase
	 *
	 * @param string $s
	 * @return string
	 */
	public static function underscoreToCamelCase($s)
	{
	    return preg_replace('/((.)_(.))/e', "$2 . mb_strtoupper($3)", $s);
	}

	/**
	 * Generate a random hash with the given length
	 * @param int $length length of
	 * @return string
	 */
	public static function randomHash($length = 8)
	{
	    $chars  = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	    $string = '';
	    for ($i = 0; $i < $length; $i++) {
	        $num     = floor(rand(0, 1000) / 1000 * strlen($chars));
	        $string .= substr($chars, $num, 1);
	    }
	    return $string;
	}

    /**
     * Transform the given string to array
     *
     * @param string $string
     * @return array
     */
    public static function toArray($string)
    {
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    /**
     * Shuffle the characters of the given string randomly
     *
     * @param string $string
     * @return string
     */
    public static function shuffle($string)
    {
        $chars = self::toArray($string);

        shuffle($chars);

        return implode('', $chars);
    }
}
