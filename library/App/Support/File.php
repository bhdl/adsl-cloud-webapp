<?php

namespace App\Support;

use Phalcon\Exception;

/**
 * Class File
 * @package App\Support
 */
final class File
{
    /**
     * Rekurzív mappa törlés
     *
     * @param string $dir
     * @throws Exception
     */
    public static function rmDirRecursive($dir)
    {
        if (!is_dir($dir)) {
            return;
        }

        $files = array_diff((array)@scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            $fileOrDir = $dir . DIRECTORY_SEPARATOR . $file;
            if (is_dir($fileOrDir)) {
                self::rmDirRecursive($fileOrDir);
                continue;
            }
            if (!@unlink($fileOrDir)) {
                throw new Exception('Nem sikerült a fájl törlése: ' . $fileOrDir);
            }
        }

        if (!@rmdir($dir)) {
            throw new Exception('Nem sikerült a mappa törlése: ' . $dir);
        }
    }

    /**
     * Fájl neve, kiterjesztés nélkül. Ha a fájlnak nincs
     * kiterjesztése, akkor a teljes névvel tér vissza
     *
     * @param string $fileName
     * @return string
     */
    public static function removeFileExtension($fileName)
    {
        $result = self::splitFileByExtension($fileName);

        if (!$result) {
            return $fileName;
        }

        return $result[0];
    }

    /**
     * A fájl felbontása névre és kiterjesztésre
     *
     * @param string $fileName
     * @return array|boolean 0 - név, 1 - kiterjesztés
     */
    public static function splitFileByExtension($fileName)
    {
        $position = mb_strrpos($fileName, '.');
        if ($position === false) {
            return false;
        }

        return array(
            mb_substr($fileName, 0, $position)
            , mb_substr($fileName, $position + 1)
        );
    }

    /**
     * @param int $bytes
     * @return string
     */
    public static function formatSize($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' kB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}