<?php

namespace App\Support;

use App\Model\Language;

/**
 * Class Date
 * @package App\Support
 */
final class Date
{
    /**
     * Mértékegységek
     * @var array
     */
    protected $_unitMap = array(
        'weeks'     => 'hét'
        , 'days'    => 'nap'
        , 'hours'   => 'óra'
        , 'minutes' => 'perc'
        , 'seconds' => 'másodperc'
    );

    /**
     * Másodpercek átalakítása string formátumra
     *
     * Pl. 3600 => 1 óra, 3660 => 1 óra, 1 perc
     * @param int $seconds
     * @return string
     */
    public function secondsToString($seconds)
    {
        $result = $this->_splitSeconds($seconds);
        $str    = '';
        foreach ($result as $unit=>$value) {
            if ($value > 0) {
                $str .= "{$value} {$this->_unitMap[$unit]}, ";
            }
        }
        return mb_strlen($str) > 0 ? substr($str, 0, -2): $str;
    }

    /**
     * Tömb formára hozza a másodperceket
     * @param int $seconds
     * @return array
     */
    protected function _splitSeconds($seconds)
    {
        $seconds = (int)$seconds;

        $units = array(
            "weeks"   => 7*24*3600,
            "days"    =>   24*3600,
            "hours"   =>      3600,
            "minutes" =>        60,
            "seconds" =>         1,
        );

        foreach ($units as &$unit) {
            $quot     = intval($seconds / $unit);
            $seconds -= $quot * $unit;
            $unit     = $quot;
        }

        return $units;
    }

    /**
     * UNIX timestamp => formázott dátum intervallum megjelenítés
     *
     * @param int|string    $from
     * @param int|string    $to
     * @param string $locale egyelőre nincs használva, magyar locale szerint működik
     * @return string
     */
    public static function dateInterval($from, $to, $locale = 'hu_HU')
    {
        if(is_string($from)) {
            $from = strtotime($from);
        }

        if(is_string($to)) {
            $to = strtotime($to);
        }

        $fromFormatted = strftime('%Y. %B %e.', $from);

        if (strcmp(date('Y', $from), date('Y', $to)) !== 0) {
            return $fromFormatted . ' - ' . strftime('%Y. %B %e.', $to);
        }

        if (strcmp(date('m', $from), date('m', $to)) !== 0) {
            return $fromFormatted . ' - ' . strftime('%B %e.', $to);
        }

        if (strcmp(date('d', $from), date('d', $to)) !== 0) {
            return $fromFormatted . ' - ' . strftime('%e.', $to);
        }

        return $fromFormatted;
    }

    /**
     * Megállapítja, hogy a bemenetként kapott időbélyeg a magyar
     * munkaszüneti napok valamelyikére esik-e
     * @param $time
     * @return bool
     */
    public static function isHungarianHoliday($time = null)
    {
        if ($time == null) {
            $time = time();
        }

        $easterTime   = easter_date(date('Y', $time));
        $easterMonday = mktime(0, 0, 0, date('m', $easterTime), date('d', $easterTime) + 1, date('Y', $easterTime));

        return in_array(date("m-d", $time), array(
            '01-01'
            , '03-15'
            , '05-01'
            , '08-20'
            , '10-23'
            , '11-01'
            , '12-25'
            , '12-26'
            , date('m-d', $easterMonday)
            , date('m-d', $easterMonday + 4233600) // pünkösd
        ));
    }

    /**
     * FIXME: nagy szögelés és egyelőre nem tudom hova lehetne tenni. Talán jó lenne a
     * language táblába helyezni a dátum/ idő/ szám formátumot és az alapján írni egy
     * transzformációs osztályt.
     * választott nyelvnek megfelelő dátum formátum
     *
     * @param $value
     * @return bool|\DateTime
     */
    public static function dateFromLocalized($value)
    {
        // alapértelmezett formátum
        $format = 'Y-m-d';

        // van megfelelő formátum?
        foreach (Language::find() as $language) {
            if (preg_match($language->dateRegexPattern, $value)) {
                $format = $language->dateFormat;
                break;
            }
        }

        return \DateTime::createFromFormat(
            $format
            , $value
        );
    }

    /**
     * választott nyelvnek megfelelő idő formátum
     * @param $value
     * @return bool|\DateTime
     */
    public static function timeFromLocalized($value)
    {
        // alapértelmezett formátum
        $format = 'H:i:s';

        // van megfelelő formátum?
        foreach (Language::find() as $language) {
            if (preg_match($language->timeRegexPattern, $value)) {
                $format = $language->timeFormat;
                break;
            }
        }

        return \DateTime::createFromFormat(
            $format
            , $value
        );
    }

    /**
     * választott nyelvnek megfelelő dátum és idő formátum
     * @param $value
     * @return bool|\DateTime
     */
    public static function dateTimeFromLocalized($value)
    {
        // alapértelmezett formátum
        $format = 'Y-m-d H:i';

        // van megfelelő formátum?
        foreach (Language::find() as $language) {
            if (preg_match(substr($language->dateRegexPattern,0,-2).' '.substr($language->timeRegexPattern,2), $value)) {
                $format = $language->dateFormat. ' '.$language->timeFormat;
                break;
            }
        }

        return \DateTime::createFromFormat(
            $format
            , $value
        );
    }
}