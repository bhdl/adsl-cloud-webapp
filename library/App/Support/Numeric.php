<?php

namespace App\Support;

/**
 * Class Numeric
 * @package App\Support
 */
final class Numeric
{
	/**
     * Megmondja egy stringről, hogy egész számot tartalmaz-e
     * @param string $string
     * @return boolean
     */
    public static function isInt($string)
    {
        return intval($string) == $string;
    }

	/**
	 * Törtszám átalakítása szabványra: 1.23
	 *
	 * @param mixed $value
	 * @return mixed
	 */
	public static function float($value)
	{
		return str_replace(array(',', ' '), array('.', ''), $value);
	}

    /**
	 * A paramétert átalakitja floattá.
	 * null, '', 'null' => null
	 * @param mixed $string
	 * @return NULL|boolean|number
	 */
	public static function strToFloat($string)
	{
		return
			$string === null || $string == 'null' || $string === ''
			// ha valamilyen formában null
			? null
			// ha nem null
			: is_numeric($string)
				// ha numerikus
				// intval kell, mert az csak 10es számrendszer alapján konvertál; ha hexa/octa, ha elírt az érték, akkor el fog térni a $string * 1-től
				? (($val = floatval($string)) == $string * 1
					// rendben van
					? $val
					// hibás megadás
					: false
				)
				// nem numerikus
				: false;
	}

	/**
	 * A paramétert átalakitja integerré. Se floatot, se üres stringet, se hibás adatot nem fogad el
	 * null, '', 'null' => null
	 * @param mixed $string
	 * @return NULL|boolean|number
	 */
	public static function strToInt($string)
	{
		return
			$string === null || $string == 'null' || $string === ''
			// ha valamilyen formában null
			? null
			// ha nem null
			: is_numeric($string)
				// ha numerikus
				// intval kell, mert az csak 10es számrendszer alapján konvertál; ha hexa/octa, ha elírt az érték, akkor el fog térni a $string * 1-től
				? (($val = intval($string)) == $string * 1
					// rendben van
					? $val
					// hibás megadás
					: false
				)
				// nem numerikus
				: false
		;
	}

	/**
	 * Numerikusan kisebb vagy egyenlő összehasonlítás (null => true)
	 *
	 * @param number $value
	 * @param number $max
	 * @return boolean
	 */
	public static function lowerThanOrEqual($value, $max)
	{
		return $max === null || $value <= $max;
	}

    /**
     * Magyar fizetési szabályok szerint kerekít.
     * @param number $value
     * @return number
     */
    public static function roundHungarianCurrency($value)
    {
        $value = round($value, 0, PHP_ROUND_HALF_UP);

        $residual = $value % 10;

        switch($residual) {
            case 1: $value-=1; break;
            case 2: $value-=2; break;
            case 3: $value+=2; break;
            case 4: $value+=1; break;
            case 6: $value-=1; break;
            case 7: $value-=2; break;
            case 8: $value+=2; break;
            case 9: $value+=1; break;
        }

        return $value;
    }

    /**
     * Áfa meghatározása bruttó árból
     *
     * @param float $rate
     * @param float $gross
     * @return float
     */
    public static function vatFromGross($rate, $gross)
    {
        return $gross - $gross / (1 + $rate / 100);
    }

    /**
     * Áfa meghatározása nettó árból
     *
     * @param float $rate
     * @param float $net
     * @return float
     */
    public static function vatFromNet($rate, $net)
    {
        return $net * $rate / 100;
    }

    /**
     * 2-es pontosságú kerekítés (minden decimal érték 2 tizedesjegyik tárolja a számokat)
     *
     * @param float $value
     * @return float
     */
    public static function round($value)
    {
        return round($value, 2);
    }

    /**
     * @param string $value
     * @param null $locale
     * @return float
     */
    public static function numericFromLocalized($value, $locale = null)
    {
        if ($value && preg_match('/^\d*\,?\d*$/', $value)) {
            $value = floatval(
                str_replace(',', '.', $value)
            );
        }

        return $value;
    }
}