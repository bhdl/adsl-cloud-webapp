<?php

namespace App\Support;

/**
 * Class HashMap
 * @package App\Support
 */
final class HashMap
{
    /**
     * @param array $array
     * @param null $stdClass
     * @return null|\stdClass
     */
    public static function assignToStdClass(array $array, $stdClass = null)
    {
        if ($stdClass == null) {
            $stdClass = new \stdClass();
        }

        foreach ($array as $k=>$v) {
            $stdClass->$k = $v;
        }

        return $stdClass;
    }

    /**
     * Ugyanazokat az értékeket tartalmazza? (sorrend és kulcs független)
     *
     * @param array $array1
     * @param array $array2
     * @return boolean
     */
    public static function areSameValues(&$array1, &$array2)
    {
    	return count($array1) == count($array2) && !array_diff($array1, $array2);
    }

    /**
     * Visszaadja az első olyan paramétert ami nem null, vagy null-t, ha mindegyik
     * paraméter null (is_null()==true)
     * @param 1..n mixed
     * @return mixed|NULL
     */
    public static function coalesce()
    {
        $args = func_get_args();

        foreach ($args as $arg) {
            if (!is_null($arg)) {
                return $arg;
            }
        }

        return null;
    }

    /**
     * Többdimenziós tömb adott indexén található elemeket gyűjti össze (max 2)
     *
     * <code>
     * $test = array(array('a'=>1, 'b'=>2), array('a'=>3, 'b'=>6));
     * print_r($test, 'b');
     * // eredmény: array(2, 6);
     * </code>
     *
     * @param array $array
     * @param string $index
     * @return array üres tömb, ha nincs találat
     */
    public static function getArrayValuesByIndex(array $array, $index)
    {
        $result = array();
        foreach ($array as $v) {
            if (is_array($v) && isset($v[$index])) {
                $result[] = $v[$index];
            }
        }
        return $result;
    }

    /**
     * Többdimenziós tömb adott indexén található elemeket gyűjti össze (akárhány dimanziós)
     *
     * @param $array
     * @param $index
     * @return array
     */
    public static function getArrayValuesByIndexMulti($array, $index)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$index])) {
                $results[] = $array[$index];
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, self::getArrayValuesByIndexMulti($subarray, $index));
            }
        }

        return $results;
    }

    /**
     * A $array-ból azokat az elemeket adja vissza, amik kulcsa szerepel a keys-ben
     *
     * @param array $array
     * @param array $keys
     * @return array
     */
    public static function intersectByKeys(array $array, array $keys)
    {
    	return array_intersect_key($array, array_flip($keys));
    }

    /**
     * Bejárható-e a tömb
     * @param $array
     * @return boolean
     */
    public static function isFilled($array)
    {
        return is_array($array) && count($array) > 0;
    }

    /**
     * Bejárható-e a tömb
     * @param $array
     * @return boolean
     */
    public static function isIterable($array)
    {
        return self::isFilled($array);
    }

    /**
     * Kulcs-szerinti ékezetnélküli rendezés (UTF8)
     *
     * @param array $array
     * @return bool
     */
    public static function keysortInsensitive(&$array)
    {
    	return uksort($array, array(self, '_keysortInsensitive'));
    }

    /**
     * Másolás kulcsok alapján (forrás_kulcs => cél_kulcs tömb)
     *
     * @param array $array
     * @param array $mapping
     * @return array
     */
    public static function mapping(array $array, array $mapping)
    {
    	$list = array();

    	foreach ($mapping as $to => $from)
    	{
    		$list[$to] = $array[$from];
    	}

    	return $list;
    }

    /**
     * Kulcsok átírása sormező értékre
     *
     * @param array $array
     * @param string $keyField
     * @return array
     */
    public static function rekeyByField(array $array, $keyField)
    {
    	$list = array();

    	while ($array)
    	{
			$item = array_shift($array);
			$list[$item[$keyField]] = $item;
    	}

    	return $list;
    }

    /**
	 * Értékek beillesztése a tömbbe rekurzívan
	 *
	 * @param array $array
	 * @param array $source
	 * @return array
	 */
	public static function set($array, array $source)
	{
		if (!is_array($array))
		{
			$array = array();
		}

		foreach ($source as $key => $value)
		{
			if (is_array($value))
			{
				$array[$key] = self::set($array[$key], $value);
			}
			else
			{
				$array[$key] = $value;
			}
		}

		return $array;
	}

	/**
	 * Flip
	 *
	 * @param array $array
	 * @return array
	 */
    public static function structureFlip($array)
	{
		$new = array();

		if (is_array($array))
		{
			foreach ($array as $property => $values)
			{
				self::_structureFlip($property, $values, $new);
			}
		}

		return $new;
	}

    /**
     * Kulcs => érték létrehozása sormező értékek alapján
     *
     * @param array $array
     * @param string $keyField
     * @param string $valueField
     * @return array
     */
    public static function toAssignByFields(array $array, $keyField, $valueField)
    {
    	$list = array();

    	while ($array)
    	{
			$item = array_shift($array);
			$list[$item[$keyField]] = $item[$valueField];
    	}

    	return $list;
    }

    /**
     * Fa-lista kialakítása
     *
     * 	A parent és id mezők alapján sorbarendezi a listát
     * 	- az elem kulcsa az elérési út: /1/2/3/
     * 	- az elem[':childNo'] a gyerekek száma
     * 	- az elem[':level'] a szint száma
     *
     * @param array $array
     * @param string $idKey
     * @param string $parentKey
     * @return array
     */
    public static function treePopulate(array $array, $idKey, $parentKey)
    {
    	// rendezés szülő és eredeti sorrend alapján
    	$items = array();
    	$c = 0;

    	while ($array)
    	{
    		$item = array_shift($array);
    		$items[sprintf('%06x/%06x', $item[$parentKey], $c)] = $item;
    		$c ++;
    	}

    	ksort($items);

		// childs, level, path, key
		$paths = array();
		$keys = array();
		$c = 0;

    	while ($items)
    	{
    		$item = array_shift($items);
    		$id = $item[$idKey];
			$path = sprintf('/%06x', $c);
    		$key = $id . '/';

			// gyerek
			if ($parentId = $item[$parentKey])
			{
				$parent = &$array[$parentId];

				// számláló
				$parent[':childNo'] ++;

				// adatok a szülő adatai alapján
				$item[':level'] = $parent[':level'] + 1;

				$path = $paths[$parentId] . $path;
				$key = $keys[$parentId] . $key;
			}
			else
			// főelem
			{
				$item[':childNo'] = 0;
				$item[':level'] = 0;
			}

			// tárolás
			$array[$id] = $item;
			$paths[$id] = $path;
			$keys[$id] = $key;

			// a sorrend megtartásához számláló
			$c ++;
    	}

		// rendezés
		asort($paths);

		// a rendezett lista felállítása
		$list = array();

		foreach (array_keys($paths) as $id)
		{
			$item = $array[$id];
			unset($array[$id]);

			$list['/' . $keys[$id]] = $item;
		}

		return $list;
    }

    /**
     * A tombbol eldobjuk a NULL erteku elemeket
     *
     * @param array $array
     * @return array
     */
    public static function withoutNulls($array)
    {
    	return array_filter($array, array('self', '_withoutNulls'));
    }

    /**
     * Kulcs-szerinti rendezés
     *
     * @param string $s1
     * @param string $s2
     * @return number
     */
    protected static function _keysortInsensitive($s1, $s2)
    {
    	return ($s1 = mb_strtolower(Shared_StringSupport::removeAccent($s1))) == ($s2 = mb_strtolower(Shared_StringSupport::removeAccent($s2)))
    		? 0
    		: ($s1 < $s2 ? -1 : 1)
    	;
    }

	/**
	 * Flip
	 *
	 * @param string $property
	 * @param array $source
	 * @param array $dest
	 */
    protected static function _structureFlip($property, $source, &$dest)
	{
		foreach ($source as $key => $value)
		{
			if (is_array($value))
			{
			    if (!array_key_exists($key, $dest))
			    {
			        $dest[$key] = null;
			    }

				self::_structureFlip($property, $value, $dest[$key]);
			}
			else
			{
				$dest[$key][$property] = $value;
			}
		}
	}

    /**
     * A withoutNulls filter-fuggvenye
     *
     * @param mixed $value
     * @return bool
     */
    protected static function _withoutNulls($value)
    {
    	return $value !== null;
    }

    /**
     * Újraépíti az 1 dimenziós tömböt 2 dimenziós tömbre.
     * A Kulcsból értéket csinál, az értékeket pedig újra kulcsozza.
     *
     * Használat!
     * array(1) {
     *      [7129] => string(2) "13"
     * }
     *
     * keyValuePairsToAssoc($array, 'productID', 'quantity');
     *
     * array(1) {
     *      [0] => array(2) {
     *          ["productID"] => int(7129)
     *          ["quantity"] => string(2) "13"
     *      }
     * }
     *
     * @param array $array
     * @param string $keyForOldKey
     * @param string $newKeyForValue
     * @return array
     */
    public static function keyValuePairsToAssoc($array, $keyForOldKey, $newKeyForValue)
    {
        $return = array();

        foreach($array as $k=>$v) {
            $return[] = array(
                $keyForOldKey     => $k
                , $newKeyForValue => $v
            );
        }

        return $return;
    }

    /**
     * @param array $array
     * @return array
     */
    public static function mapToAssignable($array)
    {
        $result = [];
        foreach ($array as $key=>$attributes) {
            $i = 0;
            foreach ($attributes as $value) {
                if (!array_key_exists($i, $result)) {
                    $result[$i] = [];
                }
                $result[$i][$key] = $value;
                $i++;
            }
        }

        return $result;
    }
}
