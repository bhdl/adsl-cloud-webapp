<?php

namespace App\Controller;

use Phalcon\Mvc\Controller;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;

/**
 * Class AbstractRestController
 */
abstract class AbstractRestController extends
    Controller
{
    /**
     * @var
     */
    protected $exception;

    /**
     * @var
     */
    protected $validator;

    /**
     * @var
     */
    protected $model;

    /**
     * @var array
     */
    protected $transform = [
        'deleted' => 'App\Filter\Boolean'
    ];

    /**
     * @var array
     */
	protected static $excluded = [
	    'createdAt'
	    , 'updatedAt'
        , 'deleted'
        , 'deletedAt'
    ];

    /**
     * @var FileAdapter object
     */
	protected $logger = false;

    /**
     * @var false|string
     */
	protected $loggerHash;

    /**
     * initialize
     */
    public function initialize()
    {
        $this->model  = $this->setup['model'];
    }

    /**
     * @param $dispatcher
     * @return bool
     */
    public function beforeExecuteRoute($dispatcher)
    {

        if (isset($this->config->logger->file)
            && isset($this->config->logger->file->enabled)
            && $this->config->logger->file->enabled === true) {
            $this->loggerHash = date('Ymd_His');

            $dir = $this->config->logger->file->path.date('Ymd');

            if(!file_exists($dir)) {
                mkdir($dir , 0777,true);
            }

            $this->logger = new FileAdapter($dir.'/'.$this->loggerHash.'.log');
        }

        $this->writeLog('Client ip: '.$this->request->getClientAddress());
        $this->writeLog('Url: '.$this->request->getURI());
        $this->writeLog('Method: '.$this->request->getMethod());
        $this->writeLog('Controller: '.$dispatcher->getControllerName());
        $this->writeLog('Controller action: '.$dispatcher->getActionName());
        $header = '';

        foreach($this->request->getHeaders() as $key => $headerElement) {
            $header .= "\n".$key.' '.$headerElement;
        }
        $this->writeLog('Header: '.$header);

        $this->writeLog('Body: '.$this->request->getRawBody());

        if ($this->request->getHeader("Content-Type") != 'application/json') {
            $dispatcher->setReturnedValue($this->generalResponse(415));
            return false;
        }
    }

    /**
     * @param string $data
     * @param bool $serialize
     */
    private function writeLog($data = "", $serialize = false)
    {
        if ($this->logger !== false) {
            $data = $serialize == true ? serialize($data) : $data;
            $this->logger->log(
               $this->loggerHash.' '.$data
            );
        }
    }
    /**
     * @return array|string
     */
    public function getShortName()
    {
        $modelName = explode("\\", $this->model);
        $modelName = strtolower(end($modelName));
        return $modelName;
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function createAction()
    {
        try {
            if ($this->request->isPost()) {
                $this->db->beginLogged();

                $model = new $this->model;

                $postData = (array)$this->request->getJsonRawBody();

                if(isset($this->validator)) {
                    $messages = $this->validator->validate($postData);
                    if(count($messages) > 0) {
                        if ($messages[0]->getType() != "PresenceOf") {
                            return $this->generalResponse(406, $messages[0]);
                        }
                        return $this->generalResponse(400, $messages[0]);
                    }
                }

                $model->assign($postData);

                $this->beforeModelCreate();

                $model->create();

                $this->afterModelCreate();

                $this->db->commitLogged();

                return $this->generalResponse(
                    201
                    , ""
                    , $this->transformData($this->model::findFirst($model->getPrimaryKey()))
                );
            }

        } catch ( \PDOException $b ) {
            $this->db->rollbackLogged();

            return $this->generalResponse(
                400
                , $b->getMessage()
            );
        } catch (Exception $e) {
            $this->db->rollbackLogged();

            $message = $e->getMessage();

            if ($model->validationHasFailed()) {
                $message = $model->getMessagesAsString();
            }

            $this->logger->error($message, ['exception' => $e]);

            return $this->generalResponse(
                400
                , $message
            );
        }
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function viewAction()
    {
        try {
            $id = $this->di->get('router')->getParams();

            if (isset($id["id"])) {
                $data = $this->model::find(
                    (int) $id["id"]
                );

                if ($data->count() == 0) {
                    return $this->generalResponse(404);
                }

                $data = $this->transformData($data[0]);

            } else {
                $elements = $this->model::find([]);
                $data = $this->getPager($elements);
            }

        } catch (\Exception $ex) {
            $this->exception = $ex;
            return $this->generalResponse(
                500
                , $ex->getMessage()
            );
        }

        return $this->generalResponse(
            200
            , ""
            , $data);
    }

    /**
     * modifyAction
     */
    public function modifyAction()
    {
        try {
            if ($this->request->isPut()) {
                $this->db->beginLogged();

                $id = $this->di->get('router')->getParams();

                $id = (int) trim($id["id"], "/");

                $model = $this->model::findFirst($id);

                if (!$model || $model->count() == 0) {
                    return $this->generalResponse(404);
                }

                $postData = (array) $this->request->getJsonRawBody();

                if(isset($this->validator)) {
                    $messages = $this->validator->validate($postData);
                    if(count($messages) > 0) {
                        return $this->generalResponse(406, $messages[0]);
                    }
                }

                $this->beforeModelModify();

                $model->update($postData);

                $this->afterModelModify();

                $this->db->commitLogged();

                return $this->generalResponse(
                    200
                    , ""
                    , $this->transformData($this->model::findFirst($id))
                );
            }

        } catch ( \PDOException $b ) {
            $this->db->rollbackLogged();

            return $this->generalResponse(
                400
                , $b->getMessage()
            );
        } catch (Exception $e) {
            $this->db->rollbackLogged();

            $message = $e->getMessage();

            if ($model->validationHasFailed()) {
                $message = $model->getMessagesAsString();
            }

            $this->logger->error($message, ['exception' => $e]);

            return $this->generalResponse(
                400
                , $message
            );
        }
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function deleteAction()
    {
        //Todo response ha már törölték
        try {

            $id = $this->di->get('router')->getParams();
            if (isset($id["id"])) {
                $this->model = $this->model::findFirst(
                    (int) trim($id["id"], "/")
                );

                if (!$this->model || $this->model->count() == 0) {
                    return $this->generalResponse(404);
                }

                $temp = $this->model;
                $this->beforeModelDelete();
                $this->model->delete();
                $this->afterModelDelete();

                $this->model = $this->model::findFirst(
                    (int) trim($id["id"], "/")
                );

                if($this->model) {
                    $temp = $this->model;
                }

                return $this->generalResponse(
                    200
                    , ""
                    , $this->transformData($temp)
                );
            } else {
                return $this->generalResponse(406);
            }
        } catch (\Exception $ex) {
            return $this->generalResponse(500);
        }
    }

    /**
     * @param $data
     */
    public function transformData($data)
    {
        if (is_array($data)) {
            foreach($data as $key => $element) {
                $data[$key] = $this->transformElement($element);
            }
        } elseif (is_object($data)) {
            $data = $this->transformElement($data);
        }

        return $data;
    }

    /**
     * @param $element
     */
    protected function transformElement($element)
    {
        if (!empty($this->transform)) {
            foreach ($this->transform as $attr => $transform) {
                if(isset($element->{$attr})) {
                    $filterObj = new $transform();
                    $element->{$attr} = $filterObj->filter($element->{$attr});
                }
            }
        }

        if (!empty(self::$excluded)) {
            $element = $element->toArrayWithoutAttributes(array_merge(self::$excluded ,static::$excluded));
        }

        return $element;
    }

    /**
     * beforeModelCreate
     */
    public function beforeModelCreate()
    {
    }

    /**
     * afterModelCreate
     */
    public function afterModelCreate()
    {
    }

    /**
     * beforeModelModify
     */
    public function beforeModelModify()
    {
    }

    /**
     * afterModelModify
     */
    public function afterModelModify()
    {
    }

    /**
     * beforeModelDelete
     */
    public function beforeModelDelete()
    {

    }

    /**
     * afterModelDelete
     */
    public function afterModelDelete()
    {
    }

    /**
     * @param string $code
     * @param string $type
     * @param string $message
     * @param string $title
     * @param null $data
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function generalResponse(int $code, string $message = "", $data = NULL)
    {
        $this->auth->logout();

        $this->writeLog('Response code: '.$code);
        $this->writeLog('Response message: '.$message);

        if($data) {
            $this->writeLog('Response data: ');
            $this->writeLog($data, true);
        }

        switch($code) {
            case 200:
            case 201:
                return $this->response(
                    $code
                    , [ "data" => $data ]
                );
                break;
            case 400:
                return $this->response(
                    $code
                    , [
                    "error" => [
                        "code"      => $this->translate->_("ERR_MISSING_ARGUMENT")
                        , "title"   => $this->translate->_("Missing Argument")
                        , 'message' => $this->translate->_($message)
                    ]
                ]);
                break;
            case 406:
                return $this->response(
                    $code
                    , [
                    "error" => [
                        "code"      => $this->translate->_("ERR_WRONG_VALUE")
                        , "title"   => $this->translate->_("Wrong value")
                        , 'message' => $this->translate->_($message)
                    ]
                ]);
                break;
            case 410:
                return $this->response(
                    $code
                    , [
                    "error" => [
                        "code"      => $this->translate->_("ERR_RESOURCE_DELETED")
                        , "title"   => $this->translate->_("Deleted Resource")
                        , 'message' => $this->translate->_("The requested element has been deleted!")
                    ]
                ]);
                break;
            case 415:
                return $this->response(
                    $code
                    , [
                    "error" => [
                        "code"      => $this->translate->_("ERR_HEADER")
                        , "title"   => $this->translate->_("Content type error")
                        , 'message' => $this->translate->_("Your data content-type is not valid!")
                    ]
                ]);
                break;
            case 500:
                return $this->response(
                    $code
                    , [
                    "error" => [
                        "code"      => $this->translate->_("ERR_INTERNAL")
                        , "title"   => $this->translate->_("Internal Server Error")
                        , 'message' => $this->translate->_("Something happened!")
                    ]
                ]);
                break;

            case 404:
            default:
                return $this->response(
                    $code
                    , [
                    "error" => [
                        "code"      => $this->translate->_("ERR_RESOURCE_NOT_EXIST")
                        , "title"   => $this->translate->_("Resource Not Exist")
                        , 'message' => $this->translate->_("The requested element does not exist!")
                    ]
                ]);
            break;
        }
    }

    /**
     * @param Exception $exception
     * @throws Exception
     */
    protected function exception(Exception $exception)
    {
        throw new Exception('An error occured', 0, $exception);
    }

    /**
     * @param array $json
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    protected function response(int $code, array $json)
    {
        $this->response->setStatusCode($code);

        return $this->response->setJsonContent(
            $json
            , JSON_PRETTY_PRINT
        );
    }

    /**
     * @param $data
     * @return array
     */
    public function getPager($data)
    {
        $page = (int) $this->request->get("page");
        $limit  = (int) $this->request->get("number");

        $page = $page && $page != 0 ? $page : 1;

        $pageData = new PaginatorModel([
                "data"  => $data
                , "limit" => $limit ? $limit : 10
                , "page"  => $page
            ]
        );

        $pageData = $pageData->getPaginate();

        $next = $pageData->total_pages <= $page ?  '' : '/'.$this->setup['dataKey']['singular'].'/?page='.($page + 1).'&number='.$pageData->limit;

        if($page > $pageData->total_pages || $page < 1) {
            return [
                $this->setup['dataKey']['plural'] => []
                , 'pagination' => [
                    'count'          => $pageData->total_items
                    , 'per_page'     => $pageData->limit
                    , 'current_page' => $page
                    , 'total_pages'  => $pageData->total_pages
                    , 'next_url'     => $next
                ]
            ];
        }
        return [
            $this->setup['dataKey']['plural'] => $this->transformData($pageData->items)
            , 'pagination' => [
                'count'          => $pageData->total_items
                , 'per_page'     => $pageData->limit
                , 'current_page' => $page
                , 'total_pages'  => $pageData->total_pages
                , 'next_url'     => $next
            ]
        ];
    }
}
