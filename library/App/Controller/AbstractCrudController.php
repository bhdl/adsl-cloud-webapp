<?php

namespace App\Controller;

use App\Model\Builder\AbstractBuilder;
use App\Model\Job;
use App\Model\Presenter\AbstractPresenter;
use App\Support\HashMap;
use DataTables\DataTable;
use Phalcon\Exception;
use App\Support\Str;

/**
 * Class AbstractCrudController
 */
abstract class AbstractCrudController
    extends AbstractController
{
    /**
     * @var \Phalcon\Mvc\Model
     */
    public $model;

    /**
     * @var \Phalcon\Forms\Form
     */
    protected $form;

    /**
     * @var array
     */
    protected $setup = [];

    /**
     * @var string
     */
    const BUILDER_PARAMS_STORAGE_DEFAULT = 'default';

    /**
     * @var string
     */
    const BUILDER_PARAMS_STORAGE_BROWSE  = 'browse';

    /**
     * initialize
     * @return mixed
     */
    public function initialize()
    {
        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'index')) {
            $this->breadcrumb->add(
                $this->translate->t($this->setup['name']['plural'])
                , $this->url('index')
            );
        }

        $this->tag->prependTitle($this->setup['name']['plural']);

        $this->view->resourceId = $this->getResourceId();

        $this->view->urls = [
            'modify'      => $this->url('modify')
            , 'create'    => $this->url('create')
            , 'export'    => $this->url('export')
            , 'index'     => $this->url('index')
            , 'delete'    => $this->url('delete')
            , 'view'      => $this->url('view')
            , 'dataTable' => $this->url('data-table')
        ];
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     * @throws Exception
     */
    public function ajaxAction()
    {
        if (!$this->request->isAjax()) {
            return $this->response->setJsonContent([]);
        }

        $json = null;

        switch ($this->request->get('operation')) {
            case 'searchBar':
                $json = $this->ajaxSearchBar();
                break;
            case 'searchBarAddParam':
                $json = $this->ajaxSearchBarAddParam(
                    $this->request->get('params')
                    , $this->request->get('paramName')
                );
                break;
            case 'searchBarClearParam':
                $json = $this->ajaxSearchBarClearParam(
                    $this->request->get('params')
                    , $this->request->get('paramName')
                    , $this->request->get('i')
                );
                break;
            case 'searchBarReset':
                $json = $this->ajaxSearchBarReset();
                break;
            case 'create':
                $json = $this->ajaxCreate();
                break;
            case 'browse':
                $json = $this->ajaxBrowse();
                break;
            case 'autoComplete':
                $json = $this->ajaxAutoComplete(
                    $this->request->get('term', 'trim')
                );
                break;
            default:
                break;
        }

        return $this->response->setJsonContent($json);
    }

    /**
     * ajaxSearchBar
     */
    protected function ajaxSearchBar()
    {
        return [
            'errors'   => false
            , 'result' => $this->getSearchBarPartial(
                $this->toSearchBarParams($this->getBuilderParams())
            )
        ];
    }

    /**
     * ajaxSearchBarAddParam
     *
     * @param array $params
     * @param string $paramName
     * @return array
     * @throws Exception
     */
    protected function ajaxSearchBarAddParam($params, $paramName)
    {
        if (!is_array($params)) {
            $params = [];
        }

        if (array_key_exists($paramName, $params)) {
            if (is_array($params[$paramName])) {
                $params[$paramName][] = null;
            } else {
                $params[$paramName] = [
                    $params[$paramName], null
                ];
            }
        } else {
            $params[$paramName] = null;
        }

        return [
            'errors'   => false
            , 'result' => $this->getSearchBarPartial(
                $this->toSearchBarParams($params)
            )
        ];
    }

    /**
     * @param $params
     * @param $paramName
     * @param $i
     * @return array
     * @throws Exception
     */
    protected function ajaxSearchBarClearParam($params, $paramName, $i)
    {
        if (is_array($params)
            && array_key_exists($paramName, $params)
        ) {
            if (is_array($params[$paramName])) {
                unset($params[$paramName][$i]);
                ksort($params[$paramName]);
            } else {
                unset($params[$paramName]);
            }
        }

        return [
            'errors'   => false
            , 'result' => $this->getSearchBarPartial(
                $this->toSearchBarParams($params)
            )
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function ajaxSearchBarReset()
    {
        $this->setBuilderParams(self::BUILDER_PARAMS_STORAGE_DEFAULT, []);

        return [
            'errors'   => false
            , 'result' => $this->getSearchBarPartial([])
        ];
    }

    /**
     * @param string $term
     * @return mixed
     * @throws Exception
     */
    protected function ajaxAutoComplete($term)
    {
        $builder = $this->getBuilder();
        $params = [
            'autoComplete' => $term
            , 'languageId' => $this->auth
                ->getIdentity()
                ->getInterfaceLanguage()
                ->languageId
        ];

        $this->beforeSetAutoCompleteBuilderParams($params);

        $builder->setParams($params);

        $items = $builder
            ->build()
            ->limit(100)
            ->getQuery()
            ->execute()
            ->toArray()
        ;

        $presenter = $this->getPresenter();

        if ($presenter) {
            $items = $presenter->setRows($items)->present();
        }

        return $items;
    }

    /**
     * ajaxCreate
     */
    protected function ajaxCreate()
    {
        $errors = false;

        $model = null;

        $entity = null;
        if ($formDefaults = $this->request->get('formDefaults')) {
            $entity = HashMap::assignToStdClass($formDefaults);
        }

        $form = $this->getForm('create', $entity);

        if ($this->request->isPost() && $form->isValid($this->request->getPost())) {
            try {

                $model = $this->getModel();

                $this->db->beginLogged();

                $form->bind($this->request->getPost(), $model);

                $this->beforeModelCreate();

                $model->create();

                $this->afterModelCreate();

                if (in_array('App\Model\Traits\HasLanguages', class_uses($model))) {
                    $model
                        ->deleteLanguages()
                        ->createLanguages($form)
                    ;
                }

                $message = $this->translate->_(
                    'A(z) %name% létrehozása sikerült.'
                    , ['name' => $this->getSingularName(true)]
                );

                $this->logger->info($message);

                $this->db->commitLogged();

                $form = null;

            } catch (Exception $e) {

                $errors = true;

                $this->db->rollbackLogged();

                $message = $this->translate->_(
                    'A(z) %name% mentése közben hiba történt: %message%'
                    , [
                        'message' => $e->getMessage()
                        , 'name'  => $this->getSingularName(true)
                    ]
                );

                if ($model->validationHasFailed()) {
                    $message = $model->getMessagesAsString();
                }

                $this->logger->error($message);

                $elements = array_keys($form->getElements());

                $form->getMessagesFor($elements[0])->appendMessage(
                    new \Phalcon\Validation\Message($message)
                );
            }
        }

        return [
            'errors'   => $errors
            , 'result' => [
                'form'     => $form ? $form->renderDecorated() : null
                , 'entity' => $model ? $model->toArray() : null
            ]
        ];
    }

    /**
     * ajaxBrowse
     */
    protected function ajaxBrowse()
    {
        $html = $this->view->getPartial(
            $this->dispatcher->getControllerName() . '/browse'
            , [
                'urls' => [
                    'dataTable' => $this->url('dataTable')
                ]
                , 'builderParams' => $this->request->has('builderParams') ? $this->request->get('builderParams') : null
            ]
        );

        return [
            'errors'   => false
            , 'result' => $html
        ];
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function getSearchBarPartial($params)
    {
        return $this->view->getPartial(
            'partials/search/columns'
            , [
                'form'     => $this->getForm('search')
                , 'params' => $params
            ]
        );
    }

    /**
     * @param array|null $p
     * @return array
     * @throws Exception
     */
    protected function toSearchBarParams($p = null)
    {
        $params = [];

        if (!is_array($p)) {
            return $params;
        }

        $form   = $this->getForm('search');

        foreach ($p as $k=>$v) {
            try {
                $e = $form->get($k);

                if (!is_array($v)) {
                    $v = [$v];
                }

                $params[$k] = [
                    'label'    => $e->getLabel()
                    , 'values' => $v
                ];

            } catch (Exception $e) {
            }
        }

        return $params;
    }

    /**
     * indexAction
     */
    public function indexAction()
    {
        // létrehozás gomb
        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'create')) {
            $this->headingButtons->add(
                $this->translate->_('Létrehozás')
                , $this->url('create')
                , 'icon-plus3'
            );
        }

        if (
            $this->acl->isAllowedByIdentity($this->getResourceId(), 'search')
            && array_key_exists('search', $this->setup['forms'])
        ) {
            // keresés form
            $this->searchBar
                ->setForm($this->getForm('search'))
                ->setController($this->dispatcher->getControllerName())
            ;
        }

        // datatable kezeli a táblázatos megjelenítést, lásd template
        $this->breadcrumb->add(
            $this->translate->t('Lista')
            , $this->url('index')
        );
    }

    /**
     * dataTableAction
     */
    public function dataTableAction()
    {
        if (!$this->request->isAjax()) {
            return;
        }

        /** @var Phalcon\Mvc\Model\Query\Builder $builder */
        $builder = $this->getBuilder();
        $builder->setParams($this->getBuilderParams(
            $this->request->get(
                'builderParamsStorage'
                , null
                , self::BUILDER_PARAMS_STORAGE_DEFAULT
            )
        ));

        $dataTables = new DataTable();
        $dataTables->fromBuilder($builder->build());

        $responseData = $dataTables->getResponse();

        if (isset($this->setup['presenter'])){
            /**
             * @var AbstractPresenter $presenter
             */
            $presenter = new $this->setup['presenter']($responseData['data']);
            $responseData['data'] = $presenter->present();
        }

        return $this->response->setJsonContent($responseData);
    }

    /**
     * @param string $storage
     * @param boolean $asEntity
     * @return array|mixed
     */
    protected function getBuilderParams(
        $storage = self::BUILDER_PARAMS_STORAGE_DEFAULT
        , $asEntity = false
    )
    {
        $sk  = $this->getBuilderParamsSessionKey($storage);

        $rsp = $this->request->get('builderParams');
        $ssp = $this->session->get($sk);
        $sp  = [];

        if ($rsp) {
            // request paraméterekkel dolgozunk
            $this->setBuilderParams($storage, $rsp);
            $sp = $rsp;
        } else if ($ssp) {
            // session paraméterekkel dolgozunk
            $sp = $ssp;
        }

        if (!array_key_exists('languageId', $sp)) {
            $sp['languageId'] = $this->auth->getIdentity()
                ->getInterfaceLanguage()
                ->languageId
            ;
        }

        if ($asEntity) {
            // objektum visszatérési érték
            $sp = HashMap::assignToStdClass($sp);
        }

        return $sp;
    }

    /**
     * @param string $storage
     * @param array $params
     */
    protected function setBuilderParams($storage, $params)
    {
        $this->session->set(
            $this->getBuilderParamsSessionKey($storage)
            , $params
        );
    }

    /**
     * createAction
     */
    public function createAction()
    {
        $this->form = $this->getForm('create');

        if ($this->request->isPost()
            && $this->form->isValid($this->request->getPost())) {

            $this->model = $this->getModel();

            try {

                $this->db->beginLogged();

                $this->form->bind(
                    $this->request->getPost()
                    , $this->model
                );

                $this->beforeModelCreate();

                $this->model->create();

                $this->afterModelCreate();

                if (in_array('App\Model\Traits\HasLanguages', class_uses($this->model))) {
                    $this->model->createLanguages($this->form);
                }

                $message = $this->translate->_(
                    'A(z) %name% létrehozása sikerült.'
                    , ['name' => $this->getSingularName(true)]
                );

                $this->logger->info($message);

                $this->db->commitLogged();

                $this->flashSession->success($message);

                return $this->redirectToViewOrIndexAction($this->model->getPrimaryKey());

            } catch (Exception $e) {

                $this->db->rollbackLogged();

                $message = $this->translate->_(
                    'A(z) %name% létrehozása közben hiba történt: %message%'
                    , [
                        'name' => $this->getSingularName(true)
                        , 'message' => $e->getMessage()
                    ]
                );

                if ($this->model->validationHasFailed()) {
                    $message = $this->model->getMessagesAsString();
                }

                $this->flash->error($message);

                $this->logger->error($message, ['exception' => $e]);
            }
        }

        $this->breadcrumb->add(
            $this->translate->t('Létrehozás')
            , $this->url('create')
        );

        // átalános vissza gomb
        $this->headingButtons->add(
            $this->translate->_('Vissza a listára')
            , $this->url('index')
            , 'icon-arrow-left16'
        );

        $this->view->form = $this->form;
        $this->view->pick('partials/crud/create');
    }

    /**
     * @param $id
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function viewAction($id)
    {
        $this->view->model = $this->model;

        // breadcrumb
        $this->addEntityBreadCrumbs();

        $this->breadcrumb->add(
            $this->translate->t('Áttekintés')
            , $this->url('view', null, null, $id)
        );

        $this->addEntityHeadingButtons();
    }

    /**
     * @param $id
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function modifyAction($id)
    {
        $this->form = $this->getForm('modify', $this->model);

        if ($this->request->isPost()
            && $this->form->isValid($this->request->getPost())) {

            try {
                $this->db->beginLogged();

                $this->form->bind(
                    $this->request->getPost()
                    , $this->model
                );

                $this->beforeModelModify();

                $this->model->update();

                $this->afterModelModify();

                if (in_array('App\Model\Traits\HasLanguages', class_uses($this->model))) {
                    $this->model
                        ->deleteLanguages()
                        ->createLanguages($this->form)
                    ;
                }

                $message = $this->translate->_(
                    'A(z) %name% módosítása sikerült.'
                    , ['name' => $this->getSingularName(true)]
                );

                $this->logger->info($message);

                $this->db->commitLogged();

                $this->flashSession->success($message);

                return $this->redirectToViewOrIndexAction($id);

            } catch (Exception $e) {

                $this->db->rollbackLogged();

                $message = $this->translate->_(
                    'A(z) %name% módosítása közben hiba történt: %message%'
                    , [
                        'name' => $this->getSingularName(true)
                        , 'message' => $e->getMessage()
                    ]
                );

                if ($this->model->validationHasFailed()) {
                    $message = $this->model->getMessagesAsString();
                }

                $this->flash->error($message);

                $this->logger->error($message, ['exception' => $e]);
            }
        }

        $this->addEntityBreadCrumbs();

        $this->breadcrumb->add(
            $this->translate->t('Módosítás')
            , $this->url('modify', null, null, $id)
        );

        $this->addEntityHeadingButtons();

        $this->view->form = $this->form;
        $this->view->pick('partials/crud/modify');
    }

    /**
     * @param $id
     * @throws Exception
     */
    public function deleteAction($id)
    {
        try {

            $this->db->beginLogged();

            $this->beforeModelDelete();

            $result = $this->model->delete();
            if (!$result) {
                throw new Exception();
            }

            $this->afterModelDelete();

            $message = $this->translate->_(
                'A(z) %name% törlése sikerült.'
                , ['name' => $this->getSingularName(true)]
            );

            $this->logger->info($message);

            $this->db->commitLogged();

            $this->flashSession->success($message);

        } catch (Exception $e) {

            $this->db->rollbackLogged();

            $message = $this->translate->_(
                'A(z) %name% törlése közben hiba történt: %message%'
                , [
                    'name' => $this->getSingularName(true)
                    , 'message' => $e->getMessage()
                ]
            );

            if ($this->model->validationHasFailed()) {
                $message = $this->model->getMessagesAsString();
            }

            $this->flashSession->error($message);

            $this->logger->error($message, ['exception' => $e]);
        }

        $this->redirect('index');
    }

    /**
     * @param string $hash
     * @param string $download
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     * @param bool $pdf
     * @throws Exception
     */
    public function exportAction($hash = null, $download = null, $pdf = false)
    {
        if (!isset($this->setup['export'])) {
            throw new Exception('Az "export" létrehozása és beállítása kötelező.');
        }

        if ($hash) {
            $job = Job::findFirstByHash($hash);

            if (!$job) {
                return $this->redirect('index');
            }

            if ($download) {
                $job->export->download();
            }
        } else {

            $columns = $this->request->get('columns');
            if ($columns) {
                $columns = explode(',', $columns);
            }

            $job = \App\Queue\JobFactory::forPut(
                Job::JOB_TYPE_EXPORT
                , [
                    'builderClass'    => $this->setup['builder']
                    , 'builderParams' => $this->getBuilderParams() + [
                        // opcionális
                        'ids' => $this->request->get('ids')
                    ]
                    , 'exportClass'   => $this->setup['export'][($pdf ? 'pdf' : 'spreadsheet')]
                    , 'columns'       => $columns
                ]
            );

            $job->put();
        }

        // átalános vissza gomb
        $this->headingButtons->add(
            $this->translate->_('Vissza a listára')
            , $this->url('index')
            , 'icon-arrow-left16'
        );

        $this->breadcrumb->add(
            $this->translate->t('Exportálás')
            , $this->url('export')
        );

        $this->view->job = $job;

        $this->view->pick('partials/crud/export');
    }

    /**
     * beforeModelCreate
     */
    public function beforeModelCreate()
    {
    }

    /**
     * afterModelCreate
     */
    public function afterModelCreate()
    {
    }

    /**
     * beforeModelModify
     */
    public function beforeModelModify()
    {
    }

    /**
     * afterModelModify
     */
    public function afterModelModify()
    {
    }

    /**
     * beforeModelDelete
     */
    public function beforeModelDelete()
    {
    }

    /**
     * afterModelDelete
     */
    public function afterModelDelete()
    {
    }

    /**
     * beforeFormCreate
     *
     * @param $form
     * @param null $entity
     * @param null $options
     */
    public function beforeFormCreate($form, $entity = null, &$options = null)
    {
    }

    /**
     * afterFormCreate
     *
     * @param $instance
     * @param $form
     * @param null $entity
     * @param null $options
     */
    public function afterFormCreate($instance, $form, $entity = null, &$options = null)
    {
    }

    /**
     * @return \Phalcon\Mvc\Model
     * @param $id
     * @throws Exception
     */
    public function getModel($id = null)
    {
        if (!isset($this->setup['model'])) {
            throw new Exception('A "model" nincs beállítva!');
        }

        if ($this->model !== null) {
            return $this->model;
        }

        $this->model = new $this->setup['model'];

        if ($id == null) {
            return $this->model;
        }

        $method = 'findFirstBy' . ucfirst($this->model->getPrimaryKeyAttribute());

        $this->model = $this->getModel()::$method($id);
        if (!is_object($this->model)) {
            throw new Exception(
                sprintf('A keresett %s nem található.', $this->getSingularName(true))
            );
        }

        return $this->model;
    }

    /**
     * @param \Phalcon\Mvc\Model $model
     * @return AbstractCrudController
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @param string $form
     * @param null $entity
     * @param null $options
     * @return App\Form\AbstractCrud
     * @throws Exception
     */
    protected function getForm($form, $entity = null, $options = null)
    {
        if (!is_array($this->setup['forms'])
            || !isset($this->setup['forms'][$form])) {
            throw new Exception(
                'A \'forms\' nincs beállítva az alábbi űrlaphoz: ' . $form
            );
        }

        $this->beforeFormCreate($form, $entity, $options);

        $className = $this->setup['forms'][$form];

        $instance = new $className($entity, $options);

        $this->afterFormCreate($instance, $form, $entity, $options);

        return $instance;
    }

    /**
     * @param boolean $lcFirst
     * @return string
     * @throws Exception
     */
    public function getSingularName($lcFirst = false)
    {
        if (!is_array($this->setup['name'])
            || !isset($this->setup['name']['singular'])) {
            throw new Exception(
                'Nincs megadva a CRUD rekord egyes száma.'
            );
        }

        $name = $this->translate->_($this->setup['name']['singular']);

        if ($lcFirst) {
            $name = Str::firstLower($name);
        }

        return $name;
    }

    /**
     * @param boolean $lcFirst
     * @return string
     * @throws Exception
     */
    public function getPluralName($lcFirst = false)
    {
        if (!is_array($this->setup['name'])
            || !isset($this->setup['name']['plural'])) {
            throw new Exception(
                'Nincs megadva a CRUD rekord többes száma.'
            );
        }

        $name = $this->translate->_($this->setup['name']['plural']);

        if ($lcFirst) {
            $name = Str::firstLower($name);
        }

        return $name;
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function getPrimaryKeyAttribute()
    {
        return $this->getModel()->getPrimaryKeyAttribute();
    }

    /**
     * @param $id
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function redirectToViewOrIndexAction($id)
    {
        $action = 'view';
        if (!$this->acl->isAllowedByIdentity($this->getResourceId(), 'view')) {
            $action = 'index';
            $id     = null;
        }

        return $this->redirect($action, null, null, $id);
    }

    /**
     * @return void
     */
    protected function addEntityHeadingButtons()
    {
        // átalános vissza gomb
        $this->headingButtons->add(
            $this->translate->_('Vissza a listára')
            , $this->url('index')
            , 'icon-arrow-left16'
        );

        // részletek gomb
        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'view')) {
            $this->headingButtons->add(
                $this->translate->_('Áttekintés')
                , $this->url('view', null, null, $this->model->getPrimaryKey())
                , 'icon-eye'
            );
        }

        // módosítás gomb
        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'modify')) {
            $this->headingButtons->add(
                $this->translate->_('Módosítás')
                , $this->url('modify', null, null, $this->model->getPrimaryKey())
                , 'icon-pencil3'
            );
        }
    }

    /**
     * @return void
     */
    protected function addEntityBreadCrumbs()
    {
        if (!method_exists($this->model, 'getName')) {
            return;
        }

        $url = null;

        if($this->acl->isAllowedByIdentity($this->getResourceId(), 'view')) {
            $url = $this->url('view', null, null, $this->model->getPrimaryKey())   ;
        }

        $this->breadcrumb->add(
            $this->model->getName() ?: 'N/A'
            , $url
        );
    }

    /**
     * @param string $storage
     * @return string
     */
    protected function getBuilderParamsSessionKey($storage)
    {
        return $this->getSessionKey('builderParams_' . $storage);
    }

    /**
     * @param string $key
     * @return string
     */
    protected function getSessionKey($key)
    {
        return get_class($this) . ucfirst($key);
    }

    /**
     * @return array | boolean
     * @throws \Exception
     */
    public function getSetup()
    {
        $setup = $this->setup;
        $args = func_get_args();
        if ($args) {
            $settings = [];
            $camelCase = false;

            foreach ($args as $arg) {
                if ($camelCase) {
                    $arg = $this->getCamelCaseAlias($arg);
                    $camelCase = false;
                }
                if (in_array($arg, ['hasMany', 'hasManyToMany'])) {
                    $camelCase = true;
                }
                $settings[] = $arg;
                if (!isset($setup[$arg])){
                    if (!in_array($arg, ['relationModelPrimaryKeys'])) {
                        throw new Exception(
                            sprintf('Az alábbiak nincsenek definiálva a CRUD setup-ban: %s', implode(' -> ', $settings))
                        );
                    }
                    else {
                        return false;
                    }
                }
                $setup = $setup[$arg];
            }
        }
        return $setup;
    }

    /**
     * @param array $setup
     * @return AbstractCrudController
     */
    public function setSetup(array $setup): AbstractCrudController
    {
        $this->setup = $setup;
        return $this;
    }

    /**
     * @param string $alias
     * @return string
     */
    public function getCamelCaseAlias($alias)
    {
        if ($alias === mb_strtolower($alias)) {
            return lcfirst(\Phalcon\Text::camelize($alias));
        }
        return $alias;
    }

    /**
     * @return AbstractBuilder
     * @throws Exception
     */
    public function getBuilder()
    {
        if (!isset($this->setup['builder'])) {
            throw new Exception('A "builder" létrehozása és beállítása kötelező.');
        }

        return new $this->setup['builder'];
    }

    /**
     * @return AbstractPresenter|null
     */
    public function getPresenter()
    {
        if (isset($this->setup['presenter'])){
            return new $this->setup['presenter']();
        }

        return null;
    }

    /**
     * AutoComplete esetén plusz paraméter hozzáadási lehetőség
     * beforeSetAutoCompleteBuilderParams
     * @param $params array
     */
    public function beforeSetAutoCompleteBuilderParams(&$params)
    {
    }
}
