<?php

namespace App\Controller;

use Phalcon\Mvc\Controller;

/**
 * Class AbstractController
 */
abstract class AbstractController extends
    Controller
{
    /**
     * @param null $action
     * @param null $controller
     * @param null $module
     * @param null $params
     * @return string
     */
    public function url(
        $action       = null
        , $controller = null
        , $module     = null
        , $params     = null
    )
    {
        $segments = $this->getSegments(
            $action
            , $controller
            , $module
            , $params
            , true
        );

        return $this->assembleUrlSegments($segments);
    }

    /**
     * @param null $action
     * @param null $controller
     * @param null $module
     * @param null $params
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function redirect($action = null, $controller = null, $module = null, $params = null)
    {
        return $this->response->redirect(
            $this->url($action, $controller, $module, $params)
        );
    }

    /**
     * @param null $action
     * @param null $controller
     * @param null $module
     * @param null $params
     * @param bool $uncamelize
     * @return array
     */
    protected function getSegments(
        $action = null
        , $controller = null
        , $module = null
        , $params = null
        , $uncamelize = false
    )
    {
        $segments = [];

        $segments[] = $module ?: $this->dispatcher->getModuleName() ?: 'admin';

        $segments[] = $controller ?: $this->dispatcher->getControllerName() ?: 'index';

        $segments[] = $action ?: $this->dispatcher->getActionName() ?: 'index';

        if (is_array($params)) {
            foreach ($params as $k=>$v) {
                $segments[] = $v;
            }
        } else if ($params) {
            $segments[] = $params;
        }

        if ($uncamelize) {
            foreach ($segments as &$segment) {
                if (!is_string($segment)) {
                    continue;
                }
                $segment = \Phalcon\Text::uncamelize($segment, '-');
            }
        }

        return $segments;
    }

    /**
     * @param array $segments
     * @return string
     */
    protected function assembleUrlSegments($segments)
    {
        return '/' . implode('/', array_filter($segments));
    }

    /**
     * @return string
     */
    public function getResourceId()
    {
        $segments = $this->getSegments();

        array_pop($segments);

        return implode('.', $segments);
    }
}