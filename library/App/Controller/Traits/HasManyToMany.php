<?php

namespace App\Controller\Traits;

use App\Form;
use App\Model\AbstractModel;
use App\Model\Presenter\AbstractPresenter;
use App\Support\Str;
use DataTables\DataTable;
use Phalcon\Exception;

/**
 * Trait HasManyToMany
 * @package App\Controller\Traits
 */
trait HasManyToMany
{
    /**
     * @var AbstractModel
     */
    protected $hasManyToManyModel;

    /**
     * @var AbstractModel
     */
    protected $hasManyToManyRelationModel;

    /**
     * @var Form
     */
    protected $hasManyToManyForm;

    /**
     * initialize
     */
    public function initializeHasManyToMany()
    {

        $this->getSetup('hasManyToMany');

        $this->view->urls += [
            'indexHasManyToMany' => $this->url('index-has-many-to-many')
        ];
    }

    /**
     * @param $id
     * @param $alias
     * @throws Exception
     */
    public function indexHasManyToManyAction($id, $alias)
    {
        parent::addEntityHeadingButtons();

        $this->addAllHasManyToManyEntityHeadingButtons($id);

        // FIXME: hack
        /*if (method_exists($this, 'addHasManyEntityHeadingButtons')) {
            $this->addHasManyEntityHeadingButtons($id, $alias);
        }*/

        $this->addHasManyToManyEntityBreadCrumbs($id, $alias);

        $params = [
            $id
            , $alias
        ];

        $this->view->urls = [
            'dataTableHasManyToMany' => $this->url('data-table-has-many-to-many', null, null, $params)
            , 'modifyHasManyToMany'  => $this->url('modify-has-many-to-many', null, null, $params)
            , 'createHasManyToMany'  => $this->url('create-has-many-to-many', null, null, $params)
            , 'deleteHasManyToMany'  => $this->url('delete-has-many-to-many', null, null, $params)
            , 'viewHasManyToMany'    => $this->url('view-has-many-to-many', null, null, $params)
            , 'assignHasManyToMany'  => $this->url('assign-has-many-to-many', null, null, $params)
        ];

        $this->addHasManyToManyUrls($params);

        $mpkAttribute = $this->getSetup('hasManyToMany', $alias, 'relationModelPrimaryKeys', 'model');
        $this->view->builderParams = [
            'relationModel' => $this->getSetup('hasManyToMany', $alias, 'relationModel')
            , 'primaryKeyAttribute'     => $mpkAttribute ? $mpkAttribute : $this->model->getPrimaryKeyAttribute()
            , 'modelId'         => $id
        ];

        $alias = $this->getCamelCaseAlias($alias);

        $indexName = $this->getHasManyToManyIndexName();
        $this->view->pick($alias . $indexName);
    }

    /**
     * @param mixed $id
     * @param string $alias
     * @throws \Phalcon\Exception
     */
    public function dataTableHasManyToManyAction($id, $alias)
    {
        if (!$this->request->isAjax()) {
            return;
        }

        $p = $this->getBuilderParams();
        $p[$this->getModel()->getPrimaryKeyAttribute()] = $id;

        $builder = $this->createHasManyToManyBuilder($alias, $p);

        $dataTables = new DataTable();
        $dataTables->fromBuilder($builder)->sendResponse();

        $responseData = $dataTables->getResponse();

        $presenter = $this->getHasManyToManyPresenter($alias);

        if ($presenter){
            $responseData['data'] = $presenter->setRows($responseData['data'])->present();
        }

        return $this->response->setJsonContent($responseData);
    }

    /**
     * @param $id
     * @param $alias
     * @param $hasManyToManyId
     * @throws Exception
     */
    public function assignHasManyToManyAction($id, $alias, $hasManyToManyId)
    {
        $this->hasManyToManyModel = $this->getHasManyToManyModel(
            $alias
            , $hasManyToManyId
        );

        $hasManyToManyRelationModel = null;

        try {
            $this->db->beginLogged();

            $hasManyToManyRelationModel = $this->getHasManyToManyRelationModel(
                $alias
                , $this->getModel()
                , $this->getHasManyToManyModel($alias)
            );

            $this->beforeHasManyToManyModelAssign();

            $hasManyToManyRelationModel->create();

            $this->afterHasManyToManyModelAssign();

            $message = $this->translate->_(
                'A(z) %name%-%related% kapcsolat létrehozása sikerült.'
                , [
                    'name'      => $this->getSingularName()
                    , 'related' => $this->getHasManyToManyName($alias, 'singular')
                ]
            );

            $this->logger->info($message);

            $this->db->commitLogged();

            $this->flashSession->success($message);

        } catch (Exception $e) {

            $this->db->rollbackLogged();

            $message = $this->translate->_(
                'A(z) %name%-%related% kapcsolat létrehozása közben hiba történt: %message%'
                , [
                    'name'      => $this->getSingularName()
                    , 'related' => $this->getHasManyToManyName($alias, 'singular')
                    , 'message' => $e->getMessage()
                ]
            );

            if ($hasManyToManyRelationModel && $hasManyToManyRelationModel->validationHasFailed()) {
                $message = $hasManyToManyRelationModel->getMessagesAsString();
            }

            $this->flashSession->error($message);

            $this->logger->error($message, ['exception' => $e]);
        }

        $this->redirectToIndexHasManyToManyAction($id, $alias);
    }

    /**
     * @param $id
     * @param $alias
     * @return mixed
     * @throws Exception
     */
    public function createHasManyToManyAction($id, $alias)
    {
        $this->hasManyToManyForm = $this->getHasManyToManyForm($alias, 'create');

        if ($this->request->isPost()
            && $this->hasManyToManyForm->isValid($this->request->getPost())) {

            try {

                $this->db->beginLogged();

                $this->hasManyToManyModel = $this->getHasManyToManyModel($alias);

                $this->hasManyToManyForm->bind(
                    $this->request->getPost()
                    , $this->hasManyToManyModel
                );

                $this->beforeHasManyToManyModelCreate();

                $this->hasManyToManyModel->create();

//                $this->afterHasManyToManyModelCreate();

                if (in_array('App\Model\Traits\HasLanguages', class_uses($this->hasManyToManyModel))) {
                    $this->hasManyToManyModel->createLanguages($this->hasManyToManyForm);
                }

                /*
                 * Many-to-many reláció létrehozása
                 */
                $this->getHasManyToManyRelationModel(
                    $alias
                    , $this->model
                    , $this->hasManyToManyModel
                )->create();

                $message = $this->translate->_(
                    'A(z) %name% létrehozása sikerült.'
                    , ['name' => $this->getHasManyToManyName($alias, 'singular')]
                );

                $this->logger->info($message);

                $this->db->commitLogged();

                $this->flashSession->success($message);

                return $this->redirectToIndexHasManyToManyAction($id, $alias);

            } catch (\Exception $e) {

                $this->db->rollbackLogged();

                $message = $this->translate->_(
                    'A(z) %name% létrehozása közben hiba történt: %message%'
                    , [
                        'name'      => $this->getHasManyToManyName($alias, 'singular')
                        , 'message' => $e->getMessage()
                    ]
                );

                if ($this->hasManyToManyModel && $this->hasManyToManyModel->validationHasFailed()) {
                    $message = $this->hasManyToManyModel->getMessagesAsString();
                }

                $this->flash->error($message);

                $this->logger->error($message, ['exception' => $e]);
            }
        }

        $this->addHasManyToManyEntityBreadCrumbs(
            $id
            , $alias
        );

        $url = $this->url('index-has-many-to-many',null,null, [$id, $alias]);

        $this->breadcrumb->add(
            $this->translate->t('Létrehozás')
        );

        $this->headingButtons->add(
            $this->translate->_('Vissza a listára')
            , $url
            , 'icon-arrow-left16'
        );

        $this->view->form = $this->hasManyToManyForm;

        $this->view->urls = [
            'index' => $url
        ];

        $this->view->pick('partials/crud/create');
    }

    /**
     * @param $id
     * @param $alias
     * @param $hasManyToManyId
     * @return mixed
     * @throws Exception
     */
    public function modifyHasManyToManyAction($id, $alias, $hasManyToManyId)
    {
        $this->hasManyToManyModel = $this->getHasManyToManyModel($alias, $hasManyToManyId);

        $this->hasManyToManyForm = $this->getHasManyToManyForm(
            $alias,
            'modify'
            , $this->hasManyToManyModel
        );

        if ($this->request->isPost()
            && $this->hasManyToManyForm->isValid($this->request->getPost())) {

            try {
                $this->db->beginLogged();

                $this->hasManyToManyForm->bind(
                    $this->request->getPost()
                    , $this->hasManyToManyModel
                );

                $this->beforeHasManyToManyModelModify();

                $this->hasManyToManyModel->update();

//                $this->afterHasManyModelModify();

                if (in_array('App\Model\Traits\HasLanguages', class_uses($this->hasManyToManyModel))) {
                    $this->hasManyToManyModel
                        ->deleteLanguages()
                        ->createLanguages($this->hasManyToManyForm)
                    ;
                }

                $message = $this->translate->_(
                    'A(z) %name% módosítása sikerült.'
                    , ['name' => $this->getHasManyToManyName($alias, 'singular')]
                );

                $this->logger->info($message);

                $this->db->commitLogged();

                $this->flashSession->success($message);

                return $this->redirectToIndexHasManyToManyAction($id, $alias);

            } catch (Exception $e) {

                $this->db->rollbackLogged();

                $message = $this->translate->_(
                    'A(z) %name% módosítása közben hiba történt: %message%'
                    , [
                        'name'      => $this->getHasManyToManyName($alias, 'singular')
                        , 'message' => $e->getMessage()
                    ]
                );

                if ($this->hasManyToManyModel && $this->hasManyToManyModel->validationHasFailed()) {
                    $message = $this->hasManyToManyModel->getMessagesAsString();
                }

                $this->flash->error($message);

                $this->logger->error($message, ['exception' => $e]);
            }
        }

        $url = $this->url('index-has-many-to-many',null,null, [$id, $alias]);

        $this->addHasManyToManyEntityBreadCrumbs($id, $alias, $hasManyToManyId);

        $this->breadcrumb->add(
            $this->translate->t('Módosítás')
        );

        $this->headingButtons->add(
            $this->translate->_('Vissza a listára')
            , $url
            , 'icon-arrow-left16'
        );

        $this->view->form = $this->hasManyToManyForm;

        $this->view->urls = [
            'index' => $url
        ];

        $this->view->pick('partials/crud/modify');
    }

    /**
     * @param $id
     * @param $alias
     * @param $hasManyToManyId
     * @throws Exception
     */
    public function deleteHasManyToManyAction($id, $alias, $hasManyToManyId)
    {
        $this->hasManyToManyModel = $this->getHasManyToManyModel(
            $alias
            , $hasManyToManyId
        );

        $hasManyToManyRelationModel = null;

        try {
            $this->db->beginLogged();

            $hasManyToManyRelationModel = $this->getHasManyToManyRelationModel(
                $alias
                , $this->getModel()
                , $this->getHasManyToManyModel($alias)
            );

//            $this->beforeHasManyToManyModelAssign();

            if (!$hasManyToManyRelationModel->delete()) {
                throw new Exception();
            }

//            $this->afterHasManyToManyModelAssign();

            $message = $this->translate->_(
                'A(z) %name%-%related% kapcsolat törlése sikerült.'
                , [
                    'name'      => $this->getSingularName()
                    , 'related' => $this->getHasManyToManyName($alias, 'singular')
                ]
            );

            $this->logger->info($message);

            $this->db->commitLogged();

            $this->flashSession->success($message);

        } catch (Exception $e) {

            $this->db->rollbackLogged();

            $message = $this->translate->_(
                'A(z) %name%-%related% kapcsolat törlése közben hiba történt: %message%'
                , [
                    'name'      => $this->getSingularName()
                    , 'related' => $this->getHasManyToManyName($alias, 'singular')
                    , 'message' => $e->getMessage()
                ]
            );

            if ($hasManyToManyRelationModel && $hasManyToManyRelationModel->validationHasFailed()) {
                $message = $hasManyToManyRelationModel->getMessagesAsString();
            }

            $this->flashSession->error($message);

            $this->logger->error($message, ['exception' => $e]);
        }

        $this->redirectToIndexHasManyToManyAction($id, $alias);
    }

    /**
     * @param $id
     * @param $alias
     * @param $hasManyToManyId
     * @throws Exception
     */
    public function viewHasManyToManyAction($id, $alias, $hasManyToManyId)
    {
        $this->hasManyToManyModel = $this->getHasManyToManyModel($alias, $hasManyToManyId);

        $this->view->model = $this->hasManyToManyModel;

        $this->addHasManyToManyEntityBreadCrumbs($id, $alias, $hasManyToManyId);

        $this->breadcrumb->add(
            $this->translate->t('Áttekintés')
        );

        $this->headingButtons->add(
            $this->translate->_('Vissza a listára')
            , $this->url('index-has-many-to-many',null,null, [$id, $alias])
            , 'icon-arrow-left16'
        );

        $alias = $this->getCamelCaseAlias($alias);
        $this->view->pick($alias . '/hasManyToMany/view');
    }

    /**
     * @param mixed $id
     * @param string $alias
     * @param mixed $hasManyToManyId
     * @throws Exception
     */
    protected function addHasManyToManyEntityBreadCrumbs($id, $alias, $hasManyToManyId = null)
    {
        parent::addEntityBreadCrumbs();

        $url = null;

        if ($this->hasManyToManyModel
            && $this->acl->isAllowedByIdentity($this->getResourceId(), 'viewHasManyToMany')
        ) {
            $url = $this->url('view-has-many-to-many', null, null, [
                $id
                , $alias
                , $this->hasManyToManyModel->getPrimaryKey()
            ]);
        }

        $this->breadcrumb->add(
            $this->translate->t($this->getHasManyToManyName($alias, 'plural'))
        );

        if ($this->hasManyToManyModel) {
            $this->breadcrumb->add(
                $this->hasManyToManyModel->getName() ?: 'N/A'
                , $url
            );
        }
    }

    /**
     * @param $id
     * @param $alias
     * @param $hasManyToManyId
     * @throws Exception
     */
    protected function addHasManyToManyEntityHeadingButtons($id, $alias, $hasManyToManyId = null)
    {
        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'indexHasManyToMany')) {
            $this->headingButtons->add(
                $this->translate->_($this->getHasManyToManyName($alias, 'plural'))
                , $this->url('index-has-many-to-many', null, null, [
                    $this->model->getPrimaryKey()
                    , $alias
                ])
                , $this->getSetup('hasManyToMany', $alias, 'icon')
            );
        }
    }

    /**
     * @param string $alias
     * @return AbstractPresenter|null
     */
    protected function getHasManyToManyPresenter($alias)
    {
        $p = $this->getSetup('hasManyToMany', $alias, 'presenter');
        return new $p;
    }

    /**
     * @param string $alias
     * @param array $p
     * @return mixed
     * @throws Exception
     */
    protected function createHasManyToManyBuilder($alias, $p = [])
    {
        $b = $this->getSetup('hasManyToMany', $alias, 'builder');
        $builder = new $b;

        $builder->setParams($p);

        return $builder->build();
    }

    /**
     * @param $alias
     * @param null $hasManyToManyId
     * @return AbstractModel
     * @throws Exception
     */
    public function getHasManyToManyModel($alias, $hasManyToManyId = null)
    {
        if ($this->hasManyToManyModel !== null) {
            return $this->hasManyToManyModel;
        }

        $m = $this->getSetup('hasManyToMany', $alias, 'model');
        $this->hasManyToManyModel = new $m;

        if ($hasManyToManyId == null) {
            return $this->hasManyToManyModel;
        }

        $method = 'findFirstBy' . ucfirst($this->hasManyToManyModel->getPrimaryKeyAttribute());

        $this->hasManyToManyModel = $this->getHasManyToManyModel($alias)::$method($hasManyToManyId);

        if (!is_object($this->hasManyToManyModel)) {
            throw new Exception(
                sprintf('A keresett %s nem található.', $this->getHasManyToManyName($alias, 'singular'))
            );
        }

        return $this->hasManyToManyModel;
    }

    /**
     * @param $alias
     * @param $form
     * @param null $entity
     * @param null $options
     * @return mixed
     * @throws Exception
     */
    protected function getHasManyToManyForm($alias, $form, $entity = null, $options = null)
    {
        $this->beforeHasManyToManyFormCreate($form, $entity, $options);

        $className = $this->getSetup('hasManyToMany', $alias, 'forms', $form);

        $instance = new $className($entity, $options);

//        $this->afterHasManyToManyFormCreate($instance, $form, $entity, $options);

        return $instance;
    }

    /**
     * @param mixed $id
     * @param string $alias
     * @return mixed
     */
    protected function redirectToIndexHasManyToManyAction($id, $alias)
    {
        return $this->redirect('index-has-many-to-many', null, null, [
            $id
            , $alias
        ]);
    }

    /**
     * @param $alias
     * @param $type
     * @param bool $lcFirst
     * @return mixed
     * @throws Exception
     */
    protected function getHasManyToManyName($alias, $type, $lcFirst = false)
    {
        $name = $this->translate->_($this->getSetup('hasManyToMany', $alias, 'name', $type));

        if ($lcFirst) {
            $name = Str::firstLower($name);
        }

        return $name;
    }

    /**
     * @param string $alias
     * @param null|AbstractModel $model
     * @param null|AbstractModel $hasManyToManyModel
     * @return AbstractModel
     * @throws Exception
     */
    protected function getHasManyToManyRelationModel(
        $alias
        , $model = null
        , $hasManyToManyModel = null
    )
    {

        $rm = $this->getSetup('hasManyToMany', $alias, 'relationModel');
        $this->hasManyToManyRelationModel = new $rm;

        $mpkAttribute = $this->getSetup('hasManyToMany', $alias, 'relationModelPrimaryKeys', 'model');
        $modelPrimaryKeyAttribute = $mpkAttribute ? $mpkAttribute : $model->getPrimaryKeyAttribute();

        $hmmpkAttribute = $this->getSetup('hasManyToMany', $alias, 'relationModelPrimaryKeys', 'hasManyToManyModel');
        $hasManyToManyModelPrimaryKeyAttribute = $hmmpkAttribute ? $hmmpkAttribute : $hasManyToManyModel->getPrimaryKeyAttribute();

        if ($model->getPrimaryKey() && $hasManyToManyModel->getPrimaryKey()) {
            // konkrét rekordot keresünk
            $arguments =
                $modelPrimaryKeyAttribute . ' = ' . $model->getPrimaryKey()
                . ' AND ' .  $hasManyToManyModelPrimaryKeyAttribute . ' = ' . $hasManyToManyModel->getPrimaryKey()
            ;

            $this->hasManyToManyRelationModel = $this->hasManyToManyRelationModel::findFirst($arguments);

            if (!$this->hasManyToManyRelationModel) {

                $this->hasManyToManyRelationModel = new $rm;

                // létrehozzuk, de még nem mentjük el ha nem létezik
                $this->hasManyToManyRelationModel->{$modelPrimaryKeyAttribute}
                    = $model->getPrimaryKey()
                ;

                $this->hasManyToManyRelationModel->{$hasManyToManyModelPrimaryKeyAttribute}
                    = $hasManyToManyModel->getPrimaryKey()
                ;
            }
        }

        return $this->hasManyToManyRelationModel;
    }

    /**
     * beforeHasManyToManyModelCreate
     */
    public function beforeHasManyToManyModelCreate()
    {
    }

    /**
     * beforeHasManyToManyModelModify
     */
    public function beforeHasManyToManyModelModify()
    {
    }

    /**
     * beforeHasManyToManyFormCreate
     */
    public function beforeHasManyToManyFormCreate($form, $entity, $options)
    {
    }

   /**
     * beforeHasManyToManyModelAssign
     */
    public function beforeHasManyToManyModelAssign()
    {
    }

    /**
     * Ósszes kapcsolat HeadingButton hozzáadása
     * @param $id null|integer
     * @throws \Exception
     */
    public function addAllHasManyToManyEntityHeadingButtons($id = null)
    {
        $setup = $this->getSetup();

        /**  hasMany kapcsolat - ilyen nem biztos, hogy van */
        if ($setup['hasMany']) {
            foreach ($setup['hasMany'] as $alias => $value) {
                $this->addHasManyEntityHeadingButtons($id, $alias);
            }
        }

        /**  hasManyToMany kapcsolat - ilyen biztosan van */
        foreach ($setup['hasManyToMany'] as $alias => $value) {
            $this->addHasManyToManyEntityHeadingButtons($id, $alias);
        }

    }

    /**
     * afterHasManyToManyModelAssign
     */
    public function afterHasManyToManyModelAssign()
    {
    }

    /**
     * getHasManyToManyIndexName
     * @return string
     */
    protected function getHasManyToManyIndexName()
    {
        return '/hasManyToMany/index';
    }

    /**
     * addHasManyToManyUrls
     * @param $params
     */
    protected function addHasManyToManyUrls($params)
    {
    }

}