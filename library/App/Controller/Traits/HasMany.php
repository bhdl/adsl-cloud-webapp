<?php

namespace App\Controller\Traits;

use App\Form;
use App\Model\AbstractModel;
use App\Model\Presenter\AbstractPresenter;
use DataTables\DataTable;
use Phalcon\Exception;

/**
 * Trait One2Many
 * @package App\Controller\Traits
 */
trait HasMany
{
    /**
     * @var AbstractModel
     */
    protected $hasManyModel;

    /**
     * @var Form
     */
    protected $hasManyForm;

    /**
     * initialize
     */
    public function initializeHasMany()
    {

        $this->getSetup('hasMany');

        $this->view->urls += [
            'indexHasMany' => $this->url('index-has-many')
        ];
    }

    /**
     * @param $id
     * @param $alias
     * @throws Exception
     */
    public function indexHasManyAction($id, $alias)
    {
        parent::addEntityHeadingButtons();

        $this->addAllHasManyEntityHeadingButtons($id);

        // FIXME: hack
        /*if (method_exists($this, 'addHasManyToManyEntityHeadingButtons')) {
            $this->addHasManyToManyEntityHeadingButtons($id, $alias);
        }*/

        $this->addHasManyEntityBreadCrumbs($id, $alias);

        $params = [
            $id
            , $alias
        ];

        $this->view->urls = [
            'dataTableHasMany' => $this->url('data-table-has-many', null, null, $params)
            , 'modifyHasMany'  => $this->url('modify-has-many', null, null, $params)
            , 'createHasMany'  => $this->url('create-has-many', null, null, $params)
            , 'deleteHasMany'  => $this->url('delete-has-many', null, null, $params)
            , 'viewHasMany'    => $this->url('view-has-many', null, null, $params)
            , 'assignHasMany'  => $this->url('assign-has-many', null, null, $params)
            , 'ajaxHasMany'    => $this->url('ajax') . '?operation=sortableHasMany&alias=' . $alias
        ];

        $this->view->builderParams = [
            'relationModel' => $this->getSetup('hasMany', $alias, 'model')
            , 'primaryKeyAttribute' => $this->model->getPrimaryKeyAttribute()
            , 'modelId' => $id
        ];
        $alias = $this->getCamelCaseAlias($alias);
        $this->view->pick($alias . '/hasMany/index');
    }

    /**
     * @param mixed $id
     * @param string $alias
     * @throws \Phalcon\Exception
     */
    public function dataTableHasManyAction($id, $alias)
    {
        if (!$this->request->isAjax()) {
            return;
        }

        $p = $this->getBuilderParams();
        $p[$this->getModel()->getPrimaryKeyAttribute()] = $id;

        $builder = $this->createHasManyBuilder($alias, $p);

        $dataTables = new DataTable();
        $dataTables->fromBuilder($builder)->sendResponse();

        $responseData = $dataTables->getResponse();

        $presenter = $this->getHasManyPresenter($alias);

        if ($presenter){
            $responseData['data'] = $presenter->setRows($responseData['data'])->present();
        }

        return $this->response->setJsonContent($responseData);
    }

    /**
     * @param $id
     * @param $alias
     * @param $hasManyId
     * @throws Exception
     */
    public function assignHasManyAction($id, $alias, $hasManyId)
    {
        $this->hasManyModel = $this->getHasManyModel($alias, $hasManyId);

        try {
            $this->db->beginLogged();

            $this->hasManyModel->{$this->getModel()->getPrimaryKeyAttribute()} = $id;

//            $this->beforeHasManyModelAssign();

            $this->hasManyModel->update();

//            $this->afterHasManyModelAssign();

            $message = $this->translate->_(
                'A(z) %name% kapcsolat hozzáadaása sikerült.'
                , [
                    'name'      => $this->getSingularName()
                    , 'related' => $this->getHasManyName($alias, 'singular')
                ]
            );

            $this->logger->info($message);

            $this->db->commitLogged();

            $this->flashSession->success($message);

        } catch (Exception $e) {

            $this->db->rollbackLogged();

            $message = $this->translate->_(
                'A(z) %name% kapcsolat hozzáadaása közben hiba történt: %message%'
                , [
                    'name'      => $this->getSingularName()
                    , 'message' => $e->getMessage()
                ]
            );

            $this->flashSession->error($message);

            $this->logger->error($message, ['exception' => $e]);
        }

        $this->redirectToIndexHasManyAction($id, $alias);
    }

    /**
     * @param $id
     * @param $alias
     * @return mixed
     * @throws Exception
     */
    public function createHasManyAction($id, $alias)
    {
        $this->hasManyForm = $this->getHasManyForm($alias, 'create');

        if ($this->request->isPost()
            && $this->hasManyForm->isValid($this->request->getPost())) {

            try {

                $this->db->beginLogged();

                $this->hasManyModel = $this->getHasManyModel($alias);
                $this->hasManyModel->{$this->model->getPrimaryKeyAttribute()} = $id;

                $this->hasManyForm->bind(
                    $this->request->getPost()
                    , $this->hasManyModel
                );

                $this->beforeHasManyModelCreate();

                $this->hasManyModel->create();

//                $this->afterHasManyModelCreate();

                if (in_array('App\Model\Traits\HasLanguages', class_uses($this->hasManyModel))) {
                    $this->hasManyModel->createLanguages($this->hasManyForm);
                }

                $message = $this->translate->_(
                    'A(z) %name% létrehozása sikerült.'
                    , ['name' => $this->getHasManyName($alias, 'singular')]
                );

                $this->logger->info($message);

                $this->db->commitLogged();

                $this->flashSession->success($message);

                return $this->redirectToIndexHasManyAction($id, $alias);

            } catch (\Exception $e) {

                $this->db->rollbackLogged();

                $message = $this->translate->_(
                    'A(z) %name% létrehozása közben hiba történt: %message%'
                    , [
                        'name'      => $this->getHasManyName($alias, 'singular')
                        , 'message' => $e->getMessage()
                    ]
                );

                if ($this->hasManyModel->validationHasFailed()) {
                    $message = $this->hasManyModel->getMessagesAsString();
                }

                $this->flash->error($message);

                $this->logger->error($message, ['exception' => $e]);
            }
        }

        $url = $this->url('index-has-many',null,null, [$id, $alias]);

        $this->addHasManyEntityBreadCrumbs($id, $alias);

        $this->breadcrumb->add(
            $this->translate->t('Létrehozás')
        );

        $this->headingButtons->add(
            $this->translate->_('Vissza a listára')
            , $url
            , 'icon-arrow-left16'
        );

        $this->view->form = $this->hasManyForm;

        $this->view->urls = [
            'index' => $url
        ];

        $this->view->pick('partials/crud/create');
    }

    /**
     * @param $id
     * @param $alias
     * @param $hasManyId
     * @return mixed
     * @throws Exception
     */
    public function modifyHasManyAction($id, $alias, $hasManyId)
    {
        $this->hasManyModel = $this->getHasManyModel($alias, $hasManyId);

        $this->hasManyForm = $this->getHasManyForm($alias, 'modify', $this->hasManyModel);

        if ($this->request->isPost()
            && $this->hasManyForm->isValid($this->request->getPost())) {

            try {
                $this->db->beginLogged();

                $this->hasManyForm->bind(
                    $this->request->getPost()
                    , $this->hasManyModel
                );

                $this->beforeHasManyModelModify();

                $this->hasManyModel->update();

//                $this->afterHasManyModelModify();

                if (in_array('App\Model\Traits\HasLanguages', class_uses($this->hasManyModel))) {
                    $this->hasManyModel
                        ->deleteLanguages()
                        ->createLanguages($this->hasManyForm)
                    ;
                }

                $message = $this->translate->_(
                    'A(z) %name% módosítása sikerült.'
                    , ['name' => $this->getHasManyName($alias, 'singular')]
                );

                $this->logger->info($message);

                $this->db->commitLogged();

                $this->flashSession->success($message);

                return $this->redirectToIndexHasManyAction($id, $alias);

            } catch (Exception $e) {

                $this->db->rollbackLogged();

                $message = $this->translate->_(
                    'A(z) %name% módosítása közben hiba történt: %message%'
                    , [
                        'name'      => $this->getHasManyName($alias, 'singular')
                        , 'message' => $e->getMessage()
                    ]
                );

                if ($this->hasManyModel->validationHasFailed()) {
                    $message = $this->hasManyModel->getMessagesAsString();
                }

                $this->flash->error($message);

                $this->logger->error($message, ['exception' => $e]);
            }
        }

        $url = $this->url('index-has-many',null,null, [$id, $alias]);

        $this->addHasManyEntityBreadCrumbs($id, $alias, $hasManyId);

        $this->breadcrumb->add(
            $this->translate->t('Módosítás')
        );

        $this->headingButtons->add(
            $this->translate->_('Vissza a listára')
            , $url
            , 'icon-arrow-left16'
        );

        $this->view->form = $this->hasManyForm;

        $this->view->urls = [
            'index' => $url
        ];

        $this->view->pick('partials/crud/modify');
    }

    /**
     * @param $id
     * @param $alias
     * @param $hasManyId
     * @throws Exception
     */
    public function deleteHasManyAction($id, $alias, $hasManyId)
    {
        $this->hasManyModel = $this->getHasManyModel($alias, $hasManyId);

        try {

            $this->db->beginLogged();

//            $this->beforeHasManyModelDelete();

            $result = $this->hasManyModel->delete();
            if (!$result) {
                throw new Exception();
            }

//            $this->afterHasManyModelDelete();

            $message = $this->translate->_(
                'A(z) %name% törlése sikerült.'
                , ['name' => $this->getHasManyName($alias, 'singular')]
            );

            $this->logger->info($message);

            $this->db->commitLogged();

            $this->flashSession->success($message);

        } catch (Exception $e) {

            $this->db->rollbackLogged();

            $message = $this->translate->_(
                'A(z) %name% törlése közben hiba történt: %message%'
                , [
                    'name'      => $this->getHasManyName($alias, 'singular')
                    , 'message' => $e->getMessage()
                ]
            );

            if ($this->hasManyModel->validationHasFailed()) {
                $message = $this->hasManyModel->getMessagesAsString();
            }

            $this->flashSession->error($message);

            $this->logger->error($message, ['exception' => $e]);
        }

        $this->redirectToIndexHasManyAction($id, $alias);
    }

    /**
     * @param $id
     * @param $alias
     * @param $hasManyId
     * @throws Exception
     */
    public function viewHasManyAction($id, $alias, $hasManyId)
    {
        $this->hasManyModel = $this->getHasManyModel($alias, $hasManyId);

        $this->view->model = $this->hasManyModel;

        $this->addHasManyEntityBreadCrumbs($id, $alias, $hasManyId);

        $this->breadcrumb->add(
            $this->translate->t('Áttekintés')
        );

        $this->headingButtons->add(
            $this->translate->_('Vissza a listára')
            , $this->url('index-has-many',null,null, [$id, $alias])
            , 'icon-arrow-left16'
        );

        $alias = $this->getCamelCaseAlias($alias);
        $this->view->pick($alias . '/hasMany/view');
    }

    /**
     * @param mixed $id
     * @param string $alias
     * @param mixed $hasManyId
     * @throws Exception
     */
    protected function addHasManyEntityBreadCrumbs($id, $alias, $hasManyId)
    {
        parent::addEntityBreadCrumbs();

        $url = null;

        if ($this->hasManyModel && $this->acl->isAllowedByIdentity($this->getResourceId(), 'viewHasMany')) {
            $url = $this->url('view-has-many', null, null, [
                $id
                , $alias
                , $this->hasManyModel->getPrimaryKey()
            ]);
        }

        $this->breadcrumb->add(
            $this->translate->t($this->getHasManyName($alias, 'plural'))
        );

        if ($this->hasManyModel) {
            $this->breadcrumb->add(
                $this->hasManyModel->getName() ?: 'N/A'
                , $url
            );
        }
    }

    /**
     * @param mixed $id
     * @param string $alias
     * @param mixed $hasManyId
     */
    protected function addHasManyEntityHeadingButtons($id, $alias, $hasManyId = null)
    {
        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'indexHasMany')) {
            $this->headingButtons->add(
                $this->translate->_($this->getSetup('hasMany', $alias, 'name', 'plural'))
                , $this->url('index-has-many', null, null, [
                    $this->model->getPrimaryKey()
                    , $alias
                ])
                , $this->getSetup('hasMany', $alias, 'icon')
            );
        }
    }

    /**
     * @param string $alias
     * @return AbstractPresenter
     * @throws \Exception
     */
    protected function getHasManyPresenter($alias)
    {
        $p = $this->getSetup('hasMany', $alias, 'presenter');
        return new $p;
    }


    /**
     * @param string $alias
     * @param array $p
     * @return mixed
     * @throws Exception
     */
    protected function createHasManyBuilder($alias, $p = [])
    {
        $b = $this->getSetup('hasMany', $alias, 'builder');
        $builder = new $b;

        $builder->setParams($p);

        return $builder->build();
    }

    /**
     * @param $alias
     * @param null $hasManyId
     * @return AbstractModel
     * @throws Exception
     */
    public function getHasManyModel($alias, $hasManyId = null)
    {
        if ($this->hasManyModel !== null) {
            return $this->hasManyModel;
        }

        $m = $this->getSetup('hasMany', $alias, 'model');
        $this->hasManyModel = new $m;

        if ($hasManyId == null) {
            return $this->hasManyModel;
        }

        $method = 'findFirstBy' . ucfirst($this->hasManyModel->getPrimaryKeyAttribute());

        $this->hasManyModel = $this->getHasManyModel($alias)::$method($hasManyId);

        if (!is_object($this->hasManyModel)) {
            throw new Exception(
                sprintf('A keresett %s nem található.', $this->getHasManyName($alias, 'singular'))
            );
        }

        return $this->hasManyModel;
    }

    /**
     * @param $alias
     * @param $form
     * @param null $entity
     * @param null $options
     * @return mixed
     * @throws Exception
     */
    protected function getHasManyForm($alias, $form, $entity = null, $options = null)
    {
        $this->beforeHasManyFormCreate($form, $entity, $options);

        $className = $this->getSetup('hasMany', $alias, 'forms', $form);

        $instance = new $className($entity, $options);

//        $this->afterHasManyFormCreate($instance, $form, $entity, $options);

        return $instance;
    }

    /**
     * @param mixed $id
     * @param string $alias
     * @return mixed
     */
    protected function redirectToIndexHasManyAction($id, $alias)
    {
        return $this->redirect('index-has-many', null, null, [
            $id
            , $alias
        ]);
    }

    /**
     * @param $alias
     * @param $type
     * @param bool $lcFirst
     * @return mixed
     * @throws Exception
     */
    protected function getHasManyName($alias, $type, $lcFirst = false)
    {
        $name = $this->translate->_($this->getSetup('hasMany', $alias, 'name', $type));

        if ($lcFirst) {
            $name = Str::firstLower($name);
        }

        return $name;
    }

    /**
     * Ósszes kapcsolat HeadingButton hozzáadása
     * @param $id null|integer
     * @throws \Exception
     */
    public function addAllHasManyEntityHeadingButtons($id = null)
    {
        $setup = $this->getSetup();

        /**  hasMany kapcsolat - ilyen biztosan van */
        foreach ($setup['hasMany'] as $alias => $value) {
            $this->addHasManyEntityHeadingButtons($id, $alias);
        }

        /**  hasManyToMany kapcsolat - ilyen nem biztos, hogy van */
        if ($setup['hasManyToMany']) {
            foreach ($setup['hasManyToMany'] as $alias => $value) {
                $this->addHasManyToManyEntityHeadingButtons($id, $alias);
            }
        }
    }

    /**
     * beforeHasManyFormCreate
     */
    public function beforeHasManyFormCreate($form, $entity, $options)
    {
    }

    /**
     * beforeHasManyModelCreate
     */
    public function beforeHasManyModelCreate()
    {
    }

    /**
     * beforeHasManyModelModify
     */
    public function beforeHasManyModelModify()
    {
    }
}