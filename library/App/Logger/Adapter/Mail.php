<?php

namespace App\Logger\Adapter;

use Phalcon\Logger\Adapter as LoggerAdapter;
use Phalcon\Logger\AdapterInterface;

/**
 * Class Mail
 * @package App\Logger\Adapter
 */
class Mail extends LoggerAdapter implements AdapterInterface
{
    /**
     * @var \Phalcon\Cache\Backend
     */
    protected $cache;

    /**
     * @var \Phalcon\Mailer\Manager
     */
    protected $mailer;

    /**
     * A rendszer neve, amiről az üzenet megy
     * @var string
     */
    protected $application;

    /**
     * @var string
     */
    protected $to = 'dev@arteries.hu';

    /**
     * @var string
     */
    protected $toName = 'Arteries developers';

    /**
     * Mail constructor.
     * @param $application
     * @param $cache
     * @param $mailer
     */
    public function __construct($application, $cache, $mailer)
    {
        $this->setCache($cache);

        $this->setMailer($mailer);

        $this->setApplication($application);
    }

    /**
     * @param $message
     * @param $type
     * @param $time
     * @param array $context
     * @return mixed
     */
    public function logInternal($message, $type, $time, $context = [])
    {
        if (!$this->accept($message)) {
            return;
        }

        $message = $this->mailer->createMessage()
            ->setFormat('text/html')
            ->to($this->to, $this->toName)
            ->subject('[' . $this->application . '] Log notification')
            ->content($this->assembleContent($message, $time, $type))
        ;

        $message->send();
    }

    /**
     * @param $message
     * @param $time
     * @param $type
     * @return string
     */
    protected function assembleContent($message, $time, $type)
    {
        $content = '<html><head></head><body>';

        $content .= sprintf(
            '<p><b>Log event at application "%s". Please take action if necessary.</b></p>'
            , $this->application
        );

        $content .= '<table>';

        $content .= '<tr>';
        $content .= '<th align="left">Date and time</th>';
        $content .= '<th align="left">Level</th>';
        $content .= '</tr>';

        $content .= '<tr>';
        $content .= '<td align="left">' . date('Y-m-d H:i:s', $time) . '</td>';
        $content .= '<td align="left">' . $type . '</td>';
        $content .= '</tr>';

        $content .= '<tr>';
        $content .= '<td colspan="2">' . $message . '</td>';
        $content .= '</tr>';
        $content .= '</table>';


        $content .= '</body></html>';

        return $content;
    }

    /**
     * Returns TRUE to accept the message, FALSE to block it.
     *
     * @param  string $message
     * @return boolean
     */
    public function accept($message)
    {
        $messageId = md5($message);

        if (!$this->cache->exists($messageId)) {
            $this->cache->save($messageId, time());
            return true;
        }

        return false;
    }

    /**
     * Returns the internal formatter
     *
     * @return \Phalcon\Logger\FormatterInterface
     */
    public function getFormatter()
    {
        if (!is_object($this->_formatter)) {
            $this->_formatter = new LineFormatter();
        }

        return $this->_formatter;
    }

    /**
     * Closes the logger
     *
     * @return bool
     */
    public function close()
    {
        return true;
    }

    /**
     * @return \Phalcon\Cache\Backend
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @param \Phalcon\Cache\Backend $cache
     * @return Mail
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * @return \Phalcon\Mailer\Manager
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * @param \Phalcon\Mailer\Manager $mailer
     */
    public function setMailer($mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @return string
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param string $application
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getToName()
    {
        return $this->toName;
    }

    /**
     * @param string $toName
     */
    public function setToName($toName)
    {
        $this->toName = $toName;
    }
}