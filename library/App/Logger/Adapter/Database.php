<?php

namespace App\Logger\Adapter;

use App\Model\AbstractLogModel;
use App\Model\Behavior\LogModelCollector;
use Phalcon\Db\Column;
use Phalcon\Mvc\Model;
use Phalcon\Di;
use Phalcon\Exception;
use Phalcon\Logger\Formatter\Line as LineFormatter;
use Phalcon\Logger\Adapter as LoggerAdapter;
use Phalcon\Logger\AdapterInterface;
use Phalcon\Db\AdapterInterface as DbAdapterInterface;

/**
 * Class Database
 * @package App\Logger\Adapter
 */
class Database
    extends LoggerAdapter
    implements AdapterInterface
{
    /**
     * Name
     * @var string
     */
    protected $name = 'app';

    /**
     * Adapter options
     * @var array
     */
    protected $options = [];

    /**
     * @var \Phalcon\Db\AdapterInterface
     */
    protected $db;

    /**
     * @var array
     */
    protected $skipModelAttributes = [];

    /**
     * Class constructor.
     *
     * @param  string $name
     * @param  array $options
     * @throws \Phalcon\Exception
     */
    public function __construct($name = 'app', array $options = [])
    {
        if (!isset($options['db'])) {
            throw new Exception("Parameter 'db' is required");
        }

        if (!$options['db'] instanceof DbAdapterInterface) {
            throw new Exception("Parameter 'db' must be object and implement AdapterInterface");
        }

        if (!isset($options['table'])) {
            throw new Exception("Parameter 'table' is required");
        }

        $this->db = $options['db'];

        if ($name) {
            $this->name = $name;
        }

        $this->options = $options;
    }

    /**
     * Sets database connection
     *
     * @param AdapterInterface $db
     * @return $this
     */
    public function setDb(AdapterInterface $db)
    {
        $this->db = $db;
        return $this;
    }

    /**
     * {@inheritdoc}
     *
     * @return \Phalcon\Logger\FormatterInterface
     */
    public function getFormatter()
    {
        if (!is_object($this->_formatter)) {
            $this->_formatter = new LineFormatter();
        }
        return $this->_formatter;
    }

    /**
     * @param Exception $exception
     * @return string
     */
    protected function assembleException($exception)
    {
        return implode("\n", [
            'Exception: ' . get_class($exception)
            , 'Message: ' . $exception->getMessage()
            , 'File: ' . $exception->getFile()
            , 'Line: ' . $exception->getLine()
            , 'Stack trace:'
            , $exception->getTraceAsString()
        ]);
    }

    /**
     * {@inheritdoc}
     *
     * @return boolean
     */
    public function close()
    {
        if ($this->db->isUnderTransaction()) {
            $this->db->commit();
        }
        $this->db->close();
        return true;
    }

    /**
     * {@inheritdoc}
     *
     * @return $this
     */
    public function begin()
    {
        $this->db->begin();
        return $this;
    }

    /**
     * Commit transaction
     *
     * @return $this
     */
    public function commit()
    {
        $this->db->commit();
        return $this;
    }

    /**
     * Rollback transaction
     * (happens automatically if commit never reached)
     *
     * @return $this
     */
    public function rollback()
    {
        $this->db->rollback();
        return $this;
    }

    /**
     * Writes the log to the file itself
     *
     * @param string $message
     * @param integer $priority
     * @param integer $time
     * @param array $context
     * @return bool|void
     */
    public function logInternal($message, $priority, $time, $context = [])
    {
        $userId = Di::getDefault()->get('auth')->getIdentity()->userId;

        $exception = null;
        if (is_array($context) && array_key_exists('exception', $context)) {
            $exception = $this->assembleException($context['exception']);
        }

        $this->db->execute(
            'INSERT INTO ' . $this->options['table'] . ' VALUES (NULL, ?, ?, ?, ?, ?, ?)',
            [$userId, $this->name, $priority, $message, $exception, date('Y-m-d H:i:s', $time)],
            [Column::BIND_PARAM_INT, Column::BIND_PARAM_STR, Column::BIND_PARAM_INT, Column::BIND_PARAM_STR, Column::BIND_PARAM_STR, Column::BIND_PARAM_STR]
        );

        $models = LogModelCollector::getModels();

        if (!is_array($models) || count($models) == 0) {
            // nincs változott model
            return;
        }

        $logId = $this->db->lastInsertId();

        foreach ($models as $m) {

            $model = new $m['model'];

            // attribútum szintű naplózás, ha AbstractLogModel-ről van szó
            if (!($model instanceof AbstractLogModel)) {
                continue;
            }

            $pkAttributes = $model
                ->getModelsMetaData()
                ->getPrimaryKeyAttributes($model)
            ;
            $primaryKeys = [];
            if (is_array($pkAttributes)) {
                foreach ($pkAttributes as $pkAttribute) {
                    $primaryKeys[] = $m['data'][$pkAttribute];
                }
            }

            // log model változás (melyik model, mi az id)
            $this->db->execute(
                'INSERT INTO log_model VALUES (NULL, ?, ?, ?, ?)',
                [$logId, implode('@', $primaryKeys), $m['operation'], get_class($model)],
                [Column::BIND_PARAM_INT, Column::BIND_PARAM_STR, Column::BIND_PARAM_STR]
            );

            $logModelId = $this->db->lastInsertId();

            /*
             * Törlés esetén nincs mező szintű lekövetés ...
             */
            if ($m['operation'] == Model::OP_DELETE) {
                continue;
            }

            // ezek a mezők változtak (létrehozás esetén az összes)
            if (!is_array($m['changed']) || count($m['changed']) == 0) {
                continue;
            }

            // megváltozott attribútumok
            foreach ($m['changed'] as $changed) {

                // nem naplózzuk, ha speciális attribútum pl. jelszó
                if ($this->isSkipModelAttribute($changed)) {
                    continue;
                }

                $newValue = $m['data'][$changed];
                $oldValue = array_key_exists($changed, $m['snapShotData'])
                    ? $m['snapShotData'][$changed]
                    : null
                ;

                if ($newValue == null && $oldValue == null) {
                    continue;
                }

                if (strcmp($newValue, $oldValue) === 0) {
                    continue;
                }

                // oldValue és newValue kiderítése (enumok, idegen kulcsok, kapcsolótáblák)
                $this
                    ->resolveBelongsTo($model, $changed, $oldValue, $newValue)
                    //->resolveHasMany($model, $changedField, $oldValue, $newValue)
                    //->resolveEnum($model, $changedField, $oldValue, $newValue)
                ;

                $this->db->execute(
                    'INSERT INTO log_model_attribute VALUES (NULL, ?, ?, ?, ?)',
                    [$logModelId, $changed, $oldValue, $newValue],
                    [Column::BIND_PARAM_INT, Column::BIND_PARAM_STR, Column::BIND_PARAM_STR, Column::BIND_PARAM_STR]
                );
            }
        }
    }

    /**
     * @param Model $model
     * @param string $changedField
     * @param mixed $oldValue
     * @param mixed $newValue
     * @return Database
     */
    protected function resolveEnum(Model $model, $changedField, &$oldValue, &$newValue)
    {
        // TODO
        return $this;
    }

    /**
     * @param Model $model
     * @param string $changedField
     * @param mixed $oldValue
     * @param mixed $newValue
     * @return Database
     */
    protected function resolveHasMany(Model $model, $changedField, &$oldValue, &$newValue)
    {
        // TODO
        return $this;
    }

    /**
     * @param Model $model
     * @param string $changedField
     * @param mixed $oldValue
     * @param mixed $newValue
     * @return Database
     */
    protected function resolveBelongsTo(Model $model, $changedField, &$oldValue, &$newValue)
    {
        // TODO: további relációk?
        $relations = $model
            ->getModelsManager()
            ->getBelongsTo($model)
        ;

        /**
         * @var Model\Relation $relation
         */
        foreach ($relations as $relation) {
            // nem idegen kulcs, vagy nem változott
            if (strcmp($relation->getFields(), $changedField) === 0) {
                $refModel = $relation->getReferencedModel();

                if ($newValue) {

                    $id = $newValue;
                    if (!is_numeric($newValue)) {
                        $id = $changedField . ' =\'' . $newValue . '\'';
                    }

                    $newRelatedModel = $refModel::findFirst($id);
                    if ($newRelatedModel instanceof AbstractLogModel) {
                        $newValue = $newRelatedModel->getName();
                    }
                }

                if ($oldValue) {

                    $id = $oldValue;
                    if (!is_numeric($oldValue)) {
                        $id = $changedField . ' =\'' . $oldValue . '\'';
                    }

                    $oldRelatedModel = $refModel::findFirst($id);
                    if ($oldRelatedModel instanceof AbstractLogModel) {
                        $oldValue = $oldRelatedModel->getName();
                    }
                }

                break;
            }
        }

        return $this;
    }

    /**
     * @param string $attribute
     * @return bool
     */
    public function isSkipModelAttribute($attribute)
    {
        return in_array($attribute, $this->getSkipModelAttributes());
    }

    /**
     * @return array
     */
    public function getSkipModelAttributes(): array
    {
        return $this->skipModelAttributes;
    }

    /**
     * @param array $skipModelAttributes
     * @return Database
     */
    public function setSkipModelAttributes(array $skipModelAttributes): Database
    {
        $this->skipModelAttributes = $skipModelAttributes;
        return $this;
    }

    /**
     * @param string $attribute
     * @return Database
     */
    public function addSkipModelAttribute($attribute)
    {
        $this->skipModelAttributes = $attribute;

        return $this;
    }

    /**
     * @param array $attributes
     * @return Database
     */
    public function addSkipModelAttributes($attributes)
    {
        foreach ($attributes as $attribute) {
            $this->addSkipModelAttribute($attribute);
        }

        return $this;
    }
}