<?php

namespace App\Validation;

use App\Validation\Validator\PresenceOf;
use Phalcon\Mvc\User\Component;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email;

class Contact
    extends Component
{
    public function isValid($contactData)
    {
        $validation = new Validation();

        $validation->add(
            'fromName'
            , new PresenceOf([
                'message'        => $this->translate->t('A név mező kitöltése kötelező!')
            ])
        );

        $validation->add(
            'fromEmail'
            , new PresenceOf(array(
                'message' => $this->translate->t('Az email mező kitöltése kötelező!')
            ))
        );

        $validation->add(
            'fromEmail'
            ,new Email(array(
                'message' => $this->translate->t('Érvénytelen e-mail cím!')
            ))
        );

        $validation->add(
            'contactMessage'
            , new PresenceOf([
                'message' => $this->translate->_('Az üzenet mező kitöltése kötelező!')
            ])
        );

        return $validation->validate($contactData);
    }
}