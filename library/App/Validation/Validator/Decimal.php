<?php

namespace App\Validation\Validator;

use Phalcon\Validation;

/**
 * Validates Decimal
 *
 * <code>
 * new \Phalcon\Validation\Validator\Decimal([
 *     'message' => {string - validation message}
 *     'scale'  => {integer - Számok a tizedesvessző után}
 *     'precision'  => {integer - Maximum ennyi számjegyből állhat}
 * ])
 * </code>
 *
 * @package App\Validation\Validator
 */
class Decimal extends Validation\Validator
{
    /**
     * Számok a tizedesvessző után
     * A tizedesjegyeket nem kötelező megadni, de ha megadjuk akkor maximum ennyi lehet
     * @var int
     */
    const DEFAULT_SCALE = 2;

    /**
     * Maximum ennyi számjegyből állhat
     * @var int
     */
    const DEFAULT_PRECISION = 4;

    /**
     * Value validation
     * @param   \Phalcon\Validation $validation - validation object
     * @param   string $attribute - validated attribute
     * @return  bool
     */
    public function validate(Validation $validation, $attribute)
    {
        $scale = $this->hasOption('scale') ? intval($this->getOption('scale')) : self::DEFAULT_SCALE;
        $precision = $this->hasOption('precision') ? intval($this->getOption('precision')) : self::DEFAULT_PRECISION;

        $allowEmpty = $this->getOption('allowEmpty');
        $value = $validation->getValue($attribute);

        if ($allowEmpty && ((is_scalar($value) && (string) $value === '') || is_null($value))) {
            return true;
        }

        if ($this->hasOption('point')) {
            $point = $this->getOption('point');
        } else {
            list($point) = array_values(localeconv());
        }

        $pattern = '/^-?\d{1,' . ($precision - $scale) . '}'
            . ($scale > 0 ? '(\\' . $point . '\d{1,' . $scale . '})?' : '')
            . '$/'
        ;

        if (preg_match($pattern, $value)) {
            return true;
        }

        $message = ($this->hasOption('message') ? $this->getOption('message') : 'Incorrect Decimal Format!');

        $validation->appendMessage(
            new Validation\Message($message, $attribute, 'DecimalValidator')
        );

        return false;
    }
}