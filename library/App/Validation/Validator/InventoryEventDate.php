<?php

namespace App\Validation\Validator;

use Phalcon\Validation;
use App\Support\Date;
use Phalcon\Di;

/**
 * @package App\Validation\Validator
 */
class InventoryEventDate extends Validation\Validator
{
    public function __construct()
    {
        $this->translate = Di::getDefault()->get('translate');
    }

    /**
     * Value validation
     *
     * @param   \Phalcon\Validation $validation - validation object
     * @param   string $attribute - validated attribute
     * @return  bool
     */
    public function validate(Validation $validation, $attribute)
    {
        $startTime = $validation->getValue(
            $this->hasOption('startTime') ? $this->getOption('startTime') : 'startTime'
        );
        $endTime = $validation->getValue($attribute);

        $newStartTimeFormat = \DateTime::createFromFormat('Y-m-d H:i:s', $startTime);
        $newEndTimeFormat = \DateTime::createFromFormat('Y-m-d H:i:s', $endTime);
        
        if ($this->endTimeErr($newEndTimeFormat)) {
            $validation->appendMessage(
                $this->compareCurrentTimeWithEndTime($newEndTimeFormat, $attribute)
            );
        }

        if ($this->startTimeErr($newStartTimeFormat, $newEndTimeFormat)) {
            $validation->appendMessage(
                $this->compareStartTimeWithEndTime($newStartTimeFormat, $newEndTimeFormat, $attribute)
            );
            return false;
        }

        return true;
    }

    private function endTimeErr($endTime)
    {
        return ($endTime->getTimestamp() > time());
    }

    private function startTimeErr($startTime, $endTime)
    {
        return ($startTime->getTimestamp() > $endTime->getTimestamp());
    }

    private function compareCurrentTimeWithEndTime($endTime, $attribute) {
        if ($endTime->getTimestamp() > time()) {
            $message = $this->translate->t("Jövőbeli leltározási esemény nem hozható létre");
            return new Validation\Message($message, $attribute, 'InventoryEventWrongEndDateTime');
        }
    }

    private function compareStartTimeWithEndTime($startTime, $endTime, string $attribute) {
        if ($startTime->getTimestamp() > $endTime->getTimestamp()) {
            $message = $this->translate->t("A vég dátum nem lehet a kezdeti dátum előtti időpont");
            return new Validation\Message($message, $attribute, 'InventoryEventWrongStartDateTime');
        }
    }
}
