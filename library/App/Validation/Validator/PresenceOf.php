<?php

namespace App\Validation\Validator;

use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;

/**
 * Class PresenceOf
 * @package App\Validation
 */
class PresenceOf
    extends Validator\PresenceOf
{
    /**
     * Kiegészítés, hogy a [] típusú tömböket is kezelje
     *
     * @param \Phalcon\Validation $validation
     * @param string $attribute
     * @return bool
     */
    public function validate(\Phalcon\Validation $validation, $attribute)
    {
        if (mb_strpos($attribute, '[]') !== false) {
            // xyz[] típusú az attribútum (<input type="text" name="xyz[]" />
            $key   = rtrim($attribute, '[]');
            $value = $validation->getValue($key);

            if ($value === null || (is_string($value) && strcmp($value, '') === 0)) {
                $validation->appendMessage(
                    new Message(
                        $this->getOption('message')
                        , $attribute
                    )
                );
                return false;
            }

            return true;
        }

        return parent::validate($validation, $attribute);
    }
}
