<?php

namespace App\Validation\Validator;

use Phalcon\Validation;
use Phalcon\Validation\Message;

use Opis\JsonSchema\{
    Validator as SchemaValidator,
    ValidationResult  as SchemaValidationResult,
    ValidationError  as SchemaValidationError,
    Schema
};
use Phalcon\Di;


/**
 * Validates JSON data against given schema
 *
 * <code>
 * new \Phalcon\Validation\Validator\JsonSchema([
 *     'message' => {string - validation message}
 *     'schema'  => {string - name of the file in the schemas/json folder without .json extension}
 *     'allowEmpty'  => {boolean - true: the field can be empty}
 * ])
 * </code>
 *
 * @package App\Validation\Validator
 */
class JsonSchema extends Validation\Validator
{
    protected $messages = [
        'cannotBeEmpty'  => 'Nem lehet üres a mező.'
        , 'schemaNameIsEmpty' => 'Hiányzik a validációhoz szükséges séma neve, jelezze az alkalmazás üzemeltetőnek.'
        , 'schemaFileIsEmpty' => 'Hiányzik a validációhoz szükséges séma file, jelezze az alkalmazás üzemeltetőnek.'
    ];

    const JSON_SCHEMA_PATH = '/schemas/json/';

    /**
     * Returns JSON Schema object from filename
     *
     * @return JSONSchema
     */
    protected function getSchema($schemaName)
    {
        if (!$schemaName) {
            throw new \Exception($this->messages["schemaNameIsEmpty"]);
        }

        $schemaPath = APP_PATH . self::JSON_SCHEMA_PATH . $schemaName . ".json";
        $schemaFileContents = file_get_contents($schemaPath);
        if (!$schemaFileContents) {
            throw new \Exception($this->messages["schemaFileIsEmpty"] . " {$schemaPath}");
        }
        return Schema::fromJsonString($schemaFileContents);
    }

    /**
     * Value validation
     * @param   \Phalcon\Validation $validation - validation object
     * @param   string $attribute - validated attribute
     * @return  bool
     */
    public function validate(Validation $validation, $attribute)
    {
        $this->logger = Di::getDefault()->get('logger');
        $schemaName = $this->hasOption('schema') ? $this->getOption('schema') : false;
        $allowEmpty = $this->hasOption('allowEmpty') ? $this->getOption('allowEmpty') : false;

        try {
            $value = $validation->getValue($attribute);
            if (!$allowEmpty && !$value) {
                throw new \Exception($this->messages['cannotBeEmpty']);
            }
            $schema = $this->getSchema($schemaName);

            $validator = new SchemaValidator();

            $data = json_decode($value);
        
            /** @var SchemaValidationResult $result */
            $result = $validator->schemaValidation($data, $schema);
        }
        catch(\Exception $e) {
            $this->logger->error($e->getMessage());
            $validation->appendMessage(new Message(
                $e->getMessage()
                , $attribute
            ));

            return false;
        }
        
        if (!$result->isValid()) {
            /** @var SchemaValidationError $error */
            $error = $result->getFirstError();

            $message = ($this->hasOption('message') ? $this->getOption('message') : 'Incorrect JSON Format!');

            $message .= ' ' . $error->keyword() . ' ' . json_encode($error->keywordArgs(), JSON_PRETTY_PRINT);
            
            $validation->appendMessage(
                new Validation\Message($message, $attribute, 'JsonSchema')
            );
    
            return false;
        }
        
        return true;
    }
}
