<?php

namespace App\Validation\Validator;

use Phalcon\Validation;

/**
 * Password confirmation
 *
 * <code>
 * new \Phalcon\Validation\Validator\PasswordConfirmation([
 *     'password' => {string - password},
 *     'message' => {string - validation message}
 * ])
 * </code>
 *
 * @package App\Validation\Validator
 */
class PasswordConfirmation extends Validation\Validator
{
    /**
     * Value validation
     *
     * @param   \Phalcon\Validation $validation - validation object
     * @param   string $attribute - validated attribute
     * @return  bool
     */
    public function validate(Validation $validation, $attribute)
    {
        $password = $validation->getValue(
            $this->hasOption('password') ? $this->getOption('password') : 'password'
        );

        $confirm = $validation->getValue($attribute);

        if(strcmp($confirm, $password) === 0) {
            return true;
        }

        $message = ($this->hasOption('message') ? $this->getOption('message') : 'A jelszavak nem egyeznek!');

        $validation->appendMessage(
            new Validation\Message($message, $attribute, 'PasswordConfirmationValidator')
        );

        return false;
    }
}
