<?php

namespace App\Validation\Validator\Db;

use Phalcon\Validation\Validator;
use Phalcon\Validation\Message;
use Phalcon\Db\Adapter\Pdo as DbConnection;
use Phalcon\Validation\Exception as ValidationException;
use Phalcon\DiInterface;
use Phalcon\Di;
use Phalcon\Db;
use Phalcon\Validation;

/**
 * Phalcon\Validation\Validator\Db\Uniqueness
 *
 * Validator for checking uniqueness of field in database
 *
 * <code>
 * $uniqueness = new Uniqueness(
 *     [
 *         'table'   => 'users',
 *         'column'  => 'login',
 *         'message' => 'already taken',
 *         'exclude' => [
 *             'column' => 'id',
 *             'value' => 1 // Some ID to exclude
 *         ],
 *     ],
 *     $di->get('db');
 * );
 * </code>
 *
 * Exclude option is optional.
 *
 * If second parameter will be null (omitted) than validator will try to get database
 * connection from default DI instance with \Phalcon\Di::getDefault()->get('db');
 */
class Uniqueness extends Validator
{
    /**
     * Database connection
     * @var \Phalcon\Db\Adapter\Pdo
     */
    private $db;
    /**
     * Class constructor.
     *
     * @param  array $options
     * @param  DbConnection  $db
     * @throws ValidationException
     */
    public function __construct(array $options = [], DbConnection $db = null)
    {
        parent::__construct($options);
        if (!$db) {
            // try to get db instance from default Dependency Injection
            $di = Di::getDefault();
            if ($di instanceof DiInterface && $di->has('db')) {
                $db = $di->get('db');
            }
        }
        if (!$db instanceof DbConnection) {
            throw new ValidationException('Validator Uniqueness require connection to database');
        }
        if (!$this->hasOption('table')) {
            throw new ValidationException('Validator require table option to be set');
        }
        if (!$this->hasOption('column')) {
            throw new ValidationException('Validator require column option to be set');
        }
        if ($this->hasOption('exclude')) {
            $exclude = $this->getOption('exclude');
            if (is_array($exclude) && !isset($exclude['column'])) {
                foreach($exclude as $ex) {
                    $this->checkExclude($ex);
                }
            } else {
                $this->checkExclude($exclude);
            }
        }
        $this->db = $db;
    }

    public function checkExclude($exclude)
    {
        if (!isset($exclude['column']) || empty($exclude['column'])) {
            throw new ValidationException('Validator with "exclude" option require column option to be set');
        }
        if (!isset($exclude['value']) || empty($exclude['value'])) {
            throw new ValidationException('Validator with "exclude" option require value option to be set');
        }
    }

    /**
     * Executes the uniqueness validation
     *
     * @param  \Phalcon\Validation $validator
     * @param  string $attribute
     * @return boolean
     */
    public function validate(Validation $validator, $attribute)
    {
        $table = $this->db->escapeIdentifier($this->getOption('table'));
        $columns = [];
        $values = [];
        $fetchValues = [];

        // Stores the columns to be filtered
        if (is_array($this->getOption('column'))) {
            foreach($this->getOption('column') as $column) {
                $columns[] = $column;
            }
        } else {
            $columns[] = $this->getOption('column');
        }

        // Optional parameter that stores constant values for columns or null to use col value
        for($i = 0; $i < count($columns); ++$i)
        {
            if ($i >= count($this->getOption('value'))) {
                $values[$i] = null;
            } else {
                $values[$i] = $this->getOption('value')[$i];
            }
        }

        // Build base of query string
        $query = sprintf(
            'SELECT COUNT(*) AS count FROM %s WHERE ',
            $table
        );

        $filters = [];

        foreach($columns as $key => $column)
        {
            $columnValues = ($values[$key] == null) ?
                $validator->getValue($column) :
                $values[$key];

            if (!is_array($columnValues)) {
                $filters[] = sprintf(
                    '%s = ?',
                    $this->db->escapeIdentifier($column)
                );

                $fetchValues[] = $columnValues;
            } else {
                $orWhere = [];
                foreach ($columnValues as $columnValue) {
                    $orWhere[] = sprintf(
                        'FIND_IN_SET(?, %s)',
                        $this->db->escapeIdentifier($column)
                    );
                    $fetchValues[] = $columnValue;
                }
                if (!empty($orWhere)) {
                    $filters[] = '(' . implode(' OR ', $orWhere) . ')';
                }
            }

        }

        if ($this->hasOption('exclude'))
        {
            $exclude = [];
            // Build array of columns to be excluded
            if (is_array($this->getOption('exclude')[0])) {
                foreach($this->getOption('exclude') as $value) {
                    $exclude[] = $value;
                }
            } else {
                $exclude[] = $this->getOption('exclude');
            }

            foreach($exclude as $key => $value)
            {
                $filters[] = sprintf(
                    '%s != ?',
                    $this->db->escapeIdentifier($value['column'])
                );

                $fetchValues[] = $value['value'];
            }
        }

        // Append filters to query
        $query .= join(' AND ', $filters);

        $result = $this->db->fetchOne(
            $query,
            Db::FETCH_ASSOC,
            $fetchValues
        );

        if ($result['count']) {
            $message = $this->getOption('message', 'A mező értéke nem egyedi!');
            $validator->appendMessage(new Message($message, $attribute, 'Uniqueness'));
            return false;
        }

        return true;
    }
}