<?php

namespace App\Validation\Validator\Db;

use App\Support\Date;
use IntlDateFormatter;
use Phalcon\Db;
use Phalcon\Di;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Message;
use Phalcon\Validation\Exception as ValidationException;
use Phalcon\Validation\ValidatorInterface;
use Phalcon\Validation;


/**
 * Van-e átfedés a most megadott intervallum és a DB-ben található intervallumok között.
 *
 * Paraméterek:
 * model - példányosított Model
 * minValue - integer || RawValue
 * maxValue - integer || RawValue
 * minColumn - adatbázisban az intervallum eleje
 * maxColumn - adatbázisban az intervallum vége
 * where - NEM kötelező paraméter. Ha saját plusz where feltételt akarsz hozzáadni.
 *
 * $validation = new Validation();
 *
 * $validation->add(
 *     'minValue',
 *      new IntervalOverlap(array(
 *      'maxValue'  => $opt['maxValue'],
 *      'minColumn' => 'datetime',
 *      'maxColumn' => 'datetime',
 *      'model'     => $this->getModel(),
 *      'where'     => array(
 *          array('datetime', '=', '2016-06-17')
 *       )
 *    ))
 * );
 *
 * Ha a where feltételben a value-t null-ra rakod, akkor az űrlapon keresi az értéket..
 */

class IntervalOverlap extends Validator
    implements ValidatorInterface
{
    /**
     * Hibaüzenetek
     * @var array
     */
    protected $messages = [
        'notValidInterval'  => 'A kezdő érték nem haladhatja meg a befejező értéket!'
        , 'intervalOverlap' => 'Átfedés van az intervallumok között! Intervallum: %s - %s'
    ];

    /**
     * Class constructor.
     *
     * minValue, maxValue, minColumn, maxColumn, model
     *
     * @param  array   $options
     * @throws ValidationException
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);

        if (!$this->hasOption('minValue') && !$this->hasOption('minColumn')) {
            throw new ValidationException('A "minValue" vagy "minColumn" paraméter megadása kötelező!');
        }
        
        if (!$this->hasOption('maxValue') && !$this->hasOption('maxColumn')) {
            throw new ValidationException('A "maxValue" vagy "maxColumn" paraméter megadása kötelező!');
        }
    }

    /**
     * Executes the uniqueness validation
     *
     * @param  \Phalcon\Validation $validator
     * @param  string $attribute
     * @return boolean
     */
    public function validate(Validation $validator, $attribute)
    {
        /*
         * Ha paraméterből nem jön a minValue akkor a minColumn alapján kell beállítani.
         */
        $minValue = $this->getOption('minValue');
        if ($minValue == null) {
            $minValue = $validator->getData()[$this->getOption('minColumn')];
        }

        /*
         * Ha POST-ból jön a maxValue vagy én magam adom meg paraméterként.
         */
        $maxValue = $this->getOption('maxValue');
        if($maxValue == null) {
            $maxValue = $validator->getData()[$this->getOption('maxColumn')];
        }

        // Kisebb-nagyobb vizsgálat
        if (is_numeric($minValue) && is_numeric($maxValue) && $minValue > $maxValue) {
            $validator->appendMessage(new Message(
                $this->messages['notValidInterval']
                , $attribute
                , 'notValidInterval'
            ));

            return false;

        } else {

            $minDate = Date::dateFromLocalized($minValue);
            $maxDate = Date::dateFromLocalized($maxValue);

            if ($minDate > $maxDate) {
                $validator->appendMessage(new Message(
                    $this->messages['notValidInterval']
                    , $attribute
                    , 'notValidInterval'
                ));

                return false;
            }
        }

        /*
         * Where kezelése
         */
        $model = $this->getOption('model');

        if ($model == null) {
            return true;
        }

        $where = "{$this->getOption('maxColumn')} >= :minValue: AND {$this->getOption('minColumn')} <= :maxValue:";
        $bind = [
            'minValue'   => $minDate ? $minDate->format('Y-m-d') : $minValue
            , 'maxValue' => $maxDate ? $maxDate->format('Y-m-d') : $maxValue
        ];

        foreach ($this->getOption('where', []) as $w) {

            if (!is_array($w) || count($w) != 3) {
                continue;
            }

            $condition = $w[0];
            $operator  = $w[1];
            $value     = $w[2];

            $where .= " AND " . $condition . " " . $operator . " :" . $condition . ":";
            $bind[$condition] = $value;
        }

        $result = $model::find([$where, "bind" => $bind]);

        if ($result->count()) {
            $validator->appendMessage(new Message(
                sprintf($this->messages['intervalOverlap'], $minValue, $maxValue)
                , $attribute
                , 'intervalOverlap'
            ));

            return false;
        }

        return true;
    }
}