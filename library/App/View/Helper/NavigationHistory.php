<?php

namespace App\View\Helper;

use App\Model\User;
use Phalcon\Mvc\User\Component;
use App\Model\NavigationHistory as NavigationHistoryModel;
use Phalcon\Exception;

/**
 * Class NavigationHistory
 * @package App\View\Helper
 */
class NavigationHistory
    extends Component
{
    /**
     * Maximum mennyi elemet tároljon a history
     * @var integer
     */
    public $max = 10;

    /**
     * NavigationHistory típusok, csak ezeket lehet betenni
     *
     * @var array
     */
    public $types = array(
        \App\Model\NavigationHistory::TYPE_USER => ['title' => 'Felhasználók', 'icon' => 'icon-user']
    );

    /**
     * Model instance
     * @var NavigationHistoryModel
     */
    protected $model;

    /**
     * History oldalak az adott userhez
     * @var
     */
    protected $pages = [];

    /**
     * History oldalak száma
     * @var
     */
    protected $num = 0;

    /**
     * visszaadja az osztály példányát
     * @return NavigationHistory
     */
    public function __construct()
    {
        // table
        $this->model = $this->getModel();

        // feltöltés
        if (!is_array($this->pages)
            || count($this->pages) == 0){
            $this->read();
        }

        return $this;
    }

    /**
     * Új előzmény elem elmentése vagy a meglévő frissítése
     * @param $type
     * @param $url
     * @param $title
     * @return mixed
     */
    public function push($type, $url, $title)
    {
        $type = lcfirst($type);
        $currentDate = date('Y-m-d H:i:s');

        if (!array_key_exists($type, $this->types)) {
            // nincs ilyen típus
            return $this->num;
        }

        $navigationHistory = NavigationHistoryModel::findFirst([
            'url = :url:'
            , 'bind' => [
                'url'      => $url
            ]
        ]);

        if($navigationHistory === false) {
            if ($this->num >= $this->max) {
                $this->pop();
            }

            $model = new NavigationHistoryModel();
            $model->assign([
                'title'       => $title
                , 'url'       => $url
                , 'type'      => $type
                , 'createdAt' => $currentDate
            ]);
            $model->create();
        } else {
            $navigationHistory->createdAt = $currentDate;
            $navigationHistory->update();
        }

        $this->read();

        return $this->num;
    }

    /**
     * Legkorábbi elem eltávolítása
     */
    public function pop()
    {
        $model = NavigationHistoryModel::findFirst([
            'order' => 'createdAt ASC'
        ]);

        if ($model !== false) {
            if($model->delete()) {
                $this->num--;
            }
        }
    }

    /**
     * Előzmény adatok lekérdezése
     */
    protected function read()
    {
        $pages = NavigationHistoryModel::find([
            'order' => 'createdAt DESC'
        ]);

        $this->clear();

        if ($pages->count() > 0) {
            $this->num = $pages->count();
            $this->pages = $pages->toArray();
        }
    }

    /**
     * Oldalak ürítése
     */
    public function clear()
    {
        $this->pages = [];
        $this->num   = 0;
    }

    /**
     * visszaadja a komponenst megjelenítő html kódot
     * @return string html kód
     */
    public function render()
    {
        $elements = $this->pages;

        $html = '
        <div class="dropdown-menu dropdown-content">
            <div class="dropdown-content-heading">'
                . $this->translate->t('Előzmények')
                . '<ul class="icons-list">
                        <li><i class="icon-history"></i></li>
                    </ul>
            </div>
            <ul id="historyList" class="media-list dropdown-content-body width-500">';

        if (count($elements) == 0) {
            $html .= $this->translate->t('Az előzmény lista üres.');
        }

        foreach ($elements as $element) {

            $html .= '<li class="media">
                <div class="media-left">
                    <a href="/'
                    . $element["url"]
                    . '" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm">' .
                    '<i class="'
                    . $this->types[$element["type"]]["icon"]
                    . '"></i></a>
                </div>
                <div class="media-body">
                    <a href="'
                    . $element["url"]
                    . '">'
                    . $element["title"]
                    . '</a>
                    <div class="media-content">'
                    . $this->translate->t($this->types[$element["type"]]["title"])
                    . '</div>
                    <div class="media-annotation">'
                    . $this->tag->datetime($element["createdAt"])
                    . '</div>
                </div>
            </li>';
        }

        $html .= '</ul></div>';

        return $html;
    }

    /**
     * Db table instance lekérése
     * @return NavigationHistoryModel
     */
    public function getModel()
    {
        if ($this->model == null) {
            $this->model = new NavigationHistoryModel();
        }

        return $this->model;
    }

    /**
     * @return int
     */
    public function getMax(): int
    {
        return $this->max;
    }

    /**
     * @param int $max
     * @return NavigationHistory
     */
    public function setMax(int $max): NavigationHistory
    {
        $this->max = $max;
        return $this;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param array $types
     * @return NavigationHistory
     */
    public function setTypes(array $types): NavigationHistory
    {
        $this->types = $types;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
} 