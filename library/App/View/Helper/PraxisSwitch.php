<?php

namespace App\View\Helper;

use App\Model\Language;
use Phalcon\Mvc\User\Component;

/**
 * Class PraxisSwitch
 * @package App\View\Helper
 */
class PraxisSwitch
    extends Component
{
    /**
     * @return string
     */
    public function render()
    {
        /**
         * @var User $user
         */
        $user = $this->auth->getIdentity();

        if (!$user->isDoctor()) {
            return '';
        }

        /**
         * @var Doctor $doctor
         */
        $doctor = $user->getDoctor();

        /**
         * @var Praxis $praxis
         */
        $praxis = $doctor->getInterfacePraxis();

        $html = '<li class="dropdown">';
        $html .= '<a class="dropdown-toggle" data-toggle="dropdown">';
        $html .= '<i class="icon-home7">&nbsp;</i>' . $praxis->getName();
        $html .= '<span class="caret"></span>';
        $html .= '</a>';

        $praxises = $doctor->getPraxises();

        $html .= '<ul class="dropdown-menu">';

        /**
         * @var Praxis $praxis
         */
        foreach ($praxises as $praxis) {
            $html .= '<li>';
            $html .= '<a href="' . $praxis->getChangeUrl() . '">';
            $html .= '&nbsp;' . $praxis->getName();
            $html .= '</a>';
            $html .= '</li>';
        }

        $html .= '</ul>';

        $html .= '</li>';

        return $html;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}