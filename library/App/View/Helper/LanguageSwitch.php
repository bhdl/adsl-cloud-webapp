<?php

namespace App\View\Helper;

use App\Model\Language;
use Phalcon\Mvc\User\Component;

/**
 * Class LanguageSwitch
 * @package App\View\Helper
 */
class LanguageSwitch
    extends Component
{
    /**
     * @return string
     */
    public function render()
    {
        /**
         * @var User $user
         */
        $user = $this->auth->getIdentity();

        /**
         * @var Language $interfaceLanguage
         */
        $interfaceLanguage = $user->getInterfaceLanguage();

        /*
         * Akutális nyelv
         */
        $html = '<li class="dropdown language-switch">';
        $html .= '<a class="dropdown-toggle" data-toggle="dropdown">';
        $html .= '<img src="assets/images/flags/' . $interfaceLanguage->iso . '.png" class="position-left" alt="">';
        $html .= $interfaceLanguage->languageName;
        $html .= '<span class="caret"></span>';
        $html .= '</a>';

        /*
         * További választható nyelvek
         */
        $languages = Language::findInterfaceLanguages($interfaceLanguage->languageId);

        $html .= '<ul class="dropdown-menu">';

        /**
         * @var Language $language
         */
        foreach ($languages as $language) {
            $html .= '<li>';
            $html .= '<a href="' . $language->getChangeUrl() . '">';
            $html .= '<img src="assets/images/flags/' . $language->iso . '.png" alt="">';
            $html .= '&nbsp;' . $language->languageName;
            $html .= '</a>';
            $html .= '</li>';
        }

        $html .= '</ul>';

        $html .= '</li>';

        return $html;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}