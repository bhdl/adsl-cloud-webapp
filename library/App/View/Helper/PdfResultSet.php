<?php

namespace App\View\Helper;

use App\Export\Pdf\Template\AbstractResultSet;
use Phalcon\Mvc\User\Component;

/**
 * Class PdfResultSet
 * @package App\View\Helper
 */
class PdfResultSet
    extends Component
{
    /**
     * @var AbstractResultSet
     */
    protected $template;

    /**
     * @return string
     */
    public function render() : string
    {
        $html = '<table class="table table-striped table-condensed">';

        // heading
        $html .= '<thead>';
        $html .= '<tr>';

        foreach ($this->getTemplate()->getColumns() as $column=>$attributes) {
            $html .= '<th>' . $attributes['label'] . '</th>';
        }

        $html .= '</tr>';
        $html .= '</thead>';

        // body
        $html .= '<tbody>';
        foreach ($this->getTemplate()->prepareRows() as $row) {
            $html .= '<tr>';
            foreach ($this->getTemplate()->orderColumnsInRow($row) as $column=>$ordered) {
                $html .= '<td>' . $ordered . '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</tbody>';

        // footer
        $html .= '</table>';

        return $html;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @return AbstractResultSet
     */
    public function getTemplate(): AbstractResultSet
    {
        return $this->template;
    }

    /**
     * @param AbstractResultSet $template
     * @return PdfResultSet
     */
    public function setTemplate(AbstractResultSet $template): PdfResultSet
    {
        $this->template = $template;
        return $this;
    }
}