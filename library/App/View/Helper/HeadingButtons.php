<?php

namespace App\View\Helper;
use Phalcon\Mvc\User\Component;

/**
 * Class HeadingButtons
 * @package App\View\Helper
 */
class HeadingButtons
    extends Component
{
    /**
     * @var array
     */
    protected $elements = [];

    /**
     * Breadcrumb constructor.
     */
    public function __construct()
    {
        $this->reset();
    }

    /**
     * @param string $title
     * @param string $href
     * @param string $iconClass
     * @param string $hrefClass
     * @return HeadingButtons
     */
    public function add($title, $href, $iconClass, $hrefClass = 'btn btn-link btn-float has-text')
    {
        $this->elements[] = [
            'title'       => $title
            , 'href'      => $href
            , 'iconClass' => $iconClass
            , 'hrefClass' => $hrefClass
        ];

        return $this;
    }

    /**
     * @return string
     */
    public function render()
    {
        $html = '';

        if (!is_array($this->elements) || count($this->elements) == 0) {
            return $html;
        }

        $html .= '<div class="heading-elements">';

        $html .= '<div class="heading-btn-group">';

        foreach ($this->elements as $element) {
            $html .= '<a'
                . ' href="' . $element['href'] . '"'
                . ' class="' . $element['hrefClass'] . '">'
                . ' <i class="' . $element['iconClass'] . '"></i>'
                . ' <span>' . $element['title'] . '</span>'
                . '</a>'
            ;
        }

        $html .= '</div>';

        $html .= '</div>';

        return $html;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * reset
     */
    public function reset()
    {
        $this->elements = [];
    }

    /**
     * @return array
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    /**
     * @param array $elements
     * @return HeadingButtons
     */
    public function setElements(array $elements): HeadingButtons
    {
        $this->elements = $elements;
        return $this;
    }

    /**
     * @param string $title
     */
    public function removeElementByTitle($title)
    {
        for($i = 0; $i < count($this->elements); ++$i)
        {
            if ($this->elements[$i]['title'] == $title){
                unset($this->elements[$i]);
                break;
            }
        }
    }
}