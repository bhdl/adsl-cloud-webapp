<?php

namespace App\View\Helper;

use App\Form\AbstractSearchBar;
use Phalcon\Mvc\User\Component;

/**
 * Class SearchBar
 * @package App\View\Helper
 */
class SearchBar
    extends Component
{
    /**
     * @var AbstractSearchBar
     */
    protected $form;

    /**
     * @var string
     */
    protected $controller;

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->form == null) {
            return '';
        }

        return $this->view->getPartial('partials/search', [
            'form'         => $this->form
            , 'controller' => $this->controller
        ]);
    }

    /**
     * @return AbstractSearchBar
     */
    public function getForm(): AbstractSearchBar
    {
        return $this->form;
    }

    /**
     * @param AbstractSearchBar $form
     * @return SearchBar
     */
    public function setForm(AbstractSearchBar $form): SearchBar
    {
        $this->form = $form;
        return $this;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     * @return SearchBar
     */
    public function setController(string $controller): SearchBar
    {
        $this->controller = $controller;
        return $this;
    }
}