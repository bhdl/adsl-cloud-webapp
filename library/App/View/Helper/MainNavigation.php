<?php

namespace App\View\Helper;

use Phalcon\Mvc\User\Component;

/**
 * Class MainNavigation
 * @package App\View\Helper
 */
class MainNavigation extends Component
{
    /**
     * @var array
     */
    protected $items;

    /**
     * @return array
     */
    public function getItems()
    {
        if ($this->items == null) {
            $this->items = $this->getDefaultItems();
        }

        return $this->items;
    }

    /**
     * @param array $items
     * @return MainNavigation
     */
    public function setItems($items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @param string $item
     * @return bool
     */
    public function isActiveItem($item)
    {
        if (!array_key_exists('href', $item)) {
            return false;
        }

        $href = trim($this->request->getURI(), '/');

        $hrefCurrent = trim($item['href'], '/');

        return strcmp($href, $hrefCurrent) === 0;
    }

    /**
     * @param array $item
     * @return string
     */
    public function renderItem(array $item)
    {
        // jogosultság vizsgálat
        if (array_key_exists('resourceId', $item)
            && array_key_exists('actionId', $item)
        ) {
            if (!$this->acl->isAllowedByIdentity($item['resourceId'], $item['actionId'])) {
                return '';
            }
        }

        $html = '<li' . ($this->isActiveItem($item) ? ' class="active"' : '') . '><a';

        if ($href = array_key_exists('href', $item)) {
            $html .= ' href="' . $item['href'] . '"';
        }

        $html .= '>';

        $html .= '<i class="' . $item['icon'] . '"></i> <span>' . $item['title'] . '</span>';

        $html .= '</a>';

        if (array_key_exists('children', $item)) {
            $temp = $this->renderItems($item['children']);
            // jogosultság vizsgálat előfordulhat, hogy nincs children
            if (strcmp($temp, '') === 0) {
                return '';
            }
            $html .= $temp;
        }

        $html .= '</li>';

        return $html;
    }

    /**
     * @param null|array $items
     * @return string
     */
    public function renderItems($items = null)
    {
        $html = '<ul'
            . ($items == null ? ' class="navigation navigation-main navigation-accordion"' : '')
            . '>'
        ;

        if ($items == null) {
            $items = $this->getItems();
        }

        $temp = '';
        foreach ($items as $item) {
            $temp .= $this->renderItem($item);
        }

        // jogosultság vizsgálat előfordulhat, hogy nincs children
        if (strcmp($temp, '') === 0) {
            return '';
        }

        $html .= $temp . '</ul>';

        return $html;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $html = '<div class="sidebar-category sidebar-category-visible">';
        $html .= '<div class="category-content no-padding">';

        $html .= $this->renderItems();

        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * @return array
     */
    public function getDefaultItems()
    {
        return [
            [
                'href'         => '/admin'
                , 'icon'       => 'icon-home4'
                , 'title'      => $this->translate->t('Dashboard')
                , 'resourceId' => 'admin.index'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/file-upload'
                , 'icon'       => 'icon-attachment'
                , 'title'      => $this->translate->t('Tömeges fájlfeltöltés')
                , 'resourceId' => 'admin.fileUpload'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/examination'
                , 'icon'       => 'icon-lab'
                , 'title'      => $this->translate->t('Vizsgálatok')
                , 'resourceId' => 'admin.examination'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/partner'
                , 'icon'       => 'icon-users'
                , 'title'      => $this->translate->t('Partnerek')
                , 'resourceId' => 'admin.partner'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/site'
                , 'icon'       => 'icon-store'
                , 'title'      => $this->translate->t('Telephelyek')
                , 'resourceId' => 'admin.site'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/tag'
                , 'icon'       => 'icon-circles'
                , 'title'      => $this->translate->t('Tag nyilvántartás')
                , 'resourceId' => 'admin.tag'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/inventory-event'
                , 'icon'       => 'icon-upload'
                , 'title'      => $this->translate->t('Leltározási események')
                , 'resourceId' => 'admin.inventoryEvent'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/product'
                , 'icon'       => 'icon-archive'
                , 'title'      => $this->translate->t('Termékek')
                , 'resourceId' => 'admin.product'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/product-instance'
                , 'icon'       => 'icon-books'
                , 'title'      => $this->translate->t('Termékek raktározása')
                , 'resourceId' => 'admin.productInstance'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/product-positioning'
                , 'icon'       => 'icon-books'
                , 'title'      => $this->translate->t('Termékek áthelyezési ajánlása')
                , 'resourceId' => 'admin.productPositioning'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/realtime-map'
                , 'icon'       => 'icon-map'
                , 'title'      => $this->translate->t('Raktár Térkép')
                , 'resourceId' => 'admin.realtimeMap'
                , 'actionId'   => 'index'
            ]
            , [
                'href'         => '/admin/referral'
                , 'icon'       => 'icon-vcard'
                , 'title'      => $this->translate->t('Beutalók')
                , 'resourceId' => 'admin.referral'
                , 'actionId'   => 'index'
                , 'children'   => [
                    [
                        'href'         => '/admin/referral-wizard'
                        , 'icon'       => 'icon-stack-plus'
                        , 'title'      => $this->translate->t('Beutaló készítése')
                        , 'resourceId' => 'admin.referralWizard'
                        , 'actionId'   => 'create'
                    ]
                    , [
                        'href'         => '/admin/referral?filter=sent'
                        , 'icon'       => 'icon-stack-empty'
                        , 'title'      => $this->translate->t('Beküldött')
                        , 'resourceId' => 'admin.referral'
                    ]
                    , [
                        'href'         => '/admin/referral?filter=receive'
                        , 'icon'       => 'icon-stack-down'
                        , 'title'      => $this->translate->t('Fogadott')
                        , 'resourceId' => 'admin.referral'
                    ]
                    , [
                        'href'         => '/admin/referral?filter=closed'
                        , 'icon'       => 'icon-stack-check'
                        , 'title'      => $this->translate->t('Lezárt')
                        , 'resourceId' => 'admin.referral'
                    ]
                ]
            ]
            , [
                'href'         => '/admin/referral-receive'
                , 'icon'       => 'icon-vcard'
                , 'title'      => $this->translate->t('Beutalók')
                , 'resourceId' => 'admin.referralReceive'
                , 'actionId'   => 'index'
                , 'children'   => [
                    [
                        'href'         => '/admin/referral-receive?filter=sent'
                        , 'icon'       => 'icon-stack-empty'
                        , 'title'      => $this->translate->t('Beküldött')
                        , 'resourceId' => 'admin.referralReceive'
                    ]
                    , [
                        'href'         => '/admin/referral-receive?filter=receive'
                        , 'icon'       => 'icon-stack-down'
                        , 'title'      => $this->translate->t('Fogadott')
                        , 'resourceId' => 'admin.referralReceive'
                    ]
                    , [
                        'href'         => '/admin/referral-receive?filter=closed'
                        , 'icon'       => 'icon-stack-check'
                        , 'title'      => $this->translate->t('Lezárt')
                        , 'resourceId' => 'admin.referralReceive'
                    ]
                ]
            ]
            , [
                'icon' => 'icon-wall'
                , 'title' => 'Törzsadatok'
                , 'children' => [
                    [
                        'href'         => '/admin/owner'
                        , 'icon'       => 'icon-man-woman'
                        , 'title'      => $this->translate->t('Tulajdonosok')
                        , 'resourceId' => 'admin.owner'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/patient'
                        , 'icon'       => 'icon-bug2'
                        , 'title'      => $this->translate->t('Páciensek')
                        , 'resourceId' => 'admin.patient'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/company'
                        , 'icon'       => 'icon-office'
                        , 'title'      => $this->translate->t('Partnerek')
                        , 'resourceId' => 'admin.company'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/praxis'
                        , 'icon'       => 'icon-home7'
                        , 'title'      => $this->translate->t('Rendelők')
                        , 'resourceId' => 'admin.praxis'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/doctor'
                        , 'icon'       => 'icon-users4'
                        , 'title'      => $this->translate->t('Orvosok')
                        , 'resourceId' => 'admin.doctor'
                        , 'actionId'   => 'index'
                    ]
                ]
            ]
            , [
                'icon' => 'icon-list'
                , 'title' => 'Szótárak'
                , 'children' => [
                    [
                        'href'         => '/admin/praxis-sender-number'
                        , 'icon'       => 'icon-list-ordered'
                        , 'title'      => $this->translate->t('Beküldő sorszámok')
                        , 'resourceId' => 'admin.praxisSenderNumber'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'          => '/admin/animal-group'
                        , 'icon'       => 'icon-make-group'
                        , 'title'      => $this->translate->t('Állat csoportok')
                        , 'resourceId' => 'admin.animalGroup'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'          => '/admin/species'
                        , 'icon'       => 'icon-grid5'
                        , 'title'      => $this->translate->t('Állat fajok')
                        , 'resourceId' => 'admin.species'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'          => '/admin/breed'
                        , 'icon'       => 'icon-grid-alt'
                        , 'title'      => $this->translate->t('Állat fajták')
                        , 'resourceId' => 'admin.breed'
                        , 'actionId'   => 'index'
                    ]
                    , ['href'          => '/admin/collector'
                        , 'icon'       => 'icon-grid2'
                        , 'title'      => $this->translate->t('Gyüjtők')
                        , 'resourceId' => 'admin.collector'
                        , 'actionId'   => 'index'
                    ]
                    , ['href'          => '/admin/panel'
                        , 'icon'       => 'icon-list2'
                        , 'title'      => $this->translate->t('Panelek')
                        , 'resourceId' => 'admin.panel'
                        , 'actionId'   => 'index'
                    ]
                    , ['href'          => '/admin/request'
                        , 'icon'       => 'icon-more'
                        , 'title'      => $this->translate->t('Kérések')
                        , 'resourceId' => 'admin.request'
                        , 'actionId'   => 'index'
                    ]
                    , ['href'          => '/admin/medical-record-group'
                        , 'icon'       => 'icon-grid2'
                        , 'title'      => $this->translate->t('Leletcsoportok')
                        , 'resourceId' => 'admin.medicalRecordGroup'
                        , 'actionId'   => 'index'
                    ]
                    , ['href'          => '/admin/tube'
                        , 'icon'       => 'icon-eyedropper'
                        , 'title'      => $this->translate->t('Csövek')
                        , 'resourceId' => 'admin.tube'
                        , 'actionId'   => 'index'
                    ]
                    , ['href'          => '/admin/tube-classification'
                        , 'icon'       => 'icon-eyedropper'
                        , 'title'      => $this->translate->t('Csőforgalmi minősítések')
                        , 'resourceId' => 'admin.tubeClassification'
                        , 'actionId'   => 'index'
                    ]
                    , ['href'          => '/admin/lab-sample'
                        , 'icon'       => 'icon-eyedropper2'
                        , 'title'      => $this->translate->t('Labor minták')
                        , 'resourceId' => 'admin.labSample'
                        , 'actionId'   => 'index'
                    ]
                    , ['href'          => '/admin/referral-sample'
                        , 'icon'       => 'icon-color-sampler'
                        , 'title'      => $this->translate->t('Beutaló minták')
                        , 'resourceId' => 'admin.referralSample'
                        , 'actionId'   => 'index'
                    ]
                    , ['href'          => '/admin/referral-sample-rule'
                        , 'icon'       => 'icon-color-sampler'
                        , 'title'      => $this->translate->t('Beutaló minta szabályok')
                        , 'resourceId' => 'admin.referralSampleRule'
                        , 'actionId'   => 'index'
                    ]
                    , ['href'          => '/admin/normal-value'
                        , 'icon'       => 'icon-color-sampler'
                        , 'title'      => $this->translate->t('Normálértékek')
                        , 'resourceId' => 'admin.normalValue'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'          => '/admin/unit'
                        , 'icon'       => 'icon-balance'
                        , 'title'      => $this->translate->t('Mértékegységek')
                        , 'resourceId' => 'admin.unit'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/bank'
                        , 'icon'       => 'icon-cash3'
                        , 'title'      => $this->translate->t('Bankok')
                        , 'resourceId' => 'admin.bank'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/payment-method'
                        , 'icon'       => 'icon-credit-card'
                        , 'title'      => $this->translate->t('Fizetési módok')
                        , 'resourceId' => 'admin.paymentMethod'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/courier'
                        , 'icon'       => 'icon-package'
                        , 'title'      => $this->translate->t('Futár')
                        , 'resourceId' => 'admin.courier'
                        , 'actionId'   => 'index'
                    ]
                ]
            ]
            , [
                'icon' => 'icon-gear'
                , 'title' => 'Rendszer'
                , 'children' => [
                    [
                        'href'         => '/admin/user'
                        , 'icon'       => 'icon-user'
                        , 'title'      => $this->translate->t('Felhasználók')
                        , 'resourceId' => 'admin.user'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/group'
                        , 'icon'       => 'icon-people'
                        , 'title'      => $this->translate->t('Csoportok')
                        , 'resourceId' => 'admin.group'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/acl'
                        , 'icon'       => 'icon-make-group'
                        , 'title'      => $this->translate->t('Szerepkörök')
                        , 'resourceId' => 'admin.acl'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/log'
                        , 'icon'       => 'icon-list'
                        , 'title'      => $this->translate->t('Napló')
                        , 'resourceId' => 'admin.log'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'          => '/admin/config'
                        , 'icon'       => 'icon-hammer'
                        , 'title'      => $this->translate->t('Beállítások')
                        , 'resourceId' => 'admin.config'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'          => '/admin/cron'
                        , 'icon'       => 'icon-alarm'
                        , 'title'      => $this->translate->t('Ütemezett feladatok')
                        , 'resourceId' => 'admin.cron'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/currency'
                        , 'icon'       => 'icon-coin-euro'
                        , 'title'      => $this->translate->t('Pénznemek')
                        , 'resourceId' => 'admin.currency'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/language'
                        , 'icon'       => 'icon-sphere'
                        , 'title'      => $this->translate->t('Nyelvek')
                        , 'resourceId' => 'admin.language'
                        , 'actionId'   => 'index'
                    ]
                    , [
                        'href'         => '/admin/page'
                        , 'icon'       => 'icon-grid'
                        , 'title'      => $this->translate->t('Oldalak')
                        , 'resourceId' => 'admin.page'
                        , 'actionId'   => 'index'
                    ]
                ]
            ]
        ];
    }
}
