<?php

namespace App\View\Helper;

use App\Form\Element\AutoCompleteItem;
use App\Form\Element\AutoCompleteUser;
use App\Form\Element\CkEditor;
use App\Form\Element\DatePicker;
use App\Model\Currency;
use App\Model\Referral;
use Phalcon\Di;
use App\Support\File;

/**
 * Class Tag
 * @package App\View\Helper
 */
class Tag
    extends \Phalcon\Tag
{
    /**
     * Input mező hibaüzenet generálása
     * @param \Phalcon\Validation\Message\Group $messages
     * @return string
     */
    public function formElementMessages(\Phalcon\Validation\Message\Group $messages) : string
    {
        $html = '';

        $translate = Di::getDefault()->get('translate');

        if(count($messages)) {
            foreach ($messages as $message) {
                $html .= '<label class="validation-error-label">';
                $html .= $translate->_($message->getMessage());
                $html .= '</label>';
            }
        }

        return $html;
    }

    /**
     * Dátum formázása
     * @param string $date
     * @return string
     */
    public function date(string $date): string
    {
        return $this->getDi()->get('filter')->sanitize($date, 'date');
    }

    /**
     * Datetime formázása
     * @param string $date
     * @return string
     */
    public function datetime(string $date): string
    {
        return $this->getDi()->get('filter')->sanitize($date, 'datetime');
    }

    /**
     * Dátum intervallum formzása
     * @param string $dateFrom
     * @param string $dateTo
     * @return string
     */
    public function dateInterval(string $dateFrom, string $dateTo): string
    {
        if (empty($dateTo)) {
            return $this->date($dateFrom);
        }

        return $this->getDi()
            ->get('filter')
            ->sanitize(serialize([$dateFrom, $dateTo]), 'dateInterval')
        ;
    }

    /**
     * Value erteke alapjan visszaad egy label markup-ot
     *
     * @param string $value
     * @param string $text
     * @return string
     */
    public function label(string $value = null, string $text)
    {
        // default, warning, success, info, danger, primary
        switch($value) {

            case Referral::FLAG_NO_PAYMENT:
                $type = 'danger';
                break;

            case Referral::FLAG_AMENDED:
                $type = 'warning';
                break;

            case Referral::FLAG_FINAL_RESULT:
            case Referral::FLAG_CLOSED:
            case 'yes':
                $type = 'success';
                break;

            case Referral::FLAG_ARRIVED:
            case Referral::FLAG_PARTIAL_RESULT:
            case Referral::FLAG_INVOICED:
            case Referral::FLAG_PAID_IN_FULL:
                $type = 'info';
                break;

            case 'no':
            case Referral::FLAG_RE_OPENED:
            case Referral::FLAG_SENT:
            default:
                $type = 'default';
                break;
        }

        return '<span class="label label-' . $type.'"> ' . $text . '</span>';
    }

    /**
     * @param int $value Szazalekban a progress merteke
     * @param null $cssColor A szabvanyos BS3 szin nevek, amelyek a class-ok neveiben is szerepelnek, pl. "teal"
     *
     * @return string
     */
    public function progressBar($value, $cssColor = null)
    {
        $cssColor = ($cssColor===null) ?
            'bg-teal':
            'bg-' . $cssColor
        ;

        $width = (int)$value;

        if ($width > 100) {
            $cssColor = 'bg-danger';
            $width = 100;
        }

        $v = $this->percentFormatter($value);

        $strMarkup = '<div class="progress" style="width:100%" data-toggle="tooltip" title="' . $v . '">';
        $strMarkup .= '<div class="progress-bar ' . $cssColor . '" style="width: ' . $width . '%">';
        $strMarkup .= '<span>' . $v . '</span>';
        $strMarkup .= '</div>';
        $strMarkup .= '</div>';
        return $strMarkup;
    }

    /**
     * @return string
     */
    public function version()
    {
        return $this
            ->getDi()
            ->get('app')
            ->getEnv()
        ;
    }

    /**
     * Pénznem formázása
     *
     * @param float $value
     * @param string|Currency $currency
     * @param boolean $formatAsText
     * @return string
     */
    public function currency(
        $value
        , $currency
        , $formatAsText = false
    ): string
    {
        if ($currency instanceof Currency) {
            $currency = $currency->currencyCode;
        }

        if ($formatAsText) {
            return $this
                ->numberToString($value) . ' ' . $currency
            ;
        }

        return $this
            ->getDi()
            ->get('filter')
            ->sanitize(serialize([$value, $currency]), 'currency')
        ;
    }

    /**
     * Szám formázása
     *
     * @param float $value
     * @return string
     */
    public function number($value): string
    {
        return $this
            ->getDi()
            ->get('filter')
            ->sanitize($value, 'number')
        ;
    }

    /**
     * %-os érték formázása
     *
     * @param $value
     * @return string
     */
    public function percent($value): string
    {
        return $this
            ->getDi()
            ->get('filter')
            ->sanitize($value, 'percent')
        ;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function numberToString($value)
    {
        return $this
            ->getDi()
            ->get('filter')
            ->sanitize($value, 'numberToString')
        ;
    }

    /**
     * Proxy fuggveny a file meretek egysegesen formazott megjelenitesere
     *
     * @param integer $bytes A meret
     * @return string A formazott meret
     */
    public function fileSize($bytes)
    {
        return File::formatSize($bytes);
    }

    /**
     * @param string|null $url
     * @return string
     */
    public function getAppUrl($url = null)
    {
        return $this->getDi()
            ->get('app')
            ->getAppUrl($url)
        ;
    }

    /**
     * Kép átalakítása Base64-es kódra
     * @param $imageUrl
     * @return string
     */
    public function imageToBase64(string $imageUrl)
    {
        $string = '';

        if(!empty($imageUrl)) {
            $string = base64_encode(file_get_contents($imageUrl));
        }

        return $string;
    }

    /**
     * "12-08" -bol "December 8." -at csinal.
     * @param strign $monthDay
     * @return string
     */
    public function monthDay($monthDay)
    {
        return strftime(
            '%m.%d.',
            strtotime(date('Y') . '-' . $monthDay)
        );
    }

    /**
     * @param string $name
     * @param null $attributes
     * @return DatePicker
     */
    public function datePicker($name, $attributes = null)
    {
        return new DatePicker($name, $attributes);
    }

    /**
     * @param string $name
     * @param null $attributes
     * @return CkEditor
     */
    public function ckEditor($name, $attributes = null)
    {
        return new CkEditor($name, $attributes);
    }

    /**
     * @param $name
     * @param null $attributes
     * @return AutoCompleteUser
     */
    public function autoCompleteUser($name, $attributes = null)
    {
        return new AutoCompleteUser($name, $attributes);
    }

    /**
     * @param string $name
     * @param null $attributes
     * @return AutoCompleteItem
     */
    public function autoCompleteItem($name, $attributes = null)
    {
        return new AutoCompleteItem($name, $attributes);
    }


    /**
     * @return PraxisSwitch
     */
    public function praxisSwitch()
    {
        return new PraxisSwitch();
    }
}
