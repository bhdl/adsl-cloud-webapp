<?php

namespace App\View\Helper;


/**
 * Class Breadcrumb
 * @package App\View\Helper
 */
class Breadcrumb
{
    /**
     * @var array
     */
    private $elements = [];

    /**
     * Breadcrumb constructor.
     */
    public function __construct()
    {
        $this->reset();
    }

    /**
     * Menüpontok törlése
     */
    public function reset()
    {
        $this->elements = [];
        return $this;
    }

    /**
     * Menüpont hozzáadása
     *
     * @param string $title
     * @param string $url
     * @param string $icon
     * @return Breadcrumb
     */
    public function add(string $title, string $url = null, string $icon = null)
    {
        $this->elements[] = [
            'title'     => $title
            , 'url'     => $url
            , 'icon'    => $icon
        ];
        return $this;
    }

    /**
     * Breadcrumb HTML generálása
     * @return string
     */
    public function render() : string
    {
        $elementsCount = count($this->elements);

        if ($elementsCount == 0) {
            return '';
        }

        /**
         * Utolsó elem index-e
         */
        $lastElementIndex = end(array_keys($this->elements));

        $html = '<ul class="breadcrumb">';

        foreach ($this->elements as $index => $element) {
            $icon = '';

            if(!empty($element['icon'])) {
                $icon = '<i class="' . $element['icon'] . ' position-left"></i>';
            }

            if($lastElementIndex == $index) {
                $html .= '<li class="active">';
                $html .= $icon;
                $html .= $element['title'];
            } else {
                $html .= '<li>';
                if (!empty($element['url'])) {
                    $html .= '<a href="' . $element['url'] . '">';
                }
                $html .= $icon;
                $html .= $element['title'];
                if (!empty($element['url'])) {
                    $html .= '</a>';
                }
            }

            $html .= '</li>';
        }

        $html .= '</ul>';


        return $html;
    }

    /**
     * @return array
     */
    public function getElements() : array
    {
        return $this->elements;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}