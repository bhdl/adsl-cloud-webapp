<?php

namespace App;

use Phalcon\Di;
use Phalcon\Exception;

/**
 * Class Application
 * @package App
 */
class Application extends \Phalcon\Mvc\Application
{
    /**
     * @var string
     */
    const ENV = 'APP_ENV';

    /**
     * Futtatási környezet lekérdezése
     * @throws Exception
     * @return string
     */
    public function getEnv()
    {
        $env = getenv(self::ENV);

        if ($env == false) {
            throw new Exception(
                'A \'' . self::ENV . '\' környezeti változó nincs beállítva.'
            );
        }

        return $env;
    }

    /**
     * Debug mode lekérdezése
     *
     * @return bool
     * @throws Exception
     */
    public function getDebug()
    {
        $debug = $this->getDI()->get('config')->debug;

        if ($debug === null) {
            throw new Exception(
                'A debug tulajdonság nincs megadva a konfigurációs állományban.'
            );
        }

        return $debug;
    }

    /**
     * getDebug() proxy
     * @return bool
     * @throws Exception
     */
    public function isDebug()
    {
        return $this->getDebug();
    }

    /**
     * @param string $url
     * @return string
     */
    public function getAppUrl($url = null)
    {
        $appUrl = $this->getDi()->get('config')->url->baseUri;

        if ($url == null) {
            return $appUrl;
        }

        return rtrim($appUrl, '\\/') . '/' . ltrim($url, '\\/');
    }

    /**
     * Projekt kód elérési útja
     *
     * @param null|string $path
     * @return string
     * @throws Exception
     */
    public function getAppPath($path = null)
    {
        if (!defined('APP_PATH')) {
            throw new Exception(
                'Az APP_PATH konstans nincs definálva. Helyezd el az index.php-ben!'
            );
        }

        return $this->getPath(APP_PATH, $path);
    }

    /**
     * @param string $base
     * @param null|string $path
     * @return string
     */
    protected function getPath($base, $path = null)
    {
        $base = rtrim($base, '/\\') . DIRECTORY_SEPARATOR;

        return $path ? $base . ltrim($path, '/\\') : $base;
    }
}