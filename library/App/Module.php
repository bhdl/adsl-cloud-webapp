<?php

namespace App;

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\ModuleDefinitionInterface;

/**
 * Class Module
 * @package App
 */
abstract class Module
    implements ModuleDefinitionInterface
{
    /**
     * @var FactoryDefault
     */
    protected $di;

    /**
     * @return array
     */
    abstract public function getDefaultServices();

    /**
     * Registers an autoloader related to the module
     *
     * @param \Phalcon\DiInterface $dependencyInjector
     */
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null)
    {
        // TODO: Implement registerAutoloaders() method.
    }

    /**
     * Registers services related to the module
     *
     * @param \Phalcon\DiInterface $dependencyInjector
     */
    public function registerServices(\Phalcon\DiInterface $dependencyInjector)
    {
        $this->di = $dependencyInjector;

        $dependencyInjector->get('bootstrap')->registerServices(
            $this->getDefaultServices()
        );
    }

    /**
     * @return FactoryDefault
     */
    public function getDi(): FactoryDefault
    {
        return $this->di;
    }

    /**
     * @param FactoryDefault $di
     * @return Module
     */
    public function setDi(FactoryDefault $di): Module
    {
        $this->di = $di;
        return $this;
    }
}