<?php
namespace App;

use Phalcon\Exception;
use Phalcon\Mvc\User\Component;
use App\Auth\Adapter\AdapterInterface;

/**
 * Class Auth
 * @package App\Auth
 */
class Auth extends Component
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var string
     */
    protected $modelClass = 'App\Model\User';

    /**
     * @const string
     */
    const SESSION_KEY = 'authIdentity';

    /**
     * @const string
     */
    const USER_KEY = 'userId';

    /**
     * @const string
     */
    const USER_KEY_ORIGIN = 'originUserId';

    /**
     * @param AdapterInterface $adapter
     * @return null|Model\User
     * @throws \Exception
     */
    public function login(AdapterInterface $adapter)
    {
        $user = null;

        try {
            $user = $adapter->login();
        } catch (Exception $e) {
            throw $e;
        }

        return $user;
    }

    /**
     * @param boolean|null $origin
     * @return Auth
     */
    public function logout($origin = null)
    {
        if ($origin == null) {
            $this->session->remove($this->getUserKey(true));
            $this->session->remove($this->getUserKey(false));
            return $this;
        }

        $this->session->remove($this->getUserKey($origin));

        return $this;
    }

    /**
     * @param User $user
     * @param bool $origin
     * @return Auth
     */
    public function setIdentity($user, $origin = false)
    {
        $this->session->set($this->getUserKey($origin), array(
            'userId' => $user->userId
        ));

        return $this;
    }

    /**
     * @param bool $origin
     * @return bool
     */
    protected function hasIdentity($origin = false)
    {
        $identity = $this->session->get(
            $this->getUserKey($origin)
        );

        return is_array($identity) && is_numeric($identity['userId']);
    }

    /**
     * Get the entity related to user in the active identity
     *
     * @param bool $origin
     * @return \App\Model\User|null
     * @throws Exception
     */
    public function getIdentity($origin = false)
    {
        $modelClass = $this->modelClass;

        if (!$this->hasIdentity($origin)) {
            if ($origin) {
                return null;
            }
            // nincs bejelentkezve senki, vendég logint készítünk
            $user = $modelClass::findGuest();
            $this->setIdentity($user, $origin);
            return $user;
        }

        $identity = $this->session->get($this->getUserKey($origin));

        // már be van lépve valaki, megnézzük tagváltozó cache-be mentettük-e korábban
        if (is_object($this->user)
            && $this->user->userId == $identity['userId']) {
            // cache találat
            return $this->user;
        }

        // létrehozzuk és mentjük cache-be
        $user = $modelClass::findFirstByUserId($identity['userId']);

        if ($user == false) {
            throw new Exception('A felhasználó nem létezik, lehetséges hogy időközben törölték.');
        }

        $this->user = $user;

        return $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Auth
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getModelClass()
    {
        return $this->modelClass;
    }

    /**
     * @param string $modelClass
     * @return Auth
     */
    public function setModelClass($modelClass)
    {
        $this->modelClass = $modelClass;
        return $this;
    }

    /**
     * @param bool $origin
     * @return string
     */
    protected function getUserKey($origin = false)
    {
        return $origin ? self::USER_KEY_ORIGIN : self::USER_KEY;
    }
}
