<?php

namespace App\Db\Adapter;

use App\Model\Behavior\LogModelCollector;
use Phalcon\Db\Adapter\Pdo\Mysql;

/**
 * Class MysqlLogged
 * @package App\Db\Adapter
 */
class MysqlLogged
    extends Mysql
{
    /**
     * @param bool $nesting
     * @return bool
     */
    public function beginLogged($nesting = true)
    {
        LogModelCollector::setEnabled(true);
        LogModelCollector::clearModels();

        return parent::begin($nesting);
    }

    /**
     * @param bool $nesting
     * @return bool
     */
    public function rollbackLogged($nesting = true)
    {
        LogModelCollector::setEnabled(false);
        LogModelCollector::clearModels();

        return parent::rollback($nesting);
    }

    /**
     * @param bool $nesting
     * @return bool
     */
    public function commitLogged($nesting = true)
    {
        LogModelCollector::setEnabled(false);

        return parent::commit($nesting);
    }
}