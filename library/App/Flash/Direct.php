<?php

namespace App\Flash;

/**
 * Class Direct
 */
class Direct
    extends \Phalcon\Flash\Direct
{
    /**
     * @var bool
     */
    protected $_autoescape = false;

    /**
     * @return array
     */
    public static function getCssClasses()
    {
        $map = [
            'success'   => 'success'
            , 'warning' => 'warning'
            , 'notice'  => 'info'
            , 'error'   => 'danger'
        ];

        $cssClasses = [];

        foreach ($map as $type=>$class) {
            $cssClasses[$type] = 'alert alert-' . $class . ' alert-styled-left alert-arrow-left alert-bordered';
        }

        return $cssClasses;
    }
}