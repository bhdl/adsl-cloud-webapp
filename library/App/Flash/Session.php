<?php

namespace App\Flash;

/**
 * Class Session
 */
class Session
    extends \Phalcon\Flash\Session
{
    /**
     * @var bool
     */
    protected $_autoescape = false;
}