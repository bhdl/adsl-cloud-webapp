<?php

namespace App\Mailer;

use App\Model\Invoice;
use App\Model\InvoicePayment;
use App\Model\Quote;
use App\Model\Traits\RowLevelAcl;

/**
 * Class Manager
 * @package App\Mailer
 */
class Manager
    extends \Phalcon\Mailer\Manager
{
    /**
     * @param string $view
     * @param array $params
     * @return \Phalcon\Mailer\Message
     */
    public function createMessageFromView($view, $params = [])
    {
        $message = $this->createMessage();

        $message->content(
            $this->renderView($view, $params)
            , $message::CONTENT_TYPE_HTML
        );

        $message->setFormat('text/html');

        return $message;
    }

    /**
     * @param $view
     * @param $params
     * @return string
     */
    protected function renderView($view, $params)
    {
        $v = $this->getView();

        $content = $v->getRender('email', $view, $params);

        return $content;
    }

    /**
     * @return \Phalcon\Mvc\View
     */
    protected function getView()
    {
        if ($this->view == null) {
            $this->view = new \Phalcon\Mvc\View();
            $this->view->setViewsDir(
                $this->getDI()->get('view')->getViewsDir()
            );
        }

        return $this->view;
    }
}
