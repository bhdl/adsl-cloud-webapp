<?php

namespace App\Cli;

use Phalcon\Di\FactoryDefault\Cli as CliDI;

/**
 * Class Bootstrap
 * @package App\Cli
 */
class Bootstrap
    extends \App\Bootstrap
{
    /**
     * @var mixed
     */
    protected $argv;

    /**
     * @var mixed
     */
    protected $argc;

    /**
     * @return array
     */
    public function getDefaultServices()
    {
        return [
            'dotEnv'         => '\\App\\ServiceProvider\\Service\\DotEnv'
            , 'app'          => '\\App\\ServiceProvider\\Service\\Cli\\App'
            , 'config'       => '\\App\\ServiceProvider\\Service\\Config'
            , 'loader'       => '\\App\\ServiceProvider\\Service\\Cli\\Loader'
            , 'errorHandler' => '\\App\\ServiceProvider\\Service\\Cli\\ErrorHandler'
            , 'router'       => '\\App\\ServiceProvider\\Service\\Cli\\Router'
        ];
    }

    /**
     * @param bool $sendResponse
     * @return mixed|void
     * @throws \Phalcon\Exception
     */
    public function run($sendResponse = true)
    {
        if (strcmp(php_sapi_name(), 'cli') !== 0) {
            return;
        }

        // Process the console arguments
        $arguments = [];

        if (is_array($this->argv)) {
            foreach ($this->argv as $k => $arg) {
                if ($k === 1) {
                    $arguments["task"] = $arg;
                } elseif ($k === 2) {
                    $arguments["action"] = $arg;
                } elseif ($k >= 3) {
                    $arguments["params"][] = $arg;
                }
            }
        }

        $this->di = new CliDI();
        $this->di->setShared('bootstrap', $this);

        $this->registerServices();

        $response = $this->di
            ->get('app')
            ->handle($arguments)
        ;

        if (!$sendResponse) {
            return $response;
        }

        echo $response;
    }

    /**
     * @return mixed
     */
    public function getArgv()
    {
        return $this->argv;
    }

    /**
     * @param mixed $argv
     * @return Bootstrap
     */
    public function setArgv($argv)
    {
        $this->argv = $argv;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArgc()
    {
        return $this->argc;
    }

    /**
     * @param mixed $argc
     * @return Bootstrap
     */
    public function setArgc($argc)
    {
        $this->argc = $argc;
        return $this;
    }
}
