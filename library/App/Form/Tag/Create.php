<?php

namespace App\Form\Tag;

use App\Form\AbstractCrud;
use App\Form\Element\Select2;
use Phalcon\Forms\Element\Text;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use App\Model\AbstractModel;
use App\Model\Group;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class Create
 */
class Create
    extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Típus
         */
        $element = new Select2(
            'type'
            , \App\Model\Tag::getTypes()
            , [
                'useEmpty'   => true,
                'emptyText'  => $this->translate->t('Kérem, válasszon!'),
                'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Típus'));
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
         * Telephely
         */
        $element = new Select2(
            'siteId'
            , \App\Model\Site::find()
            , [
                'using' => array(
                    'siteId',
                    'address'
                ),
                'useEmpty'   => true,
                'emptyText'  => $this->translate->t('Kérem, válasszon!'),
                'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Telephely'));
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
         * Név
         */
        $element = new Text('name');
        $element->setLabel($this->translate->t('Név'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min' => 3,
                'max' => 255,
                'messageMaximum' => $this->translate->t('Az érték maximum 255 karakter legyen!'),
                'messageMinimum' => $this->translate->t('Az érték minimum 3 karakter legyen!')
            ])
        ]);
        $this->add($element);

        /*
         * TagToken
         */
        $options = [
            'table'     => 'tag'
            , 'column'  => ['siteId', 'tagToken']
            , 'message'   => $this->translate->t('Ehhez a telephelyhez már létezik az alábbi tagToken!')
        ];
        if ($this->getEntity()) {
            $options['exclude'] = [
                'column' => 'tagId', 'value' => $this->getEntity()->tagId
            ];
        } else {
            $options['exclude'] = [
                'column' => 'deleted', 'value' => 'yes'
            ];
        }

        $element = new Text('tagToken');
        $element->setLabel($this->translate->t('Token'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min' => 1,
                'max' => 255,
                'messageMaximum' => $this->translate->t('Az érték maximum 255 karakter legyen!'),
                'messageMinimum' => $this->translate->t('Az érték minimum 1 karakter legyen!')
            ])
            , new Uniqueness($options)
        ]);
        $this->add($element);

        /*
         * Description
         */
        $element = new Text('description');
        $element->setLabel($this->translate->t('Leírás'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min' => 3,
                'max' => 255,
                'messageMaximum' => $this->translate->t('Az érték maximum 255 karakter legyen!'),
                'messageMinimum' => $this->translate->t('Az érték minimum 3 karakter legyen!')
            ])
        ]);
        $this->add($element);
    }
}