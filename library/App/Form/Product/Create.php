<?php

namespace App\Form\Product;

use App\Form\AbstractCrud;
use App\Form\Element\Select2;
use Phalcon\Forms\Element\TextArea;
use App\Model\Product;
use App\Model\Partner;
use Phalcon\Forms\Element\Text;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use App\Model\AbstractModel;
use App\Model\Group;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class Create
 */
class Create extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Partner
         */
        $element = new Select2(
            'partnerId'
            , Partner::find()
            , [
                'using' => array(
                    'partnerId',
                    'name'
                ),
                'useEmpty'   => true,
                'emptyText'  => $this->translate->t('Kérem, válasszon!'),
                'emptyValue' => null
            ]
        );

        $element->setLabel($this->translate->t('Partner'));
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
         * Név
         */
        $options = [
            'table'     => 'product'
            , 'column'  => ['partnerId', 'name']
            , 'message' => $this->translate->t('Ez a terméknév már létezik a kijelölt partnerhez!')
            , 'expect'    => [
                'deleted' => 'yes'
            ]
        ];
        if ($this->getEntity()) {
            $options['exclude'] = [
                'column' => 'partnerId', 'value' => $this->getEntity()->partnerId
            ];
        } else {
            $options['exclude'] = [
                'column' => 'deleted', 'value' => 'yes'
            ];
        }

        $element = new Text('name');
        $element->setLabel($this->translate->t('Név'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min' => 3,
                'max' => 255,
                'messageMaximum' => $this->translate->t('Az érték maximum 255 karakter legyen!'),
                'messageMinimum' => $this->translate->t('Az érték minimum 3 karakter legyen!')
            ]),
            new Uniqueness($options)
        ]);
        $this->add($element);

        /*
        * Termék leírása
        */
        $element = new TextArea('description');
        $element->setLabel($this->translate->t('Leírás'));
        $element->setAttribute('help', $language->languageName);
        $element->setFilters([
            "trim"
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                ,
                'cancelOnFail' => true
            ])
        ]);

        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
        ]);
        $this->add($element);
    }
}