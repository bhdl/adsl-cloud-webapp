<?php

namespace App\Form\Site;

use App\Form\AbstractCrud;
use App\Form\Element\Select2;
use Phalcon\Forms\Element\TextArea;
use App\Model\Partner;
use Phalcon\Forms\Element\Text;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use App\Model\AbstractModel;
use App\Model\Group;
use App\Validation\Validator\Db\Uniqueness;
use App\Validation\Validator\JsonSchema as JsonSchemaValidator;

/**
 * Class Create
 */
class Create extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Partner
         */
        $element = new Select2(
            'partnerId'
            , Partner::find()
            , [
                'using' => array(
                    'partnerId',
                    'name'
                ),
                'useEmpty'   => true,
                'emptyText'  => $this->translate->t('Kérem, válasszon!'),
                'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Partner'));
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
         * Gateway ID
         */

        $options = [
            "table"       => "site"
            , "column"    => "gatewayId"
            , "message"   => $this->translate->t("Ez Gateway ID már tartozik telephelyhez!")
        ];
        if ($this->getEntity()) {
            $options['exclude'] = [
                'column' => 'gatewayId', 'value' => $this->getEntity()->gatewayId
            ];
        } else {
            $options['exclude'] = [
                'column' => 'deleted', 'value' => 'yes'
            ];
        }

        $element = new Text('gatewayId');
        $element->setLabel($this->translate->t('Gateway ID'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min' => 3,
                'max' => 255,
                'messageMaximum' => $this->translate->t('Az érték maximum 255 karakter legyen!'),
                'messageMinimum' => $this->translate->t('Az érték minimum 3 karakter legyen!')
            ])    
            , new Uniqueness($options)
        ]);
        $this->add($element);

        /*
        * Cím
        */
        $element = new Text('address');
        $element->setLabel($this->translate->t('Address'));
        $element->setFilters([
            "trim"
        ]);

        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
        ]);
        $this->add($element);

        /**
         * Vetítés típus
         */
        $element = new Text('projection');
        $element->setLabel($this->translate->t('Projection'));
        $element->setFilters([
            "trim"
        ]);
        $this->add($element);

        /**
         * Bal longitude
         */
        $element = new Text('leftLongitude');
        $element->setLabel($this->translate->t('Bal longitude'));
        $element->setFilters([
            "trim"
        ]);
        $this->add($element);

        /**
         * Jobb longitude
         */
        $element = new Text('rightLongitude');
        $element->setLabel($this->translate->t('Jobb longitude'));
        $element->setFilters([
            "trim"
        ]);
        $this->add($element);

        /**
         * Fenti longitude
         */
        $element = new Text('topLatitude');
        $element->setLabel($this->translate->t('Fenti longitude'));
        $element->setFilters([
            "trim"
        ]);
        $this->add($element);

        /**
         * Lenti longitude
         */
        $element = new Text('bottomLatitude');
        $element->setLabel($this->translate->t('Lenti longitude'));
        $element->setFilters([
            "trim"
        ]);
        $this->add($element);

        /**
         * Svg json
         */
        $element = new TextArea('mapSvg');
        $element->setLabel($this->translate->t('Térkép alakzat (JSON)'));
        $element->setAttribute('class', 'jsonEditor');
        $element->setAttribute('style', 'height: 250px; font-family: monospace;');
        $element->setAttribute("help", 'JSON példa: <pre>
        [  
            {  
               "type": "line",
               "points":[  
                {"longitude":123.222, "latitude":123.122},
                {...}
               ]
            },
            {  
               "type": "polygon",
               "points":[  
                  {"longitude":123.222, "latitude":123.122},
                  {...}
               ]
            }
         ]
        </pre>
        ');
        $element->setFilters([
            "string"
        ]);

        $element->addValidators([
            new JsonSchemaValidator([
                'schema'         => 'map',
                'allowEmpty'     => 'true',
                'message'        => $this->translate->t('Érvénytelen JSON formátum ')
                , 'cancelOnFail' => true
            ])
        ]);

        $this->add($element);
    }
}