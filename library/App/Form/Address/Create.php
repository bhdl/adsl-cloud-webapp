<?php

namespace App\Form\Address;

use App\Form\AbstractCrud;
use App\Form\Element\Select2;
use App\Model\Address;
use App\Validation\Validator\Db\Uniqueness;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\StringLength;
use App\Validation\Validator\PresenceOf;

/**
 * Class Create
 * @package App\Form\Address
 */
class Create
    extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Név
         */
        $element = new Text("addressName");
        $element->setLabel($this->translate->t('Név'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ]),
            new StringLength([
                'min'               => 3,
                'max'               => 255,
                'messageMaximum'    => $this->translate->t('Az érték hossza maximum 255 karakter legyen!'),
                'messageMinimum'    => $this->translate->t('Az érték hossza minimum 3 karakter legyen!'),
            ])
        ]);
        $this->add($element);

        /*
         * Település
         */
        $element = new Text("settlement");
        $element->setLabel($this->translate->t('Település'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->_('A mező kitöltése kötelező!')
            ]),
            new StringLength([
                'min'               => 3,
                'max'               => 255,
                'messageMaximum'    => $this->translate->t('Az érték hossza maximum 255 karakter legyen!'),
                'messageMinimum'    => $this->translate->t('Az érték hossza minimum 3 karakter legyen!'),
            ])
        ]);
        $this->add($element);

        /*
         * Irányítószám
         */
        $element = new Text("zip");
        $element->setLabel($this->translate->t('Irányítószám'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ]),
            new StringLength([
                'min'               => 3,
                'max'               => 10,
                'messageMaximum'    => $this->translate->t('Az érték hossza maximum 10 karakter legyen!'),
                'messageMinimum'    => $this->translate->t('Az érték hossza minimum 3 karakter legyen!'),
            ])
        ]);
        $this->add($element);

        /*
         * Ország
         */
        $element = new Text("country");
        $element->setLabel($this->translate->t('Ország'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new StringLength([
                'max'               => 80,
                'messageMaximum'    => $this->translate->t('Az érték hossza maximum 80 karakter legyen!'),
                'allowEmpty'        => true
            ])
        ]);
        $this->add($element);

        /*
         * Régió
         */
        $element = new Text("region");
        $element->setLabel($this->translate->t('Régió'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new StringLength([
                'max'               => 80,
                'messageMaximum'    => $this->translate->t('Az érték hossza maximum 80 karakter legyen!'),
                'allowEmpty'        => true
            ])
        ]);
        $this->add($element);

        /*
         * Cím első sor
         */
        $element = new Text("line1");
        $element->setLabel($this->translate->t('Cím'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ]),
            new StringLength([
                'min'               => 3,
                'max'               => 80,
                'messageMaximum'    => $this->translate->t('Az érték hossza maximum 80 karakter legyen!'),
                'messageMinimum'    => $this->translate->t('Az érték hossza minimum 3 karakter legyen!'),
            ])
        ]);
        $this->add($element);

        /*
         * Cím második sor
         */
        $element = new Text("line2");
        $element->setLabel($this->translate->t('Cím 2'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new StringLength([
                'max'               => 80,
                'messageMaximum'    => $this->translate->t('Az érték hossza maximum 80 karakter legyen!'),
                'allowEmpty'        => true
            ])
        ]);
        $this->add($element);

        /*
         * Cím típusa
         */
        $element = new Select2(
            'addressTypes[]'
            , Address::getAddressTypes()
            ,[
                'multiple' => 'multiple'
                , 'id' => 'addressTypes'
            ]
        );
        $element->setDefault(
            $this->request->isPost() ?
                $this->request->get('addressTypes') :
                ($entity ? $entity->getAddressTypesAsArray() : null)
        );

        $element->setLabel($this->translate->t('Cím típus'));

        $column = ['addressTypes'];
        $value = [null];
        if (isset($options['addressOtherColumn']) && isset($options['addressOtherColumnValue'])) {
            $column[] = $options['addressOtherColumn'];
            $value[] = $options['addressOtherColumnValue'];
        }
        $uniqueOptions = [
            'table'     => 'address'
            , 'column'  => $column
            , 'value'   => $value
        ];
        if ($this->getEntity()) {
            $uniqueOptions['exclude'] = [
                'column' => 'addressId', 'value' => $this->getEntity()->addressId
            ];
        }
        $element->setLabel('Cím típus');
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
            , new Uniqueness($uniqueOptions)
        ]);
        $this->add($element);

    }
}
