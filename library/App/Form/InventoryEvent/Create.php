<?php

namespace App\Form\InventoryEvent;

use App\Form\AbstractCrud;
use App\Form\Element\Select2;
use App\Model\InventoryEvent;
use App\Model\Site;
use Phalcon\Forms\Element\Hidden;
use App\Form\Element\EasyTimer;
use App\Validation\Validator\PresenceOf;
use App\Validation\Validator\InventoryEventDate;
use App\Model\AbstractModel;

/**
 * Class Create
 */
class Create extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Site
         */
        $element = new Select2(
            'siteId'
            , Site::find()
            , [
                'using' => array(
                    'siteId',
                    'address'
                ),
                'useEmpty'   => true,
                'emptyText'  => $this->translate->t('Kérem, válasszon!'),
                'emptyValue' => null
            ]
        );

        $element->setLabel($this->translate->t('Telephely'));
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        $element = new EasyTimer('timerWrapper');
        $element->setLabel($this->translate->t('Meddig'));
        $this->add($element);

        $element = new Hidden('startTime');
        $this->add($element);

        $element = new Hidden('endTime');
        $this->add($element);

        $element = new Hidden('inventoryEventId');
        $this->add($element);
    }
}