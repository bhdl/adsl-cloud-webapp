<?php

namespace App\Form;

use Phalcon\Forms\Element;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Form;
use App\Validation\Validator\PresenceOf;

/**
 * Class AbstractCrudForm
 */
abstract class AbstractCrud
    extends Form
{
    /**
     * @return string
     */
    public function renderDecorated()
    {
        $string = '';

        foreach ($this->getElements() as $element) {
            $string .= $this->renderElementDecorated($element);
        }

        return $string;
    }

    /**
     * @param Element $element
     * @return string
     */
    public function renderElementDecorated($element)
    {
        $string = '';
        $errorMessages = '';

        // Get any generated messages for the current element
        $messages = $this->getMessagesFor($element->getName());

        if (count($messages)) {

            if($element instanceof Hidden) {
                $errorMessages .= '<div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">';
            }

            // Print each element
            $errorMessages .= '<span class="help-block">';
            $errorMessages .= '<ul>';
            foreach ($messages as $message) {
                $errorMessages .= '<li>';
                $errorMessages .= $this->translate->_($message->__toString());
                $errorMessages .= '</li>';
            }
            $errorMessages .= '</ul>';
            $errorMessages .= '</span>';

            if($element instanceof Hidden) {
                $errorMessages .= '</div>';
            }
        }

        $inputErrorClass = !empty($errorMessages) ? 'has-error has-feedback' : '';

        if (!($element instanceof Hidden)) {
            $string .= '<div class="form-group element-' . $element->getName() . ' ' . $inputErrorClass . '">';
        }

        if ($element->getLabel()) {
            $string .= '<label class="control-label col-lg-4" for="' . $element->getName() . '">';
            $string .= $this->translate->_($element->getLabel());

            if($this->hasPresenceOfValidator($element)) {
                $string .= '<span class="text-danger"> *</span>';
            }

            $string .= '</label>';
        }

        if (!($element instanceof Hidden)) {
            $string .= '<div class="col-lg-8 flex">';
        }

        $string .= $element->setAttribute('class', 'form-control' . ($element->getAttribute('class') ? ' ' . $element->getAttribute('class') : ''));

        if ($help = $element->getAttribute('help')) {
            $string .= '<span class="help-block" style="width:100%;">' . $this->translate->_($help) . '</span>';
        }

        if (!empty($errorMessages)) {
            if(!($element instanceof Hidden)) {
                $string .= '<div class="form-control-feedback"><i class="icon-cancel-circle2"></i></div>';
            }

            $string .= $errorMessages;
        }

        if (!($element instanceof Hidden)) {
            $string .= '</div>';
            $string .= '</div>';
        }


        return $string;
    }

    /**
     * @param $element
     * @return bool
     */
    protected function hasPresenceOfValidator($element)
    {
        $status = false;

        foreach ($element->getValidators() as $validator) {
            if ($validator instanceof PresenceOf) {
                $status = true;
                break;
            }
        }

        return $status;
    }
}