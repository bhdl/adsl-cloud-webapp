<?php

namespace App\Form\Password;

use App\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;

/**
 * Class Email
 * @package Qs\Form\ForgotPassword
 */
class Email extends Form
{
    public function initialize()
    {
        /**
         * Email input
         */
        $email = new Text('email', array(
            'class' => 'form-control'
        ));
        $email->setLabel($this->translate->t('E-mail'));
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            )),
            new EmailValidator(array(
                'message' => $this->translate->t('Érvénytelen e-mail cím!')
            ))
        ));
        $this->add($email);
    }
}