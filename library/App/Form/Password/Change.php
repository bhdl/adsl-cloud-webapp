<?php

namespace App\Form\Password;

use App\Form;
use Phalcon\Forms\Element\Password as PasswordFormElement;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\PasswordStrength;
use App\Validation\Validator\PasswordConfirmation;

/**
 * Class Email
 * @package Qs\Form\ForgotPassword
 */
class Change extends Form
{
    public function initialize()
    {
        /**
         * Jelszó input
         */
        $password = new PasswordFormElement('password', array(
            'class' => 'form-control'
        ));
        $password->setLabel($this->translate->t('Jelszó'));
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            )),
            new PasswordStrength(array(
                'message'       => $this->translate->t('Nem megfelelő jelszó formátum! A jelszónak tartalmaznia kell kis és nagybetűt valamint számot és minimum 8 karakter hosszúnak kell lennie!')
                , 'minScore'    => 3
            ))
        ));
        $this->add($password);
        
        /**
         * Jelszó mégegyszer input
         */
        $passwordAgain = new PasswordFormElement('passwordRepeat', array(
            'class' => 'form-control'
        ));
        $passwordAgain->setLabel($this->translate->t('Jelszó ismétlése'));
        $passwordAgain->addValidators(array(
            new PresenceOf(array(
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            )),
            new PasswordConfirmation()
        ));
        $this->add($passwordAgain);
    }
}