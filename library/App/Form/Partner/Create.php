<?php

namespace App\Form\Partner;

use App\Form\AbstractCrud;
use App\Model\User;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use App\Model\AbstractModel;
use App\Model\Group;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class Create
 */
class Create extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Név
         */
        $options = [
            'table' => 'partner'
            , 'column' => 'name'
            , 'message'   => $this->translate->t('Ilyen nevű partner már létezik a rendszerben!')
        ];
        $options['exclude'] = [
            'column' => 'deleted', 'value' => 'yes'
        ];

        $element = new Text('name');
        $element->setLabel($this->translate->t('Név'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min' => 3,
                'max' => 255,
                'messageMaximum' => $this->translate->t('Az érték maximum 255 karakter legyen!'),
                'messageMinimum' => $this->translate->t('Az érték minimum 3 karakter legyen!')
            ])
            , new Uniqueness($options)
        ]);
        $this->add($element);
    }
}