<?php

namespace App\Form\Language;

use App\Form\AbstractCrud;
use App\Form\Element\Select2;
use App\Model\AbstractModel;
use Phalcon\Forms\Element\Text;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class Create
 */
class Create
    extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Nyelv neve
         */
        $element = new Text('languageName');
        $element->setLabel($this->translate->t('Nyelv'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min'               => 3,
                'max'               => 255,
                'messageMaximum'    => $this->translate->t('Az érték hossza maximum 255 karakter legyen!'),
                'messageMinimum'    => $this->translate->t('Az érték hossza minimum 3 karakter legyen!')
            ])
        ]);
        $this->add($element);

        /*
         * Kódlap
         */
        $options = [
            'table'     => 'language'
            , 'column'  => 'locale'
        ];

        if ($this->getEntity()) {
            $options['exclude'] = [
                'column' => 'languageId', 'value' => $this->getEntity()->languageId
            ];
        }

        $element = new Text('locale');
        $element->setLabel($this->translate->t('Kódlap'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min'            => 5,
                'max'            => 5,
                'messageMaximum' => $this->translate->t('Az érték hossza 5 karakter legyen!'),
                'messageMinimum' => $this->translate->t('Az érték hossza 5 karakter legyen!')
                , 'cancelOnFail' => true
            ])
            , new Uniqueness($options)
        ]);
        $this->add($element);

        /*
         * ISO
         */
        $options = [
            'table'     => 'language'
            , 'column'  => 'iso'
        ];

        if ($this->getEntity()) {
            $options['exclude'] = [
                'column' => 'languageId', 'value' => $this->getEntity()->languageId
            ];
        }

        $element = new Text('iso');
        $element->setLabel($this->translate->t('ISO kód'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min'            => 2,
                'max'            => 2,
                'messageMaximum' => $this->translate->t('A(z) ISO kód hossza 2 karakter legyen!'),
                'messageMinimum' => $this->translate->t('A(z) ISO kód hossza 2 karakter legyen!')
                , 'cancelOnFail' => true
            ])
            , new Uniqueness($options)
        ]);
        $this->add($element);

        /*
         * Dátum formátum
         */
        $element = new Text('dateFormat');
        $element->setLabel($this->translate->t('Dátum formátum'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        ]);
        $this->add($element);

        /*
         * Dátum formátum - regexp
         */
        $element = new Text('dateRegexPattern');
        $element->setLabel($this->translate->t('Dátum formátum regex szabály'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        ]);
        $this->add($element);

        /*
         * Dátum formátum - input mask
         */
        $element = new Text('dateInputMask');
        $element->setLabel($this->translate->t('Dátum beviteli mező maszk'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        ]);
        $this->add($element);

        /*
         * UI nyelv
         */
        $element = new Select2('interface', AbstractModel::getBooleans());
        $element->setLabel($this->translate->t('UI nyelv'));
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        ]);
        $element->setDefault(AbstractModel::YES);
        $this->add($element);
    }
}