<?php

namespace App\Form\Log;


use App\Form\AbstractSearchBar;
use App\Form\Element\AutoCompleteUser;
use App\Form\Element\DatePicker;
use App\Model\Log;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Text;
use App\Form\Element\Select2;

/**
 * Class Search
 * @package App\Form\Log
 */
class Search
    extends AbstractSearchBar
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Felhasználó
         */
        $element = new Hidden('userId');
        $this->add($element);

        $element = new AutoCompleteUser('userName');
        $element->setLabel($this->translate->t('Felhasználó'));
        $this->add($element);

        /*
         * Prioritás
         */
        $element = new Select2(
            'priority'
            , Log::getPriorities()
            , [
                'useEmpty'   => true,
                'emptyText'  => $this->translate->t('Kérem, válasszon!'),
                'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Prioritás'));
        $this->add($element);

        /*
         * Üzenet
         */
        $element = new Text('content');
        $element->setLabel($this->translate->t('Üzenet'));
        $this->add($element);

        /*
         * Dátum -tól
         */
        $element = new DatePicker('from');
        $element->setLabel($this->translate->t('Időpont (tól)'));
        $this->add($element);

        /*
         * Dátum -ig
         */
        $element = new DatePicker('to');
        $element->setLabel($this->translate->t('Időpont (ig)'));
        $this->add($element);
    }
}