<?php

namespace App\Form\Profile;


use App\Form\AbstractCrud;
use Phalcon\Forms\Element\Password as Pw;
use App\Validation\Validator\PresenceOf;
use App\Validation\Validator\PasswordConfirmation;
use Phalcon\Validation\Validator\PasswordStrength;

/**
 * Class PasswordModify
 * @package App\Form\Profile
 */
class Password extends AbstractCrud
{
    public function initialize()
    {
        /*
         * Jelenlegi jelszó
         */
        $element = new Pw('currentPassword');
        $element->setLabel($this->translate->t('Jelenlegi jelszó'));
        $element->setAttributes(['autocomplete' => 'off']);
        $element->addValidator(
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
        );
        $this->add($element);

        /*
         * Új jelszó
         */
        $element = new Pw('newPassword');
        $element->setLabel($this->translate->t('Új jelszó'));
        $element->setAttributes(['autocomplete' => 'off']);
        $element->setAttribute('help', $this->translate->t('A jelszónak tartalmaznia kell kis és nagybetűt valamint számot és minimum 8 karakter hosszúnak kell lennie!'));
        $element->addValidator(
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
        );
        $element->addValidator(
            new PasswordStrength([
                'message'       => $this->translate->t('Nem megfelelő jelszó formátum! A jelszónak tartalmaznia kell kis és nagybetűt valamint számot és minimum 8 karakter hosszúnak kell lennie!')
                , 'allowEmpty'  => false
                , 'minScore'    => 3
            ])
        );
        $this->add($element);

        /*
         * Új jelszó mégegyszer
         */
        $element = new Pw('newPasswordAgain');
        $element->setLabel($this->translate->t('Új jelszó ismétlése'));
        $element->setAttributes(['autocomplete' => 'off']);
        $element->addValidator(
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
        );
        $element->addValidator(
            new PasswordConfirmation([
                'password' => 'newPassword'
            ])
        );
        $this->add($element);
    }
}