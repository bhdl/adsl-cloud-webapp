<?php

namespace App\Form\Profile;

use App\Form\AbstractCrud;
use App\Form\Element\Select2;
use App\Model\AbstractModel;
use App\Model\Language;
use App\Validation\Validator\PresenceOf;

/**
 * Class Config
 * @package App\Form\Profile
 */
class Config extends AbstractCrud
{
    /**
     * initialize
     */
    public function initialize()
    {
        /*
         * Nyelv
         */
        $element = new Select2(
            'languageId'
            , Language::findInterfaceLanguages()
            , [
                'using' => array(
                    'languageId',
                    'languageName'
                ),
                'useEmpty'   => true,
                'emptyText'  => $this->translate->t('Alapértelmezett'),
                'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Alapértelmezett nyelv'));
        $this->add($element);
    }
}