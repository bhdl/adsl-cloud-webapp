<?php

namespace App\Form\Profile;

use App\Form\AbstractCrud;
use Phalcon\Validation\Validator\File as FileValidator;
use Phalcon\Forms\Element\File;

/**
 * Class ProfilePictureUpload
 * @package App\Form\Profile
 */
class Avatar extends AbstractCrud
{
    /**
     *
     */
    public function initialize()
    {
        $element = new File(
            'avatar',
            [
                "class" => "autoHeight"
            ]
        );

        $element->setLabel($this->translate->t('Avatár'));
        $element->setAttribute(
            'help'
            , $this->translate->t('A feltölthető maximális fájlméret: %max% MB', ['max' => intval(ini_get("upload_max_filesize"))])
        );
        $element->addValidator(new FileValidator([
            'allowedTypes' => ['image/jpeg', 'image/png']
            , 'messageType' => $this->translate->t('Csak PNG vagy JPG formátumú kép tölthető fel!')
            , 'messageEmpty' => $this->translate->t('A mező kitöltése kötelező!')
        ]));

        $this->add($element);
    }
}