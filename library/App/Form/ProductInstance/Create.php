<?php

namespace App\Form\ProductInstance;

use App\Form\AbstractCrud;
use App\Form\Element\Select2;
use App\Model\ProductStatus;
use Phalcon\Forms\Element\TextArea;
use App\Model\Product;
use App\Model\Partner;
use Phalcon\Forms\Element\Text;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use App\Model\AbstractModel;
use App\Model\Group;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class Create
 */
class Create extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /**
         * Telephely
         */
        $element = new Select2(
            'siteId'
            , \App\Model\Site::find()
            , [
                'using' => array(
                    'siteId',
                    'gatewayId'
                )
            ]
        );

        $element->setLabel($this->translate->t('Telephely'));
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /**
         * Termék
         */
        $element = new Select2(
            'productId'
            , \App\Model\Product::find()
            , [
                'using' => array(
                    'productId',
                    'name'
                )
            ]
        );

        $element->setLabel($this->translate->t('Termék'));
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /**
         * Raktározási művelet
         */
        $element = new Select2(
            'productStorageAction'
            , ["Bevételezés"]
        );
        $element->setDefault(0);
        $element->setLabel($this->translate->t('Raktározási művelet '));
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
         * Rfid
         */
        $element = new TextArea('rfid');
        $element->setLabel($this->translate->t('Rfid'));
        $element->setFilters([
            "trim"
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
        ]);
        $this->add($element);

        /*
         * Státusz
         */
        $element = new Select2(
            'productStatusId'
            , \App\Model\ProductStatus::find()
            , [
                'using' => array(
                    'productStatusId',
                    'name'
                )
            ]
        );

        $element->setLabel($this->translate->t('Státusz'));
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

    }
}