<?php

namespace App\Form\Auth;

use App\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

/**
 * Class Login
 * @package App\Form\Auth
 */
class Login extends Form
{
    public function initialize()
    {
        /**
         * Email input
         */
        $email = new Text('email', array(
            'class' => 'form-control'
        ));
        $email->setLabel($this->translate->t('E-mail cím'));
        $email->setFilters([
            'striptags'
            , 'trim'
        ]);
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            )),
            new Email(array(
                'message' => $this->translate->t('Érvénytelen e-mail cím!')
            ))
        ));
        $this->add($email);

        /**
         * Jelszó input
         */
        $password = new Password('password', array(
            'class' => 'form-control'
        ));
        $password->setLabel($this->translate->t('Jelszó'));
        $password->setFilters([
            'striptags'
            , 'trim'
        ]);
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            )),
        ));
        $this->add($password);
    }
}