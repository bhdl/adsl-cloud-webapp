<?php

namespace App\Form\Document;

use App\Form\AbstractCrud;
use Phalcon\Validation\Validator\File as FileValidator;
use Phalcon\Forms\Element\File;

/**
 * Class Upload
 * @package App\Form\Document
 */
class Upload
    extends AbstractCrud
{
    /**
     * initialize
     */
    public function initialize()
    {
        $element = new File(
            'files[]',
            [
                "class" => "autoHeight"
                , "multiple" => true
                , "data-preview-file-type" => "text"
                , "id" => 'fileUpload'
            ]
        );

        //$element->setLabel($this->translate->t('Új dokumentum feltöltése'));
        $element->setAttribute(
            'help'
            , $this->translate->t('A feltölthető maximális fájlméret: %max% MB', ['max' => intval(ini_get("upload_max_filesize"))])
        );

        $this->add($element);
    }
}