<?php

namespace App\Form\Page;

use App\Form\AbstractSearchBar;
use App\Form\Element\Select2;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use App\Model\AbstractModel;
use App\Model\Group;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class Search
 */
class Search
    extends AbstractSearchBar
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * pageName
         */
        $element = new Text('pageName');
        $element->setLabel($this->translate->t('Név'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $this->add($element);

        /*
        * pageName
        */
        $element = new Text('slug');
        $element->setLabel($this->translate->t('Url'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $this->add($element);

        /*
         * Aktív
         */
        $element = new Select2(
            'active'
            , AbstractModel::getBooleans()
            , [
                'useEmpty'   => true
                , 'emptyText'  => $this->translate->t('Kérem, válasszon!')
                , 'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Aktív'));
        $element->setDefault(AbstractModel::YES);
        $this->add($element);

        /*
         * Slideshow
         */
        $element = new Select2(
            'slideshow'
            , AbstractModel::getBooleans()
            , [
                'useEmpty'   => true
                , 'emptyText'  => $this->translate->t('Kérem, válasszon!')
                , 'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Slideshow'));
        $element->setDefault(AbstractModel::NO);
        $this->add($element);

        /*
         * Térkép
         */
        $element = new Select2(
            'map'
            , AbstractModel::getBooleans()
            , [
                'useEmpty'   => true
                , 'emptyText'  => $this->translate->t('Kérem, válasszon!')
                , 'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Térkép'));
        $element->setDefault(AbstractModel::NO);
        $this->add($element);
    }
}