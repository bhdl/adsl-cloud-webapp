<?php

namespace App\Form\Page;

use App\Form\AbstractCrud;
use App\Form\Validator;
use App\Model\Language;
use App\Model\View\Page;
use Phalcon\Forms\Element\Text;
use App\Form\Element\Select2;
use App\Form\Element\CkEditor;
use App\Model\AbstractModel;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

/**
 * Class Create
 */
class Create
    extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
          * Oldal neve
          */
        $languages = Language::findInterfaceLanguages();
        foreach ($languages as $language) {
            $element = new Text('pageName_'. $language->languageId);
            $element->setLabel($this->translate->t('Név'));
            $element->setAttribute('help', $language->languageName);
            $element->setFilters([
                "striptags",
                "trim",
            ]);
            $element->addValidators([
                new PresenceOf([
                    'message' => $this->translate->t('A mező kitöltése kötelező!')
                    ,
                    'cancelOnFail' => true
                ])
                ,
                new StringLength([
                    'min' => 3,
                    'messageMinimum' => $this->translate->t('Az érték minimum 3 karakter legyen!')
                    ,
                    'cancelOnFail' => true
                ])
            ]);

            if ($this->getEntity()) {
                $entityLanguage = $this->getEntity()
                    ->getPageLanguages('languageId = ' . $language->languageId)
                    ->getFirst()
                ;

                if ($entityLanguage) {
                    $element->setDefault($entityLanguage->pageName);
                }
            }

            $this->add($element);

            /*
              * Oldal neve
              */
            $element = new CkEditor('content_'. $language->languageId);
            $element->setLabel($this->translate->t('Tartalom'));
            $element->setAttribute('help', $language->languageName);
            $element->setFilters([
                "trim"
            ]);
            $element->addValidators([
                new PresenceOf([
                    'message' => $this->translate->t('A mező kitöltése kötelező!')
                    ,
                    'cancelOnFail' => true
                ])
            ]);

            if ($this->getEntity()) {
                $entityLanguage = $this->getEntity()
                    ->getPageLanguages('languageId = ' . $language->languageId)
                    ->getFirst()
                ;

                if ($entityLanguage) {
                    $element->setDefault($entityLanguage->content);
                }
            }

            $this->add($element);
        }

        /*
         * Aktív -e az oldal
         */
        $element = new Select2('active', AbstractModel::getBooleans());
        $element->setLabel($this->translate->t('Aktív'));
        $element->setDefault(AbstractModel::YES);
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
        * Tartalmaz-e slideshowt az oldal
        */
        $element = new Select2('slideshow', AbstractModel::getBooleans());
        $element->setLabel($this->translate->t('Slideshow'));
        $element->setDefault(AbstractModel::NO);
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
        * Tartalmaz-e térképet az oldal
        */
        $element = new Select2('map', AbstractModel::getBooleans());
        $element->setLabel($this->translate->t('Térkép'));
        $element->setDefault(AbstractModel::NO);
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
        * Szülő
        */
        $user = $this->auth->getIdentity();
        $element = new Select2(
            'parentId'
            , Page::find([" active = '" . AbstractModel::YES . "'"
                . " AND parentId IS NULL"
                . " AND languageId = " . $user->getInterfaceLanguage()->languageId
                ])
            , [
            'using' => array(
                'pageId',
                'pageName'
            ),
            'useEmpty'   => true,
            'emptyText'  => $this->translate->t('Nincs szülő!'),
            'emptyValue' => null
        ]);
        $element->setLabel($this->translate->t('Szülő'));
        $this->add($element);
    }
}