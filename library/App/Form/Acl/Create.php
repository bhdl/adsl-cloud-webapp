<?php

namespace App\Form\Acl;

use App\Form\AbstractCrud;
use Phalcon\Forms\Element\Text;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use App\Form\Element\AclMatrix;
use App\Model\AclResource;
use App\Model\AclAction;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class Create
 */
class Create
    extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Szerepkör neve
         */
        $element = new Text('roleName');
        $element->setLabel($this->translate->t('Szerepkör'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $options = [
            'table'     => 'acl_role'
            , 'column'  => 'roleName'
        ];
        if ($this->getEntity()) {
            $options['exclude'] = [
                'column' => 'roleId', 'value' => $this->getEntity()->roleId
            ];
        }
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min'               => 3,
                'max'               => 80,
                'messageMaximum'    => $this->translate->t('Az érték hossza maximum 80 karakter legyen!'),
                'messageMinimum'    => $this->translate->t('Az érték hossza minimum 3 karakter legyen!')
            ])
            , new Uniqueness($options)
        ]);
        $this->add($element);

        /*
         * acl matrix
         */
        $element = new AclMatrix('aclMatrix');
        $element->setLabel($this->translate->t('Jogosultságok'));
        $element->setResources(AclResource::find(['order' => 'resourceName ASC']));
        $element->setActions(AclAction::find(['order' => 'actionName ASC']));
        $element->setAcl($this->acl);
        $element->setAttribute('id','aclMatrix');

        if ($entity){
            $default = [];

            foreach ($entity->matrix as $aclMatrix){
                $default[$aclMatrix->resourceId][] = $aclMatrix->actionId;
            }

            $element->setChecked($default);
        }

        $this->add($element);
    }
}