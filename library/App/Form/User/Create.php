<?php

namespace App\Form\User;

use App\Form\AbstractCrud;
use App\Form\Element\Select2;
use App\Model\User;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Email;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use App\Model\AbstractModel;
use App\Model\Group;
use App\Validation\Validator\Db\Uniqueness;
use App\Validation\Validator\PasswordConfirmation;
use Phalcon\Validation\Validator\PasswordStrength;

/**
 * Class Create
 */
class Create
    extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Csoport
         */
        $element = new Select2(
            'groupId'
            , Group::find('guest = \'no\'')
            , [
                'using' => array(
                    'groupId',
                    'groupName'
                ),
                'useEmpty'   => true,
                'emptyText'  => $this->translate->t('Kérem, válasszon!'),
                'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Csoport'));
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
         * Név
         */
        $element = new Text('name');
        $element->setLabel($this->translate->t('Név'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min' => 3,
                'max' => 255,
                'messageMaximum' => $this->translate->t('Az érték maximum 255 karakter legyen!'),
                'messageMinimum' => $this->translate->t('Az érték minimum 3 karakter legyen!')
            ])
        ]);
        $this->add($element);

        /*
         * E-mail cím
         */
        $element = new Text('email');
        $element->setLabel($this->translate->t('E-mail'));
        $element->setFilters([
            "email",
            "striptags",
            "trim",
        ]);
        $options = [
            'table'     => 'user'
            , 'column'  => 'email'
        ];
        if ($this->getEntity()) {
            $options['exclude'] = [
                'column' => 'userId', 'value' => $this->getEntity()->userId
            ];
        }
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new Email([
                'message'        => $this->translate->t('Érvénytelen e-mail cím!')
                , 'cancelOnFail' => true
            ])
            , new Uniqueness($options)
        ]);
        $this->add($element);

        /*
         * Jelszó
         */
        $element = new Password('password');
        $element->setLabel($this->translate->t('Jelszó'));
        $element->setAttributes(['autocomplete' => 'off']);
        $element->setAttribute(
            'help'
            , $this->translate->t('A jelszónak tartalmaznia kell kis és nagybetűt valamint számot és minimum 8 karakter hosszúnak kell lennie!')
        );

        if (!$this->getEntity()) {
            // modify
            $element->addValidator(
                new PresenceOf([
                    'message'        => $this->translate->t('A mező kitöltése kötelező!')
                    , 'cancelOnFail' => true
                ])
            );
        }
        $element->addValidator(
            new PasswordStrength([
                'message' => $this->translate->t('Nem megfelelő jelszó formátum! A jelszónak tartalmaznia kell kis és nagybetűt valamint számot és minimum 8 karakter hosszúnak kell lennie!')
                , 'allowEmpty' => true
                , 'minScore'    => 3
            ])
        );

        $this->add($element);

        /*
         * Jelszó ismétlés
         */
        $element = new Password('passwordRepeat');
        $element->setLabel($this->translate->t('Jelszó ismétlése'));

        if (!$this->getEntity()) {
            // modify
            $element->addValidator(
                new PresenceOf([
                    'message'        => $this->translate->t('A mező kitöltése kötelező!')
                    , 'cancelOnFail' => true
                ])
            );
        }
        $element->addValidator(
            new PasswordConfirmation([
                'allowEmpty' => true
            ])
        );
        $this->add($element);

        /*
         * Jelszó módosítás kérése következő belépésnél
         */
        $element = new Select2('passwordChange', AbstractModel::getBooleans());
        $element->setLabel($this->translate->t('Új jelszó megadása első belépésnél'));
        $element->setDefault(AbstractModel::NO);
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
         * Aktív
         */
        $element = new Select2('active', AbstractModel::getBooleans());
        $element->setLabel($this->translate->t('Aktív'));
        $element->setDefault(AbstractModel::YES);
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);

        /*
         * Jóváhagyás
         */
        $element = new Select2('approve', AbstractModel::getBooleans());
        $element->setLabel($this->translate->t('Jóváhagyva'));
        $element->setDefault(AbstractModel::NO);
        $element->addValidator(
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        );
        $this->add($element);
    }
}