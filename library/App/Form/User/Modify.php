<?php

namespace App\Form\User;

/**
 * Class Modify
 * @package App\Form\User
 */
class Modify
    extends Create
{
    /**
     * renderDecorated
     */
    public function renderDecorated()
    {
        $this->getEntity()->password = null;

        return parent::renderDecorated();
    }

    /**
     * Első beléptetéses jelcsószere mező levétele
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        parent::initialize($entity, $options);
        $this->remove('passwordChange');
    }
}