<?php

namespace App\Form\User;

use App\Form\AbstractSearchBar;
use App\Form\Element\Select;
use Phalcon\Forms\Element\Text;
use App\Model\AbstractModel;
use App\Model\Group;

/**
 * Class Search
 */
class Search
    extends AbstractSearchBar
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
        * Csoport
        */
        $element = new Select(
            'groupId'
            , Group::find('guest = \'no\'')
            , [
                'using' => array(
                    'groupId',
                    'groupName'
                ),
                'useEmpty'   => true,
                'emptyText'  => $this->translate->t('Kérem, válasszon!'),
                'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Csoport'));
        $this->add($element);

        /*
         * Név
         */
        $element = new Text('name');
        $element->setLabel($this->translate->t('Név'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $this->add($element);

        /*
         * E-mail cím
         */
        $element = new Text('email');
        $element->setLabel($this->translate->t('E-mail'));
        $element->setFilters([
            "email",
            "striptags",
            "trim",
        ]);
        $this->add($element);

        /*
         * Aktív
         */
        $element = new Select(
            'active'
            , AbstractModel::getBooleans()
            , [
                'useEmpty'   => true
                , 'emptyText'  => $this->translate->t('Kérem, válasszon!')
                , 'emptyValue' => null
            ]
        );
        $element->setLabel($this->translate->t('Aktív'));
        $this->add($element);
    }
}