<?php

namespace App\Form\Group;

use App\Form\AbstractCrud;
use App\Validation\Validator\PresenceOf;
use Phalcon\Forms\Element\Text;
use App\Form\Element\Select2;
use App\Form\Element\MultiSelect;
use Phalcon\Validation\Validator\StringLength;
use App\Model\AbstractModel;
use App\Model\AclRole;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class Create
 */
class Create
    extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * Csoport neve
         */
        $element = new Text('groupName');
        $element->setLabel($this->translate->t('Csoport'));
        $element->setFilters([
            "striptags",
            "trim",
        ]);
        $options = [
            'table'     => 'group'
            , 'column'  => 'groupName'
        ];
        if ($this->getEntity()) {
            $options['exclude'] = [
                'column' => 'groupId', 'value' => $this->getEntity()->groupId
            ];
        }
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
            , new StringLength([
                'min'               => 3,
                'max'               => 80,
                'messageMaximum'    => $this->translate->t('Az érték hossza maximum 80 karakter legyen!'),
                'messageMinimum'    => $this->translate->t('Az érték hossza minimum 3 karakter legyen!')
            ])
            , new Uniqueness($options)
        ]);
        $this->add($element);

        /*
         * Aktív
         */
        $element = new Select2('active', AbstractModel::getBooleans());
        $element->setLabel($this->translate->t('Aktív'));
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
            ])
        ]);
        $element->setDefault(AbstractModel::YES);
        $this->add($element);

        /*
         * Szerepkörök
         */
        $element = new Select2(
            'roleId[]'
            , AclRole::find()
            , [
                'using' => array(
                    'roleId',
                    'roleName'
                ),
                'useEmpty' => false,
                'multiple' => 'multiple',
                'id' => 'roleId'
            ]
        );

        $element->setLabel($this->translate->t('Szerepkör'));
        $element->addValidators([
            new PresenceOf([
                'message' => $this->translate->t('A mező kitöltése kötelező!')
            ])
        ]);

        $element->setDefault(
            $this->request->isPost() ?
                $this->request->get('roleId') :
                ($entity ? $entity->getRoleIds() : null)
        );

        $this->add($element);
    }
}