<?php

namespace App\Form\Config;

use App\Form\AbstractCrud;
use App\Form\Element\Numeric;
use Phalcon\Forms\Element\Text;
use App\Validation\Validator\Integer;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Digit;

/**
 * Class Modify
 */
class Modify
    extends AbstractCrud
{
    /**
     * @param null $entity
     * @param null $options
     */
    public function initialize($entity = null, $options = null)
    {
        /*
         * hány nap után járjon le a jelszó
         */
        $element = new Numeric('outDatedPasswordDay');
        $element->setLabel($this->translate->t('Jelszó lejárata (nap)'));
        $element->setAttribute('help', $this->translate->t('Hány nap után járjon le a jelszó.'));
        $element->setFilters([
            "striptags"
            , "trim"
        ]);
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ]),
            new Digit([
                'message'        => $this->translate->t('Az érték csak szám lehet!')
            ])
        ]);
        $this->add($element);

        /*
         * 	Kapcsolat menüpontos formban megadott adatokat erre az emailcimre küldjük
         */
        $element = new Text('contactEmail');
        $element->setLabel($this->translate->t('Email'));
        $element->setAttribute('help', $this->translate->t('Kapcsolat menüpontos formban megadott adatokat erre az emailcimre küldjük.'));
        $element->setFilters([
            "email"
            , "striptags"
            , "trim"
        ]);
        $element->addValidators([
            new Email([
                'message'        => $this->translate->t('Érvénytelen e-mail cím!')
                , 'cancelOnFail' => true
                , 'allowEmpty' => true
            ])
        ]);
        $this->add($element);

        /*
         * 		Kapcsolat menüpontos formban megadott adatok címzettje
         */
        $element = new Text('contactName');
        $element->setLabel($this->translate->t('Név'));
        $element->setAttribute('help', $this->translate->t('Kapcsolat menüpontos formban megadott adatok címzettje.'));
        $element->setFilters([
            "striptags"
            , "trim"
        ]);
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
        ]);
        $this->add($element);

        /*
         * 		A laboratórium címe
         */
        $element = new Text('contactAddress');
        $element->setLabel($this->translate->t('Cím'));
        $element->setAttribute('help', $this->translate->t('A laboratórium címe'));
        $element->setFilters([
            "striptags"
            , "trim"
        ]);
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
        ]);
        $this->add($element);

        /*
         * 		A laboratórium telefonszáma
         */
        $element = new Text('contactPhone');
        $element->setLabel($this->translate->t('Telefonszám'));
        $element->setAttribute('help', $this->translate->t('A laboratórium telefonszáma'));
        $element->setFilters([
            "striptags"
            , "trim"
        ]);
        $element->addValidators([
            new PresenceOf([
                'message'        => $this->translate->t('A mező kitöltése kötelező!')
                , 'cancelOnFail' => true
            ])
        ]);
        $this->add($element);

        if ($this->getDI()->get('app')->isDebug()) {
            /*
             * Teszt e-mail cím
             */
            $element = new Text('testEmail');
            $element->setLabel('Teszt e-mail');
            $element->setFilters([
                "email",
                "striptags",
                "trim",
            ]);
            $element->addValidators([
                new Email([
                    'message' => $this->translate->t('Érvénytelen e-mail cím!')
                    , 'allowEmpty' => true
                ])
            ]);
            $this->add($element);

            /*
             * Teszt név
             */
            $element = new Text('testName');
            $element->setLabel('Teszt név');
            $element->setFilters([
                "email",
                "striptags",
                "trim",
            ]);
            $this->add($element);
        }

    }
}