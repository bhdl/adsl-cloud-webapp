<?php

namespace App\Form\Element;

use Phalcon\Exception;

/**
 * Class AutoCompleteItemPlus
 * @package App\Form\Element
 */
class AutoCompleteItemPlus
    extends AutoCompleteAbstract
{

    /**
     * @var string
     */
    protected $source = '/admin/item/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.item';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $requestCallback = 'request.selectedItems = ReferralReceive.getSelectedItems();';

    /**
     * @var string
     */
    protected $selectCallback = 'ReferralReceive.selectedItems(ui.item);';

    /**
     * @var string
     */
    protected $afterSelectCallback = '$("#itemName").val("");';

    /**
     * @var string
     */
    protected $focusCallback = 'return false;';

    /**
     * Lásd jQuery UI autocomplete dokumentáció
     * @var int
     */
    protected $minLength = 2;

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.name + " </strong>"';
    }

}