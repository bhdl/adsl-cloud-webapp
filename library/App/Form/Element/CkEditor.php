<?php

namespace App\Form\Element;

use Phalcon\Forms\Element\TextArea;

/**
 * Class CkEditor
 * @package App\Form\Element
 */
class CkEditor
    extends TextArea
{
    /**
     * CKEditor config mappa elérése
     */
    const CUSTOM_CONFIG_PATH = '/assets/js/plugins/forms/ckeditor/config/';

    /**
     * Config fájl neve
     * @var string
     */
    protected $customConfig;

    /**
     * @param null $attributes
     * @return string
     */
    public function render($attributes = null)
    {
        $options = [];
        if ($this->getCustomConfig()) {
            $options['customConfig'] = self::CUSTOM_CONFIG_PATH . $this->getCustomConfig();
        }

        // CkEditor config beállítása
        $js = sprintf('CKEDITOR.replace(\'%s\', %s);',
            $this->getName()
            , json_encode($options)
        );

        // JavaScript
        $html = '<script type="text/javascript">';
        $html .= sprintf(
            '$(document).ready(function(){%s})'
            , $js
        );
        $html .= '</script>';

        $html .= parent::render($attributes);

        return $html;
    }

    /**
     * @return string|null
     */
    public function getCustomConfig()
    {
        return $this->customConfig;
    }

    /**
     * @param string $customConfig
     * @return CkEditor
     */
    public function setCustomConfig(string $customConfig): CkEditor
    {
        $this->customConfig = $customConfig;

        return $this;
    }
}