<?php

namespace App\Form\Element;

use Phalcon\Exception;

/**
 * Class AutoCompletePraxisHasDoctor
 * @package App\Form\Element
 */
class AutoCompletePraxisHasDoctor
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'praxisId';

    /**
     * @var string
     */
    protected $hiddenElementNameOther = 'doctorId';

    /**
     * @var string
     */
    protected $source = '/admin/referral-receive/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.referralReceive';

    /**
     * @var string
     */
    protected $labelKey = 'praxisDoctorName';

    /**
     * @var string
     */
    protected $valueKey = 'praxisId';

    /**
     * @var int
     */
    protected $minLength = 3;

    /** Páciens és Páciens azonosító közötti kiírási függőség
     * @var string
     */
    protected $selectCallback = '$("#doctorId").val(ui.item.doctorId);';

    /** Páciens és Páciens azonosító közötti kiírási függőség
     * @var string
     */
    protected $focusCallback = '$("#doctorId").val(ui.item.doctorId);';

    /** Páciens és Páciens azonosító közötti kiírási függőség
     * törléskor törlődjön az elem
     * @var string
     */
    protected $deleteValueCallback = '$("#doctorId").val("");';


    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.praxisDoctorName+ "</strong>"';
    }


}