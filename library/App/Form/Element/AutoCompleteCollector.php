<?php

namespace App\Form\Element;

use Phalcon\Exception;

/**
 * Class AutoCompleteCollector
 * @package App\Form\Element
 */
class AutoCompleteCollector
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'collectorId';

    /**
     * @var string
     */
    protected $source = '/admin/collector/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.collector';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Collector';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $valueKey = 'collectorId';

    /**
     * @var bool
     */
    protected $useCrudBrowse = true;

    /**
     * @var string
     */
    protected $crudBrowseAlias = 'collector';

    /**
     * @var string
     */
    protected $crudBrowseCallback = 'function(entity){
        $(\'#%elementName%\').val(entity.name );
        $(\'#%hiddenElementName%\').val(entity.collectorId);
    }';

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.name + " </strong>"';
    }

}