<?php

namespace App\Form\Element;

/**
 * Class AutoCompleteOwner
 * @package App\Form\Element
 */
class AutoCompleteOwner
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'ownerId';

    /**
     * @var string
     */
    protected $source = '/admin/owner/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'admin.owner';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Owner';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $valueKey = 'ownerId';

    /**
     * @var bool
     */
    protected $useCrudBrowse = true;

    /**
     * @var bool
     */
    protected $useCrudCreate = true;

    /**
     * @var bool
     */
    protected $nested = false;

    /**
     * AutoCompleteOwner constructor.
     * @param $name
     * @param null $attributes
     */
    public function __construct($name, $attributes = null)
    {
        $this->crudCreateAlias = $this->crudBrowseAlias = 'owner';

        parent::__construct($name, $attributes);
    }

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.lastName + " </strong>" + item.firstName';
    }

    /**
     * @param $id
     * @return string
     */
    protected function getCrudCreateJs($id)
    {
        if ($this->isNested()) {
            return sprintf(
                '$("#%sCrudCreateModal").click(function(e){'
                . 'e.preventDefault();'
                . 'var crudCreateModal = new CrudCreateModal("%s");'
                . 'var previousModal = CrudCreateModalRegistry.get().persist();'
                . 'crudCreateModal'
                . '.setCallbacks({create : %s})'
                . '.setPreviousModal(previousModal)'
                . '.form($(this));'
                . '});'
                , $this->nameToId($this->getName())
                , $this->getCrudCreateAlias()
                , $this->replaceCrudCallbackVariables(
                    $this->getCrudCreateCallback()
                    , [
                        '%elementName%' => $id
                        , '%hiddenElementName%' => $this->hiddenElementName
                    ]
                )
            );
        }

        return parent::getCrudCreateJs($id);
    }

    /**
     * @return string
     */
    public function getCrudCreateCallback(): string
    {
        if ($this->crudCreateCallback == null) {
            if ($this->isNested()) {
                $this->crudCreateCallback = $this->crudBrowseCallback = 'function(entity){
                    CrudCreateModalRegistry.get().getPreviousModal().setFormDefault(\'ownerId\', entity.ownerId);
                }';
            } else {
                $this->crudCreateCallback = $this->crudBrowseCallback = 'function(entity){
                    $(\'#%elementName%\').val(entity.lastName + \' \' + entity.firstName);
                    $(\'#%hiddenElementName%\').val(entity.ownerId);
                }';
            }
        }

        return parent::getCrudCreateCallback();
    }

    /**
     * @return string
     */
    public function getCrudBrowseCallback(): string
    {
        return $this->getCrudCreateCallback();
    }

    /**
     * @return bool
     */
    public function isNested(): bool
    {
        return $this->nested;
    }

    /**
     * @param bool $nested
     * @return AutoCompleteOwner
     */
    public function setNested(bool $nested): AutoCompleteOwner
    {
        $this->nested = $nested;
        return $this;
    }
}