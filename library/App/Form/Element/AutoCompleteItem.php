<?php

namespace App\Form\Element;

use Phalcon\Exception;

/**
 * Class AutoCompleteItem
 * @package App\Form\Element
 */
class AutoCompleteItem
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'itemId';

    /**
     * @var string
     */
    protected $source = '/admin/item/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.item';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Item';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $valueKey = 'itemId';

    /**
     * @var bool
     */
    protected $useCrudBrowse = true;

    /**
     * @var string
     */
    protected $crudBrowseAlias = 'item';

    /**
     * @var string
     */
    protected $crudBrowseCallback = 'function(entity){
        $(\'#%elementName%\').val(entity.name );
        $(\'#%hiddenElementName%\').val(entity.itemId);
    }';

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.name + " </strong>"';
    }

}