<?php

namespace App\Form\Element;

/**
 * Class AutoCompleteUser
 * @package App\Form\Element
 */
class AutoCompleteUser
    extends AutoCompleteAbstract
{

    /**
     * @var string
     */
    protected $hiddenElementName = 'userId';

    /**
     * @var string
    */
    protected $source = '/admin/user/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.user';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\User';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $valueKey = 'userId';

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.name + " </strong> <br />" + item.email';
    }
}