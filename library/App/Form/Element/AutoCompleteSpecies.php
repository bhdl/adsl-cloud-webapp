<?php

namespace App\Form\Element;

/**
 * Class AutoCompleteSpecies
 * @package App\Form\Element
 */
class AutoCompleteSpecies
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'speciesId';

    /**
     * @var string
     */
    protected $source = '/admin/species/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.species';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Species';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $valueKey = 'speciesId';

    /**
     * @var int
     */
    protected $minLength = 2;

    /** Faj és fajta közötti függőség
     * @var string
     */
    protected $selectCallback = '$("#breedName").removeAttr("readOnly");';

    /** Faj és fajta közötti függőség
     * @var string
     */
    protected $focusCallback = '$("#breedName").removeAttr("readOnly");';

    /** Faj és fajta közötti függőség
     * Faj érték törlésekor a fajta korábban beállított értékei (breedId és breedName) törlődjön
     * @var string
     */
    protected $deleteValueCallback = '$("#breedId").val(""); $("#breedName").val(""); $("#breedName").attr("readOnly","readOnly");';

    /**
     * Állatcsoport és faj közötti függőség
     * @var string
     */
    protected $requestCallback = ' if ($("#animalGroupId").length > 0) {request.animalGroupId = $("#animalGroupId").val();}';

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.name + " </strong>" + " (" + item.identifier + ")"';
    }
}