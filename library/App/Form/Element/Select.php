<?php

namespace App\Form\Element;

use Phalcon\Di;
use Phalcon\Forms\Element\Select as SelectBase;

/**
 * Class Select
 * @package App\Form\Element
 */
class Select extends SelectBase
{
    /**
     * Select constructor.
     * @param $name
     * @param null $options
     * @param null $attributes
     */
    public function __construct($name, $options=null, $attributes=null)
    {
        if (is_array($options)) {
            $options = $this->translateOptions($options);
        }

        parent::__construct($name, $options, $attributes);
    }

    /**
     * @param array|object $options
     * @return \Phalcon\Forms\Element
     */
    public function setOptions($options)
    {
        return parent::setOptions($this->translateOptions($options));
    }

    /**
     * @param $options
     * @return mixed
     */
    public function translateOptions($options)
    {
        $translate = Di::getDefault()->get('translate');

        if (count($options)) {
            foreach ($options as $index => $option) {
                $options[$index] = $translate->_($option);
            }
        }

        return $options;
    }
}