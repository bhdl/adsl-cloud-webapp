<?php

namespace App\Form\Element;

use App\Model\Language;
use App\Support\Date;


/**
 * Class TimePicker
 * @package App\Form\Element
 */
class TimePicker
    extends DatePicker
{
    /**
     * @param null $attributes
     * @return string
     */
    public function render($attributes = null): string
    {
        // attribútum konverzió
        $attributes['id'] = $this->nameToId($this->getName());

        // inline javascript
        $js = '<script type="text/javascript">';
        $js .= '$(document).ready(function(){';

        $this->setParam('timeInput',true);

        // timepicker paraméterek
        $js .= sprintf(
            '$("#%s").timepicker(%s);'
            , $attributes['id']
            , json_encode($this->getParams(), JSON_NUMERIC_CHECK)
        );

        /*
         * Alapértelmezett érték beállítása, használva a felület i18n beállításait
         */
        $value = $this->getValue();
        if (!$value) {
            $value = $this->getAttribute('value');
        }

        if ($value) {
            $v = Date::timeFromLocalized($value);
            if ($v) {
                $js .= sprintf(
                    '$("#%s").timepicker("setDate", new Date(\'' . $v->format('Y-m-d H:i') . '\'));'
                    , $attributes['id']
                );
            }
        }

        /*
         * ENTER event
         */
        $js .= sprintf(
            '$("#%s").timepicker().keydown(function(event) {
                if (event.which === $.ui.keyCode.ENTER) {
                    $(this).trigger("change");
                    event.preventDefault();
                }
            });'
            , $attributes['id']
        );

        /**
         * @var Language $language
         */
        $language = $this->getInterfaceLanguage();

        /*
         * Input mask
         */
        $js .= sprintf(
            '$("#%s").formatter({pattern: "%s"});'
            , $attributes['id']
            , $language->timeInputMask
        );

        $js .= '});';
        $js .= '</script>';

        $js .= parent::render($attributes);

        return $js;
    }
}