<?php

namespace App\Form\Element;

use App\Support\Str;
use Phalcon\Di;
use Phalcon\Exception;
use Phalcon\Forms\Element;
use Phalcon\Tag;

/**
 * Class AutoCompleteAbstract
 * @package App\Form\Element
 */
abstract class AutoCompleteAbstract
    extends Element
{
    /**
     * A hidden form elem neve, ahová az AC a kiválasztott
     * listaelem azonosítóját menti.
     * @var string
     */
    protected $hiddenElementName;

    /**
     * @var string
     */
    protected $labelKey = 'label';

    /**
     * @var string
     */
    protected $persistLabelKey = 'persistLabel';

    /**
     * @var string
     */
    protected $valueKey = 'value';

    /**
     * Lásd jQuery UI autocomplete dokumentáció
     * @var string
     */
    protected $source;

    /**
     * @var string
     */
    protected $resourceId;

    /**
     * @var string
     */
    protected $requestCallback;

    /**
     * @var string
     */
    protected $deleteValueCallback;

    /**
     * Lásd jQuery UI autocomplete dokumentáció
     * @var int
     */
    protected $minLength = 3;

    /**
     * Lásd jQuery UI autocomplete dokumentáció
     * @var int
     */
    protected $delay;

    /**
     * @var string
     */
    protected $modelClass;

    /**
     * @var string
     */
    protected $focusCallback;

    /**
     * @var string
     */
    protected $afterFocusCallback;

    /**
     * @var string
     */
    protected $selectCallback;

    /**
     * @var string
     */
    protected $afterSelectCallback;

    /**
     * @var string
     */
    protected $nameToId;

    /**
     * @var boolean
     */
    protected $useCrudBrowse = false;

    /**
     * @var string
     */
    protected $crudBrowseAlias;

    /**
     * @var string
     */
    protected $crudBrowseCallback;

    /**
     * @var bool
     */
    protected $useCrudCreate = false;

    /**
     * @var string
     */
    protected $crudCreateAlias;

    /**
     * @var string
     */
    protected $crudCreateCallback;

    /**
     * @var string
     */
    protected $builderParams = [];

    /**
     * @param null|array $attributes
     * @return mixed
     * @throws Exception
     */
    public function render($attributes = null)
    {
        $attributes = $this->getAttributes();

        $attributes['id'] = $this->nameToId(
            $this->getName()
        );

        $js = $html = '';
        /*
         * Model
         */
        if ($this->hiddenElementName && $this->modelClass) {
            $form = $this->getForm();
            $id   = null;

            if ($form) {
                $id = $form->get($this->hiddenElementName)->getValue();
            }
            if ($id) {
                // set defaults
                $modelClass = $this->modelClass;
                $model = $modelClass::findFirst($id);
                if ($model) {
                    $this->setDefault($this->getDefaultValue($model->getName()));
                    $js .= sprintf('$("#%s").val("%s");',
                        $attributes['id']
                        , addslashes($this->getDefaultValue($model->getName()))
                    );
                }
            }
        }

        // vizsgáljuk, hogy name vagy id alapján kell összeállítani a selector-t
        $hiddenElementSelector = null;

        if ($this->getHiddenElementName()) {
            $hiddenElementSelector = '#' . $this->getHiddenElementName();
        }

        if ($hiddenElementSelector !== null) {
            // egyedi callback
            $deleteValueCallback = '';
            if ($this->getDeleteValueCallback()) {
                $deleteValueCallback = $this->getDeleteValueCallback();
            }
            // keyup - üres vagy kicsi mezőérték esetén töröljük a hidden mező értékét
            $js .= sprintf('$("#%s").keyup(function(){if(this.value == "" || this.value.length < $("#%s")
                .autocomplete("option", "minLength")){
                    $("%s").val("");'
                    . $deleteValueCallback
                . '}});',
                $attributes['id'],
                $attributes['id'],
                $hiddenElementSelector
            );

            // autocompletesearch - keresés előtt töröljük a hidden értékét (tehát ha nem üres és a minLength is jó)
            $js .= sprintf('$("#%s").bind("autocompletesearch", function(event, ui){$("%s").val("")});',
                $attributes['id'],
                $hiddenElementSelector
            );

            // blur - ha nem választ ki érvényes értéket, töröljük a mező szöveget
            $js .= sprintf('$("#%s").blur(function(){if($("%s").val() == ""){this.value = ""}});',
                $attributes['id'],
                $hiddenElementSelector
            );
        }

        // autocompletefocus - billentyű és egér hover esetén változtatjuk a hidden és a text értékét
        // autocompleteselect - enter és egérkattintás esetén változtatjuk a hidden és a text értékét
        foreach (array('autocompletefocus', 'autocompleteselect') as $event) {
            // egyedi callback
            $focusCallback = '';
            if ($this->getFocusCallback() && strcmp($event, 'autocompletefocus') === 0) {
                $focusCallback = $this->getFocusCallback();
            }

            $afterFocusCallback = '';
            if ($this->getAfterFocusCallback() && strcmp($event, 'autocompletefocus') === 0) {
                $afterFocusCallback = $this->getAfterFocusCallback();
            }

            $selectCallback = '';
            if ($this->getSelectCallback() && strcmp($event, 'autocompleteselect') === 0) {
                $selectCallback = $this->getSelectCallback();
            }

            $afterSelectCallback = '';
            if ($this->getAfterSelectCallback() && strcmp($event, 'autocompleteselect') === 0) {
                $afterSelectCallback = $this->getAfterSelectCallback();
            }

            // hidden mező értékének frissítését végző callback js szkript
            $hiddenElementCallback = '';
            if ($hiddenElementSelector !== null) {
                $hiddenElementCallback = sprintf(
                    '$("%s").val(ui.item.%s);'
                    , $hiddenElementSelector
                    , $this->getValueKey()
                );
            }

            // text mező értékének frissítését végző callback
            $js .= sprintf('$("#%s").bind("%s", function(event, ui){'
                . $hiddenElementCallback
                . $focusCallback
                . $selectCallback
                // a szövegmező értéke vagy a persistLabel, vagy a label lesz
                // (persistLabel elsőbbséget élvez, ha be van állítva)
                . 'this.value=(ui.item.%s !== undefined) ? '
                . 'ui.item.%s : ui.item.%s;'
                . 'event.preventDefault();'
                . $afterFocusCallback
                . $afterSelectCallback
                . '});',
                $attributes['id'],
                $event,
                $this->getPersistLabelKey(),
                $this->getPersistLabelKey(),
                $this->getLabelKey()
            );
        }

        $acAttributes = [];
        $acAttributesJs = '';

        if (!$this->getSource()) {
            throw new Exception(
                "Cannot construct AutoComplete field without specifying 'source' field, ".
                "either an url or an array of elements."
            );
        }

        $acAttributes['source'] = sprintf(
            'function(request, response) {'
                .'%s'
                .'$.ajax({'
                    .'url: "%s",'
                    .'data: request,'
                    .'success: function( data ) {'
                        .'response(data);'
                    .'}'
                .'});'
            .'}'
            , $this->getRequestCallback()
            , $this->getSource()
        );

        if ($this->getDelay()) {
            $acAttributes['delay'] = $this->getDelay();
        }

        if ($this->getMinLength()) {
            $acAttributes['minLength'] = $this->getMinLength();
        }

        foreach ($acAttributes as $index => $attribute) {
            $acAttributesJs .= sprintf('"%s": %s,', $index, $attribute);
        }

        $js .= sprintf('$("#%s").autocomplete({%s});',
            $attributes['id'],
            $acAttributesJs
        );

        array_unshift($attributes, $this->getName());

        $textField = Tag::textField($attributes)
            . '<div class="form-control-feedback autocomplete-loader">'
            . '<i class="icon-spinner2 spinner"></i>'
            . '</div>'
        ;

        $allowBrowse = Di::getDefault()->get('acl')->isAllowedByIdentity($this->getResourceId(), 'browse');
        $allowCreate = Di::getDefault()->get('acl')->isAllowedByIdentity($this->getResourceId(), 'create');

        if (($this->isUseCrudBrowse() && $allowBrowse) || ($this->isUseCrudCreate() && $allowCreate)) {

            $html = '<div class="row">';
            $html .= '<div class="col-sm-9">';
            $html .= $textField;
            $html .= '</div>';

            $html .= '<div class="col-sm-3">';

            $html .= '<div class="btn-group">';

            // browse
            if ($this->isUseCrudBrowse() && $allowBrowse) {
                $html .= $this->getCrudBrowseButton();
                $js   .= $this->getCrudBrowseJs($attributes['id']);
            }

            // create
            if ($this->isUseCrudCreate() && $allowCreate) {
                $html .= $this->getCrudCreateButton();
                $js   .= $this->getCrudCreateJs($attributes['id']);
            }

            $html .= '</div>';

            $html .= '</div>';

            $html .= '</div>';
        } else {
            $html = $textField;
        }

        // render item handle
        $js .= '$("#' . $this->nameToId($this->getName()) . '").autocomplete()'
            . '.data("ui-autocomplete")._renderItem = function(ul, item){'
            . 'return $("<li></li>").data("item.autocomplete", item).append('
            . '"<a>" + '
            . $this->getRenderItemCallback()
            .' + "</a>"'
            . ')' // append
            . '.appendTo(ul);'
            . '};'
        ;

        $html .= sprintf('<script type="text/javascript">$(document).ready(function(){%s});</script>', $js);

        return $html;
    }

    /**
     * @param string $name
     * @return string
     */
    protected function nameToId($name)
    {
        if ($this->nameToId == null) {
            $this->nameToId = $name;
            if (mb_strpos($name, '[') !== false || mb_strpos($name, ']') !== false) {
                $this->nameToId = Str::randomHash();
            }
        }

        return $this->nameToId;
    }

    /**
     * @return mixed
     */
    public function getHiddenElementName()
    {
        return $this->hiddenElementName;
    }

    /**
     * @param mixed $hiddenElementName
     * @return AutoCompleteAbstract
     */
    public function setHiddenElementName($hiddenElementName)
    {
        $this->hiddenElementName = $hiddenElementName;

        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return AutoCompleteAbstract
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return string
     */
    public function getResourceId(): string
    {
        return $this->resourceId;
    }

    /**
     * @param string $resourceId
     * @return AutoCompleteAbstract
     */
    public function setResourceId($resourceId)
    {
        $this->resourceId = $resourceId;

        return $this;
    }

    /**
     * @return int
     */
    public function getMinLength()
    {
        return $this->minLength;
    }

    /**
     * @param int $minLength
     * @return AutoCompleteAbstract
     */
    public function setMinLength($minLength)
    {
        $this->minLength = $minLength;

        return $this;
    }

    /**
     * @return int
     */
    public function getDelay()
    {
        return $this->delay;
    }

    /**
     * @param int $delay
     * @return AutoCompleteAbstract
     */
    public function setDelay($delay)
    {
        $this->delay = $delay;

        return $this;
    }

    /**
     * A metodus szerepe, hogy az eddigi mukodest nem befolyasolja, illetve lehetoseget adjon a kivetelezesre, amikor
     * a jelen autocomplete kontroll default erteket valamilyen kalkulalt ertekkel akarjuk beallitani
     *
     * Az okot az autocompleteSettlement adta, mivel a megjelenitendo erteket a JS allitja document ready-re,
     * es nem lett volna lehetosegunk (egyszeruen) beavatkozni
     *
     * @param string $defaultValue
     * @return mixed
     */
    protected function getDefaultValue($defaultValue = '')
    {
        return $defaultValue;
    }

    /**
     * @return string
     */
    public function getLabelKey(): string
    {
        return $this->labelKey;
    }

    /**
     * @param string $labelKey
     * @return AutoCompleteAbstract
     */
    public function setLabelKey(string $labelKey): AutoCompleteAbstract
    {
        $this->labelKey = $labelKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getPersistLabelKey(): string
    {
        return $this->persistLabelKey;
    }

    /**
     * @param string $persistLabelKey
     * @return AutoCompleteAbstract
     */
    public function setPersistLabelKey(string $persistLabelKey): AutoCompleteAbstract
    {
        $this->persistLabelKey = $persistLabelKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getValueKey(): string
    {
        return $this->valueKey;
    }

    /**
     * @param string $valueKey
     * @return AutoCompleteAbstract
     */
    public function setValueKey(string $valueKey): AutoCompleteAbstract
    {
        $this->valueKey = $valueKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getFocusCallback()
    {
        return $this->focusCallback;
    }

    /**
     * @param string $focusCallback
     * @return AutoCompleteAbstract
     */
    public function setFocusCallback(string $focusCallback): AutoCompleteAbstract
    {
        $this->focusCallback = $focusCallback;
        return $this;
    }

    /**
     * @return string
     */
    public function getAfterFocusCallback()
    {
        return $this->afterFocusCallback;
    }

    /**
     * @param string $afterFocusCallback
     * @return AutoCompleteAbstract
     */
    public function setAfterFocusCallback(string $afterFocusCallback): AutoCompleteAbstract
    {
        $this->afterFocusCallback = $afterFocusCallback;
        return $this;
    }



    /**
     * @return string
     */
    public function getSelectCallback()
    {
        return $this->selectCallback;
    }

    /**
     * @param string $selectCallback
     * @return AutoCompleteAbstract
     */
    public function setSelectCallback(string $selectCallback): AutoCompleteAbstract
    {
        $this->selectCallback = $selectCallback;
        return $this;
    }

    /**
     * @return string
     */
    public function getAfterSelectCallback()
    {
        return $this->afterSelectCallback;
    }

    /**
     * @param string $afterSelectCallback
     * @return AutoCompleteAbstract
     */
    public function setAfterSelectCallback(string $afterSelectCallback): AutoCompleteAbstract
    {
        $this->afterSelectCallback = $afterSelectCallback;
        return $this;
    }

    /**
     * @return string
     */
    public function getRequestCallback()
    {
        return $this->requestCallback;
    }

    /**
     * @param string $requestCallback
     * @return AutoCompleteAbstract
     */
    public function setRequestCallback($requestCallback)
    {
        $this->requestCallback = $requestCallback;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeleteValueCallback()
    {
        return $this->deleteValueCallback;
    }

    /**
     * @param string $deleteValueCallback
     * @return AutoCompleteAbstract
     */
    public function setDeleteValueCallback($deleteValueCallback)
    {
        $this->deleteValueCallback = $deleteValueCallback;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUseCrudBrowse(): bool
    {
        return $this->useCrudBrowse;
    }

    /**
     * @param bool $useCrudBrowse
     * @return AutoCompleteAbstract
     */
    public function setUseCrudBrowse(bool $useCrudBrowse): AutoCompleteAbstract
    {
        $this->useCrudBrowse = $useCrudBrowse;
        return $this;
    }

    /**
     * @return string
     */
    public function getCrudBrowseAlias(): string
    {
        return $this->crudBrowseAlias;
    }

    /**
     * @param string $crudBrowseAlias
     * @return AutoCompleteAbstract
     */
    public function setCrudBrowseAlias(string $crudBrowseAlias): AutoCompleteAbstract
    {
        $this->crudBrowseAlias = $crudBrowseAlias;
        return $this;
    }

    /**
     * @param $callback
     * @param $variables
     * @return mixed
     */
    protected function replaceCrudCallbackVariables($callback, $variables)
    {
        return str_replace(array_keys($variables), array_values($variables), $callback);
    }

    /**
     * @return string
     */
    public function getCrudBrowseCallback(): string
    {
        return $this->crudBrowseCallback;
    }

    /**
     * @param string $crudBrowseCallback
     * @return AutoCompleteAbstract
     */
    public function setCrudBrowseCallback(string $crudBrowseCallback): AutoCompleteAbstract
    {
        $this->crudBrowseCallback = $crudBrowseCallback;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUseCrudCreate(): bool
    {
        return $this->useCrudCreate;
    }

    /**
     * @param bool $useCrudCreate
     * @return AutoCompleteAbstract
     */
    public function setUseCrudCreate(bool $useCrudCreate): AutoCompleteAbstract
    {
        $this->useCrudCreate = $useCrudCreate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCrudCreateAlias(): string
    {
        return $this->crudCreateAlias;
    }

    /**
     * @param string $crudCreateAlias
     * @return AutoCompleteAbstract
     */
    public function setCrudCreateAlias(string $crudCreateAlias): AutoCompleteAbstract
    {
        $this->crudCreateAlias = $crudCreateAlias;
        return $this;
    }

    /**
     * @return string
     */
    public function getCrudCreateCallback(): string
    {
        return $this->crudCreateCallback;
    }

    /**
     * @param string $crudCreateCallback
     * @return AutoCompleteAbstract
     */
    public function setCrudCreateCallback(string $crudCreateCallback): AutoCompleteAbstract
    {
        $this->crudCreateCallback = $crudCreateCallback;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getBuilderParams()
    {
        return $this->builderParams;
    }

    /**
     * @param mixed $builderParams
     * @return AutoCompleteAbstract
     */
    public function setBuilderParams($builderParams): AutoCompleteAbstract
    {
        $this->builderParams = $builderParams;
        return $this;
    }

    /**
     * @return string
     */
    abstract protected function getRenderItemCallback();

    /**
     * @return string
     */
    protected function getCrudBrowseButton()
    {
        $translate = Di::getDefault()->get('translate');

        return sprintf(
            '<button class="btn btn-default" title="%s" id="%sCrudBrowseModal"><i class="icon-search4"></i></button>'
            , $translate->t('Tallózás')
            , $this->nameToId($this->getName())
        );
    }

    /**
     * @param $id
     * @return string
     */
    protected function getCrudBrowseJs($id)
    {
        return sprintf(
            '$("#%sCrudBrowseModal").click(function(e){'
            . 'e.preventDefault();'
            . 'var crudBrowseModal = new CrudBrowseModal("%s");'
            . 'crudBrowseModal'
            . '.setCallbacks({select : %s, builderParams : %s})'
            . '.browse($(this));'
            . '});'
            , $this->nameToId($this->getName())
            , $this->getCrudBrowseAlias()
            , $this->replaceCrudCallbackVariables(
                $this->getCrudBrowseCallback()
                , [
                    '%elementName%'         => $id
                    , '%hiddenElementName%' => $this->hiddenElementName
                ]
            )
            , $this->getBuilderParams()
        );
    }

    /**
     * @return string
     */
    protected function getCrudCreateButton()
    {
        $translate = Di::getDefault()->get('translate');

        return sprintf(
            '<button class="btn btn-default" title="%s" id="%sCrudCreateModal"><i class="icon-plus3"></i></button>'
            , $translate->t('Létrehozás')
            , $this->nameToId($this->getName())
        );
    }

    /**
     * @param $id
     * @return string
     */
    protected function getCrudCreateJs($id)
    {
        return sprintf(
            '$("#%sCrudCreateModal").click(function(e){'
            . 'e.preventDefault();'
            . 'var crudCreateModal = new CrudCreateModal("%s");'
            . 'crudCreateModal'
            . '.setCallbacks({create : %s})'
            . '.form($(this));'
            . '});'
            , $this->nameToId($this->getName())
            , $this->getCrudCreateAlias()
            , $this->replaceCrudCallbackVariables(
                $this->getCrudCreateCallback()
                , [
                    '%elementName%'         => $id
                    , '%hiddenElementName%' => $this->hiddenElementName
                ]
            )
        );
    }
}