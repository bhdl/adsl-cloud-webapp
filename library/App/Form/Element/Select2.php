<?php

namespace App\Form\Element;


use App\Support\Str;

class Select2 extends Select
{
    /**
     * @var string
     */
    public $nameToId;

    /**
     * Renders the element widget
     *
     * @param array $attributes
     * @return string
     */
    public function render($attributes = null)
    {
        $html = parent::render($attributes);

        $js = '$("#' . $this->nameToId($this->getName()) . '").select2().on("select2:open", function(evt) {
            var context = $(evt.target);
            var select2 = $(this);

            $(document).on("keyup .select2", function(e) {
                if (e.which === 13) { // enter
                    var highlighted = context
                              .data("select2")
                              .$dropdown
                              .find(".select2-results__option--highlighted");
                    if (highlighted) {
                        var id = highlighted.data("data").id;
                        if (select2.attr("multiple") != "multiple") {
                            context.val(id).trigger("change");
                        } else {
                            var selectedItems = [];
                            if (select2.val()) { 
                                selectedItems = select2.val();
                            }
                            selectedItems.push(id);
                            context.val(selectedItems).trigger("change");
                        }
                    }
                }
            });
        
        });
        $(document).on("focus", ".select2", function(evt){
            if (evt.originalEvent) {
                $(this).siblings("select").select2("open");
            }
        });';

        $html .= sprintf('<script type="text/javascript">$(document).ready(function(){%s});</script>', $js);

        return $html;
    }

    /**
     * @param string $name
     * @return string
     */
    protected function nameToId($name)
    {
        if ($this->nameToId == null) {
            $this->nameToId = $name;
            if (mb_strpos($name, '[') !== false || mb_strpos($name, ']') !== false) {
                $this->nameToId = $this->getAttribute('id', Str::randomHash());
            }
        }

        return $this->nameToId;
    }
}