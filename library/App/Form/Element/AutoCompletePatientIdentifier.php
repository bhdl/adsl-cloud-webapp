<?php

namespace App\Form\Element;

use Phalcon\Exception;

/**
 * Class AutoCompletePatient
 * @package App\Form\Element
 */
class AutoCompletePatientIdentifier
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'patientId';

    /**
     * @var string
     */
    protected $source = '/admin/patient/ajax?operation=autoComplete&patientIdentifier=true';

    /**
     * @var string
     */
    protected $resourceId = 'lab.patient';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Patient';

    /**
     * @var string
     */
    protected $labelKey = 'patientId';

    /**
     * @var string
     */
    protected $valueKey = 'patientId';

    /** Páciens és Páciens azonosító közötti kiírási függőség
     * @var string
     */
    protected $selectCallback = '$("#patientName").val(ui.item.name);';

    /** Páciens és Páciens azonosító közötti kiírási függőség
     * @var string
     */
    protected $focusCallback = '$("#patientName").val(ui.item.name);';

    /** Páciens és Páciens azonosító közötti kiírási függőség
     * törléskor törlődjön az elem
     * @var string
     */
    protected $deleteValueCallback = '$("#patientName").val("");';

    /**
     * @var int
     */
    protected $minLength = 1;

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.name+ "</strong>"';
    }

}