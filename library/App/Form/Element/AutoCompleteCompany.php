<?php

namespace App\Form\Element;

/**
 * Class AutoCompleteCompany
 * @package App\Form\Element
 */
class AutoCompleteCompany
    extends AutoCompleteAbstract
{

    /**
     * @var string
     */
    protected $hiddenElementName = 'companyId';

    /**
     * @var string
     */
    protected $source = '/admin/company/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.company';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Company';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $valueKey = 'companyId';

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return 'item.name + " (<strong>" + item.companyIdentifier + ")</strong>"';
    }
}