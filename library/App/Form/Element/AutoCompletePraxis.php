<?php

namespace App\Form\Element;

use Phalcon\Exception;

/**
 * Class AutoCompletePraxis
 * @package App\Form\Element
 */
class AutoCompletePraxis
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'praxisId';

    /**
     * @var string
     */
    protected $source = '/admin/praxis/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.praxis';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Praxis';

    /**
     * @var string
     */
    protected $labelKey = 'praxisName';

    /**
     * @var string
     */
    protected $valueKey = 'praxisId';

    /**
     * @var int
     */
    protected $minLength = 3;

    /** Rendelő és orvos közötti függőség
     * @var string
     */
    protected $selectCallback = '$("#doctorName").removeAttr("readOnly");';

    /** Rendelő és orvos közötti függőség
     * @var string
     */
    protected $focusCallback = '$("#doctorName").removeAttr("readOnly");';

    /** Rendelő és orvos közötti függőség
     * Rendelő érték törlésekor az orvos korábban beállított értékei (doctorId és doctorName) törlődjön
     * @var string
     */
    protected $deleteValueCallback = '$("#doctorId").val(""); $("#doctorName").val(""); $("#doctorName").attr("readOnly","readOnly");';

    /**
     * @var bool
     */
    protected $useCrudBrowse = true;

    /**
     * @var bool
     */
    protected $useCrudCreate = true;

    /**
     * AutoCompletePraxis constructor.
     * @param $name
     * @param null $attributes
     */
    public function __construct($name, $attributes = null)
    {
        $this->crudCreateCallback = $this->crudBrowseCallback = 'function(entity){
            $(\'#%elementName%\').val(entity.name);
            $(\'#%hiddenElementName%\').val(entity.praxisId);
            if ($("#doctorName").length > 0) {
                $("#doctorName").removeAttr("readOnly");
            }
        }';

        $this->crudCreateAlias = $this->crudBrowseAlias = 'praxis';

        parent::__construct($name, $attributes);
    }
    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.praxisName+ "</strong>" + " (" + item.praxisIdentifier+ ")"';
    }


}