<?php

namespace App\Form\Element;

use Phalcon\Di;
use Phalcon\Forms\Element\Select as SelectBase;

/**
 * Class MultiSelect
 * @package App\Form\Element
 */
class MultiSelect extends SelectBase
{
    /**
     * @var \Phalcon\Assets\Manager
     */
    protected $assets;

    /**
     * @param null|array $attributes
     * @return mixed
     */
    public function render($attributes = null)
    {
        $id = $this->getAttribute('id');
        // request (get, post) értékek
        $values = $this->getForm()->request->get($id);

        if (is_array($values) && count($values) > 0) {
            // felülírja az entity-ből vett adatokat
            $this->setDefault($values);
        }

        $js = sprintf('$("#%s").select2();',
            $id
        );

        $this->getAssets()->addInlineJs(sprintf(
            '$(document).ready(function(){%s});'
            , $js
        ));

        return parent::render($attributes);
    }

    /**
     * @return \Phalcon\Assets\Manager
     */
    public function getAssets()
    {
        if ($this->assets == null) {
            $this->assets = Di::getDefault()->get('assets');
        }

        return $this->assets;
    }

    /**
     * @param \Phalcon\Assets\Manager $assets
     * @return AutoComplete
     */
    public function setAssets($assets)
    {
        $this->assets = $assets;
        return $this;
    }
}