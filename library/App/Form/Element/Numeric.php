<?php

namespace App\Form\Element;

use Phalcon\Forms\Element\Text;

/**
 * Class Numeric
 * @package App\Form\Element
 */
class Numeric
    extends Text
{
    /**
     * Renders the element widget
     *
     * @param array $attributes
     * @return string
     */
    public function render($attributes = null)
    {
        $entity = $this->getForm()->getEntity();

        // ha van entity, akkor átalakítjuk benne a lebegőpontos mezőt így az űrlapra "," kerül
        list($point) = array_values(localeconv());

        if ($entity && strcmp($point, '.') !== 0) {
            $entity->{$this->getName()}
                = str_replace('.', $point, $entity->{$this->getName()})
            ;
        }

        return parent::render($attributes);
    }
}