<?php

namespace App\Form\Element;

use Phalcon\Exception;

/**
 * Class AutoCompletePatient
 * @package App\Form\Element
 */
class AutoCompletePatient
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'patientId';

    /**
     * @var string
     */
    protected $source = '/admin/patient/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'admin.patient';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Patient';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $valueKey = 'patientId';

    /** Páciens és Páciens azonosító közötti kiírási függőség
     * @var string
     */
    protected $selectCallback = '$("#patientIdentifier").val("");';

    /** Páciens és Páciens azonosító közötti kiírási függőség
     * @var string
     */
    protected $focusCallback = '$("#patientIdentifier").val("");';

    /** Páciens és Páciens azonosító közötti kiírási függőség
     * törléskor törlődjön az elem
     * @var string
     */
    protected $deleteValueCallback = '$(\'#patientIdentifier\').val(\'\');';

    /**
     * @var bool
     */
    protected $useCrudBrowse = true;

    /**
     * @var bool
     */
    protected $useCrudCreate = true;

    /** crudBrowse-hoz paraméter
     * @var string
     */
    protected $builderParams = '{praxisId: ($("#praxisId").length > 0 ? $("#praxisId").val() : {})}';

    /**
     * AutoCompleteOwner constructor.
     * @param $name
     * @param null $attributes
     */
    public function __construct($name, $attributes = null)
    {
        $this->crudCreateCallback = $this->crudBrowseCallback = 'function(entity){
            $(\'#%elementName%\').val(entity.name);
            $(\'#%hiddenElementName%\').val(entity.patientId);
            ' . $this->deleteValueCallback . '
        }';

        $this->crudCreateAlias = $this->crudBrowseAlias = 'patient';

        parent::__construct($name, $attributes);
    }

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.name+ "</strong>"';
    }

}