<?php

namespace App\Form\Element;

use Phalcon\Di;
use Phalcon\Exception;
use Phalcon\Forms\Element;
use App\Acl;

/**
 * Class AclMatrix
 * @package App\Form\Element
 */
class AclMatrix extends Element
{
    /**
     * @var string
     */
    protected $jQueryHandler = '$';

    /**
     * @var \Phalcon\Assets\Manager
     */
    protected $assets;

    /**
     * @var array
     */
    protected $resources;

    /**
     * @var array
     */
    protected $actions;

    /**
     * @var Acl
     */
    protected $acl;

    /**
     * @var array
     */
    protected $checked;

	/**
	 * @param null|array $attributes
	 * @return mixed
	 * @throws Exception
	 */
	public function render($attributes = null)
	{
        if ($this->resources == null) {
            throw new Exception(
                'A \'resources\' megadása kötelező (setResources()).'
            );
        }

        if ($this->actions == null) {
            throw new Exception(
                'Az \'actions\' megadása kötelező (setActions()).'
            );
        }

        // js kód
        $this->getAssets()->addInlineJs(sprintf(
            '$(document).ready(function(){%s});'
            , $this->getJs()
        ));

        // html kód
        return $this->getHtml(
            $this->assembleAcl()
        );
	}

    /**
     * @return array
     */
	protected function assembleAcl()
    {
        $resources = $this->getResources()->toArray();
        $actions = $this->getActions()->toArray();

        $id = $this->getAttribute('id');

        // request (get, post) értékek
        $values = $this->getForm()->request->get($id);

        if (is_array($values) && count($values) > 0) {
            $checked = [];

            /* Összeállítjuk az action listát ami már be van pipálva*/
            foreach ($values as $resource => $actionList) {
                foreach ($actionList as $action => $value) {
                    $checked[$resource][] = $action;
                }
            }

            $this->setChecked($checked);
        }

        $isChecked = $this->getChecked();

        //translate majd utana ujra kell rendezni a tomboket
        $translate = Di::getDefault()->get('translate');

        $temp = [];
        foreach ($resources as $resource) {
            $t = $translate->_($resource["resourceName"]);
            $temp[$t] = array("resourceId" => $resource["resourceId"], "resourceName" => $t);
        }

        $resources = $temp;

        $temp = [];
        foreach ($actions as $action) {
            $t = $translate->_($action["actionName"]);
            $temp[$t] = array("actionId" => $action["actionId"], "actionName" => $t);
        }

        $actions = $temp;

        ksort($resources, SORT_LOCALE_STRING);
        ksort($actions, SORT_LOCALE_STRING);

        $acl = [];
        $group = null;
        $gi = -1;
        $ri = 0;
        foreach ($resources as $resource) {
            $part = substr($resource["resourceName"], 0,
                strpos(trim($resource["resourceName"]), ' - ') > 0 ? strpos(trim($resource["resourceName"]), ' - ') : strlen($resource["resourceName"])
            );

            if ($group != $part) {
                $group = $part;
                $gi++;

                $acl[$gi] = [
                    'groupName' => $part
                    , 'checked' => true
                    , 'resources' => []
                ];
            }

            $acl[$gi]['resources'][$ri] = array_merge(
                $resource
                , ['checked' => true]
            );

            foreach ($actions as $action) {

                if (!$this->acl->resourceHasAction(
                    $resource['resourceId']
                    , $action['actionId'])
                ) {
                    continue;
                }

                $checked = false;
                if (array_key_exists($resource['resourceId'], $isChecked)
                    && in_array($action['actionId'], $isChecked[$resource['resourceId']])
                ) {
                    $checked = true;
                } else {
                    $acl[$gi]['resources'][$ri]['checked'] = false;
                    $acl[$gi]['checked'] = false;
                }

                $acl[$gi]['resources'][$ri]['actions'][] = array_merge(
                    $action
                    , ['checked' => $checked]
                );
            }

            $ri++;
        }

        return $acl;
    }

    /**
     * @return string
     */
    protected function getJs()
    {
        $js = '';

        $js .= '$(".styled").uniform({radioClass: "choice"});';

        /* Ha bepipálja az "összes" resource-t bejelölő checkboxot */
        $js .= '$(".resourceChk").change(function(){
            var resource = $(this).data("resource");
            var group = $(this).data("group");
            var checked = $(this).is(":checked");
            var countActionUnChecked = 0;

            $(\'input[name*="aclMatrix[\' + resource + \']"]\').each(function(){
                $(this).prop("checked", checked);
            })

            $(\'.actionChk[data-group="\'+ group +\'"]\').each(function(){
                if($(this).is(":checked") == false){
                    countActionUnChecked += 1;
                }
            })

            /* Csoport checkbox bejelölése */
            $(\'.groupChk[data-group="\'+ group +\'"]\').prop("checked", true);

            /* csoport checkbox kijelölése ha van olyan action checkbox ami ki van pipálva */
            if(countActionUnChecked != 0){
                $(\'.groupChk[data-group="\'+ group +\'"]\').prop("checked", false);
            }

            $.uniform.update();
        });';

        /* Ha action checkboxot pipál be/ki */
        $js .= '$(".actionChk").change(function(){
            var checked = $(this).is(":checked");
            var countUnChecked = 0;
            var countResourceUnChecked = 0;
            var countActionUnChecked = 0;
            var resource = $(this).data("resource");
            var group = $(this).data("group");

            if(checked){

                /* Adott resource csoporton megyek végig hogy tudjuk be kell-e pipálni a csoportosító checkboxot*/
                $(\'input[name*="aclMatrix[\' + resource + \']"]\').each(function(){
                    if($(this).is(":checked") == false){
                        countResourceUnChecked += 1;
                    }
                })

                if(countResourceUnChecked == 0){
                    $("#checkAll-" + resource).prop("checked", true);
                    $.uniform.update();
                }

                $(\'.resourceChk[data-group="\'+ group +\'"]\').each(function(){
                    if($(this).is(":checked") == false){
                        countActionUnChecked += 1;
                    }
                })

                if(countActionUnChecked == 0){
                    $(\'.groupChk[data-group="\'+ group +\'"]\').prop("checked", true);
                }
            }else{
                $("#checkAll-" + resource).prop("checked", false);
                $(\'.groupChk[data-group="\'+ group +\'"]\').prop("checked", false);
            }

            $.uniform.update();
        });';

        /* Ha csoport checkboxot pipál be/ki */
        $js .= '$(".groupChk").change(function(){
            var checked = $(this).is(":checked");
            var group = $(this).data("group");

            /* resource checkbox beállítása és update trigger hogy frissüljenek az action checkboxok is*/
            $(\'.resourceChk[data-group="\'+ group +\'"]\').prop("checked", checked).trigger(\'change\');

            $.uniform.update();
        });';

        return $js;
    }

    /**
     * @param array $acl
     * @return string
     */
    protected function getHtml($acl)
    {
        $html = '<div class="panel-group content-group-lg col-xs-12" style="padding-left: 0; padding-right: 0;">';

        foreach ($acl as $gi=>$group) {

            //blokk kezdete
            $html .= '<div class="panel panel-white">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <input 
                        type="checkbox" 
                        class="styled groupChk" 
                        data-group="'. $gi .'"
                        ' . ($group['checked'] ? ' checked="checked"' : '') . '
                    >&nbsp;<a data-toggle="collapse" href="#collapse-group' . $gi . '">'. $group['groupName'] .'</a>
                </h6>
            </div>
            <div id="collapse-group' . $gi . '" class="panel-collapse collapse">
            <div class="panel-body">
            ';

            foreach ($group['resources'] as $ri=>$resource) {

                $html .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3" style="margin-bottom: 30px;"><div class="form-group pt-15">';
                $html .= '
                <div class="checkbox">
                    <label class="display-block text-semibold" style="font-weight: 500;">
                        <input 
                            type="checkbox" 
                            id="checkAll-'. $resource['resourceId'] .'" 
                            class="styled resourceChk" 
                            data-group="'. $gi .'" 
                            data-resource="' . $resource['resourceId'] . '"' . ($resource['checked'] ? ' checked="checked"' : '') . '>'. $resource['resourceName'] . '
                    </label>
                </div>
                ';

                foreach ($resource['actions'] as $action) {
                    $html .= '
                    <div class="checkbox">
                        <label>
                            <input 
                                type="checkbox" 
                                class="styled actionChk" 
                                data-group="'. $gi .'" 
                                data-resource="'. $resource["resourceId"]  .'" 
                                name="aclMatrix['. $resource['resourceId'] .']['. $action['actionId'] .']"' . ($action['checked'] ? ' checked="checked"' : '') . '>' . $action['actionName'] .'
                        </label>
                    </div>
                    ';
                }

                $html .= '</div></div>';
            }

            $html   .= '</div></div></div>';
        }

        $html .= '</div>';

        return $html;
    }

    /**
     * @return string
     */
    public function getJQueryHandler()
    {
        return $this->jQueryHandler;
    }

    /**
     * @param string $jQueryHandler
     */
    public function setJQueryHandler($jQueryHandler)
    {
        $this->jQueryHandler = $jQueryHandler;
    }

    /**
     * @return \Phalcon\Assets\Manager
     */
    public function getAssets()
    {
        if ($this->assets == null) {
            $this->assets = Di::getDefault()->get('assets');
        }

        return $this->assets;
    }

    /**
     * @param \Phalcon\Assets\Manager $assets
     * @return AutoComplete
     */
    public function setAssets($assets)
    {
        $this->assets = $assets;
        return $this;
    }

    /**
     * @return array
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * @param array $resources
     * @return AclMatrix
     */
    public function setResources($resources)
    {
        $this->resources = $resources;
        return $this;
    }

    /**
     * @return array
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param array $actions
     * @return AclMatrix
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
        return $this;
    }

    /**
     * @return array
     */
    public function getChecked()
    {
        return $this->checked;
    }

    /**
     * @param array $checked
     * @return AclMatrix
     */
    public function setChecked($checked)
    {
        $this->checked = $checked;
        return $this;
    }

    /**
     * @return Acl
     */
    public function getAcl(): Acl
    {
        return $this->acl;
    }

    /**
     * @param Acl $acl
     * @return AclMatrix
     */
    public function setAcl(Acl $acl): AclMatrix
    {
        $this->acl = $acl;
        return $this;
    }
}