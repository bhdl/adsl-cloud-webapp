<?php

namespace App\Form\Element;

use Phalcon\Exception;

/**
 * Class AutoCompleteDoctor
 * @package App\Form\Element
 */
class AutoCompleteDoctor
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'doctorId';

    /**
     * @var string
     */
    protected $source = '/admin/doctor/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'admin.doctor';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Doctor';

    /**
     * @var string
     */
    protected $labelKey = 'doctorName';

    /**
     * @var string
     */
    protected $valueKey = 'doctorId';

    /**
     * Rendelő és orvos közötti függőség
     * @var string
     */
    protected $requestCallback = 'request.praxisId = $("#praxisId").val();';

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.doctorName+ "</strong>"';
    }

}