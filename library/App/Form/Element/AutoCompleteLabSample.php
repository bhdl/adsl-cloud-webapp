<?php

namespace App\Form\Element;

/**
 * Class AutoCompleteLabSample
 * @package App\Form\Element
 */
class AutoCompleteLabSample
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'labSampleId';

    /**
     * @var string
     */
    protected $source = '/admin/lab-sample/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.labSample';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\LabSample';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $valueKey = 'labSampleId';

    /**
     * @var int
     */
    protected $minLength = 2;

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.name + " </strong>"';
    }
}