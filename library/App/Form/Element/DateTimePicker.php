<?php

namespace App\Form\Element;

use App\Model\Language;
use App\Support\Date;


/**
 * Class DateTimePicker
 * @package App\Form\Element
 */
class DateTimePicker
    extends DatePicker
{
    /**
     * @param null $attributes
     * @return string
     */
    public function render($attributes = null): string
    {
        // attribútum konverzió
        $attributes['id'] = $this->nameToId($this->getName());

        // inline javascript
        $js = '<script type="text/javascript">';
        $js .= '$(document).ready(function(){';

        // datepicker paraméterek
        $js .= sprintf(
            '$("#%s").datetimepicker(%s);'
            , $attributes['id']
            , json_encode($this->getParams(), JSON_NUMERIC_CHECK)
        );

        /*
         * Alapértelmezett érték beállítása, használva a felület i18n beállításait
         */
        $value = $this->getValue();
        if (!$value) {
            $value = $this->getAttribute('value');
        }

        if ($value) {
            $v = Date::dateTimeFromLocalized($value);
            if ($v) {
                $js .= sprintf(
                    '$("#%s").datetimepicker("setDate", new Date(\'' . $v->format('Y-m-d H:i') . '\'));'
                    , $attributes['id']
                );
            }
        }

        /*
         * ENTER event
         */
        $js .= sprintf(
            '$("#%s").datetimepicker().keydown(function(event) {
                if (event.which === $.ui.keyCode.ENTER) {
                    $(this).trigger("change");
                    event.preventDefault();
                }
            });'
            , $attributes['id']
        );

        /**
         * @var Language $language
         */
        $language = $this->getInterfaceLanguage();

        /*
         * Input mask
         */
        $js .= sprintf(
            '$("#%s").formatter({pattern: "%s"});'
            , $attributes['id']
            , $language->dateInputMask.' '.$language->timeInputMask
        );

        $js .= '});';
        $js .= '</script>';

        $js .= parent::render($attributes);

        return $js;
    }
}