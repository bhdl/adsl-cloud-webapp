<?php

namespace App\Form\Element;

/**
 * Class AutoCompleteBreed
 * @package App\Form\Element
 */
class AutoCompleteBreed
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'breedId';

    /**
     * @var string
     */
    protected $source = '/admin/breed/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.breed';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Breed';

    /**
     * @var string
     */
    protected $labelKey = 'breedName';

    /**
     * @var string
     */
    protected $valueKey = 'breedId';

    /**
     * Faj és fajta közötti függőség
     * @var string
     */
    protected $requestCallback = 'request.speciesId = $("#speciesId").val();';

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.breedName + " </strong>" + " (" + item.breedIdentifier + ")"';
    }
}