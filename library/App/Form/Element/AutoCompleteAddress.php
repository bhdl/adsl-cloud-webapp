<?php

namespace App\Form\Element;

/**
 * Class AutoCompleteAddress
 * @package App\Form\Element
 */
class AutoCompleteAddress
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'addressId';

    /**
     * @var string
     */
    protected $source = '/admin/address/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.address';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\Address';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $valueKey = 'addressId';

    /**
     * Cím és tulajdonos közötti függőség
     * @var string
     */
    protected $requestCallback = 'request.ownerId = $("#ownerId").val();';

    /**
     * @var bool
     */
    protected $useCrudBrowse = true;

    /**
     * @var bool
     */
    protected $useCrudCreate = true;

    /** crudBrowse-hoz paraméter
     * @var string
     */
    protected $builderParams = '{ownerId: ($("#ownerId").length > 0 ? $("#ownerId").val() : {})}';


    /**
     * AutoCompleteAddress constructor.
     * @param $name
     * @param null $attributes
     */
    public function __construct($name, $attributes = null)
    {
        $this->crudCreateCallback = $this->crudBrowseCallback = 'function(entity){
            $(\'#%elementName%\').val(entity.name);
            $(\'#%hiddenElementName%\').val(entity.addressId);
        }';

        $this->crudCreateAlias = $this->crudBrowseAlias = 'address';

        parent::__construct($name, $attributes);
    }

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.name + " </strong>"';
    }

}