<?php

namespace App\Form\Element;

use App\Model\Language;
use App\Support\Date;
use App\Support\Str;
use App\Traits\InterfaceLanguage;
use Phalcon\Forms\Element;

/**
 * Class DatePicker
 * @package App\Form\Element
 */
class DatePicker
    extends Element\Text
{
    use InterfaceLanguage;

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @var string
     */
    protected $nameToId;

    /**
     * @param null $attributes
     * @return string
     */
    public function render($attributes = null): string
    {
        // attribútum konverzió
        $attributes['id'] = $this->nameToId($this->getName());

        // inline javascript
        $js = '<script type="text/javascript">';
        $js .= '$(document).ready(function(){';

        // datepicker paraméterek
        $js .= sprintf(
            '$("#%s").datepicker(%s);'
            , $attributes['id']
            , json_encode($this->getParams(), JSON_NUMERIC_CHECK)
        );

        /*
         * Alapértelmezett érték beállítása, használva a felület i18n beállításait
         */
        $value = $this->getValue();
        if (!$value) {
            $value = $this->getAttribute('value');
        }

        if ($value) {
            $v = Date::dateFromLocalized($value);
            if ($v) {
                $js .= sprintf(
                    '$("#%s").datepicker("setDate", new Date(\'' . $v->format('Y-m-d') . '\'));'
                    , $attributes['id']
                );
            }
        }

        /*
         * ENTER event
         */
        $js .= sprintf(
            '$("#%s").datepicker().keydown(function(event) {
                if (event.which === $.ui.keyCode.ENTER) {
                    $(this).trigger("change");
                    event.preventDefault();
                }
            });'
            , $attributes['id']
        );

        /**
         * @var Language $language
         */
        $language = $this->getInterfaceLanguage();

        /*
         * Input mask
         */
        $js .= sprintf(
            '$("#%s").formatter({pattern: "%s"});'
            , $attributes['id']
            , $language->dateInputMask
        );

        $js .= '});';
        $js .= '</script>';

        $js .= parent::render($attributes);

        return $js;
    }

    /**
     * @param string $name
     * @return string
     */
    protected function nameToId($name)
    {
        if ($this->nameToId == null) {
            $this->nameToId = $name;
            if (mb_strpos($name, '[') !== false || mb_strpos($name, ']') !== false) {
                $this->nameToId = Str::randomHash();
            }
        }

        return $this->nameToId;
    }

    /**
     * @param string $key
     * @param string $value
     * @return DatePicker
     */
    public function setParam($key, $value)
    {
        $this->params[$key] = $value;

        return $this;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getParam($key)
    {
        return $this->params[$key];
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     * @return DatePicker
     */
    public function setParams(array $params): DatePicker
    {
        $this->params = $params;
        return $this;
    }
}