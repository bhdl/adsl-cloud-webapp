<?php

namespace App\Form\Element;

use Phalcon\Exception;

/**
 * Class AutoCompleteMedicalRecordGroup
 * @package App\Form\Element
 */
class AutoCompleteMedicalRecordGroup
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'medicalRecordGroupId';

    /**
     * @var string
     */
    protected $source = '/admin/medical-record-group/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.medicalRecordGroup';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\MedicalRecordGroup';

    /**
     * @var string
     */
    protected $labelKey = 'name';

    /**
     * @var string
     */
    protected $valueKey = 'medicalRecordGroupId';

    /**
     * @var bool
     */
    protected $useCrudBrowse = true;

    /**
     * @var string
     */
    protected $crudBrowseAlias = 'medical-record-group';

    /**
     * @var string
     */
    protected $crudBrowseCallback = 'function(entity){
        $(\'#%elementName%\').val(entity.name );
        $(\'#%hiddenElementName%\').val(entity.medicalRecordGroupId);
    }';

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.name+ "</strong>"';
    }
}