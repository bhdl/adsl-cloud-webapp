<?php

namespace App\Form\Element;

/**
 * Class AutoCompleteAnimalGroup
 * @package App\Form\Element
 */
class AutoCompleteAnimalGroup
    extends AutoCompleteAbstract
{
    /**
     * @var string
     */
    protected $hiddenElementName = 'animalGroupId';

    /**
     * @var string
     */
    protected $source = '/admin/animal-group/ajax?operation=autoComplete';

    /**
     * @var string
     */
    protected $resourceId = 'lab.animalGroup';

    /**
     * @var string
     */
    protected $modelClass = 'App\\Model\\AnimalGroup';

    /**
     * @var string
     */
    protected $labelKey = 'animalGroupName';

    /**
     * @var string
     */
    protected $valueKey = 'animalGroupId';

    /**
     * @var int
     */
    protected $minLength = 2;

    /**
     * @return string
     */
    protected function getRenderItemCallback()
    {
        return '"<strong>" + item.animalGroupName + " </strong>"';
    }
}