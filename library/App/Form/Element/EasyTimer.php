<?php

namespace App\Form\Element;

use Phalcon\Forms\Element;

/**
 * Class EasyTimer
 * @package App\Form\Element
 */
class EasyTimer
    extends Element
{
    /**
     * 
     */
    public function render($attributes = null)
    {
        $html .= '<div id="timer" class="text-center"></div>';

        return $html;
    }
}