<?php

namespace App\Queue;

use App\Model\Job;
use Phalcon\Exception;

/**
 * Class JobFactory
 * @package App\Queue
 */
final class JobFactory
{
    /**
     * @param \Phalcon\Queue\Beanstalk\Job $job
     * @return Job
     * @throws Exception
     */
    public static function forRun(\Phalcon\Queue\Beanstalk\Job $job)
    {
        $jobId = $job->getBody();

        if (!is_numeric($jobId)) {
            throw new Exception(
                'A jobId nem szám típusú érték.'
            );
        }

        $job = Job::findFirst([
            'jobId = ' . $jobId
            , 'rowLevelAclEnabled' => false
        ]);

        if (!$job) {
            throw new Exception(
                'Nem található az alábbi azonosítóval a feladat: ' . $jobId
            );
        }

        return $job;
    }

    /**
     * @param $jobType
     * @param array $data
     * @return Job
     * @throws Exception
     */
    public static function forPut($jobType, array $data = [])
    {
        $job = new Job();
        $job->jobType = $jobType;
        $job->create();

        $job->createRelated($jobType, $data);

        return $job;
    }
}