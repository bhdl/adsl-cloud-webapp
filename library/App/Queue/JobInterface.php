<?php

namespace App\Queue;

/**
 * Class JobAbstract
 * @package App\Queue
 */
interface JobInterface
{
    /**
     * @return void
     */
    public function run();

    /**
     * @return void
     */
    public function put();
}