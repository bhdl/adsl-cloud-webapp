<?php

namespace App\Event\Listener;

use App\Controller\AbstractCrudController;
use App\Controller\HotKeys\Parser;
use App\Model\User;
use App\Support\HashMap;
use Phalcon\Dispatcher;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Component;
use Phalcon\Exception;

/**
 * Class HotKeys
 * @package App\Event\Listener
 */
class HotKeys
    extends Component
{
    /**
     * @var array
     */
    protected $callbacks = [];

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @throws Exception
     */
    public function afterInitialize(Event $event, Dispatcher $dispatcher)
    {
        /**
         * @var User $user
         */
        $user = $this->auth->getIdentity();

        if ($user->isGuest()) {
            // vendégnél nem foglalkozunk a billentyűkombinációkkal
            return;
        }

        // controller 'setup' kiolvasása, feldolgozása
        $controller = $dispatcher->getActiveController();
        if ($controller instanceof AbstractCrudController) {
            $this->addHotKeyCallbacksFromController($controller);
        }

        // HotKey lib&wrapper
        $this->assets
            ->addJs('/assets/js/plugins/extensions/jquery.hotkeys.js')
            ->addJs('/assets/js/app/HotKeys.js')
        ;
    }

    /**
     * JS proxy: javascript callback hozzádása
     *
     * @param string $hotKey
     * @param string $description
     * @param string $callback
     * @return HotKeys
     */
    public function addHotKeyCallback($hotKey, $description, $callback)
    {
        $this->callbacks[] = sprintf(
            'HotKeys.addHotKeyCallback("%s", "%s", %s);'
            , $hotKey
            , $description
            , $callback
        );

        return $this;
    }

    /**
     * JS proxy: URL callback hozzáadása
     *
     * @param string $hotKey
     * @param string $description
     * @param string $url
     * @return HotKeys
     */
    public function addHotKeyCallbackUrl($hotKey, $description, $url)
    {
        $this->callbacks[] = sprintf(
            'HotKeys.addHotKeyCallbackUrl("%s", "%s", "%s");'
            , $hotKey
            , $description
            , $url
        );

        return $this;
    }

    /**
     * @param AbstractCrudController $controller
     * @return HotKeys
     * @throws Exception
     */
    protected function addHotKeyCallbacksFromController(AbstractCrudController $controller)
    {
        $setup = $controller->getSetup();

        if (array_key_exists('hotKeys', $setup)) {

            foreach ($setup['hotKeys'] as $hotKey=>$options) {

                if (!array_key_exists('callback', $options)
                    || !array_key_exists('description', $options)
                ) {
                    throw new Exception(
                        'HotKeys setup hiba: a callback/ description kitöltése hiányos.'
                    );
                }

                // URL or JS
                $method = mb_substr($options['callback'], 0, 1) === '/' ?
                    'addHotKeyCallbackUrl'
                    : 'addHotKeyCallback'
                ;

                // callback hozzáadása
                $this->$method(
                    $hotKey
                    , $options['description']
                    , $options['callback']
                );
            }
        }

        return $this;
    }

    /**
     * A dispatch loop végén hozzáadjuk az onload szkripteket
     *
     * @param Event $event
     * @param Dispatcher $dispatcher
     */
    public function afterDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        if (HashMap::isFilled($this->callbacks)) {

            // egyben hozzáadjuk az összes elemet
            $this->assets->addInlineJs(
                sprintf(
                    '$(document).ready(function(){%s});'
                    , implode('', $this->callbacks)
                )
            );

            // hozzáadjuk a headingButtons végére a Gyorsbillentyűk
            $this->headingButtons->add(
                $this->translate->_('Billentyű kombinációk')
                , 'javascript:HotKeys.hint();'
                , 'icon-keyboard'
            );
        }
    }
}