<?php

namespace App\Event\Listener;

use Phalcon\Dispatcher;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Component;

/**
 * Class Profile
 * @package App\Event\Listener
 */
class Profile
    extends Component
{
    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        /**
         * @var User $user
         */
        $user = $this->auth->getIdentity();

        // FIXME: állítható legyen
        $this->response->setHeader('X-XSS-Protection', 0);

        if ($user->isGuest() || $user->dataTableResponsive == null) {
            return;
        }

        // alapértelmezett datatable beállítások felülbírálata (assets/js/app/init/datatables.js)
        $inlineJs = '
            $(function () {
                $.extend( $.fn.dataTable.defaults, {
                    responsive : ' . ($user->isDataTableResponsive() ? 'true' : 'false') . '
                    , scrollX  : ' . ($user->isDataTableResponsive() ? 'false' : 'true') . ' 
                });
            });
        ';

        $this->assets
            ->addInlineJs($inlineJs)
        ;
    }
}