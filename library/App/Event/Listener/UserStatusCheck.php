<?php

namespace App\Event\Listener;

use App\Model\User;
use Phalcon\Dispatcher;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Component;

/**
 * Class IdleTimeout
 */
class UserStatusCheck
    extends Component
{
    /**
     * Ide irányítjuk át, ha nem jók a flagek
     *
     * @var string
     */
    protected $redirectUrl = '/admin/auth';

    /**
     * Felhasználó flagek ellenőrzése élő session esetén
     *
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        /**
         * @var User $user
         */
        $user = $this->auth->getIdentity();

        if ($user->isGuest()) {
            return true;
        }

        $messages = $user->checkStatus();
        if (count($messages) == 0) {
            // ok
            return true;
        }

        $messagesStr = implode(' ', $messages);

        $this->logger->error($messagesStr);

        $this->flashSession->error($messagesStr);

        $this->auth->logout();

        $this->response->redirect($this->redirectUrl);

        $event->stop();

        return false;
    }

    /**
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }

    /**
     * @param string $redirectUrl
     * @return UserStatusCheck
     */
    public function setRedirectUrl(string $redirectUrl): UserStatusCheck
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }
}