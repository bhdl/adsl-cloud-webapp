<?php

namespace App\Event\Listener;

use App\Model\User;
use Phalcon\Dispatcher;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Component;

/**
 * Class UserPasswordOutdated
 */
class UserPasswordOutdated
    extends Component
{
    /**
     * Elavult jelszó oldalátirányítás url-je
     * @var string
     */
    protected $redirectUrl = '/admin/password/outdated';

    /**
     * @var string
     */
    protected $loginUrl = '/admin/auth';

    /**
     * Felhasználó jelszó lejárat vizsgálata
     *
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        /**
         * @var User $user
         */
        $user = $this->auth->getIdentity();

        // vendég felhasználóval nem törődünk
        if ($user->isGuest()) {
            return true;
        }

        // épp a jelszó módosító oldalon vagyunk
        if (strcmp($this->request->getURI(), $this->redirectUrl) === 0) {
            return true;
        }

        // login oldalon vagyunk
        if (strcmp($this->request->getURI(), $this->loginUrl) === 0) {
            return true;
        }

        /*
         * Elavult jelszó esetén a jelszó módosítás űrlapra írányítjuk a felhasználót
         */
        if($user->isPasswordOutdated()) {
            $this->response->redirect(
                $this->redirectUrl
            );

            $event->stop();

            return false;
        }
    }

    /**
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }

    /**
     * @param string $redirectUrl
     * @return UserPasswordOutdated
     */
    public function setRedirectUrl(string $redirectUrl): UserPasswordOutdated
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getLoginUrl(): string
    {
        return $this->loginUrl;
    }

    /**
     * @param string $loginUrl
     * @return UserPasswordOutdated
     */
    public function setLoginUrl(string $loginUrl): UserPasswordOutdated
    {
        $this->loginUrl = $loginUrl;
        return $this;
    }
}