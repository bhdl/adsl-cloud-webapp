<?php

/**
 * Config file Example:
 *  'browser' => array(
 *      'desktop'   => array(
 *          'Chrome'            => 40,
 *          'Firefox'           => 40,
 *          'Internet Explorer' => 10,
 *          'Safari'            => 9
 *      ),
 *      'mobile'    => array(
 *          'Safari'    => 11,
 *          'Chrome'    => 20
 *      )
 *  )
 */

namespace App\Event\Listener;

use Phalcon\Exception;
use Phalcon\Mvc\User\Component;
use Phalcon\Events\Event;
use Phalcon\Dispatcher;

class Browser extends Component
{
    /**+
     * @var \WhichBrowser\Parser
     */
    private $browser;

    /**
     * @var string
     */
    private $browserViewUrl;

    /**
     * Browser constructor.
     * @param string $browserViewUrl
     */
    public function __construct($browserViewUrl = '')
    {
        $this->setBrowser(new \WhichBrowser\Parser($this->request->getUserAgent()));

        $this->setBrowserViewUrl($browserViewUrl);
    }

    /**
     * Event listener
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        if(isset($this->config->browser) &&
            $this->request->getURI() != $this->getBrowserViewUrl()) {

            if(empty($this->getBrowserViewUrl())) {
                throw new Exception('A böngésző verzió tájékoztató oldal url-t kötelező megadni!');
            }

            $browserAvailable = $this->config->browser->toArray();
            $browserType = $this->getBrowser()->getType();
            $browserName = $this->getBrowser()->browser->getName();
            $browserVersion = $this->getBrowser()->browser->getVersion();

            if(!isset($browserAvailable[$browserType][$browserName])
                || version_compare($browserAvailable[$browserType][$browserName], $browserVersion, '>')) {
                $this->response->redirect($this->getBrowserViewUrl());
                $event->stop();
                return false;
            }
        }

        return true;
    }

    /**
     * @return \WhichBrowser\Parser
     */
    public function getBrowser()
    {
        return $this->browser;
    }

    /**
     * @return string
     */
    public function getBrowserViewUrl()
    {
        return $this->browserViewUrl;
    }

    /**
     * @param \WhichBrowser\Parser $browser
     */
    public function setBrowser(\WhichBrowser\Parser $browser)
    {
        $this->browser = $browser;
    }

    /**
     * @param string $browserViewUrl
     */
    public function setBrowserViewUrl($browserViewUrl)
    {
        $this->browserViewUrl = $browserViewUrl;
    }
}