<?php

namespace App\Event\Listener;

use App\Model\AbstractModel;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Component;

/**
 * Class NavigationHistory
 * @package App\Event\Listener
 */
class NavigationHistory
    extends Component
{
    /**
     * @var array
     */
    protected $listen = [
        'UserController' => [
            'type'      => \App\Model\NavigationHistory::TYPE_USER
            , 'actions' => [
                \App\Model\AclAction::ACTION_MODIFY
            ]
            , 'action' => \App\Model\AclAction::ACTION_MODIFY
        ]
    ];

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function afterDispatch(Event $event, Dispatcher $dispatcher)
    {
        $controllerClass = $dispatcher->getControllerClass();
        if (!array_key_exists($controllerClass, $this->listen)) {
            return true;
        }

        $action = $dispatcher->getActionName();
        if (!in_array($action, $this->listen[$controllerClass]['actions'])) {
            return true;
        }

        $controller = $dispatcher->getActiveController();
        $model      = $controller->getModel();

        if ($model instanceof AbstractModel) {
            $this->navigationHistory->push(
                $this->listen[$controllerClass]['type']
                , $controller->url(
                    $this->listen[$controllerClass]['action']
                    , null
                    , null
                    , $model->getPrimaryKey()
                )
                , $model->getName()
            );
        }

        return true;
    }
}