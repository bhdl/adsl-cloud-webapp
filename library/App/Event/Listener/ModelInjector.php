<?php

namespace App\Event\Listener;

use App\Controller\AbstractCrudController;
use App\Model\AclAction;
use Phalcon\Events\Event;
use Phalcon\Exception;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Component;

/**
 * Class ModelInjector
 * @package App\Event\Listener
 */
class ModelInjector
    extends Component
{
    /**
     * Ezeknél az action-öknél figyeli a requestet
     *
     * @var array
     */
    protected $actions = [
        AclAction::ACTION_DELETE
        , AclAction::ACTION_MODIFY
        , AclAction::ACTION_PRINT
        , AclAction::ACTION_PREVIEW
        , AclAction::ACTION_RESTORE
        , AclAction::ACTION_SEND
        , AclAction::ACTION_VIEW
        , AclAction::ACTION_READ
        , AclAction::ACTION_TRANSLATE
        , AclAction::ACTION_POSITION
        , AclAction::ACTION_DOCUMENT
        , AclAction::ACTION_CONVERT
        , AclAction::ACTION_TRANSACTION
        , AclAction::ACTION_DOWNLOAD

        // crud-hasMany
        , AclAction::ACTION_INDEX_HAS_MANY
        , AclAction::ACTION_DATA_TABLE_HAS_MANY
        , AclAction::ACTION_CREATE_HAS_MANY
        , AclAction::ACTION_MODIFY_HAS_MANY
        , AclAction::ACTION_DELETE_HAS_MANY
        , AclAction::ACTION_VIEW_HAS_MANY

        // crud-hasManyToMany
        , AclAction::ACTION_INDEX_HAS_MANY_TO_MANY
        , AclAction::ACTION_DATA_TABLE_HAS_MANY_TO_MANY
        , AclAction::ACTION_CREATE_HAS_MANY_TO_MANY
        , AclAction::ACTION_MODIFY_HAS_MANY_TO_MANY
        , AclAction::ACTION_DELETE_HAS_MANY_TO_MANY
        , AclAction::ACTION_VIEW_HAS_MANY_TO_MANY
        , AclAction::ACTION_ASSIGN_HAS_MANY_TO_MANY
    ];

    /**
     * Model automatikus hozzákapcsolása a controllerhez
     *
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function afterInitialize(Event $event, Dispatcher $dispatcher)
    {
        $controller = $dispatcher->getActiveController();

        if (
            !($controller instanceof AbstractCrudController)
            || !in_array($dispatcher->getActionName(), $this->actions)
        ) {
            return true;
        }

        $params = $dispatcher->getParams();

        $model = null;

        try {

            if (!is_array($params) || !isset($params[0])) {
                throw new Exception('Nincs megadva a művelethez szükséges paraméter.');
            }

            $id = $params[0];

            // inject entity
            $controller->getModel($id);

        } catch (Exception $e) {
            $this->logger->warning($e->getMessage());

            $this->flashSession->error($e->getMessage());

            $controller->redirect(
                AclAction::ACTION_INDEX
            );

            $event->stop();

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getActions(): array
    {
        return $this->actions;
    }

    /**
     * @param array $actions
     * @return ModelInjector
     */
    public function setActions(array $actions): ModelInjector
    {
        $this->actions = $actions;
        return $this;
    }
}