<?php

namespace App\Event\Listener;

use Phalcon\Events\Event;
use Phalcon\Exception;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Component;
use App\Debug;
use App\Model\User;

/**
 * Class Acl
 * @package App\Event\Listener
 */
class Acl extends Component
{
	/**
	 * A megtekinteni kívánt de védett oldal URL-je. A bejelentkező
	 * felület feladata, hogy ide irányítsa vissza a böngészőt a beléptetés után.
	 *
	 * @var string
	 */
	protected $redirectUrl;

	/**
	 * Bejelentkezési felület URL
	 * @var string
	 */
	protected $loginUrl = '/admin/auth';

    /**
     * Használjon-e a komponens flash üzeneteket (belépétetés kényszerítés, jogosultsági hiba)?
     * @var bool
     */
    protected $useFlash = true;

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     * @throws Exception
     */
	public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
	{
        if (!$this->acl->isAllowedByIdentity($this->getResourceId(), $this->getActionId())) {

            $event->stop();

            return $this->handleNoAccess(
                $this->getResourceId()
                , $this->getActionId()
            );
        }

        return true;
	}

    /**
     * @param string $resourceId
     * @param string $actionId
     * @return bool
     * @throws Exception
     */
    protected function handleNoAccess($resourceId, $actionId)
	{
        /*
         * json fájlon vagyunk (ilyenkor nincs átirányítás) VAGY
         * be van lépve, de nincs joga az oldal betöltésére
         */
		if (!$this->auth->getIdentity()->isGuest()) {

            $this->logger->warning(sprintf(
                'Hozzáférés megtagadva (resourceId: %s, actionId: %s)'
                , $resourceId
                , $actionId
            ));

            $message = 'Nincs jogosultsága az oldal megtekintéséhez.';

            if ($this->isUseFlash()) {
                $this->flashSession->error($message);
                $this->response->redirect('/admin');
                return false;
            }

			// a kivétel megy a hibakezelőnek
			throw new Exception(
				$message
            );
		}

		// nincs belépve
        return $this->forceLogin();
	}

	/**
	 * Belépés kikényszerítése a dispatch loop újraindításával
	 */
	public function forceLogin()
	{
        if ($this->isUseFlash()) {
            $this->flashSession->error('Kérjük, jelentkezzen be az oldal megtekintéséhez.');
        }

        // visszatérési paraméterek, ha még nem lettek beállítva
        //TODO visszatenni
        if ($this->redirectUrl == null) {
            $this->redirectUrl = $this->autoDetectRedirectUrl();
        }

        // redirect login
        if(!$this->request->get('redirectUrl')) {
            $this->response->redirect(
                $this->loginUrl . '?redirectUrl=' . base64_encode($this->redirectUrl)
            );
        }

        return false;
	}

    /**
     * @return string
     */
	public function autoDetectRedirectUrl()
	{
        $url = $this->request->getURI();

        /*$query = $this->request->getQuery();

        $queryString = '';

        if (is_array($query) && count($query) > 0) {

            // phalcon .htaccess hack
            if (array_key_exists('_url', $query)) {
                unset($query['_url']);
            }

            $queryString = http_build_query($query);
        }

        $url .= mb_strlen($queryString) > 0 ? '?' . $queryString : '';*/

        return $url;
	}

    /**
     * @return string
     */
    public function getResourceId()
    {
        $resourceId = $this->getModule();

        if (!empty($resourceId)) {
            $resourceId .= '.';
        }

        $resourceId .= $this->getController();

        return $resourceId;
    }

    /**
     * @return string
     */
    public function getActionId()
    {
        return $this->getAction();
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->dispatcher->getModuleName() ?: '';
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->dispatcher->getControllerName() ?: 'index';
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->dispatcher->getActionName() ?: 'index';
    }

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param string $redirectUrl
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @return string
     */
    public function getLoginUrl()
    {
        return $this->loginUrl;
    }

    /**
     * @param string $loginUrl
     */
    public function setLoginUrl($loginUrl)
    {
        $this->loginUrl = $loginUrl;
    }

    /**
     * @return boolean
     */
    public function isUseFlash()
    {
        return $this->useFlash;
    }

    /**
     * @param boolean $useFlash
     */
    public function setUseFlash($useFlash)
    {
        $this->useFlash = $useFlash;
    }
}