<?php

namespace App\Event\Listener;

use App\Model\User;
use Phalcon\Dispatcher;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Component;

/**
 * Class KeepAlive
 * @package App\Event\Listener
 */
class KeepAlive
    extends Component
{
    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        /**
         * @var User $user
         */
        $user = $this->auth->getIdentity();

        if ($user->isGuest()) {
            return;
        }

        // keep alive onload
        $inlineJs = '$.fn.keepAlive(';
        $inlineJs .= json_encode([
            'url'     => '/admin/index/ajax?operation=keepAlive'
            , 'timer' => 5 * 60 * 1000
        ]);
        $inlineJs .= ');';

        $this->assets
            ->addJs('/assets/js/plugins/extensions/jQuery.keepAlive.js')
            ->addInlineJs($inlineJs)
        ;
    }
}