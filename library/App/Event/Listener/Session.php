<?php

namespace App\Event\Listener;

use Phalcon\Exception;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Component;
use Phalcon\Events\Event;
use Phalcon\Mvc\Application;

/**
 * Class Session
 * @package App\Event\Listener
 *
 * @property \Phalcon\Logger\Multiple $logger
 */
class Session extends Component
{
    /**
     * @var string
     */
    private $unauthorizedViewUrl;

    /**
     * Session constructor.
     * @param string $unauthorizedViewUrl
     */
    public function __construct($unauthorizedViewUrl = '')
    {
        $this->setUnauthorizedViewUrl($unauthorizedViewUrl);
    }

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     * @throws Exception
     */
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        if (empty($this->getUnauthorizedViewUrl())) {
            throw new Exception('A(z) "unauthorizedViewUrl" beállítása kötelező!');
        }

        $session = $this->session;
        $user    = $this->auth->getIdentity();

        if ($session->isStarted() && !$user->isGuest()) {
            if ($session->has('userIp')) {
                if ($session->get('userIp') != $this->request->getClientAddress()) {
                    // user ip remove
                    $session->remove('userIp');
                    // üzenet
                    $message = 'A munkamenetben IP cím változás történt.';
                    $this->logger->error($message);
                    $this->flashSession->error($message);
                    // kiléptetés
                    $this->auth->logout();
                    // stop event
                    $event->stop();
                    // redirect
                    $this->response->redirect($this->getUnauthorizedViewUrl());
                    // dispatch loop megszakad
                    return false;
                }
            } else {
                $session->set('userIp', $this->request->getClientAddress());
            }
        }
    }

    /**
     * @return string
     */
    public function getUnauthorizedViewUrl()
    {
        return $this->unauthorizedViewUrl;
    }

    /**
     * @param string $unauthorizedViewUrl
     */
    public function setUnauthorizedViewUrl($unauthorizedViewUrl)
    {
        $this->unauthorizedViewUrl = $unauthorizedViewUrl;
    }
}
