<?php

namespace App\Event\Listener;

use Phalcon\Events\Event;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\User\Component;

/**
 * Class Profiler
 * @package App\Event\Listener
 */
class Profiler extends Component
{
	/**
	 * Mutassa-e a futtatott SQL parancsokat (összeset)
	 * @var boolean
	 */
	protected $showQueryProfiles = false;

    /**
     * AJAX jellegű requesteket hagyja figyelmen kívül (pl. keep-alive)
     * @var bool
     */
    protected $ignoreAjaxRequests = false;

    /**
     * @var float
     */
    protected $microTimeOnBoot;

	/**
	 * @param Event $event
	 * @param Application $application
	 */
    public function boot(Event $event, Application $application)
    {
        $this->microTimeOnBoot = microtime(true);
    }

	/**
	 * @param Event $event
	 * @param Application $application
	 */
	public function afterHandleRequest(Event $event, Application $application)
	{
	    if ($this->ignoreAjaxRequests
            && $this->request->isAjax()) {
	        return true;
        }

        $line = '';

        $line .= "*****************" . PHP_EOL;
        $line .= "SQL/ PHP profiler" . PHP_EOL;
        $line .= "*****************" . PHP_EOL;

        $line .= "Request: "
            . $this->request->getScheme()
            . '://'
            . $this->request->getHttpHost()
            . $this->request->getURI()
            . PHP_EOL
        ;

        $line .= "SQL lekérdezés szám: "
            . $this->profiler->getNumberTotalStatements() . PHP_EOL
        ;

		$line .= "SQL lekérdezés idő: "
            . sprintf('%.5f sec', $this->profiler->getTotalElapsedSeconds()) . PHP_EOL
        ;

        // TODO: táblázatos formában megjeleníteni

		if ($this->showQueryProfiles) {

            $line .= "SQL profilok" . PHP_EOL;

			$i = 1;

            /**
             * @var \Phalcon\Db\Profiler\Item $profile
             */
			foreach ($this->profiler->getProfiles() as $profile) {
                $line .= "--------" . PHP_EOL;
                $line .= "Sorszám: " . ($i++) . PHP_EOL;
                $line .= "Idő: " . sprintf('%.5f sec', $profile->getTotalElapsedSeconds()) . PHP_EOL;
                $line .= "Parancs:" . PHP_EOL . $profile->getSqlStatement() . PHP_EOL;
                $line .= "Változók:" . PHP_EOL . (implode(', ', $profile->getSqlVariables()) ?: 'N/A') . PHP_EOL;
			}
		}

		// memóriahasználat
        $line .= "PHP memória: "
            . sprintf("%.2f", memory_get_usage() / 1024 / 1024) . " Mb" . PHP_EOL
        ;

        // PHP idő
        $line .= "PHP szkript idő: "
            . sprintf('%.5f sec', (microtime(true) - $this->microTimeOnBoot)) . PHP_EOL
        ;

        $this->logger->debug($line);
	}

	/**
	 * @return boolean
	 */
	public function isShowQueryProfiles()
	{
		return $this->showQueryProfiles;
	}

	/**
	 * @param boolean $showQueryProfiles
	 * @return Profiler
	 */
	public function setShowQueryProfiles($showQueryProfiles)
	{
		$this->showQueryProfiles = $showQueryProfiles;
		return $this;
	}

    /**
     * @return boolean
     */
    public function isIgnoreAjaxRequests(): bool
    {
        return $this->ignoreAjaxRequests;
    }

    /**
     * @param boolean $ignoreAjaxRequests
     * @return Profiler
     */
    public function setIgnoreAjaxRequests(bool $ignoreAjaxRequests): Profiler
    {
        $this->ignoreAjaxRequests = $ignoreAjaxRequests;
        return $this;
    }
}