<?php

namespace App\Event\Listener;

use App\Model\AclAction;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Component;

/**
 * Class ModelAcl
 * @package App\Event\Listener
 */
class ModelAcl
    extends Component
{
    /**
     * @var array
     */
    protected $listen = [
        /*'XyzController' => [
            'resourceId' => 'lab.xyz'
            , 'actions'  => [
                AclAction::ACTION_DELETE
                , AclAction::ACTION_MODIFY
            ]
        ]*/
    ];

    /**
     * Ellenőrzi, hogy az adott művelet elvégezhető-e a számlán/ ajánlaton. Ehhez a modellnek
     * implementálnia kell az \App\Model\Interfaces\Acl-t.
     *
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool|\Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function afterInitialize(Event $event, Dispatcher $dispatcher)
    {
        $controllerClass = $dispatcher->getControllerClass();
        if (!array_key_exists($controllerClass, $this->listen)) {
            return true;
        }

        $action = $dispatcher->getActionName();
        if (!in_array($action, $this->listen[$controllerClass]['actions'])) {
            return true;
        }

        $controller = $dispatcher->getActiveController();

        if (!$this->acl->isAllowedModelByIdentity(
            $this->listen[$controllerClass]['resourceId']
            , $action
            , $controller->getModel()
        )) {
            $message = $this->translate->t('Nem engedélyezett művelet!');

            $this->logger->warning($message);

            $this->flashSession->error($message);

            $controller->redirect(
                AclAction::ACTION_INDEX
            );

            $event->stop();

            return false;
        }

        return true;
    }
}