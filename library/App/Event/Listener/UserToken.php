<?php

namespace App\Event\Listener;

use Phalcon\Exception;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Component;
use App\Auth\Adapter\UserToken as UserTokenAdapter;

/**
 * Class UserToken
 * @package App\Event\Listener
 */
class UserToken extends Component
{
    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        $token = $this->request->get('token');

        if ($token == null) {
            return true;
        }

        try {
            $adapter = new UserTokenAdapter();
            $adapter->setToken($token);

            /**
             * @var \App\Model\User $user
             */
            $user = $this->auth->login($adapter);

            // beléptetés
            $this->auth->setIdentity($user);

            // log
            $this->logger->info(
                $this->translate->_('Bejelentkezés sikeres (token).')
            );

        } catch (Exception $e) {

            $message = $this->translate->_($e->getMessage());

            $this->logger->error(
                $message
                , ['exception' => $e]
            );

            $this->flashSession->error($message);
        }
    }
}