<?php

namespace App\Traits;

use App\Model\Language;
use Phalcon\Di;

/**
 * Class InterfaceLanguage
 * @package App\Traits
 */
trait InterfaceLanguage
{
    /**
     * @return Language
     */
    public function getInterfaceLanguage()
    {
        $di = Di::getDefault();

        $user = $di->get('auth')->getIdentity();

        return $user->getInterfaceLanguage();
    }
}