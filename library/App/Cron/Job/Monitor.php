<?php

namespace App\Cron\Job;

use App\Model\Cron as CronModel;

/**
 * Class Monitor
 * @package App\Cron\Job
 */
class Monitor extends CronModel
{
    /**
     * run
     */
	public function run()
	{
	    // régóta futó jobok ellenőrzése
		foreach (self::getGarbageJobs() as $job) {
            $this->logger->error($job->className . ' túl régóta fut, ellenőrizd!');
		}

		// worker ellenőrzése
        $output = shell_exec('ps -Af | grep "queue worker"');
        if (!preg_match('/\/usr\/bin\/php cli\.php queue worker/', $output)) {
            $this->logger->error('A queue worker nem fut, ellenőrizd!');
        }
	}
}