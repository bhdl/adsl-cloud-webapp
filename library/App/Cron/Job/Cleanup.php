<?php

namespace App\Cron\Job;

use App\Model\Cron as CronModel;

/**
 * Class Cleanup
 * @package App\Cron\Job
 */
class Cleanup extends CronModel
{
    /**
     * @var int
     */
    protected $tmpFileLifeTime = 3600;

    /**
     * run
     */
	public function run()
	{
	    $di = $this->getDI();

	    $dir = $di->get('app')->getAppPath('workspace/tmp');

        $files = array_diff(
            (array)@scandir($dir)
            , array('.', '..', '.gitkeep')
        );

        foreach ($files as $file) {
            $path = $dir . DIRECTORY_SEPARATOR . $file;
            $time = filectime($path) ?: 0;
            if ($time < time() - $this->tmpFileLifeTime) {
                @unlink($path);
            }
        }
	}
}