<?php

namespace App\Migration;

use App\Filter\StringClean;
use Phalcon\Mvc\User\Component;

/**
 * Class AbstractMigration
 * @package App\Migration
 */
abstract class AbstractMigration 
    extends Component
{
    /**
     * @var bool
     */
    protected $dryRun;

    /**
     * AbstractMigration constructor.
     * @param boolean $dryRun
     */
    public function __construct($dryRun)
    {
        $this->dryRun = $dryRun;

        @unlink($this->getLogFilePath());
    }

    /**
     * @return mixed
     */
    abstract public function run();

    /**
     * @return string
     */
    protected function getLogFilePath()
    {
        $className = mb_strtolower(array_pop(explode('\\', get_class($this))));

        return APP_PATH . '/workspace/logs/migration-' . $className . '.log';
    }

    /**
     * @param $message
     * @param $tab
     */
    protected function log($message, $tab = 0)
    {
        $str = str_repeat("\t", $tab) . $message . "\n";

        @file_put_contents(
            $this->getLogFilePath()
            , $str
            , FILE_APPEND
        );
    }

    /**
     * @param array $row
     * @return array
     */
    protected function cleanRow($row)
    {
        $filter = new StringClean;

        foreach ($row as $key=>$value) {
            $row[$key] = html_entity_decode($filter->filter($value));

            // dátum fix
            if (strcmp($row[$key], '0000-00-00') === 0) {
                $row[$key] = '1970-01-01'; // nem tudok vele mit csinálni
            }
        }

        return $row;
    }
}