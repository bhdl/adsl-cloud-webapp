<?php

namespace App\Session\Adapter;

use Phalcon\Db;
use Phalcon\Di;
use Phalcon\Session\Adapter;
use Phalcon\Session\AdapterInterface;
use Phalcon\Session\Exception;
use Phalcon\Db\AdapterInterface as DbAdapter;

/**
 * App\Session\Adapter\Database
 * Database adapter for Phalcon\Session
 */
class Database extends Adapter implements AdapterInterface
{
    /**
     * @var DbAdapter
     */
    protected $connection;

    /**
     * {@inheritdoc}
     *
     * @param  array $options
     * @throws Exception
     */
    public function __construct($options = null)
    {
        if (!isset($options['db']) || !$options['db'] instanceof DbAdapter) {
            throw new Exception(
                'Parameter "db" is required and it must be an instance of Phalcon\Db\AdapterInterface'
            );
        }

        $this->connection = $options['db'];
        unset($options['db']);

        parent::__construct($options);

        session_set_save_handler(
            [$this, 'open'],
            [$this, 'close'],
            [$this, 'read'],
            [$this, 'write'],
            [$this, 'destroy'],
            [$this, 'gc']
        );
    }

    /**
     * {@inheritdoc}
     *
     * @return boolean
     */
    public function open()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     *
     * @return boolean
     */
    public function close()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $token
     * @return string
     */
    public function read($token)
    {
        $maxLifetime = (int) ini_get('session.gc_maxlifetime');

        if (php_sapi_name() == "cli") {
            $this->gc($maxLifetime);
        }

        $row = $this->connection->fetchOne(
            sprintf(
                'SELECT `data` FROM user_session WHERE token = ? AND DATE_ADD(COALESCE(modifiedAt, createdAt), INTERVAL %d SECOND) >= NOW()',
                $maxLifetime
            ),
            Db::FETCH_NUM,
            [$token]
        );

        if (empty($row)) {
            return '';
        }

        return $row[0];
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $token
     * @param  string $data
     * @return boolean
     */
    public function write($token, $data)
    {
        $row = $this->connection->fetchOne(
            'SELECT COUNT(*) FROM user_session WHERE token = ?',
            Db::FETCH_NUM,
            [$token]
        );

        $auth = Di::getDefault()->get('auth');

        $userId = $auth->getIdentity()->userId;

        if (!empty($row) && intval($row[0]) > 0) {
            return $this->connection->execute(
                'UPDATE user_session SET `data` = ?, userId = ?, modifiedAt = NOW() WHERE token = ?',
                [$data, $userId, $token]
            );
        } else {
            return $this->connection->execute(
                'INSERT INTO user_session (token, userId, `data`, createdAt) VALUES (?, ?, ?, NOW())',
                [$token, $userId, $data]
            );
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return boolean
     */
    public function destroy($session_id = null)
    {
        if (!$this->isStarted()) {
            return true;
        }

        if (is_null($session_id)) {
            $session_id = $this->getId();
        }

        $this->_started = false;
        $result = $this->connection->execute(
            'DELETE FROM user_session WHERE token = ?',
            [$session_id]
        );

        return $result && session_destroy();
    }

    /**
     * {@inheritdoc}
     * @param  integer $maxLifetime
     *
     * @return boolean
     */
    public function gc($maxLifetime)
    {
        return $this->connection->execute(
            sprintf('DELETE FROM user_session WHERE DATE_ADD(COALESCE(modifiedAt, createdAt), INTERVAL %d SECOND) < NOW()', $maxLifetime)
        );
    }
}