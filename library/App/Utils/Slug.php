<?php

namespace App\Utils;
use Phalcon\Exception;
use Transliterator;

/**
 * Class Slug
 * @package App\Utils
 */
class Slug
    extends \Phalcon\Utils\Slug
{
    /**
     * @param string $string
     * @param array $replace
     * @param string $delimiter
     * @param bool $lowerCase
     * @return null|string|string[]
     * @throws Exception
     */
    public static function generate($string, $replace = [], $delimiter = ' ', $lowerCase = false)
    {
        if (!extension_loaded('intl')) {
            throw new Exception('intl module not loaded');
        }

        if (!extension_loaded('mbstring')) {
            throw new Exception('mbstring module not loaded');
        }

        // Save the old locale and set the new locale to UTF-8
        $oldLocale = setlocale(LC_ALL, '0');
        setlocale(LC_ALL, 'en_US.UTF-8');

        // Better to replace given $replace array as index => value
        // Example $replace['ı' => 'i', 'İ' => 'i'];
        if (!empty($replace) && is_array($replace)) {
            $string = str_replace(array_keys($replace), array_values($replace), $string);
        }

        $transliterator = Transliterator::create('Any-Latin; Latin-ASCII');
        $string = $transliterator->transliterate(
            mb_convert_encoding(htmlspecialchars_decode($string), 'UTF-8', 'auto')
        );

        self::restoreLocale($oldLocale);

        return self::cleanString($string, $delimiter, $lowerCase);
    }

    /**
     * @param $string
     * @param $delimiter
     * @param $lowerCase
     * @return null|string|string[]
     */
    protected static function cleanString($string, $delimiter, $lowerCase)
    {
        // replace non letter or non digits by -
        $string = preg_replace('#[^\pL\d]+#u', '-', $string);

        // Trim trailing -
        $string = trim($string, '-');

        $clean = preg_replace('~[^-\w]+~', '', $string);
        $clean = $lowerCase ? strtolower($clean) : strtoupper($clean);
        $clean = preg_replace('#[\/_|+ -]+#', $delimiter, $clean);
        $clean = trim($clean, $delimiter);

        return $clean;
    }
}