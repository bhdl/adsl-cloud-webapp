<?php

namespace App;

use Phalcon\Exception;
use Phalcon\Logger;
use Phalcon\Mvc\User\Component;

/**
 * Class ErrorHandler
 * @package App
 */
final class ErrorHandler extends Component
{
    /**
     * Alapértelmezett adapter
     * @var string
     */
    protected $displayAdapter = self::DISPLAY_ADAPTER_HTML;

    /**
     * Használjon-e display adaptert
     * @var boolean
     */
    protected $useDisplayAdapter = true;

    /**
     * Jelzi, hogy be lett-e töltve az error handler
     * @var boolean
     */
    protected $errorHandlerRegistered = false;

    /**
     * Error map
     *
     * Ezek az üzenetek látszódnak a felhasználónak
     * @var array
     */
    protected $errorMap = array(
        E_NOTICE            => ['level' => Logger::NOTICE, 'message' => 'NOTICE'],
        E_USER_NOTICE       => ['level' => Logger::NOTICE, 'message' => 'USER_NOTICE'],
        E_WARNING           => ['level' => Logger::WARNING, 'message' => 'WARNING'],
        E_CORE_WARNING      => ['level' => Logger::WARNING, 'message' => 'CORE_WARNING'],
        E_USER_WARNING      => ['level' => Logger::WARNING, 'message' => 'USER_WARNING'],
        E_ERROR             => ['level' => Logger::ERROR, 'message' => 'ERROR'],
        E_USER_ERROR        => ['level' => Logger::ERROR, 'message' => 'USER_ERROR'],
        E_CORE_ERROR        => ['level' => Logger::ERROR, 'message' => 'CORE_ERROR'],
        E_RECOVERABLE_ERROR => ['level' => Logger::ERROR, 'message' => 'RECOVERABLE_ERROR'],
        E_STRICT            => ['level' => Logger::DEBUG, 'message' => 'STRICT'],
        E_DEPRECATED        => ['level' => Logger::DEBUG, 'message' => 'DEPRECATED'],
        E_USER_DEPRECATED   => ['level' => Logger::DEBUG, 'message' => 'USER_DEPRECATED'],
        E_PARSE             => ['level' => Logger::ERROR, 'message' => 'PARSE'],
        E_COMPILE_ERROR     => ['level' => Logger::ERROR, 'message' => 'COMPILE_ERROR']
    );

    /**
     * Kapott kivétel
     * @var Exception
     */
    protected $exception;

    /**
     * Fejlesztői rendszer?
     *
     * Csak akkor írunk ki hibát, ha nem élesen vagyunk
     * @var boolean
     */
    protected $isDebug = false;

    /**
     * Jelzi, hogy be lett-e töltve a shutdown handler
     * @var boolean
     */
    protected $shutdownHandlerRegistered = false;

    /**
     * Jelzi, hogy be lett-e töltve a kivétel kezelő
     * @var bool
     */
    protected $exceptionHandlerRegistered = false;

    /**
     * Display adapter lehetőségek
     *
     * A működésért lásd az egyes adapter osztályokat
     */
    const DISPLAY_ADAPTER_HTML = 'App\\ErrorHandler\\Display\\Html';
    const DISPLAY_ADAPTER_TEXT = 'App\\ErrorHandler\\Display\\Text';
    const DISPLAY_ADAPTER_XML  = 'App\\ErrorHandler\\Display\\Xml';

    /**
     * Alapértelmezett megjelenítési adapter lekérése
     *
     * @return string
     */
    public function getDisplayAdapter()
    {
        return $this->displayAdapter;
    }

    /**
     * Getter - használjon-e a display adaptert
     *
     * @return bool
     */
    public function getUseDisplayAdapter()
    {
        return $this->useDisplayAdapter;
    }

    /**
     * Kivétel getter
     * @return Exception
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * isDebug getter
     * @return boolean
     */
    public function getIsDebug()
    {
        return $this->isDebug;
    }

    /**
     * @return ErrorHandler
     */
    public function registerExceptionHandler()
    {
        if ($this->exceptionHandlerRegistered) {
            return $this;
        }
        set_exception_handler(array($this, 'exceptionHandler'));
        $this->exceptionHandlerRegistered = true;
        return $this;
    }

    /**
     * @param Exception $exception
     */
    public function exceptionHandler($exception)
    {
        $this->setException($exception);

        // TODO: a shutdownhandler meghívódik ebben az esetben?
        // return $this->shutdownHandler();
    }

    /**
     * Error handler regisztrálása egyszerűbb php hibákhoz
     * @return ErrorHandler
     */
    public function registerErrorHandler()
    {
        if ($this->errorHandlerRegistered) {
            return $this;
        }
        set_error_handler(array($this, 'errorHandler'));
        $this->errorHandlerRegistered = true;
        return $this;
    }

    /**
     * Error handler
     *
     * Egyszerűbb PHP hibákhoz
     */
    public function errorHandler()
    {
        if ($this->exception == null) {
            $this->exception = $this->createException();
            if ($this->exception == null) {
                return;
            }
        }

        $this->log();
    }

    /**
     * Shutdown handler regisztrálása fogósabb php hibákhoz
     * @return ErrorHandler
     */
    public function registerShutdownHandler()
    {
        if ($this->shutdownHandlerRegistered) {
            return $this;
        }
        register_shutdown_function(array($this, 'shutdownHandler'));
        $this->shutdownHandlerRegistered = true;
        return $this;
    }

    /**
     * Shutdown handler
     *
     * Komolyabb PHP hibákhoz
     */
    public function shutdownHandler()
    {
        if ($this->exception == null) {
            $this->exception = $this->createException();
            if ($this->exception == null) {
                return;
            }
        }

        // hiba naplózása
        $this->log();

        if (!$this->useDisplayAdapter) {
            return;
        }

        // megjelenítés
        $this->display($this->displayAdapter);
    }

    /**
     * @return Exception
     */
    protected function createException()
    {
        $error = error_get_last();

        if (in_array($code = $error['type'], array_keys($this->errorMap))) {
            // elkészítjük a hibaüzenetet és a kivételt
            $message = $this->errorMap[$code]['message']
                . ": {$error['message']} in {$error['file']} on line {$error['line']}"
            ;

            return new Exception($message, $code);
        }

        return null;
    }

    /**
     * Alapértelmezett megjelenítési adapter beállítása
     *
     * @param string $displayAdapter
     * @return ErrorHandler
     */
    public function setDisplayAdapter($displayAdapter)
    {
        $this->displayAdapter = $displayAdapter;
        return $this;
    }

    /**
     * Használjon-e display adaptert a hiba megjelenítéshez
     *
     * @param bool $useDisplayAdapter
     * @return ErrorHandler
     */
    public function setUseDisplayAdapter($useDisplayAdapter)
    {
        $this->useDisplayAdapter = (bool)$useDisplayAdapter;
        return $this;
    }

    /**
     * Kivétel, amit meg kell jeleníteni
     * @param Exception $exception
     * @return ErrorHandler
     */
    public function setException($exception)
    {
        $this->exception = $exception;
        return $this;
    }

    /**
     * isDebug setter
     * @param boolean $isDebug
     * @return ErrorHandler
     */
    public function setIsDebug($isDebug)
    {
        $this->isDebug = (boolean)$isDebug;
        return $this;
    }

    /**
     * @param int $code
     * @return string
     */
    protected function getErrorMessage($code)
    {
        if (array_key_exists($code, $this->errorMap)) {
            return $this->errorMap[$code]['message'];
        }

        return $this->errorMap[E_ERROR]['message'];
    }

    /**
     * @param int $code
     * @return string
     */
    protected function getErrorLevel($code)
    {
        if (array_key_exists($code, $this->errorMap)) {
            return $this->errorMap[$code]['level'];
        }

        return $this->errorMap[E_ERROR]['level'];
    }

    /**
     * Hibaüzenet megjelenítése
     *
     * @param string $className megjelenítési adapter osztály
     */
    protected function display($className)
    {
        $displayAdapter = new $className;

        $exception = $this->getException();

        // csak simán átadjuk az adatokat
        $displayAdapter->error = $this->getErrorMessage($exception->getCode());

        // dev esetén további infó
        if ($this->getIsDebug()) {
            $displayAdapter->code          = $this->getException()->getCode();
            $displayAdapter->file          = $this->getException()->getFile();
            $displayAdapter->line          = $this->getException()->getLine();
            $displayAdapter->message       = $this->getException()->getMessage();
            $displayAdapter->traceAsString = $this->getException()->getTraceAsString();

        }

        // kiürítjuk a puffert
        ob_end_clean();

        // ha lehet akkor 500-at állítunk be
        if (!headers_sent()) {
            header(
                $_SERVER['SERVER_PROTOCOL']
                . ' 500 Internal Server Error'
                , true
                , 500
            );
        }

        // megjelenítjük a hibát
        die($displayAdapter->getResponse());
    }

    /**
     * Hibaüzenet naplózása
     *
     * Ha még nincs error resource, akkor php logba tolja ki. Minden esetben
     * LOG_EMERG lesz a hibakód, hiszen ezek nagyon alacsony szintű hibák.
     */
    protected function log()
    {
        $exception = $this->getException();

        if (property_exists($this, 'logger')) {
            // tök mindegy milyen hiba mindegyik a legsúlyosabbnak számít
            $this->logger->log(
                $this->getErrorLevel($exception->getCode())
                , $exception->getMessage()
                , ['exception' => $exception]
            );
            return;
        }

        // megy a syslog-ba
        error_log($exception->getMessage());
    }
}