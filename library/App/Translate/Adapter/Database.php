<?php

namespace App\Translate\Adapter;

use App\Model\Language;
use App\Model\Translation;
use App\Support\HashMap;
use Phalcon\Db;
use Phalcon\Translate\Adapter\Base;
use Phalcon\Translate\Adapter;
use Phalcon\Translate\AdapterInterface;
use Phalcon\Translate\Exception;

/**
 * TODO: módosítani, hogy ne egyesével kérdezze le a mezőket
 * - ha van cache, akkor onnan
 * - ha nincs, akkor töltse be memóriába
 *
 * Class Database
 * @package App\Translate\Adapter
 */
class Database
    extends Base
    implements AdapterInterface, \ArrayAccess
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var array
     */
    protected $translations = [];

    /**
     * Use ICU MessageFormatter to parse message
     *
     * @var boolean
     */
    protected $useIcuMessageFormatter = false;

    /**
     * Statement for Exist
     * @var array
     */
    protected $stmtExists;

    /**
     * Statement for Read
     * @var array
     */
    protected $stmtSelect;

    /**
     * Statement for read all
     * @var array
     */
    protected $stmtSelectAll;

    /**
     * Class constructor.
     *
     * @param  array $options
     * @throws \Phalcon\Translate\Exception
     */
    public function __construct(array $options)
    {
        if (!isset($options['db'])) {
            throw new Exception("Parameter 'db' is required");
        }

        if (!isset($options['table'])) {
            throw new Exception("Parameter 'table' is required");
        }

        if (!isset($options['languageId'])) {
            throw new Exception("Parameter 'languageId' is required");
        }

        if (isset($options['useIcuMessageFormatter'])) {
            if (!class_exists('\MessageFormatter')) {
                throw new Exception('"MessageFormatter" class is required');
            }

            $this->useIcuMessageFormatter = (boolean) $options['useIcuMessageFormatter'];
        }

        $this->stmtSelectAll = sprintf(
            'SELECT `key`, `value` FROM %s WHERE languageId = :languageId'
            , $options['table']
        );

        $this->stmtSelect = sprintf(
            'SELECT value FROM %s WHERE languageId = :languageId AND `key` = :key'
            , $options['table']
        );

        $this->stmtExists = sprintf(
            'SELECT COUNT(*) AS `count` FROM %s WHERE languageId = :languageId AND `key` = :key'
            , $options['table']
        );

        $this->translations = HashMap::toAssignByFields(
            $options['db']->fetchAll(
                $this->stmtSelectAll,
                Db::FETCH_ASSOC,
                ['languageId' => $options['languageId']]
            ) ?: []
            , 'key'
            , 'value'
        );

        $this->options = $options;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $translateKey
     * @param  array  $placeholders
     * @return string
     */
    public function query($translateKey, $placeholders = null)
    {
        $options = $this->options;

        $value = $translateKey;
        if (array_key_exists($translateKey, $this->translations)) {
            // key found
            $value = $this->translations[$translateKey];
        } else if (
            array_key_exists('addMissingTranslations', $this->options)
            && $this->options['addMissingTranslations']
        ) {
            // missing
            $this->addMissingTranslations($translateKey);
        }

        if (is_array($placeholders) && !empty($placeholders)) {
            if (true === $this->useIcuMessageFormatter) {
                $value = \MessageFormatter::formatMessage($options['languageId'], $value, $placeholders);
            } else {
                foreach ($placeholders as $placeHolderKey => $placeHolderValue) {
                    $value = str_replace('%' . $placeHolderKey . '%', $placeHolderValue, $value);
                }
            }
        }

        return $value;
    }

    /**
     * Returns the translation string of the given key
     *
     * @param  string $translateKey
     * @param  array  $placeholders
     * @return string
     */
    // @codingStandardsIgnoreStart
    public function _($translateKey, $placeholders = null)
    {
        return $this->query($translateKey, $placeholders);
    }
    // @codingStandardsIgnoreEnd

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->translations;
    }

    /**
     * {@inheritdoc}
     *
     * @param  string  $translateKey
     * @return boolean
     */
    public function exists($translateKey)
    {
        $options = $this->options;

        $result = $options['db']->fetchOne(
            $this->stmtExists,
            Db::FETCH_ASSOC,
            ['languageId' => $options['languageId'], 'key' => $translateKey]
        );

        return !empty($result['count']);
    }

    /**
     * @param string $translateKey
     * @return Database
     */
    public function addMissingTranslations($translateKey)
    {
        foreach (Language::findInterfaceLanguages() as $language) {
            try {
                $translation = new Translation([
                    'languageId' => $language->languageId
                    , 'key'      => $translateKey
                    , 'value'    => $translateKey
                ]);
                $translation->create();
            } catch (\Exception $e) {
            }
        }

        // megelőzve az ismételt beillesztési hibát (ha nem létezik és 1-nél többször próbál fordítani)
        $this->translations[$translateKey] = $translateKey;

        return $this;
    }

    /**
     * Adds a translation for given key (No existence check!)
     *
     * @param  string  $translateKey
     * @param  string  $message
     * @return boolean
     */
    public function add($translateKey, $message)
    {
        $options = $this->options;
        $data = ['languageId' => $options['languageId'], 'key' => $translateKey, 'value' => $message];

        return $options['db']->insert($options['table'], array_values($data), array_keys($data));
    }

    /**
     * Update a translation for given key (No existence check!)
     *
     * @param  string  $translateKey
     * @param  string  $message
     * @return boolean
     */
    public function update($translateKey, $message)
    {
        $options = $this->options;

        return $options['db']->update($options['table'], ['value'], [$message], [
            'conditions' => 'key = ? AND languageId = ?',
            'bind' => ['key' => $translateKey, 'languageId' => $options['languageId']]
        ]);
    }

    /**
     * Deletes a translation for given key (No existence check!)
     *
     * @param  string  $translateKey
     * @return boolean
     */
    public function delete($translateKey)
    {
        $options = $this->options;

        return $options['db']->delete(
            $options['table'],
            'key = :key AND languageId = :languageId',
            ['key' => $translateKey, 'languageId' => $options['languageId']]
        );
    }

    /**
     * Sets (insert or updates) a translation for given key
     *
     * @param  string  $translateKey
     * @param  string  $message
     * @return boolean
     */
    public function set($translateKey, $message)
    {
        return $this->exists($translateKey) ?
            $this->update($translateKey, $message) : $this->add($translateKey, $message);
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $translateKey
     * @return string
     */
    public function offsetExists($translateKey)
    {
        return $this->exists($translateKey);
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $translateKey
     * @param  string $message
     * @return string
     */
    public function offsetSet($translateKey, $message)
    {
        return $this->update($translateKey, $message);
    }

    /**
     * {@inheritdoc}
     *
     * @param string $translateKey
     * @return string
     */
    public function offsetGet($translateKey)
    {
        return $this->query($translateKey);
    }

    /**
     * {@inheritdoc}
     *
     * @param  string $translateKey
     * @return string
     */
    public function offsetUnset($translateKey)
    {
        return $this->delete($translateKey);
    }
}
