<?php

namespace App\Translate;

use App\Support\HashMap;

/**
 * Class Parser
 * @package App\Translate
 */
final class Parser
{
    /**
     * @var array
     */
    protected $extensions = [
        'php'
        , 'phtml'
        , 'js'
    ];

    /**
     * PHP és JS fájlokban található translate kulcsokat "kinyerő" regexp
     * kifejezés
     *
     * @var array
     */
    protected $expressions = [
        // translate kulcsok
        "/(\-\>t|Translate\.t|\-\>translate|Translate\.translate|\-\>_|Translate\._)\(('|\")(.+?)('|\")(,.*)?\)/" => 3
        // kivételek
        , "/Exception\(('|\")(.+?)('|\")(,.*)?\)/" => 2
    ];

    /**
     * @param array $directories
     * @return array
     */
    public function scan($directories = [])
    {
        $array = [];

        foreach ($directories as $directory) {
            $files = $this->getFilesInDirectory($directory);
            if (HashMap::isFilled($files)) {
                $keys = $this->parseFiles($files);
                if (HashMap::isFilled($keys)) {
                    $array = array_merge($array, $keys);
                }
            }
        }

        return array_unique($array);
    }

    /**
     * @param string $file
     * @return mixed
     */
    protected function parseFile($file)
    {
        $content = file_get_contents($file);

        $keys = [];

        foreach ($this->expressions as $expression=>$key) {
            preg_match_all($expression, $content, $matches);
            $keys = array_merge(
                $keys
                , $matches[$key] ? array_filter($matches[$key], 'trim') : []
            );
        }

        return array_unique($keys);
    }

    /**
     * @param string $directory
     * @return array
     */
    protected function getFilesInDirectory($directory)
    {
        $files = [];

        foreach (scandir($directory) as $file) {
            if ($file == "." || $file == "..") {
                continue;
            }

            $realPath = realpath($directory . DIRECTORY_SEPARATOR . $file);

            if (!is_dir($realPath)) {
                if (in_array(pathinfo($file, PATHINFO_EXTENSION), $this->extensions)){
                    $files[] = $realPath;
                }
            } else {
                $files = array_merge($this->getFilesInDirectory($realPath), $files);
            }
        }

        return $files;
    }

    /**
     * @param array $files
     * @return array
     */
    protected function parseFiles($files = [])
    {
        $array = [];

        foreach($files as $file){
            $keys = $this->parseFile($file);
            if (is_array($keys)) {
                $array = array_merge(
                    $array
                    , $keys
                );
            }
        }

        return array_unique($array);
    }
}