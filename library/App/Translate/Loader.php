<?php

namespace App\Translate;

use App\Model\Language;
use App\Support\File;
use App\Translate\Adapter\Database;
use Phalcon\Exception;
use Phalcon\Mvc\User\Component;
use Phalcon\Translate\Adapter\NativeArray;

/**
 * Class Translate
 * @package App
 */
class Loader
    extends Component
{
    /**
     * A fordításhoz használható Phalcon translate adapter (NativeArray)
     * @var NativeArray
     */
    protected $adapter;

    /**
     * Javascript fordító engedélyezett-e
     * @var bool
     */
    protected $jsMessagesEnabled = false;

    /**
     * Relatív vagy abszolút elérési útja a messages fájlnak
     *
     * Slash nem kell a végére!
     * @var string
     */
    protected $jsMessagesPath;

    /**
     * Elérési útja a messages fájlnak a böngészőben
     *
     * Slash nem kell a végére!
     * @var string
     */
    protected $jsMessagesUrl;

    /**
     * A fordításokhoz használható Phalcon translate adapter előállítása. A DI-ban
     * ennek a függvénynek a visszatérési értékét (NativeArray) kell regisztrálni. Ha
     * ez megtörtént, a controllerekben és egyéb injectable jellegű osztályokban a
     * $this->translate hívással elérhető.
     *
     * @param Language $language
     * @param boolean $reload újratöltse-e a nyelvi értékeket?
     * @return NativeArray
     * @throws Exception
     */
    public function getAdapter($language, $reload = false)
    {
        if (!$reload && $this->adapter !== null) {
            // már van betöltött cucc
            return $this->adapter;
        }

        // adapter előállítása
        $this->adapter = new Database([
            'table'                    => 'translation'
            , 'db'                     => $this->di->get('db')
            , 'languageId'             => $language->languageId
            , 'useIcuMessageFormatter' => false
            /*
             * Óvatosan ezzel! Ha bekapcsolod, akkor a programozási hibák miatt a duplán lefordított
             * stringek is bekerülnek a translation táblába!
             */
            , 'addMissingTranslations' => false
        ]);

        // engedélyezve van a js message használat (javascript translate)?
        if ($this->isJsMessagesEnabled()) {
            // js message fájl betöltése
            $this->loadJs($language->locale, $this->adapter->all());
        }

        return $this->adapter;
    }

    /**
     * @param string $locale
     * @param array $messages
     * @return Loader
     * @throws Exception
     */
    protected function loadJs($locale, $messages)
    {
        if ($this->assets == null) {
            return $this;
        }

        // translate init
        $inlineJs = "$(document).ready(function(){";
        $inlineJs .= "\t\t" . 'Translate.setLocale("' . $locale . '");';
        $inlineJs .= "\t\t" . 'Translate.setMessages(messages);';
        $inlineJs .= "});";

        // messages json fájl
        $this->assets
            ->addJs($this->createJsMessages($locale, $messages))
            ->addInlineJs($inlineJs)
        ;

        return $this;
    }

    /**
     * Javascript messages létrehozása a fájlrendszerben (pl. hu_HU/hash.js)
     *
     * Paraméterezhető, hogy hol hozza létre
     * @param string $locale
     * @param string $messages
     * @return string
     * @throws Exception
     */
    protected function createJsMessages($locale, $messages)
    {
        $hash = md5(serialize($messages));
        $dir  = $this->jsMessagesPath . DIRECTORY_SEPARATOR . $locale;
        if (!is_dir($dir)) {
            if (!@mkdir($dir)) {
                throw new Exception('Nem sikerült a jsMessages munkamappa létrehozása.');
            }
        }

        $files = array_diff((array)@scandir($dir), array('.', '..'));
        $found = false;
        foreach ($files as $file) {
            $file = $dir . DIRECTORY_SEPARATOR . $file;
            if (is_file($file)) {
                $pi = pathinfo($file);
                if (
                    is_array($pi)
                    && strcmp(mb_strtolower($pi['extension']), 'js') === 0
                ) {
                    if (strcmp($pi['filename'], $hash) === 0) {
                        $found = true;
                    } else {
                        @unlink($file);
                    }
                }
            }
        }

        // ha nem létezik a fájl, vagy ki van kapcsolva a cache
        if (!$found) {
            $js = 'if (typeof messages == "undefined") {var messages = {};}'
                . 'messages.' . $locale
                . ' = ' . json_encode($messages) . ';'
            ;

            if (@file_put_contents($dir . DIRECTORY_SEPARATOR . $hash . '.js', $js) === false) {
                throw new Exception(
                    'Nem sikerült létrehozni a "messages" fájlt'
                    . ' (lehetséges hogy nem létezik a mappa, vagy írásvédett): '
                    . $this->jsMessagesPath
                );
            }
        }

        // ezt kell beágyazni
        return $this->jsMessagesUrl
            . '/'
            . $locale
            . '/'
            . $hash
            . '.js'
        ;
    }

    /**
     * @return boolean
     */
    public function isJsMessagesEnabled()
    {
        return $this->jsMessagesEnabled;
    }

    /**
     * @param boolean $jsMessagesEnabled
     * @return Loader
     */
    public function setJsMessagesEnabled($jsMessagesEnabled)
    {
        $this->jsMessagesEnabled = $jsMessagesEnabled;
        return $this;
    }

    /**
     * @return string
     */
    public function getJsMessagesPath()
    {
        return $this->jsMessagesPath;
    }

    /**
     * @param string $jsMessagesPath
     * @return Loader
     */
    public function setJsMessagesPath($jsMessagesPath)
    {
        $this->jsMessagesPath = $jsMessagesPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getJsMessagesUrl()
    {
        return $this->jsMessagesUrl;
    }

    /**
     * @param string $jsMessagesUrl
     * @return Loader
     */
    public function setJsMessagesUrl($jsMessagesUrl)
    {
        $this->jsMessagesUrl = $jsMessagesUrl;
        return $this;
    }
}