<?php

namespace App\ServiceProvider;

/**
 * Class AbstractServiceProvider
 * @package App\ServiceProvider
 */
abstract class AbstractServiceProvider
{
    /**
     * @var \Phalcon\Di
     */
    protected $di;

    /**
     * @return mixed
     */
    abstract public function getService();

    /**
     * @return \Phalcon\Di
     */
    public function getDi(): \Phalcon\Di
    {
        return $this->di;
    }

    /**
     * @param \Phalcon\Di $di
     * @return AbstractServiceProvider
     */
    public function setDi(\Phalcon\Di $di): AbstractServiceProvider
    {
        $this->di = $di;
        return $this;
    }
}