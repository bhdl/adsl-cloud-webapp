<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class View
 * @package App\ServiceProvider\Service
 */
class View
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Mvc\View
     */
    public function getService()
    {
        $view = new \Phalcon\Mvc\View();

        $view->registerEngines([
            '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
        ]);

        return $view;
    }
}