<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Profiler
 * @package App\ServiceProvider\Service
 */
class Profiler
    extends AbstractServiceProvider
{
    /**
     * @return \App\Event\Listener\Profiler
     */
    public function getService()
    {
        $config = $this->di->get('config')->get('profiler');

        if ($config == null || $config->enabled == false) {
            return;
        }

        $listener = new \App\Event\Listener\Profiler();

        if ($config->showQueryProfiles) {
            $listener->setShowQueryProfiles(true);
        }

        if ($config->ignoreAjaxRequests) {
            $listener->setIgnoreAjaxRequests(true);
        }

        $eventsManager = $this->di->get('eventsManager');

        // listener hozzáadása az eseménykezelőhöz
        $eventsManager->attach('application', $listener);

        // profiler hozzáadása az eseménykezelőhöz
        $profiler = new Profiler();
        $eventsManager->attach('db', function($event, $db) use ($profiler) {
            if ($event->getType() == 'beforeQuery') {
                $profiler->startProfile($db->getSQLStatement());
            }
            if ($event->getType() == 'afterQuery') {
                $profiler->stopProfile();
            }
        });

        return $profiler;
    }
}