<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Loader
 * @package App\ServiceProvider\Service
 */
class Loader
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Loader
     */
    public function getService()
    {
        $loader = new \Phalcon\Loader();

        $loader->registerNamespaces(array(
            'App' => $this->di->get('app')->getAppPath('library/App')
        ));

        $loader->register();

        return $loader;
    }
}