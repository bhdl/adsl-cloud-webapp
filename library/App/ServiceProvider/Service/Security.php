<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Security
 * @package App\ServiceProvider\Service
 */
class Security
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Security
     */
    public function getService()
    {
        $security = new \Phalcon\Security();
        $security->setWorkFactor(12);

        return $security;
    }
}