<?php

namespace App\ServiceProvider\Service\Admin;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Tag
 * @package App\ServiceProvider\Service\Admin
 */
class Tag
    extends AbstractServiceProvider
{
    /**
     * @return \App\View\Helper\Tag
     */
    public function getService()
    {
        $tag = new \App\View\Helper\Tag();
        $tag->setTitle('ADSL');
        $tag->setTitleSeparator(' - ');

        return $tag;
    }
}