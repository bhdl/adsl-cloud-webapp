<?php

namespace App\ServiceProvider\Service\Admin;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class LanguageSwitch
 * @package App\ServiceProvider\Service\Admin
 */
class LanguageSwitch
    extends AbstractServiceProvider
{
    /**
     * @return \App\View\Helper\LanguageSwitch
     */
    public function getService()
    {
        return new \App\View\Helper\LanguageSwitch();
    }
}