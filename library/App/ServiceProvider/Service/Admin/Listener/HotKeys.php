<?php

namespace App\ServiceProvider\Service\Admin\Listener;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class HotKeys
 * @package App\ServiceProvider\Service\Admin\Listener
 */
class HotKeys
    extends AbstractServiceProvider
{
    /**
     * @return \App\Event\Listener\HotKeys
     */
    public function getService()
    {
        $listener = new \App\Event\Listener\HotKeys();

        $this->di->get('eventsManager')->attach(
            'dispatch'
            , $listener
        );

        return $listener;
    }
}