<?php

namespace App\ServiceProvider\Service\Admin\Listener;

use App\Event\Listener\Browser;
use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class BrowserDetect
 * @package App\ServiceProvider\Service\Admin\Listener
 */
class BrowserDetect
    extends AbstractServiceProvider
{
    /**
     * @return void
     */
    public function getService()
    {
        $this->di->get('eventsManager')->attach(
            'dispatch'
            , new Browser('/admin/browser')
        );
    }
}