<?php

namespace App\ServiceProvider\Service\Admin\Listener;

use App\Event\Listener\Session;
use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class SessionDetect
 * @package App\ServiceProvider\Service\Admin\Listener
 */
class SessionDetect
    extends AbstractServiceProvider
{
    /**
     * @return void
     */
    public function getService()
    {
        $this->di->get('eventsManager')->attach(
            'dispatch'
            , new Session('/admin/auth')
        );
    }
}