<?php

namespace App\ServiceProvider\Service\Admin\Listener;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class UserToken
 * @package App\ServiceProvider\Service\Admin\Listener
 */
class UserToken
    extends AbstractServiceProvider
{
    /**
     *
     */
    public function getService()
    {
        $listener = new \App\Event\Listener\UserToken();

        $this->di->get('eventsManager')->attach('dispatch', $listener, 1000);
    }
}