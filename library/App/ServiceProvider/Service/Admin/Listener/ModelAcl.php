<?php

namespace App\ServiceProvider\Service\Admin\Listener;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class ModelAcl
 * @package App\ServiceProvider\Service\Admin\Listener
 */
class ModelAcl
    extends AbstractServiceProvider
{
    /**
     * @return void
     */
    public function getService()
    {
        $this->di->get('eventsManager')->attach(
            'dispatch'
            , new \App\Event\Listener\ModelAcl()
            , 50
        );
    }
}