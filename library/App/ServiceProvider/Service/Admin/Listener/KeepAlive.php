<?php

namespace App\ServiceProvider\Service\Admin\Listener;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class KeepAlive
 * @package App\ServiceProvider\Service\Admin\Listener
 */
class KeepAlive
    extends AbstractServiceProvider
{
    /**
     * @return void
     */
    public function getService()
    {
        $this->di->get('eventsManager')->attach(
            'dispatch'
            , new \App\Event\Listener\KeepAlive()
        );
    }
}