<?php

namespace App\ServiceProvider\Service\Admin\Listener;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Notification
 * @package App\ServiceProvider\Service\Admin\Listener
 */
class Notification
    extends AbstractServiceProvider
{
    /**
     * @return void
     */
    public function getService()
    {
        $this->di->get('eventsManager')->attach(
            'dispatch'
            , new \App\Event\Listener\Notification()
        );
    }
}