<?php

namespace App\ServiceProvider\Service\Admin;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class HeadingButtons
 * @package App\ServiceProvider\Service\Admin
 */
class HeadingButtons
    extends AbstractServiceProvider
{
    /**
     * @return \App\View\Helper\HeadingButtons
     */
    public function getService()
    {
        return new \App\View\Helper\HeadingButtons();
    }
}