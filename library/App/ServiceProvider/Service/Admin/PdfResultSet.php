<?php

namespace App\ServiceProvider\Service\Admin;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class PdfResultSet
 * @package App\ServiceProvider\Service\Admin
 */
class PdfResultSet
    extends AbstractServiceProvider
{
    /**
     * @return \App\View\Helper\PdfResultSet
     */
    public function getService()
    {
        return new \App\View\Helper\PdfResultSet();
    }
}