<?php

namespace App\ServiceProvider\Service\Admin;

/**
 * Class View
 * @package App\ServiceProvider\Service\Admin
 */
class View
    extends \App\ServiceProvider\Service\View
{
    /**
     * @return \Phalcon\Mvc\View
     */
    public function getService()
    {
        $view = parent::getService();

        $view->setViewsDir(
            $this->di->get('app')->getAppPath('modules/admin/views')
        );

        $view->setLayoutsDir(
            $this->di->get('app')->getAppPath('modules/admin/views/layouts')
        );

        $view->setMainView('layouts/default');

        $view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_MAIN_LAYOUT);

        return $view;
    }
}