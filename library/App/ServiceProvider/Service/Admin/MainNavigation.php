<?php

namespace App\ServiceProvider\Service\Admin;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class MainNavigation
 * @package App\ServiceProvider\Service\Admin
 */
class MainNavigation
    extends AbstractServiceProvider
{
    /**
     * @return \App\View\Helper\MainNavigation
     */
    public function getService()
    {
        return new \App\View\Helper\MainNavigation();
    }
}