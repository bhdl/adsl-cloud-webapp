<?php

namespace App\ServiceProvider\Service\Admin;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class NavigationHistory
 * @package App\ServiceProvider\Service\Admin
 */
class NavigationHistory
    extends AbstractServiceProvider
{
    /**
     * @return \App\View\Helper\NavigationHistory
     */
    public function getService()
    {
        return new \App\View\Helper\NavigationHistory();
    }
}