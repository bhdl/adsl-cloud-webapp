<?php

namespace App\ServiceProvider\Service\Admin;

use App\ServiceProvider\AbstractServiceProvider;
use Phalcon\Assets\Manager;
use App\Traits\InterfaceLanguage;

/**
 * Class Assets
 * @package App\ServiceProvider\Service\Admin
 */
class Assets
    extends AbstractServiceProvider
{
    use InterfaceLanguage;

    /**
     * @return \Phalcon\Assets\Manager
     */
    public function getService()
    {
        $assets = new Manager();

        $language = $this->getInterfaceLanguage();

        /*
         * Css fájlok
         */
        $assets
            ->addCss('/assets/css/fonts.min.css')
            ->addCss('/assets/css/icons/icomoon/styles.css')
            ->addCss('/assets/css/bootstrap.min.css')
            ->addCss('/assets/css/core.min.css')
            ->addCss('/assets/css/components.min.css')
            ->addCss('/assets/css/colors.min.css')
            ->addCss('/assets/css/extras/animate.min.css')
            ->addCss('/assets/css/jquery-ui-timepicker-addon.css')
            ->addCss('/assets/css/select2.css')
            ->addCss('/assets/css/positioning.css')
        ;

        /*
         * Javascript libs
         */
        $assets
            ->addJs('/assets/js/core/libraries/jquery.min.js')
            ->addJs('/assets/js/core/libraries/jquery_ui/widgets.min.js')
            ->addJs('/assets/js/core/libraries/bootstrap.min.js')
            ->addJs('/assets/js/plugins/ui/moment/moment.min.js')
            ->addJs('/assets/js/plugins/ui/moment/moment_locales.min.js')
            ->addJs('/assets/js/plugins/ui/numeral/numeral.min.js')
            ->addJs('/assets/js/plugins/ui/numeral/locales.min.js')
            ->addJs('/assets/js/plugins/easytimer/easytimer.min.js')
            ->addJs('/assets/js/app/App.js')
        ;

        /*
         * Javascript components
         */
        $assets
            ->addJs('/assets/js/plugins/loaders/pace.min.js')
            ->addJs('/assets/js/plugins/loaders/blockui.min.js')
            /*
             * Forms
             */
            ->addJs('/assets/js/plugins/forms/styling/uniform.min.js')
            ->addJs('/assets/js/plugins/forms/selects/select2.min.js')
            ->addJs('/assets/js/plugins/forms/selects/i18n/' . $language->iso . '.js')
            ->addJs('/assets/js/plugins/forms/ckeditor/ckeditor.js')
            ->addJs('/assets/js/plugins/forms/inputs/formatter.min.js')
            ->addJs('/assets/js/plugins/forms/inputs/bootstrap-switch.min.js')
            /*
             * Datatable
             */
            ->addJs('/assets/js/plugins/tables/datatables/datatables.min.js')
            ->addJs('/assets/js/plugins/tables/datatables/extensions/responsive.min.js')
            ->addJs('/assets/js/plugins/tables/datatables/extensions/buttons.min.js')
            ->addJs('/assets/js/plugins/tables/datatables/extensions/select.min.js')
            ->addJs('/assets/js/plugins/tables/datatables/extensions/col_reorder.min.js')
            ->addJs('/assets/js/plugins/tables/datatables/extensions/row_reorder.min.js')
            ->addJs('/assets/js/plugins/tables/datatables/extensions/row_group.min.js')
            ->addJs('/assets/js/plugins/tables/datatables/extensions/fixed_header.min.js')
            /*
             * PNotify
             */
            ->addJs('/assets/js/plugins/notifications/pnotify.min.js')
            /*
             * DatePicker
             */
            ->addJs('/assets/js/core/libraries/jquery_ui/datepicker/i18n/datepicker-' . $language->iso . '.js')
            ->addJs('/assets/js/plugins/pickers/timepicker-addon.js')
            ->addJs('/assets/js/core/libraries/jquery_ui/timepicker/i18n/jquery-ui-timepicker-' . $language->iso . '.js')
            ->addJs('/assets/js/core/libraries/jquery_ui/timepicker/jquery-ui-sliderAccess.js')
            ->addJs('/assets/js/plugins/pickers/daterangepicker.js')
            /*
             * Bootbox
             */
            ->addJs('/assets/js/plugins/notifications/bootbox.min.js')
            /*
             * ChartJs
             */
            ->addJs('/assets/js/plugins/charts/chart.min.js')
            /*
             * StepWizard
             */
            ->addJs('/assets/js/app/WizardSteps.js')
            /**
             * Templates
             */
            ->addJs('/assets/js/plugins/templates/handlebars-v4.0.12.js')
        ;

        /*
         * Custom javascript components
         */
        $assets
            ->addJs('/assets/js/app/Acl.js')
            ->addJs('/assets/js/app/SearchBar.js')
            ->addJs('/assets/js/app/Translate.js')
            ->addJs('/assets/js/app/BlockUi.js')
            ->addJs('/assets/js/app/BootBox.js')
            ->addJs('/assets/js/app/ProductInstance.js')
            ->addJs('/assets/js/app/ProductPositioning.js')
        ;

        /*
         * Modals
         */
        $assets
            ->addJs('/assets/js/app/modal/CrudBrowse.js')
            ->addJs('/assets/js/app/modal/CrudCreate.js')
        ;

        /*
         * Some initializations
         */
        $assets
            ->addJs('/assets/js/core/app.js')
            ->addJs('/assets/js/app/init/datatables.js')
            ->addJs('/assets/js/app/prototype/serializeObject.js')
            //->addJs('/assets/js/app/init/datepicker.js')
            ->addJs('/assets/js/app/init/uniform.js')
        ;

        return $assets;
    }
}
