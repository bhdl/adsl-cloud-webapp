<?php

namespace App\ServiceProvider\Service\Admin;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class SearchBar
 * @package App\ServiceProvider\Service\Admin
 */
class SearchBar
    extends AbstractServiceProvider
{
    /**
     * @return \App\View\Helper\SearchBar
     */
    public function getService()
    {
        return new \App\View\Helper\SearchBar();
    }
}