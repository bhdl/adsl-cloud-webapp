<?php

namespace App\ServiceProvider\Service\Admin;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Breadcrumb
 * @package App\ServiceProvider\Service\Admin
 */
class Breadcrumb
    extends AbstractServiceProvider
{
    /**
     * @return \App\View\Helper\Breadcrumb
     */
    public function getService()
    {
        return new \App\View\Helper\Breadcrumb();
    }
}