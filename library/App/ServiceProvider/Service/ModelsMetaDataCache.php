<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class ModelsMetaDataCache
 * @package App\ServiceProvider\Service
 */
class ModelsMetaDataCache
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Mvc\Model\MetaData
     */
    public function getService()
    {
        $config = $this->di->get('config')->get('cache')->modelsMetaData;

        if ($config == null) {
            // ha nincs megadva a megfelelő config érték akkor nem folytatjuk
            return null;
        }

        $config = $config->toArray();

        $adapter = '\Phalcon\Mvc\Model\MetaData\\' . $config['adapter'];

        unset($config['adapter']);

        $metadata = new $adapter($config);

        return $metadata;
    }
}