<?php

namespace App\ServiceProvider\Service\Listener;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Acl
 * @package App\ServiceProvider\Service
 */
class Acl
    extends AbstractServiceProvider
{
    /**
     * @return \App\Event\Listener\Acl
     */
    public function getService()
    {
        $listener = new \App\Event\Listener\Acl();

        $this->di
            ->get('eventsManager')
            ->attach('dispatch', $listener)
        ;

        return $listener;
    }
}