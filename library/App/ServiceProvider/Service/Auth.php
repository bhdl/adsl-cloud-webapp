<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Auth
 * @package App\ServiceProvider\Service
 */
class Auth
    extends AbstractServiceProvider
{
    /**
     * @return \App\Auth
     */
    public function getService()
    {
        return new \App\Auth();
    }
}