<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class ModelsCache
 * @package App\ServiceProvider\Service
 */
class ModelsCache
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Cache\Backend
     */
    public function getService()
    {
        $config = $this->di->get('config')->get('cache')->models;

        if ($config == null) {
            return;
        }

        $frontend = '\Phalcon\Cache\Frontend\\' . $config->frontend;
        $frontend = new $frontend([
            'lifetime' => $config->lifetime
        ]);

        $cfg = $config->toArray();
        $backend = '\Phalcon\Cache\Backend\\' . $cfg['backend'];
        unset($cfg['backend'], $cfg['lifetime'], $cfg['frontend']);

        return new $backend($frontend, $cfg);
    }
}