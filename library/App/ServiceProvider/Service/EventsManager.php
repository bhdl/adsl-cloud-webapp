<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class EventsManager
 * @package App\ServiceProvider\Service
 */
class EventsManager
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Events\Manager
     */
    public function getService()
    {
        $eventsManager = new \Phalcon\Events\Manager();
        $eventsManager->enablePriorities(true);

        $this->di->setInternalEventsManager($eventsManager);

        $services = [
            'app'
            , 'loader'
            , 'db'
            , 'modelsManager'
            , 'dispatcher'
        ];

        foreach ($services as $service) {
            if ($s = $this->di->get($service)) {
                $s->setEventsManager($eventsManager);
            }
        }

        return $eventsManager;
    }
}