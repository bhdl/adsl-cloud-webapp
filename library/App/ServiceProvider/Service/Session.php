<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Session
 * @package App\ServiceProvider\Service
 */
class Session
    extends AbstractServiceProvider
{
    /**
     * @return \App\Session\Adapter\Database
     */
    public function getService()
    {
        $session = new \App\Session\Adapter\Database([
            'db' => $this->getDi()->get('db')
        ]);

        $session->start();

        return $session;
    }
}