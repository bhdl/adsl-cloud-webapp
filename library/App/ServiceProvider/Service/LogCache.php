<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class LogCache
 * @package App\ServiceProvider\Service
 */
class LogCache
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Cache\Backend
     */
    public function getService()
    {
        $config = $this->di->get('config')->get('cache')->log;

        if ($config != null) {

            $frontend = '\Phalcon\Cache\Frontend\\' . $config->frontend;
            $frontend = new $frontend([
                'lifetime' => $config->lifetime
            ]);

            $backend = '\Phalcon\Cache\Backend\\' . $config->backend;

            return new $backend($frontend, [
                'cacheDir' => $config->cacheDir
            ]);
        }

        return null;
    }
}