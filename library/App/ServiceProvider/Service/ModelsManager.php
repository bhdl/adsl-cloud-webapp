<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;
use Phalcon\Mvc\Model;

/**
 * Class ModelsManager
 * @package App\ServiceProvider\Service
 */
class ModelsManager
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Mvc\Model\Manager
     */
    public function getService()
    {
        $modelsManager = new \Phalcon\Mvc\Model\Manager();

        /**
         * @see https://docs.phalconphp.com/en/3.2/db-models#disabling-enabling-features
         */
        Model::setup([
            'exceptionOnFailedSave'  => true
//            , 'updateSnapshotOnSave' => false
        ]);

        return $modelsManager;
    }
}