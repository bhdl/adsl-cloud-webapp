<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;
use Phalcon\Exception;

/**
 * Class Translate
 * @package App\ServiceProvider\Service
 */
class Translate
    extends AbstractServiceProvider
{
    /**
     * @return mixed|\Phalcon\Translate\Adapter\NativeArray
     * @throws Exception
     */
    public function getService()
    {
        // translate config
        $config = $this->di->get('config')->get('translate');
        if ($config == null) {
            throw new Exception('A "translate" config nincs beállítva.');
        }

        $language = $this->di->get('auth')
            ->getIdentity()
            ->getInterfaceLanguage()
        ;

        $loader = new \App\Translate\Loader();

        // js messages
        $loader
            ->setJsMessagesEnabled($config->jsMessages->enabled)
            ->setJsMessagesPath($config->jsMessages->path)
            ->setJsMessagesUrl($config->jsMessages->url)
        ;

        // alapértelmezett nyelv
        return $loader->getAdapter($language);
    }
}