<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Acl
 * @package App\ServiceProvider\Service
 */
class Acl
    extends AbstractServiceProvider
{
    /**
     * @return \App\Acl
     */
    public function getService()
    {
        $acl = new \App\Acl();

        // php backend acl init
        $acl->getAcl();

        // js acl készítése, ha kész a dispatch folyamat
        $this->di
            ->get('eventsManager')
            ->attach('dispatch:afterDispatchLoop', function() use ($acl) {
                // js acl init
                $acl->buildJsAcl();
            })
        ;

        return $acl;
    }
}