<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Router
 * @package App\ServiceProvider\Service
 */
class Router
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Mvc\Router
     */
    public function getService()
    {
        $router = new \Phalcon\Mvc\Router();

        $router->removeExtraSlashes(true);

        $convertCallback = function($string) {
            return lcfirst(\Phalcon\Text::camelize($string));
        };

        /**
         * Api
         */
        $router->addGet(
            '/rest/v([0-9]+)/:controller'
            , [
                'module'       => 'rest'
                , 'version'    => 1
                , 'controller' => 2
                , 'action'     => 'view'
            ]
        )->convert('controller', $convertCallback);

        $router->addGet(
            '/rest/v([0-9]+)/:controller/:int'
            , [
                'module'       => 'rest'
                , 'version'    => 1
                , 'controller' => 2
                , 'action'     => 'view'
                , 'id'         => 3
            ]
        )->convert('controller', $convertCallback);

        $router->addPost(
            '/rest/v([0-9]+)/:controller'
            , [
                'module'       => 'rest'
                , 'version'    => 1
                , 'controller' => 2
                , 'action'     => 'create'
            ]
        )->convert('controller', $convertCallback);

        $router->addPut(
            '/rest/v([0-9]+)/:controller/:int'
            , [
                'module'       => 'rest'
                , 'version'    => 1
                , 'controller' => 2
                , 'action'     => 'modify'
                , 'id'         => 3
            ]
        )->convert('controller', $convertCallback);

        $router->addDelete(
            '/rest/v([0-9]+)/:controller/:params'
            , [
                'module'       => 'rest'
                , 'version'    => 1
                , 'controller' => 2
                , 'action'     => 'delete'
                , 'id'         => 3
            ]
        )->convert('controller', $convertCallback);

        /*
         * Admin
         */
        $router->add(
            '/admin'
            , [
                'module'       => 'admin'
                , 'controller' => 'index'
            ]
        );

        $router->add(
            '/admin/:controller'
            , [
                'module'       => 'admin'
                , 'controller' => 1
                , 'action'     => 'index'
            ]
        )->convert('controller', $convertCallback);

        $router->add(
            '/admin/:controller/:action'
            , [
                'module'       => 'admin'
                , 'controller' => 1
                , 'action'     => 2
            ]
        )->convert('action', $convertCallback)->convert('controller', $convertCallback);

        $router->add(
            '/admin/:controller/:action/:params'
            , [
                'module'       => 'admin'
                , 'controller' => 1
                , 'action'     => 2
                , 'params'     => 3
            ]
        )->convert('action', $convertCallback)->convert('controller', $convertCallback);

        $router->setDefaultModule('admin');

        return $router;
    }
}