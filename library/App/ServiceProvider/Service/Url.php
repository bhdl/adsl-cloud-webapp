<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Url
 * @package App\ServiceProvider\Service
 */
class Url
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Mvc\Url
     */
    public function getService()
    {
        $url = new \Phalcon\Mvc\Url();

        $baseUri = rtrim($this->di->get('config')->url->baseUri, '/\\');

        $url->setBaseUri($baseUri);

//        $url->setStaticBaseUri($baseUri);

        return $url;
    }
}