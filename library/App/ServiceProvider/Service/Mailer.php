<?php

namespace App\ServiceProvider\Service;

use App\Mailer\Manager;
use App\ServiceProvider\AbstractServiceProvider;
use Phalcon\Exception;

/**
 * Class Mailer
 * @package App\ServiceProvider\Service
 */
class Mailer
    extends AbstractServiceProvider
{
    /**
     * @return Manager
     * @throws Exception
     */
    public function getService()
    {
        $config = $this->di->get('config')->get('mailer');

        if ($config == null) {
            throw new Exception(
                'A \'mailer\' beállítások hiányoznak a konfigurációs állományból!'
            );
        }

        $mailer = new Manager(
            $config->toArray()
        );

        return $mailer;
    }
}