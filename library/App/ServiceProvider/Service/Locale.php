<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Locale
 * @package App\ServiceProvider\Service
 */
class Locale
    extends AbstractServiceProvider
{
    /**
     * @return null
     */
    public function getService()
    {
        $language = $this->di->get('auth')
            ->getIdentity()
            ->getInterfaceLanguage();

        // PHP locale
        $params = array_merge(
            [LC_ALL]
            , $this->localeAliases($language->locale)
        );

        call_user_func_array(
            'setlocale'
            , $params
        );

        // MySQL locale
        $this->di
            ->get('db')
            ->query(sprintf("SET lc_time_names = '%s';", $language->locale))
        ;
    }

    /**
     * @param string $locale
     * @return array
     */
    protected function localeAliases($locale)
    {
        $aliases = [$locale];

        $aliases[] = $locale . '.UTF-8';

        $aliases[] = $locale . '.utf-8';

        return $aliases;
    }
}