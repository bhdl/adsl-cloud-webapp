<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class ErrorHandler
 * @package App\ServiceProvider\Service
 */
class ErrorHandler
    extends AbstractServiceProvider
{
    /**
     * @return \App\ErrorHandler
     */
    public function getService()
    {
        $errorHandler = new \App\ErrorHandler();

        $errorHandler
            ->registerErrorHandler()
            ->registerShutdownHandler()
            ->registerExceptionHandler()
            ->setIsDebug($this->di->get('app')->isDebug())
        ;

        $errorHandler->setDisplayAdapter('App\\ErrorHandler\\Display\\Html');

        return $errorHandler;
    }
}