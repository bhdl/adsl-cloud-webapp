<?php

namespace App\ServiceProvider\Service;

use App\Application;
use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class App
 * @package App\ServiceProvider\Service
 */
class App
    extends AbstractServiceProvider
{
    /**
     * @return \App\Application
     */
    public function getService()
    {
        $app = new Application($this->di);

        $app->registerModules([
            'admin' => [
                'className' => 'App\Admin\Module',
                'path'      => APP_PATH . '/modules/admin/Module.php',
            ]
            , 'rest' => [
                'className' => 'App\Rest\Module',
                'path'      => APP_PATH . '/modules/rest/Module.php',
            ]
        ]);

        return $app;
    }
}