<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Flash
 * @package App\ServiceProvider\Service
 */
class Flash
    extends AbstractServiceProvider
{
    /**
     * @return \App\Flash\Direct
     */
    public function getService()
    {
        return new \App\Flash\Direct(\App\Flash\Direct::getCssClasses());
    }
}