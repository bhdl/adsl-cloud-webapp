<?php

namespace App\ServiceProvider\Service;

use App\Db\Adapter\MysqlLogged;
use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Db
 * @package App\ServiceProvider\Service
 */
class Db
    extends AbstractServiceProvider
{
    /**
     * @return MysqlLogged
     */
    public function getService()
    {
        $config  = $this->di
            ->get('config')
            ->get('db')
            ->toArray()
        ;

        $db = new MysqlLogged($config);

        return $db;
    }
}