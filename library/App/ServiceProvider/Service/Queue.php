<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

use Phalcon\Exception;

/**
 * Class Queue
 * @package App\ServiceProvider\Service
 */
class Queue
    extends AbstractServiceProvider
{
    /**
     * @return mixed
     * @throws Exception
     */
    public function getService()
    {
        // TODO: Phalcon\Queue\Beanstalk\Extended

        $config = $this->getDi()->get('config');

        if (!$config->queue) {
            throw new Exception(
                'A konfigurációs állomány nem tartalmazza a queue-ra vonatkozó kapcsolódási paramétereket'
            );
        }

        return new \Phalcon\Queue\Beanstalk(
            [
                'host' => $config->queue->host,
                'port' => $config->queue->port
            ]
        );
    }
}