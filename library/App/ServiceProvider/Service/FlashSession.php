<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class FlashSession
 * @package App\ServiceProvider\Service
 */
class FlashSession
    extends AbstractServiceProvider
{
    /**
     * @return \App\Flash\Session
     */
    public function getService()
    {
        return new \App\Flash\Session(\App\Flash\Direct::getCssClasses());
    }
}