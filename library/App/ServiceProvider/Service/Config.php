<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;
use Phalcon\Exception;

/**
 * Class Config
 * @package App\ServiceProvider\Service
 */
class Config
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Mvc\Router
     * @throws Exception
     */
    public function getService()
    {
        $app = $this->di->get('app');

        $baseFile = $app->getAppPath('config/base.php');

        if (!is_readable($baseFile)) {
            throw new Exception(
                'Nincs meg a konfigurációs állomány: ' . $baseFile
            );
        }

        $config = include_once $baseFile;

        if (!$config instanceof \Phalcon\Config) {
            throw new Exception(
                'Nem olvasható a base konfigurációs állomány. Phalcon\Config példányt várok.'
            );
        }

        $envFile = $app->getAppPath(
            'config/' . $app->getEnv() . '.php'
        );

        if (is_readable($envFile)) {
            $override = include_once $envFile;

            if (!$override instanceof \Phalcon\Config) {
                throw new Exception(
                    'Nem olvasható az env konfigurációs állomány. Phalcon\Config példányt várok.'
                );
            }

            $config->merge($override);
        }

        return $config;
    }
}