<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class DotEnv
 * @package App\ServiceProvider\Service
 */
class DotEnv
    extends AbstractServiceProvider
{
    /**
     * @return \Dotenv\Dotenv|mixed
     */
    public function getService()
    {
        $dotEnv = new \Dotenv\Dotenv(APP_PATH);
        $dotEnv->load();

        return $dotEnv;
    }
}