<?php

namespace App\ServiceProvider\Service;

use App\Logger\Adapter\Database;
use App\Logger\Adapter\Mail;
use App\ServiceProvider\AbstractServiceProvider;
use Phalcon\Exception;
use Phalcon\Logger\Multiple;

/**
 * Class Logger
 * @package App\ServiceProvider\Service
 */
class Logger
    extends AbstractServiceProvider
{
    /**
     * @return Multiple
     * @throws Exception
     */
    public function getService()
    {
        $config = $this->di->get('config')->logger;

        // kompozit logot használunk
        $logger = new Multiple();

        // először az adatbázis adaptert adjuk hozzá
        $dbAdapter = new Database($config->database->name, [
            'db'      => $this->di->get('db')
            , 'table' => $config->database->table
        ]);

        if ($config->database->logLevel) {
            $dbAdapter->setLogLevel($config->database->logLevel);
        }

        $dbAdapter->setSkipModelAttributes(['password', 'passwordRepeat']);

        $logger->push($dbAdapter);

        // aztán a mail loggert, ha nem dev módban vagyunk
        if ($config->mail->enabled) {

            $mailer = $this->di->get('mailer');
            $logCache = $this->di->get('logCache');

            if ($mailer == null
                || $logCache == null
            ) {
                throw new Exception(
                    'A \'mailer\' és \'logCache\' szolgáltatások nincsenek beállítva a DI-ben!'
                );
            }

            // mail adapter
            $mailAdapter = new Mail(
                $config->mail->application
                , $logCache
                , $mailer
            );

            if ($config->mail->logLevel) {
                $mailAdapter->setLogLevel($config->mail->logLevel);
            }

            if ($config->mail->to) {
                $mailAdapter->setTo($config->mail->to);
            }

            if ($config->mail->toName) {
                $mailAdapter->setToName($config->mail->toName);
            }

            $logger->push($mailAdapter);
        }

        return $logger;
    }
}