<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Dispatcher
 * @package App\ServiceProvider\Service
 */
class Dispatcher
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Mvc\Dispatcher
     */
    public function getService()
    {
        $dispatcher = new \Phalcon\Mvc\Dispatcher();
        /*
        $dispatcher->setDefaultNamespace('App\Lab');
        $dispatcher->setDefaultController('index');
        $dispatcher->setDefaultAction('index');*/

        return $dispatcher;
    }
}