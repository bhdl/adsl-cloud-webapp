<?php

namespace App\ServiceProvider\Service\Rest;

/**
 * Class View
 * @package App\ServiceProvider\Service\Rest
 */
class View
    extends \App\ServiceProvider\Service\View
{
    /**
     * @return \Phalcon\Mvc\View
     */
    public function getService()
    {
        return parent::getService()->disable();
    }
}
