<?php

namespace App\ServiceProvider\Service\Rest;

/**
 * Class ErrorHandler
 * @package App\ServiceProvider\Service\Rest
 */
class ErrorHandler
    extends \App\ServiceProvider\Service\ErrorHandler
{
    /**
     * @return \App\ErrorHandler
     */
    public function getService()
    {
        // felülírjuk a display adaptert
        $errorHandler = $this->getDi()->get('errorHandler');
        $errorHandler->setDisplayAdapter('App\\ErrorHandler\\Display\\Json');

        return null;
    }
}
