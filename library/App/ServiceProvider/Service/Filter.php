<?php

namespace App\ServiceProvider\Service;

use App\Filter\Currency;
use App\Filter\Date;
use App\Filter\DateInterval;
use App\Filter\Datetime;
use App\Filter\Number;
use App\Filter\Percent;
use App\Filter\StringClean;
use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class Filter
 * @package App\ServiceProvider\Service
 */
class Filter
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Filter
     */
    public function getService()
    {
        $filter = new \Phalcon\Filter();

        $filter->add('date', new Date());
        $filter->add('datetime', new Datetime());
        $filter->add('dateInterval', new DateInterval());
        $filter->add('number', new Number());
        $filter->add('percent', new Percent());
        $filter->add('currency', new Currency());
        $filter->add('string', new StringClean());


        return $filter;
    }
}