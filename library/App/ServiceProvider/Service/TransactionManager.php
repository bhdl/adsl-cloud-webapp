<?php

namespace App\ServiceProvider\Service;

use App\ServiceProvider\AbstractServiceProvider;

/**
 * Class TransactionManager
 * @package App\ServiceProvider\Service
 */
class TransactionManager
    extends AbstractServiceProvider
{
    /**
     * @return \Phalcon\Mvc\Model\Transaction\Manager
     */
    public function getService()
    {
        return new \Phalcon\Mvc\Model\Transaction\Manager();
    }
}