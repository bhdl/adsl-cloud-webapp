<?php

namespace App\ErrorHandler\Display;

/**
 * Class Text
 * @package App\ErrorHandler\Display
 */
class Text
{
    /**
     * Hibaüzenet elkészítése
     * @return string
     */
    public function getResponse()
    {
        $text = "Sorry, an unexpected error occurred:\t{$this->error}";

        if (property_exists($this, 'message')) {
            $text .= "\nException:\t{$this->message}";
            $text .= "\nCode:\t{$this->code}";
            $text .= "\nFile:\t{$this->file}";
            $text .= "\nLine:\t{$this->line}";
            $text .= "\nStack trace:\n\n";
            $text .= $this->traceAsString;
        }


        return $text;
    }
}