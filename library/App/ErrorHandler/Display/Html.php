<?php

namespace App\ErrorHandler\Display;

/**
 * Class Html
 * @package App\Errorhandler\Display
 */
class Html
{
    /**
     * Hibaüzenet elkészítése
     * @return string
     */
    public function getResponse()
    {
        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="hu-HU" />
	<base href="/">
	<link rel="stylesheet" type="text/css" href="/assets/css/fonts.css" />
	<title>ADSL - Váratlan hiba / Unexpected error</title>
</head>
<body style="margin: 0;font-family: \'Roboto\';">

        <h1 style="margin-left: 10px;font-size: 20px;">
       		Sajnáljuk, váratlan hiba történt. / Sorry, an unexpected error occurred.
        </h1>
 
        <table cellpadding="5" cellspacing="0" width="50%" style="margin-left: 10px;">

        <tr style="background-color: #58585A;color: #fff;border: 1px solid #000;">
	        <th align="left">
	        	' . $this->error . '
	        </th>
        </tr>
        <tr>
	        <td colspan="2" align="left" style="border: 1px solid rgb(135, 135, 135);color: rgb(48, 48, 48);border-right: 1px solid rgb(135, 135, 135);">';

        if (property_exists($this, 'message')) {
            $html .=  '
            <p><b>Üzenet/ message: ' . $this->message . '</b></p>
            <p>Kód/ code: ' . $this->code . '</p>
            <p>Fájl/ file: ' . $this->file . '</p>
            <p>Sor/ line: ' . $this->line . '</p>
            <pre>' . $this->traceAsString . '</pre>';

            
        }

			$html .= '</td>
        </tr>
        </table>
    </body>
</html>';

        return $html;
    }
}