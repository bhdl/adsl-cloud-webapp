<?php

namespace App\ErrorHandler\Display;

/**
 * Class Json
 * @package App\ErrorHandler\Display
 */
class Json
{
    /**
     * @return string
     */
    public function getResponse()
    {
        header('Content-type: text/json');

        $json = [
            'error' => [
                'code' => 'ERR_INTERNAL'
            ]
        ];

        if (property_exists($this, 'message')) {
            $json['error']['title']   = 'Internal Server Error';
            $json['error']['message'] = $this->message;
            $json['error']['file']    = $this->file;
            $json['error']['line']    = $this->line;
        }

        return json_encode($json, JSON_PRETTY_PRINT);
    }
}
