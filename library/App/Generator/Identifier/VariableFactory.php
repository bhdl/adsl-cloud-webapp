<?php

namespace App\Generator\Identifier;

use App\Generator\Identifier\Variable\AbstractVariable;
use Phalcon\Exception;

/**
 * Class VariableFactory
 * @package App\Generator\Identifier
 */
class VariableFactory
{
    /**
     * Teljes évszám, pl. "2017
     * @var string
     */
    const VARIABLE_YEAR = 'year';

    /**
     * Rövidített évszám, pl. "17" (2017).
     * @var string
     */
    const VARIABLE_YEAR_SHORT = 'yearShort';

    /**
     * Egyszerű szekvencia, x-ről indul és nem befolyásolja semmilyen paraméter.
     *
     * @var string
     */
    const VARIABLE_SEQUENCE = 'sequence';

    /**
     * Év szekvencia, évenként x-ről indul.
     *
     * @var string
     */
    const VARIABLE_SEQUENCE_YEAR = 'sequenceYear';

    /**
     * Napi sorszám szekvencia, x-ről indul.
     *
     * @var string
     */
    const VARIABLE_SEQUENCE_DAILY_NUMBER = 'sequenceDailyNumber';

    /**
     * @var array
     */
    public static $variables = [
        self::VARIABLE_YEAR                    => 'Év (teljes, pl. "2017")'
        , self::VARIABLE_YEAR_SHORT            => 'Év (rövidített, pl. "17")'
        , self::VARIABLE_SEQUENCE              => 'Sorszám (globális, x-ről indul)'
        , self::VARIABLE_SEQUENCE_YEAR         => 'Sorszám (évenként eltér, x-ről indul)'
        , self::VARIABLE_SEQUENCE_DAILY_NUMBER => 'Napi sorszám'
    ];

    /**
     * Variable gyártó
     *
     * @param string $key
     * @return AbstractVariable
     * @throws Exception
     */
    public static function factory($key)
    {
        $variableClass = __NAMESPACE__ . '\\Variable\\' . ucfirst($key);

        if (!class_exists($variableClass)) {
            throw new Exception('Nincs ilyen számlasorszám változó: ' . $key);
        }

        return new $variableClass();
    }

    /**
     * @return array
     */
    public static function getVariables(): array
    {
        return self::$variables;
    }

    /**
     * @param array $variables
     */
    public static function setVariables(array $variables)
    {
        self::$variables = $variables;
    }
}