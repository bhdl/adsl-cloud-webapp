<?php

namespace App\Generator\Identifier;

use App\Model\IdentifierPattern;
use App\Model\Sequence;

/**
 * Class InvoicePayment
 * @package App\Generator\Identifier
 */
class Referral
    extends AbstractIdentifier
{
    /**
     * @var IdentifierPattern|null
     */
    protected $identifierPattern;

    /**
     * @param array $options
     * @return mixed|string
     * @throws \Phalcon\Exception
     */
    public function generate($options = [])
    {
        $this->identifierPattern = IdentifierPattern::createReferralDailyNumber();
        $options['sequenceType'] = Sequence::SEQUENCE_TYPE_REFERRAL_DAILY_NUMBER;
        $options['year']         = date('Y', strtotime($this->model->arrialDateAt));

        $result = $this->assemble($options);

        return $result;
    }

    /**
     * @param array $options
     * @return mixed|string
     * @throws \Phalcon\Exception
     */
    protected function assemble($options = [])
    {
        $assembled = $this->identifierPattern->pattern;

        $options += [
            'sequenceLength' => $this->identifierPattern->sequenceLength
        ];

        // változók cseréje
        foreach (VariableFactory::getVariables() as $key => $value) {
            $searchKey = "%{$key}%";
            if (mb_strpos($assembled, $searchKey) === false) {
                continue;
            }

            // variable class elkészítése
            $variable = VariableFactory::factory($key);

            // csere
            $assembled = str_replace(
                $searchKey
                , $variable->getValue($options)
                , $assembled
            );
        }

        return $assembled;
    }

    /**
     * @return IdentifierPattern|null
     */
    public function getIdentifierPattern()
    {
        return $this->identifierPattern;
    }

    /**
     * @param IdentifierPattern $identifierPattern
     * @return Referral
     */
    public function setIdentifierPattern(IdentifierPattern $identifierPattern): Referral
    {
        $this->identifierPattern = $identifierPattern;
        return $this;
    }
}