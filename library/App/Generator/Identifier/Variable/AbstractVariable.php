<?php

namespace App\Generator\Identifier\Variable;

/**
 * Class AbstractVariable
 * @package App\Generator\Identifier\Variable
 */
abstract class AbstractVariable
{
    /**
     * @param array $options
     * @return string
     */
    abstract public function getValue($options = []);
}