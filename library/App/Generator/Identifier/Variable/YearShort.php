<?php

namespace App\Generator\Identifier\Variable;

/**
 * Class YearShort
 * @package App\Generator\Identifier\Variable
 */
class YearShort
    extends AbstractVariable
{
    /**
     * @param array $options
     * @return string
     */
    public function getValue($options = [])
    {
        return mb_strcut(
            $options['year']
            , 2
            , 2
        );
    }
}