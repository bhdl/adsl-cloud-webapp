<?php

namespace App\Generator\Identifier\Variable;

use App\Model\Sequence as SequenceModel;

/**
 * Class Sequence
 * @package App\Generator\Identifier\Variable
 */
class Sequence
    extends AbstractVariable
{
    /**
     * @param array $options
     * @return string
     */
    public function getValue($options = [])
    {
        $sequence = SequenceModel::findSequence(
            $options['sequenceType']
            , null
            , array_key_exists('dryRun', $options) ? $options['dryRun'] : false
        )->sequence;

        return str_pad(
            $sequence
            , $options['sequenceLength']
            , 0
            , STR_PAD_LEFT
        );
    }
}