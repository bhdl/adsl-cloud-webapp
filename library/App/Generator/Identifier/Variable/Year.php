<?php

namespace App\Generator\Identifier\Variable;

/**
 * Class Year
 * @package App\Generator\Identifier\Variable
 */
class Year
    extends AbstractVariable
{
    /**
     * @param array $options
     * @return string
     */
    public function getValue($options = [])
    {
        return $options['year'];
    }
}