<?php

namespace App\Generator\Identifier\Variable;

use App\Model\Sequence;

/**
 * Class SequenceYear
 * @package App\Generator\Identifier\Variable
 */
class SequenceYear
    extends AbstractVariable
{
    /**
     * @param array $options
     * @return string
     */
    public function getValue($options = [])
    {
        $sequence = Sequence::findSequence(
            $options['sequenceType']
            , $options['year']
            , array_key_exists('dryRun', $options) ? $options['dryRun'] : false
        )->sequence;

        return str_pad(
            $sequence
            , $options['sequenceLength']
            , 0
            , STR_PAD_LEFT
        );
    }
}