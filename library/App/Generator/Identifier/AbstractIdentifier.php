<?php

namespace App\Generator\Identifier;

use App\Model\AbstractModel;

/**
 * Interface IdentifierInterface
 * @package App\Generator\Identifier
 */
abstract class AbstractIdentifier
{
    /**
     * @var AbstractModel
     */
    protected $model;

    /**
     * AbstractIdentifier constructor.
     * @param AbstractModel $model
     */
    public function __construct(AbstractModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param array
     * @return string
     */
    abstract public function generate($options = []);

    /**
     * @return AbstractModel
     */
    public function getModel(): AbstractModel
    {
        return $this->model;
    }

    /**
     * @param AbstractModel $model
     * @return AbstractIdentifier
     */
    public function setModel(AbstractModel $model): AbstractIdentifier
    {
        $this->model = $model;
        return $this;
    }
}