<?php

namespace App\Export\Pdf;

use mikehaertl\wkhtmlto\Pdf;
use Phalcon\Exception;
use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\View;

/**
 * Class AbstractTemplate
 * @package App\Export\Pdf
 */
abstract class AbstractTemplate
    extends Component
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @var string
     */
    protected $layout;

    /**
     * A "pdf" mappán belüli elérési út
     *
     * @var string
     */
    protected $layoutDirectory;

    /**
     * A "pdf" mappán belüli elérési út
     *
     * @var string
     */
    protected $viewDirectory;

    /**
     * @var string
     */
    protected $temporaryDirectory = 'workspace/tmp';

    /**
     * @var string
     */
    protected $temporaryFilePrefix = 'template_';

    /**
     * @var array
     */
    protected $temporaryFiles = [];

    /**
     * @var string
     */
    protected $fileName = 'export.pdf';

    /**
     * Változók (layout, template)
     * @return array
     */
    abstract public function getViewVars();

    /**
     * @return string
     */
    abstract public function getName();

    /**
     * @param Pdf $pdf
     * @return Pdf
     */
    abstract public function preparePdf(Pdf $pdf);

    /**
     * Konstruktor
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * init
     */
    public function init()
    {
    }

    /**
     * @return Pdf
     * @throws \Exception
     */
    public function getPdf()
    {
        $files = $this->getFiles();

        // alap beállítások
        $pdf = new Pdf;
        $pdf->addPage($files['body']);

        // header&footer
        $options = [];
        if (isset($files['footer'])) {
            $options['footer-html'] = $files['footer'];
        }

        if (isset($files['header'])) {
            $options['header-html'] = $files['header'];
        }

        $pdf->setOptions($options);

        // egyedi beállítások
        $this->preparePdf($pdf);

        return $pdf;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getFiles()
    {
        return $this->createTemporaryFiles([
            'body'     => $this->getViewDirectory()
            , 'header' => $this->getViewDirectory()
            , 'footer' => $this->getViewDirectory()
        ]);
    }

    /**
     * @param array       $template
     * @param string|null $directory
     * @return string
     * @throws Exception
     */
    public function createTemporaryFile($template, $directory = null)
    {
        $temporaryFile = @tempnam(
            $this->app->getAppPath($this->temporaryDirectory)
            , $this->temporaryFilePrefix
        );

        if ($temporaryFile === false
            || rename($temporaryFile, $temporaryFile . '.html') === false
        ) {
            throw new Exception(
                'Nem sikerült a sablon állomány elnevezése'
            );
        }

        $temporaryFile .= '.html';

        if (@file_put_contents($temporaryFile, $this->render($template, $directory)) === false) {
            throw new Exception(
                'Nem sikerült a sablon állomány létrehozása'
            );
        }

        $this->temporaryFiles[] = $temporaryFile;

        return $temporaryFile;
    }

    /**
     * @param array $templates
     * @return array
     * @throws Exception
     */
    public function createTemporaryFiles(array $templates)
    {
        $rendered = [];

        foreach ($templates as $template=>$directory) {
            $rendered[$template] = $this->createTemporaryFile(
                $template
                , $directory
            );
        }

        return $rendered;
    }

    /**
     * @param string $template
     * @param string $templateDirectory
     * @return string
     */
    public function render($template, $templateDirectory = null)
    {
        // init view
        $view = $this->getView();

        // layout és template view render
        return $view->getRender($this->getLayoutDirectory(), $this->getLayout(), array_merge(
            [
                'content' => $view->getRender($templateDirectory, $template, $this->getViewVars())
                , 'name'  => $this->getName()
            ]
            , $this->getViewVars()
        ));
    }

    /**
     * @return View
     */
    public function getView()
    {
        if ($this->view == null) {
            $this->view = new View;
            $this->view->setViewsDir(
                $this->app->getAppPath('modules/admin/views/pdf')
            );
        }

        return $this->view;
    }

    /**
     * @param View $view
     * @return AbstractTemplate
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        if ($this->layout == null) {
            $this->layout = 'default';
        }

        return $this->layout;
    }

    /**
     * @param string $layout
     * @return AbstractTemplate
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * @return string
     */
    public function getLayoutDirectory()
    {
        if ($this->layoutDirectory == null) {
            $this->layoutDirectory = 'layouts';
        }

        return $this->layoutDirectory;
    }

    /**
     * @param string $layoutDirectory
     * @return AbstractTemplate
     */
    public function setLayoutDirectory($layoutDirectory)
    {
        $this->layoutDirectory = $layoutDirectory;
        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return AbstractTemplate
     */
    public function setFileName(string $fileName): AbstractTemplate
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return string
     */
    public function getViewDirectory(): string
    {
        return $this->viewDirectory;
    }

    /**
     * @param string $viewDirectory
     * @return AbstractTemplate
     */
    public function setViewDirectory(string $viewDirectory): AbstractTemplate
    {
        $this->viewDirectory = $viewDirectory;
        return $this;
    }
}