<?php

namespace App\Export\Pdf\Template;

/**
 * Class User
 * @package App\Export\Pdf\Template
 */
class User
    extends AbstractResultSet
{
    /**
     * @return mixed
     */
    public function init()
    {
        $this->setFileName('user.pdf');

        $this->setColumns([
            'groupName'          => ['label' => 'Csoport']
            , 'name'             => ['label' => 'Név']
            , 'email'            => ['label' => 'E-mail cím']
            , 'active'           => ['label' => 'Aktív']
        ]);

        $this->setPresenter((new \App\Model\Presenter\User));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->translate->t('Felhasználók');
    }
}
