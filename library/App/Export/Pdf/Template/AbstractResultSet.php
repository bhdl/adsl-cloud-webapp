<?php

namespace App\Export\Pdf\Template;

use App\Export\Pdf\AbstractTemplate;
use App\Model\Presenter\AbstractPresenter;
use mikehaertl\wkhtmlto\Pdf;

/**
 * Class AbstractResultSet
 * @package App\Export\Pdf\Template
 */
abstract class AbstractResultSet
    extends AbstractTemplate
{
    /**
     * @var \Phalcon\Mvc\Model\Query\Builder
     */
    protected $builder;

    /**
     * @var array
     */
    protected $columns = [];

    /**
     * @var AbstractPresenter|null
     */
    protected $presenter;

    /**
     * @var string
     */
    protected $fileName = 'export.pdf';

    /**
     * A "pdf" mappán belüli elérési út
     *
     * @var string
     */
    protected $viewDirectory = 'resultSet';

    /**
     * @return array
     */
    public function getViewVars()
    {
        return [
            'template' => $this
        ];
    }

    /**
     * @return array|null
     */
    public function prepareRows()
    {
        $rows = $this->getBuilder()->getQuery()->execute()->toArray();

        // presenter
        if ($this->getPresenter()) {
            $rows = $this
                ->getPresenter()
                ->setRows($rows)
                ->present()
            ;
        }

        return $rows;
    }

    /**
     * @return \Phalcon\Mvc\Model\Query\Builder
     */
    public function getBuilder(): \Phalcon\Mvc\Model\Query\Builder
    {
        return $this->builder;
    }

    /**
     * @param \Phalcon\Mvc\Model\Query\Builder $builder
     * @return AbstractResultSet
     */
    public function setBuilder(\Phalcon\Mvc\Model\Query\Builder $builder)
    {
        $this->builder = $builder;

        return $this;
    }

    /**
     * @param Pdf $pdf
     * @return void
     */
    public function preparePdf(Pdf $pdf)
    {
        $pdf->setOptions([
            'margin-top'    => 0,
            'margin-right'  => 0,
//            'margin-bottom' => 0, // footer nem látszik, ha ez meg van adva
            'margin-left'   => 0,
        ]);
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     * @return AbstractResultSet
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
        return $this;
    }

    /**
     * @param $column
     * @param $key
     * @param $value
     * @return AbstractResultSet
     */
    public function setColumnAttribute($column, $key, $value): AbstractResultSet
    {
        if (array_key_exists($column, $this->columns)) {
            $this->columns[$column][$key] = $value;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getColumnKeys()
    {
        return array_keys($this->getColumns());
    }

    /**
     * @param array $columns
     * @return AbstractResultSet
     */
    public function orderColumnsByKeys($columns)
    {
        $ordered = [];

        foreach ($columns as $column) {
            if (!array_key_exists($column, $this->columns)) {
                continue;
            }

            $ordered[$column] = $this->columns[$column];
        }

        return $this->setColumns($ordered);
    }

    /**
     * @param $row
     * @return array
     */
    public function orderColumnsInRow($row)
    {
        $ordered = [];

        foreach ($this->getColumnKeys() as $key) {
            if (array_key_exists($key, $row)) {
                $ordered[$key] = $row[$key];
            }
        }

        return $ordered;
    }

    /**
     * @return AbstractPresenter
     */
    public function getPresenter()
    {
        return $this->presenter;
    }

    /**
     * @param AbstractPresenter $presenter
     * @return AbstractResultSet
     */
    public function setPresenter(AbstractPresenter $presenter): AbstractResultSet
    {
        $this->presenter = $presenter;
        return $this;
    }
}