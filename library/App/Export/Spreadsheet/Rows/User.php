<?php

namespace App\Export\Spreadsheet\Rows;

use App\Export\Spreadsheet\Rows;

/**
 * Class User
 * @package App\Export\Spreadsheet\Rows
 */
class User
    extends Rows
{
    /**
     * init
     */
    public function init()
    {
        $this->setFileName('users-export.xlsx');

        $this->setColumns([
            'groupName'          => ['label' => 'Csoport']
            , 'name'             => ['label' => 'Név']
            , 'email'            => ['label' => 'E-mail cím']
            , 'active'           => ['label' => 'Aktív']
        ]);

        $this->setPresenter((new \App\Model\Presenter\User)->setUseHtmlMarkup(false));

        parent::init();
    }
}