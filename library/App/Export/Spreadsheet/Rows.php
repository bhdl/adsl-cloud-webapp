<?php

namespace App\Export\Spreadsheet;

use App\Export\AbstractSpreadsheet;
use App\Model\Presenter\AbstractPresenter;
use Phalcon\Exception;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class Rows
 * @package App\Export
 */
class Rows
    extends AbstractSpreadsheet
{
    /**
     * @var array
     */
    protected $rows = [];

    /**
     * @var \Phalcon\Mvc\Model\Query\BuilderInterface|null
     */
    protected $builder;

    /**
     * Fejléc adatok
     *
     * Pl. array('name' => array('label' => 'Megnevezés', ...), 'email' => array(...))
     *
     * Használható kulcsok (szabadon bővíthető):
     *
     * - label
     * - format
     *
     * @var array
     */
    protected $columns = [];

    /**
     * Automatikus oszlop szélesség beállítás az oszlop tartalma alapján
     *
     * @var bool
     */
    protected $autoColumnsWidth = false;

    /**
     * @var AbstractPresenter
     */
    protected $presenter;

    /**
     * @throws Exception
     * @return Rows
     */
    public function prepare()
    {
        parent::prepare();

        if (!is_array($this->rows) && $this->builder == null) {
            throw new Exception('Állítsd be az adatokat az exportálás előtt -> setRows() vagy setBuilder()');
        }

        if (!is_array($this->columns) || count($this->columns) == 0) {
            throw new Exception('Állítsd be a fejléc definíciót az exportálás előtt -> setColumns()');
        }

        return $this;
    }

    /**
     * @param Worksheet $worksheet
     * @return Rows
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function addColumnsToWorksheet($worksheet)
    {
        $worksheet->fromArray(
            $this->getColumnLabels()
        );

        return $this;
    }

    /**
     * @param Worksheet $worksheet
     * @param $rows
     * @return Rows
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function addRowsToWorksheet($worksheet, $rows)
    {
        foreach ($rows as $rowNum=>$row) {
            $this->addRowToWorksheet($worksheet, $row, $rowNum + 2);
        }

        return $this;
    }

    /**
     * @param Worksheet $worksheet
     * @param array $row
     * @param int $rowNum
     * @return Rows
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function addRowToWorksheet($worksheet, $row, $rowNum)
    {
        // megfelelő sorrend beállítása
        $orderedRow = $this->orderColumnsInRow($row);

        // hiányzó mező esetén kivétel
        $this->checkMissingColumnsInRow(
            $orderedRow
        );

        $worksheet->fromArray(
            $orderedRow
            , null
            , 'A' . $rowNum
        );

        return $this;
    }

    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function export()
    {
        $worksheet = $this->getSpreadsheet()->getActiveSheet();

        $this->addColumnsToWorksheet($worksheet);

        // presenter
        $rows = $this->getRowsOrBuilderAsArray();
        if ($this->getPresenter()) {
            $rows = $this
                ->getPresenter()
                ->setRows($rows)
                ->present()
            ;
        }

        $this->addRowsToWorksheet($worksheet, $rows);

        // automatikus mezőszélességek
        if ($this->isAutoColumnsWidth()) {
            $this->setColumnsWidth($worksheet, $rows);
        }

        $this->setColumnsFormat($worksheet);

        return parent::export();
    }

    /**
     * Az order tömbben megadott értékeknek megfelelően rakja sorba a line
     * paraméterben kapott adatokat kulcsuk szerint.
     * Az eredményben nem fognak szerepelni azok a mezők (kulcsok), amik nem
     * szerepelnek az order tömbben.
     *
     * @param array[fieldName] => value $line - asszociativ tömb mezőnév=>érték párosokkal
     * @param array[0..n] => fieldName $order - a mezőnevek a kívánt sorrendben
     * @return array
     * @returns array[fieldName] => value - order szerint rendezett mezőnév=>érték párosok
     */
    protected function orderColumnsInRow($row)
    {
        $outLine = [];

        foreach ($this->getColumnKeys() as $fieldName) {
            if (array_key_exists($fieldName, $row)) {
                $outLine[$fieldName] = $row[$fieldName];
            }
        }

        return $outLine;
    }

    /**
     * Ellenőrzi, hogy a line tömbben létezik-e az összes mező (kulcs) a fields tömb kulcsai közül
     * Kivételt dob, ha valamelyik mező nem létezik.
     * @param array[fieldName] $row
     * @return Rows
     * @throws Exception
     */
    protected function checkMissingColumnsInRow($row)
    {
        $columns = array_keys(array_diff_key($this->getColumns(), $row));

        if (count($columns) > 0) {
            throw new Exception(sprintf(
                'Hiányzó oszlop(ok): %s'
                , implode(', ', $columns)
            ));
        }

        return $this;
    }

    /**
     * @param Worksheet $worksheet
     * @return Rows
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function setColumnsFormat($worksheet)
    {
        // number format
        $columnFormat = $this->getColumnFormat();
        if (count($columnFormat) > 0) {
            foreach ($columnFormat as $colNumber=>$format) {
                $pCellCoordinate = Coordinate::stringFromColumnIndex($colNumber);

                $worksheet
                    ->getStyle($pCellCoordinate . ':' . $pCellCoordinate)
                    ->getNumberFormat()
                    ->setFormatCode($format)
                ;
            }
        }

        return $this;
    }

    /**
     * Auto szélesség állítás
     *
     * @param Worksheet $worksheet
     * @param array $rows
     * @return Rows
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    protected function setColumnsWidth($worksheet, $rows)
    {
        // automatikus igazítás
        if (count($rows) > 0) {
            $stringLengths = array();
            foreach ($rows as $d) {
                foreach ($d as $column=>$v) {
                    if (!array_key_exists($column, $stringLengths)) {
                        $stringLengths[$column] = array();
                    }
                    if (mb_strpos($v, "\n") !== false) {
                        // amiben sortörés van, azt hagyjuk
                        continue;
                    }
                    $stringLengths[$column][] = mb_strlen($v);
                }
            }

            $i = 0;
            foreach ($this->getColumnKeys() as $column) {
                $i++;
                if (!array_key_exists($column, $stringLengths)) {
                    continue;
                }

                $width = max($stringLengths[$column]);

                if ($width < 10) {
                    $width = 10;
                }

                $worksheet
                    ->getColumnDimension(Coordinate::columnIndexFromString($i))
                    ->setWidth($width)
                ;
            }
        }

        return $this;
    }

    /**
     * Beállított column-ből lekérédezi az oszlop kulcsokat
     * @return array
     */
    public function getColumnKeys()
    {
        return array_keys($this->columns);
    }

    /**
     * Beállított column-ből lekérédezi az oszlopneveket
     * @return array
     */
    public function getColumnLabels()
    {
        $labels = [];

        foreach ($this->getColumnOption('label') as $label) {
            $labels[] = $this->translate->_($label);
        }

        return $labels;
    }

    /**
     * @return array
     */
    public function getColumnFormat()
    {
        return $this->getColumnOption('format');
    }

    /**
     * @param $option
     * @return array
     */
    protected function getColumnOption($option)
    {
        $options = [];

        $i = 0;
        foreach ($this->columns as $column) {
            if (isset($column[$option])) {
                $options[$i] = $column[$option];
            }

            $i++;
        }

        return $options;
    }

    /**
     * @param array $columns
     * @return Rows
     */
    public function orderColumnsByKeys($columns)
    {
        $ordered = [];

        foreach ($columns as $column) {
            if (!array_key_exists($column, $this->columns)) {
                continue;
            }

            $ordered[$column] = $this->columns[$column];
        }

        return $this->setColumns($ordered);
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     * @return Rows
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @return array
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * @param array $rows
     * @return Rows
     */
    public function setRows($rows)
    {
        $this->rows = $rows;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isAutoColumnsWidth()
    {
        return $this->autoColumnsWidth;
    }

    /**
     * @param boolean $autoColumnsWidth
     * @return Rows
     */
    public function setAutoColumnsWidth($autoColumnsWidth)
    {
        $this->autoColumnsWidth = $autoColumnsWidth;

        return $this;
    }

    /**
     * @return null|\Phalcon\Mvc\Model\Query\BuilderInterface
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * @param null|\Phalcon\Mvc\Model\Query\BuilderInterface $builder
     * @return Rows
     */
    public function setBuilder(\Phalcon\Mvc\Model\Query\BuilderInterface $builder): Rows
    {
        $this->builder = $builder;
        return $this;
    }

    /**
     * @return AbstractPresenter
     */
    public function getPresenter()
    {
        return $this->presenter;
    }

    /**
     * @param AbstractPresenter $presenter
     * @return Rows
     */
    public function setPresenter(AbstractPresenter $presenter): Rows
    {
        $this->presenter = $presenter;

        return $this;
    }

    /**
     * @return array
     */
    protected function getRowsOrBuilderAsArray()
    {
        if ($this->getBuilder()) {
            return $this
                ->getBuilder()
                ->getQuery()
                ->execute()
                ->toArray()
            ;
        }

        return $this->rows;
    }
}