<?php

namespace App\Export\Spreadsheet;

use App\Export\AbstractSpreadsheet;
use PhpOffice\PhpSpreadsheet\Shared\StringHelper;

/**
 * Class Csv
 * @package App\Export
 */
class Csv
    extends AbstractSpreadsheet
{
    /**
     * @var \PhpOffice\PhpSpreadsheet\Writer\Csv
     */
    protected $writer;

    /**
     * init
     */
    public function init()
    {
        // CSV formátum beállítása a writernek
        $this->setWriterType(self::WRITER_TYPE_CSV);

        // alapértelmezett CSV értékek
        $this->getWriter()
            ->setUseBOM(true)
            ->setLineEnding("\r\n")
            ->setEnclosure('')
            ->setDelimiter(';')
            ->setPreCalculateFormulas(false)
        ;

        // szám elválasztó
        StringHelper::setDecimalSeparator('.');
        StringHelper::setThousandsSeparator('');
    }
}