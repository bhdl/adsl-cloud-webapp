<?php

namespace App\Export;

use Phalcon\Exception;
use Phalcon\Mvc\User\Component;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Settings;

/**
 * Class AbstractSpreadsheet
 * @package App
 */
abstract class AbstractSpreadsheet
    extends Component
{
    /**
     * \PhpOffice\PhpSpreadsheet\Spreadsheet instance
     * @var \PhpOffice\PhpSpreadsheet\Spreadsheet
     */
    protected $spreadsheet;

    /**
     * \PhpOffice\PhpSpreadsheet\Spreadsheet writer instance
     * @var \PhpOffice\PhpSpreadsheet\Writer\IWriter
     */
    protected $writer;

    /**
     * Fájlrendszerbe helyezzük az eredményt
     * @var string
     */
    const DESTINATION_FILE = 'file';

    /**
     * Átadjuk a böngészőnek az eredményt
     * @var string
     */
    const DESTINATION_BROWSER = 'browser';

    /**
     * Hova menjen az eredmény?
     * @var string
     */
    protected $destination = self::DESTINATION_BROWSER;

    /**
     * Fájlrendszerbe mentés esetén a fájl elérési útja
     *
     * Célszerű abszolút (teljes) elérési utat megadni.
     * @var string
     */
    protected $fileName;

    /**
     * Microsoft Excel 2007 formátum
     * @var string
     */
    const WRITER_TYPE_XLSX = 'Xlsx';

    /**
     * HTML formátum
     * @var string
     */
    const WRITER_TYPE_HTML = 'Html';

    /**
     * CSV formátum
     * @var string
     */
    const WRITER_TYPE_CSV = 'Csv';

    /**
     * Felhasznált formátum az exportálás során
     * @var string
     */
    protected $writerType = self::WRITER_TYPE_XLSX;

    /**
     * @var string
     */
    protected $locale = 'hu_HU';

    /**
     * Mime típusok az egyes formátumokhoz
     * @var array
     */
    protected $mimeTypes = array(
        self::WRITER_TYPE_XLSX   => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        , self::WRITER_TYPE_HTML => 'text/html'
        , self::WRITER_TYPE_CSV  => 'text/csv'
    );

    /**
     * @var string
     */
    protected $memoryLimit;

    /**
     * Konstruktor
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Gyermek osztályok számára fenntartott init, amit a konstruktor hív
     */
    public function init()
    {
    }

    /**
     * Előkészítési feladatok export előtt, setterek meghívása után
     *
     * @return AbstractSpreadsheet
     * @throws Exception
     */
    public function prepare()
    {
        if ($this->getFileName() == null) {
            throw new Exception('Állítsd be a fájlnevet az exportálás előtt -> setFileName()');
        }

        if (is_string($this->getLocale())) {
            Settings::setLocale($this->getLocale());
        }

        if (is_string($this->getMemoryLimit())) {
            ini_set('memory_limit', $this->getMemoryLimit());
        }

        return $this;
    }

    /**
     * Export logika
     *
     * @return mixed
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function export()
    {
        // hova kell tenni?
        if ($this->isFileDestination()) {
            // mehet fájlba
            return $this->exportToFile();
        }

        $this->exportToBrowser();
    }

    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function exportToBrowser()
    {
        // output buffert ürítjük
        ob_end_clean();

        $fileName = $this->getFileName();

        if (strstr($fileName, PATH_SEPARATOR) !== false) {
            $pi = pathinfo($fileName);
            $fileName = $pi['basename'];
        }

        // mehet a böngészőnek
        header('Content-Type: ' . $this->getMimeTypeForWriterType($this->getWriterType()));
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');
        $this->getWriter()->save('php://output');

        die();
    }

    /**
     * @return mixed
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function exportToFile()
    {
        return $this->getWriter()->save($this->getFileName());
    }

    /**
     * @param string $writerType
     * @return mixed
     * @throws Exception
     */
    public function getMimeTypeForWriterType($writerType)
    {
        if (!array_key_exists($writerType, $this->mimeTypes)) {
            throw new Exception('Az alábbi writerType-hoz nincs mime típus: ' . $writerType);
        }

        return $this->mimeTypes[$writerType];
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param string $destination
     * @return AbstractSpreadsheet
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     * Ellenőrzi, hogy fájlrendszerbe mentünk-e
     * @return boolean
     */
    public function isFileDestination()
    {
        return $this->destination == self::DESTINATION_FILE;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return AbstractSpreadsheet
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return string
     */
    public function getWriterType()
    {
        return $this->writerType;
    }

    /**
     * @param string $writerType
     * @return AbstractSpreadsheet
     */
    public function setWriterType($writerType)
    {
        $this->writerType = $writerType;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return AbstractSpreadsheet
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string
     */
    public function getMemoryLimit()
    {
        return $this->memoryLimit;
    }

    /**
     * 128M, 64M, stb. formában
     * @param string $memoryLimit
     * @return AbstractSpreadsheet
     */
    public function setMemoryLimit($memoryLimit)
    {
        $this->memoryLimit = $memoryLimit;

        return $this;
    }

    /**
     * @return \PhpOffice\PhpSpreadsheet\Spreadsheet
     */
    public function getSpreadsheet()
    {
        if ($this->spreadsheet == null) {
            $this->spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet;
        }

        return $this->spreadsheet;
    }

    /**
     * @param \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet
     * @return AbstractSpreadsheet
     */
    public function setSpreadsheet($spreadsheet)
    {
        $this->spreadsheet = $spreadsheet;

        return $this;
    }

    /**
     * @return \PhpOffice\PhpSpreadsheet\Writer\IWriter
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function getWriter()
    {
        if ($this->writer == null) {

            $this->writer = IOFactory::createWriter(
                $this->getSpreadsheet()
                , $this->getWriterType()
            );
        }

        return $this->writer;
    }

    /**
     * @param \PhpOffice\PhpSpreadsheet\Writer\Csv $writer
     * @return AbstractSpreadsheet
     */
    public function setWriter($writer)
    {
        $this->writer = $writer;

        return $this;
    }
}