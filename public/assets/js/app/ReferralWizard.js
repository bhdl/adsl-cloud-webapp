/**
 * ReferralWizard
 */

var ReferralWizard = {

    /** Step 2*/
    panelStep2Id: '#step2',

    collectorPanelId: '#collectorPanel',
    collectorsId: '#collectors',
    searchItemRemoveId: '#serarchItemRemove',

    collectorHeaderClass: '.collectorHeader',
    itemClass: '.item',
    itemNameClass: '.itemName',
    itemRowClass: '.itemRow',

    selectedItemsPanelId: '#selectedItemsPanel',
    selectedItemsId: '#selectedItems',
    duplicatedRequestId: '#duplicatedRequest',

    dropItemId: '#dropItem',

    searchItemId: '#searchItem',

    /** Step 3*/
    panelStep3Id: '#step3',
    referralSampleClass: '.referralSample',
    withoutReferralSampleClass: '.withoutReferralSample',

    /** Step 4*/
    panelStep4Id: '#step4',
    examinationReasonId: '#examinationReason',
    otherCountry: 'otherCountry',
    examinationReasonCountryElement: '.element-examinationReasonCountry',
    deleteModalId: '#documentDeleteModal',
    deleteId: '#documentDelete',

    /**
     * onClick-ek inicializalasa Step2 esetében
     */
    initStep2: function () {
        ReferralWizard.selectedItems();
        ReferralWizard.collectorHeaderClick();
        ReferralWizard.searchItem();
        ReferralWizard.searchItemRemove();
        ReferralWizard.dropItem();
    }
    /**
     * onClick-ek inicializalasa Step3 esetében
     */
    , initStep3: function (isPost) {
        if (isPost) {
            ReferralWizard.errorClass();
        }
        ReferralWizard.setRequiredLabelClick();
        ReferralWizard.selectedWithoutSampleClick();
    }
    /**
     * onClick-ek inicializalasa Step4 esetében
     */
    , initStep4: function (isPost) {
        if (isPost) {
            ReferralWizard.errorClass();
        }

        $('#fileUpload').fileinput({
            language: 'hu',
            showUpload: false
        });

        ReferralWizard.examinationReasonSelected();
        ReferralWizard.dropFile();
    }
    /**
     * Elem kiválasztása vagy eldobása
     */
    , selectedItems: function () {
        $(ReferralWizard.panelStep2Id).on('click', ReferralWizard.itemClass, function () {
            var $panel = $(ReferralWizard.collectorPanelId);
            var selectedItems = [];
            $.each($('input[type="checkbox"]:checked'), function (index, value) {
                selectedItems.push($(value).val());
            });
            BlockUi.block($panel);
            $.ajax({
                url: '/admin/referral-wizard/ajax?operation=selectedItems',
                data: {selectedItems: selectedItems},
                type: 'POST',
                dataType: 'json',
                async: false,
                success: function (response) {
                    if (response.errors == false) {
                        $(ReferralWizard.collectorsId).html(response.collectors);
                        $(ReferralWizard.selectedItemsId).html(response.selectedItems);
                        $(ReferralWizard.duplicatedRequestId).html(response.duplicatedRequest);
                        var keyword = $(ReferralWizard.searchItemId).val().trim().toLowerCase();
                        if (keyword.length > 2) {
                            ReferralWizard.search(keyword);
                        }
                    }
                },
                complete: function () {
                    BlockUi.unblock($panel);
                }
            });
        });
    }
    /**
     * Gyüjtő ki-be csukása
     */
    , collectorHeaderClick: function () {
        $(ReferralWizard.panelStep2Id).on('click', ReferralWizard.collectorHeaderClass, function () {
            var collectorId = $(this).data('collector_id');

            if ($(ReferralWizard.itemRowClass + "[data-collector_id='" + collectorId + "']").is(":visible")) {
                $(ReferralWizard.itemRowClass + "[data-collector_id='" + collectorId + "']").hide();
            } else {
                $(ReferralWizard.itemRowClass + "[data-collector_id='" + collectorId + "']").show();
            }
        });
    }
    /**
     * KeyUp-ra keresés
     */
    , searchItem: function () {
        $(ReferralWizard.panelStep2Id).on('keyup', ReferralWizard.searchItemId, function () {
            ReferralWizard.search($(this).val().trim().toLowerCase());
        });
    }
    /**
     * Keyword-del megegyező szavak kiválaogatása a panelek és tételek közül
     * @param keyword
     */
    , search: function (keyword) {
        if (keyword && keyword.length > 2) {
            $(ReferralWizard.collectorHeaderClass).hide();
            $(ReferralWizard.itemRowClass).hide();
            $(ReferralWizard.itemNameClass).each(function(index, value){
                if ($(value).html().toLowerCase().search(keyword) > -1) {
                    $(value).parents(ReferralWizard.itemRowClass).show();
                }
            });
        }
        if (!keyword) {
            $(ReferralWizard.collectorHeaderClass).show();
            $(ReferralWizard.itemRowClass).hide();
        }
    }
    /**
     * Keresési feltétel eltávolítása
     */
    , searchItemRemove: function () {
        $(ReferralWizard.panelStep2Id).on('click', ReferralWizard.searchItemRemoveId, function () {
            $(ReferralWizard.searchItemId).val('');
            $(ReferralWizard.collectorHeaderClass).show();
            $(ReferralWizard.itemRowClass).hide();
        });
    }
    /**
     * Kiválasztott elem eldobása a kiválasztott elem listából
     */
    , dropItem: function () {
        $(ReferralWizard.panelStep2Id).on('click', ReferralWizard.dropItemId, function () {
            $('input[name="itemIds[' + $(this).data('item_id') + ']"').click();
        });
    }
    , errorClass: function () {
        $('input.required').each(function (index,value) {
            if ($(value).val().length == 0) {
                $(value).closest('.form-group').addClass('has-error');
            }
        });
    }
    /**
     *  kötelező mezők mutatása 3-as lépésnél
     */
    , setRequiredLabelClick: function () {
        $(ReferralWizard.panelStep3Id).on('click', ReferralWizard.referralSampleClass, function () {
            var parent = $(this).closest('.sample');
            var set = $(this).closest('.set');
            if ($(this).is(':checked')) {
                parent.find('.location input').addClass('required');
                parent.find('.location label').addClass('required');
                parent.find('.sampleItem input').addClass('required');
                parent.find('.sampleItem label').addClass('required');
            } else {
                if (set.find('input[type=checkbox]').length > 1) {
                    parent.find('.required').removeClass('required');
                    parent.find('.location input').val('');
                    parent.find('.comment input').val('');
                    parent.find('.withoutReferralSample').removeAttr('checked');
                }
            }
        });

    }
    /**
     *  minta nélküli küldés esetén a megjegyzés kötelező
     */
    , selectedWithoutSampleClick: function () {
        $(ReferralWizard.panelStep3Id).on('click', ReferralWizard.withoutReferralSampleClass, function () {
            var parent = $(this).closest('.sample');
            if ($(this).is(':checked')) {
                var sample = parent.find('.referralSample');
                if (!sample.is(':checked')) {
                    sample.trigger('click');
                }
                parent.find('.comment input').addClass('required');
                parent.find('.comment label').addClass('required');
            } else {
                parent.find('.comment input').removeClass('required');
                parent.find('.comment label').removeClass('required');
                parent.find('.comment').removeClass('has-error');
            }
        });

    }
    /**
     * Vizsgálati ok kiválasztásakor célország beállítása
     */
    , examinationReasonSelected: function () {
        if ($(ReferralWizard.examinationReasonId).val() != ReferralWizard.otherCountry) {
            $(ReferralWizard.examinationReasonCountryElement).hide();
        }
        $(ReferralWizard.panelStep4Id).on('change', ReferralWizard.examinationReasonId, function () {
            if ($(this).val() == ReferralWizard.otherCountry) {
                $(ReferralWizard.examinationReasonCountryElement).show();
            } else {
                $(ReferralWizard.examinationReasonCountryElement).hide();
            }
        });
    }
    /**
     * Kiválasztott kérés / panel törlése
     */
    , dropFile: function () {
        $(ReferralWizard.deleteModalId).on('shown.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var parent = $(e.relatedTarget).parents('.uploadedFile');
            $(ReferralWizard.deleteId).off().on('click', function(){
                $.ajax({
                    url: '/admin/referral-wizard/ajax?operation=documentDelete',
                    type: 'POST',
                    dataType: 'json',
                    data: {hash: id},
                    async: false,
                    success: function (response) {
                        $(ReferralWizard.deleteModalId).modal('hide');
                        if (response.errors == false) {
                            $(parent).remove();
                        } else {
                            BootBox.alert(response.message);
                        }
                    }
                });
            });
        });
    }
};