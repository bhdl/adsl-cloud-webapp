/**
 * @type {{instance: null, set: CrudBrowseModalRegistry.set, get: CrudBrowseModalRegistry.get}}
 */
var CrudBrowseModalRegistry = {
    instance : null

    , set : function(instance) {
        CrudBrowseModalRegistry.instance = instance;
    }

    , get : function() {
        return CrudBrowseModalRegistry.instance;
    }
};

/**
 * @param alias
 * @constructor
 */
function CrudBrowseModal(alias) {

    /**
     * @type {CrudBrowseModal}
     * @private
     */
    var _this = this;

    /**
     * @type {{}}
     */
    this.callbacks = {};

    /**
     * @type {jQuery|HTMLElement}
     */
    this.$modal = $('#crudBrowseModal');

    /**
     * @type {null}
     */
    this.alias = alias;

    /**
     * @param $element
     */
    this.browse = function($element) {

        BlockUi.block($element);
        var builderParams;
        if (_this.callbacks && _this.callbacks.builderParams) {
            builderParams = _this.callbacks.builderParams;
        }

        $.ajax({
            type   : 'POST'
            , url  : '/admin/' + this.alias + '/ajax'
            , data : {
                operation : 'browse'
                , builderParams: builderParams
            }
            , success : function (response) {
                if (response.errors == false && response.result) {
                    _this.$modal.find('.modal-body').empty().html(
                        response.result
                    );

                    _this.$modal.modal('show');

                    CrudBrowseModalRegistry.set(_this);
                }
            }
            , error : function () {
            }
            , complete : function () {
                BlockUi.unblock($element);
            }
        });
    };

    /**
     * @param entity
     */
    this.select = function(entity) {

        this.$modal.off('hidden.bs.modal').on('hidden.bs.modal', function () {

            if (_this.callbacks && _this.callbacks.select) {
                _this.callbacks.select.apply(this, [
                    entity
                ]);
            }

            CrudBrowseModalRegistry.set(null);
        });

        this.$modal.modal('hide');
    };

    /**
     * @param alias
     * @returns {CrudBrowseModal}
     */
    this.setAlias = function(alias) {
        this.alias = alias;

        return this;
    };

    /**
     * @param callbacks
     * @returns {CrudBrowseModal}
     */
    this.setCallbacks = function(callbacks) {
        this.callbacks = callbacks;

        return this;
    };
}