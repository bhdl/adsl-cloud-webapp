/**
 * @type {{instance: null, set: CrudCreateModalRegistry.set, get: CrudCreateModalRegistry.get}}
 */
var CrudCreateModalRegistry = {
    instance : null

    , set : function(instance) {
        CrudCreateModalRegistry.instance = instance;
    }

    , get : function() {
        return CrudCreateModalRegistry.instance;
    }
};

/**
 * @param alias
 * @constructor
 */
function CrudCreateModal(alias) {

    /**
     * @type {CrudCreateModal}
     * @private
     */
    var _this = this;

    /**
     * @type {{}}
     */
    this.callbacks = {};

    /**
     * Egymásba ágyazott modal ablakok esetén az előző ablak példánya (elmentett állapottal)
     *
     * @type CrudCreateModal
     */
    this.previousModal = null;

    /**
     * Az űrlap alapértelmezett értkei (opcionális)
     *
     * @type {null}
     */
    this.formDefaults = null;

    /**
     * @type {jQuery|HTMLElement}
     */
    this.$modal = $('#crudFormModal');

    /**
     * @type {jQuery|HTMLElement}
     */
    this.$form = $('#crudFormModalForm');

    /**
     * @type {jQuery|HTMLElement}
     */
    this.$saveButton = $('#crudFormModalSave');

    /**
     * @type {jQuery|HTMLElement}
     */
    this.$cancelButton = $('#crudFormModalCancel');

    /**
     * @type {null}
     */
    this.alias = alias;

    /**
     * @param $element
     * @param formDefaults
     */
    this.form = function($element, formDefaults) {

        if ($element) {
            BlockUi.block($element);
        }

        $.ajax({
            type   : 'GET'
            , url  : '/admin/' + this.alias + '/ajax'
            , data : {
                operation  : 'create'
                , formDefaults : formDefaults
            }
            , success : function (response) {
                if (response.errors == false) {

                    _this.$form.empty().html(
                        response.result.form
                    );

                    _this.show();
                }
            }
            , error : function () {
            }
            , complete : function () {
                if ($element) {
                    BlockUi.unblock($element);
                }
            }
        });
    };

    /**
     * @param $element
     */
    this.create = function($element) {

        BlockUi.block($element);

        var data       = this.$form.serializeObject();
        data.operation = 'create';

        $.ajax({
            type      : 'POST'
            , url     : '/admin/' + this.alias + '/ajax'
            , data    : data
            , success : function (response) {
                if (response.errors == false) {
                    if (response.result.form) {
                        _this.$form.empty().html(response.result.form);
                        return;
                    }

                    if (_this.callbacks.create) {
                        _this.callbacks.create.apply(_this, [
                            response.result.entity
                        ]);
                    }

                    _this.hide();
                }
            }
            , error : function () {
            }
            , complete : function () {
                BlockUi.unblock($element);
            }
        });
    };

    /**
     * Show
     */
    this.show = function() {
        this.$modal.off('show.bs.modal').on('show.bs.modal', function(){

            if (_this.callbacks.show) {
                _this.callbacks.show.apply(_this, []);
            }

            _this.initButtonEventListeners();

            CrudCreateModalRegistry.set(_this);
        });

        this.$modal.modal('show');
    };

    /**
     * Hide
     */
    this.hide = function() {

        this.$modal.off('hidden.bs.modal').on('hidden.bs.modal', function(){

            if (_this.callbacks.hide) {
                _this.callbacks.hide.apply(_this, []);
            }

            if (_this.previousModal) {
                _this.previousModal.form(
                    null
                    , _this.previousModal.getFormDefaults()
                );

                delete _this.previousModal;
            }

            CrudCreateModalRegistry.set(null);
        });

        this.$modal.modal('hide');
    };

    /**
     * initButtonEventListeners
     */
    this.initButtonEventListeners = function(){

        this.$saveButton.off('click').on('click', function(){
            _this.create(
                $(this)
            );
        });

        this.$cancelButton.off('click').on('click', function(){
            _this.hide();
        });
    };

    /**
     * @param alias
     * @returns {CrudCreateModal}
     */
    this.setAlias = function(alias) {
        this.alias = alias;

        return this;
    };

    /**
     * @param callbacks
     * @returns {CrudCreateModal}
     */
    this.setCallbacks = function(callbacks) {
        this.callbacks = callbacks;

        return this;
    };

    /**
     * @param key
     * @param callback
     * @returns {CrudCreateModal}
     */
    this.setCallback = function(key, callback) {
        this.callbacks[key] = callback;

        return this;
    };

    /**
     * @returns {CrudCreateModal}
     */
    this.persist = function() {

        this.formDefaults = this.$form.serializeObject();

        return this;
    };

    /**
     * @param modal
     */
    this.setPreviousModal = function(modal) {
        this.previousModal = modal;

        return this;
    };

    /**
     * @returns {CrudCreateModal}
     */
    this.getPreviousModal = function() {
        return this.previousModal;
    };

    /**
     * @param formDefaults
     */
    this.setFormDefaults = function(formDefaults) {
        this.formDefaults = formDefaults;
    };

    /**
     *
     * @param k
     * @param v
     * @returns {CrudCreateModal}
     */
    this.setFormDefault = function(k, v) {
        this.formDefaults[k] = v;
        return this;
    };

    /**
     * @returns {CrudCreateModal}
     */
    this.getFormDefaults = function() {
        return this.formDefaults;
    };
}

