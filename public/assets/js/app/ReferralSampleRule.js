/**
 * ReferralSampleRule
 */

var ReferralSampleRule = {

    referralSampleRuleId: '#referralSampleRule',

    referralSampleSetPlusId: '#referralSampleSetPlus',

    select2Options: {},
    /**
     * onClick-ek inicializalasa
     */
    init: function () {
        ReferralSampleRule.referralSampleSetClick();
        ReferralSampleRule.setSelect2Options();
        $('select').select2(ReferralSampleRule.select2Options);
    }
    , setSelect2Options: function () {
        ReferralSampleRule.select2Options = {
            ajax: {
                url: 'lab/referral-sample-rule/ajax'
                , dataType: 'json'
                , data: function (params) {
                    var query = {
                        term: params.term
                        , selectedReferralSampleIds: ReferralSampleRule.getSelectedSamples()
                        , operation: 'referralSampleSelect2'
                    };

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                }
                , processResults: function (data) {
                    // Tranforms the top-level key of the response object from 'items' to 'results'
                    return {
                        results: data
                    };
                }

            }
            , minimumInputLength: 2
            , placeholder: Translate.t('Kezdje el gépelni a minta nevét')
        }
    }
    /**
     * Új halmaz hozzáadása gomb click
     */
    , referralSampleSetClick: function () {
        $(ReferralSampleRule.referralSampleRuleId).on('click', ReferralSampleRule.referralSampleSetPlusId, function () {
            var setSelect = ReferralSampleRule.getReferralSampleSetSelect();
            $(this).before(setSelect);
            $('select').select2(ReferralSampleRule.select2Options);
        });
    }
    /**
     * Halmaz input legyártása
     */
    , getReferralSampleSetSelect: function () {
        return  '<div class="form-group ">' +
                    '<select name="set['+ Math.random() +'][]" class="referralSampleSetSelect col-lg-12" multiple="multiple">' +
                '</div>';
    }
    , getSelectedSamples: function () {
        var referralSampleIds = [];
        $('.referralSampleSetSelect option').each(function (index, value) {
            referralSampleIds.push($(value).val());
        });
        return referralSampleIds;
    }
};