
var Acl = {

    /**
     * Acl matrix
     */
    acl : null

    /**
     * @param resourceId
     * @param actionId
     * @returns boolean
     */
    , isAllowed : function(resourceId, actionId) {
        if (Acl.acl == null
            || typeof Acl.acl[resourceId] == 'undefined'
            || typeof Acl.acl[resourceId][actionId] == 'undefined') {
            return false;
        }

        return Acl.acl[resourceId][actionId];
    }

    /**
     * @param acl
     */
    , setAcl : function(acl) {
        Acl.acl = acl;
    }

    /**
     * @returns {null}
     */
    , getAcl : function() {
        return Acl.acl;
    }
};