/**
 * @type {{block: BlockUi.block, unblock: BlockUi.unblock}}
 */
var BlockUi = {
    /**
     * @param element
     */
    block : function (element) {
        element.block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS          : {
                backgroundColor : '#fff',
                opacity         : 0.8,
                cursor          : 'wait'
            },
            css : {
                border          : 0,
                padding         : 0,
                backgroundColor : 'transparent'
            }
        });
    }

    /**
     * @param element
     */
    , unblock : function(element) {
        element.unblock();
    }
};