/**
 * @type {{}}
 */
var SearchBar = {

    /**
     * Melyik controllernél használjuk
     */
    controller : null

    /**
     * Init
     */
    , init : function(controller) {

        SearchBar.controller = controller;

        SearchBar.load({
            operation : 'searchBar'
        });
    }

    /**
     * initEventListeners
     */
    , initEventListeners : function() {

        $("#searchBar .reset").off('click').click(function(e){
            SearchBar.reset();
            e.preventDefault();
        });

        $("#searchBar .search").off('click').click(function(e){
            SearchBar.search();
            e.preventDefault();
        });

        $("#searchBar .addParam").off('click').click(function(e){
            SearchBar.addParam(
                $(this).data('param-name')
            );
            e.preventDefault();
        });

        $("#searchBar .clearParam").off('click').click(function(e){

            var $param = $(this).parent().parent();

            SearchBar.clearParam(
                $param.data('param-name')
                , $param.data('param-i')
            );
            e.preventDefault();
        });
    }

    /**
     * Új keresőparaméter hozzáadása
     * @param id
     */
    , addParam : function(paramName) {
        SearchBar.load({
            operation   : 'searchBarAddParam'
            , paramName : paramName
            , params    : SearchBar.getParams()
        });
    }

    /**
     * Paraméter törlése
     * @param paramName
     */
    , clearParam : function(paramName, i) {
        SearchBar.load({
            operation   : 'searchBarClearParam'
            , paramName : paramName
            , i         : i
            , params    : SearchBar.getParams()
        });
    }

    /**
     *
     * @param data
     */
    , load : function(data, callback) {
        var $columns = $('#searchBar .columns');

        BlockUi.block($columns);

        $.ajax({
            type   : 'POST'
            , url  : '/admin/' + SearchBar.controller + '/ajax'
            , data : data
            , success: function (response) {
                if (response.errors == false) {
                    $columns.html(response.result);

                    if (callback) {
                        callback.apply(this, []);
                    }
                }
            }
            , complete: function () {
                BlockUi.unblock($columns);

                SearchBar.initEventListeners();
            }
        });
    }

    /**
     * Keresés indítása
     */
    , search : function() {
        table.ajax.reload();
    }

    /**
     * Mik a builder paraméterei az űrlapon?
     * @returns {null}
     */
    , getParams : function() {
        var $form = $('#searchBar').find('form');

        if ($form.length > 0) {
            return $form.serializeObject();
        }

        return null;
    }

    /**
     * Alaphelyzetbe állítás
     * @returns {*|void}
     */
    , reset : function() {

        SearchBar.load({
            operation : 'searchBarReset'
        }, function() {
            SearchBar.search();
        });
    }
};