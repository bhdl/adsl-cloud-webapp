/**
 * @type {{}}
 */

var HotKeys = {

    /**
     * HotKey callback regiszter
     */
    hotKeyCallbacks : []

    /**
     * Egyszerű URL átirányítás hozzáadása az oldalhoz
     *
     * @param string hotKey
     * @param string description
     * @param string url
     */
    , addHotKeyCallbackUrl : function(hotKey, description, url) {
        HotKeys.addHotKeyCallback(hotKey, description, function(event) {
            document.location.href = url;
        });
    }

    /**
     * Callback jellegű akció hozzáadása az oldalhoz. A callback function típusú.
     *
     * @param string hotKey
     * @param string hotKey
     * @param function callback
     */
    , addHotKeyCallback : function(hotKey, description, callback) {

        $(document).bind('keydown', hotKey, function(event) {
            callback.apply(this, [event]);

            event.preventDefault();
        });

        HotKeys.hotKeyCallbacks.push({
            hotKey        : hotKey
            , description : description
            , callback    : callback
        });
    }

    /**
     * Hint modal megnyitása, a regiszterben tárolt adatok kiolvasása.
     */
    , hint : function() {

        var html = '<table class="table">';
        html += '<thead>';
        html += '<tr>';
        html += '<th style="width:10%;">' + Translate.t('Kombináció') + '</th>';
        html += '<th>' + Translate.t('Megnevezés') + '</th>';
        html += '</tr>';
        html += '</thead>';

        html += '<tbody>';

        $(HotKeys.hotKeyCallbacks).each(function(i, o){
            html += '<tr>';
            html += '<td>' + o.hotKey + '</td>';
            html += '<td>' + o.description + '</td>';
            html += '</tr>';
        });

        html += '</tbody>';
        html += '</table>';

        $('#hotKeysModal').find('.modal-body').html(html);

        $('#hotKeysModal').modal('show');
    }
};