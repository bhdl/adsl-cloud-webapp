/**
 * Datatables init
 */
$(function () {
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        responsive: true,
        serverSide: true,
        stateSave: true,
        processing: true,
        /*fixedHeader: true,*/
        colReorder: true,
        dom: '<"datatable-header"ifpBl><"datatable-scroll"t><"datatable-footer"ipr>',
        language: {
            emptyTable:     Translate.translate('Nincs rendelkezésre álló adat.'),
            info:           Translate.translate('Összesen: _TOTAL_ találat. Találatok: _START_ - _END_.'),
            infoEmpty:      Translate.translate('Nincs találat.'),
            infoFiltered:   Translate.translate('Összesen _MAX_ db rekord közül szűrt eredmény.'),
            infoPostFix:    '',
            infoThousands:  ' ',
            lengthMenu:     Translate.translate('<span>Találat oldalanként: </span> _MENU_'),
            loadingRecords: Translate.translate('Betöltés ...'),
            processing:     '<div class="icon-spinner2 spinner"></div>'+
                            '<p>'+Translate.translate('Feldolgozás ...')+'</p>',

            search:         '_INPUT_',
            searchPlaceholder : Translate.translate('Keresés ...'),
            zeroRecords:    Translate.translate('Nincs a keresésnek megfelelő találat.'),
            paginate: {
                first:    Translate.translate('Első'),
                previous: Translate.translate('Előző'),
                next:     Translate.translate('Következő'),
                last:     Translate.translate('Utolsó')
            },
            aria: {
                sortAscending:  Translate.translate(': aktiválja a növekvő rendezéshez'),
                sortDescending: Translate.translate(': aktiválja a csökkenő rendezéshez')
            },
            select: {
                rows: {
                    "_": Translate.translate('%d elem kiválasztva.'),
                    "0": '',
                    "1": Translate.translate('1 elem kiválasztva.')
                }
            }
        }
        , initComplete : function() {
            // pager select2 átalakítás
            $('.dataTables_length select').select2({
                minimumResultsForSearch: Infinity,
                width: 'auto'
            });

            // Tooltip
            $('[data-popup="tooltip"]').tooltip({
                container: 'body'
            });
        }
        , stateSaveCallback: function(settings, data) {
            $.ajax({
                type       : 'POST'
                , url      : '/admin/index/ajax'
                , data     : {
                    operation : 'dataTableStateSave'
                    , id      : settings.sInstance
                    , data    : data
                }
            });
        }
        , stateLoadCallback: function(settings) {
            var json;
            $.ajax({
                type       : 'POST'
                , url      : '/admin/index/ajax'
                , async    : false
                , data     : {
                    operation : 'dataTableStateLoad'
                    , id      : settings.sInstance
                }
                , success: function (response) {
                    if (response.errors == false) {
                        json = response.result;
                    }
                }
            });

            return json;
        }
    });
});




