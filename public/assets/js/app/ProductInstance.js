function getProducts() {
    var siteId    = $("#siteId").val();
    var productId = $("#productId").val();

    if(!siteId || !productId) {
        return;
    }

    $.ajax({
        url: '/admin/product-instance/ajax?operation=getProductInstances',
        type: 'POST',
        dataType: 'json',
        data: {
            siteId:          siteId,
            productId:       productId
        },
        success:function(data){
            let temp = "";
            var template = $('#product-template').html();
            template = Handlebars.compile(template);

            for (let i = 0; i< data.length; i++) {
                let values = {
                    productInstanceId: data[i].productInstanceId
                    , productStatusId: data[i].productStatusId
                    , productId: data[i].productId
                    , siteId: data[i].siteId
                    , rfid: data[i].rfid
                };

                temp += template(values);
            }
            $(".product-list-body").html('')
            $(".product-list-body").html(temp)
        }
    });
}

$(function() {
    getProducts();

    $("#siteId, #productId").on('change', function (){
        getProducts();
    });

    $(".save-button").on('click', function(){

        $(".form-group").removeClass("has-error")

        var siteId               = $("#siteId").val();
        var productId            = $("#productId").val();
        var productStorageAction = $("#productStorageAction").val();
        var rfid                 = $("#rfid").val();
        var productStatusId        = $("#productStatusId").val();
        var operation            = $("#productStorageAction").val() == 0 ? "createProductInstances" : "deleteProductInstances";

        $.ajax({
            url: '/admin/product-instance/ajax?operation=' + operation,
            type: 'POST',
            dataType: 'json',
            data: {
                siteId:          siteId,
                productId:       productId,
                productStorageAction: productStorageAction,
                rfid: rfid,
                productStatusId: productStatusId
            },
            success:function(data){
                let message = data;
                if(data.error_message) {
                    $(".element-"+data.error_field).addClass("has-error");
                    message = data.error_message;
                } else {
                    $("#rfid").val('')
                    getProducts();
                }

                alert(message);
            }
        });
    })
})

$(document).on("click",".removeProductInstance",function() {
    var productInstanceId = $(this).attr("data-productInstanceId");
    $.ajax({
        url: '/admin/product-instance/ajax?operation=deleteProductInstance',
        type: 'POST',
        dataType: 'json',
        data: {
            productInstanceId:       productInstanceId
        },
        success:function(data){
            getProducts();
            alert(data);
        }
    });
});
