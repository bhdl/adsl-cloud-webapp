var EasyTimer = new Timer();
if (localStorage.getItem("timer")) {
    EasyTimer.start({
        precision: "seconds",
        startValues: {
            seconds: parseInt((localStorage.getItem("hours") * 60 * 60)) + parseInt((localStorage.getItem("minutes") * 60)) + parseInt(localStorage.getItem("seconds"))
        }
    });
    EasyTimer.addEventListener("secondsUpdated", function(e) {
        if ($("#timer").length > 0) {
            $("#timer").html( EasyTimer.getTimeValues().toString(["hours", "minutes", "seconds"]));
        }
        localStorage.setItem("timer", EasyTimer.getTimeValues().toString(["hours", "minutes", "seconds"]));
        localStorage.setItem("hours", EasyTimer.getTimeValues().hours);
        localStorage.setItem("minutes", EasyTimer.getTimeValues().minutes);
        localStorage.setItem("seconds", EasyTimer.getTimeValues().seconds);
    });
}