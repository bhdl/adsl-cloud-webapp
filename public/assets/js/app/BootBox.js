/**
 * BootBox wrapper
 */
var BootBox = {
    /**
     *
     * @param message
     * @param callback
     */
    confirm : function (message, callback) {
        bootbox.confirm({
            message: message,
            buttons: {
                confirm: {
                    label: Translate.t('Igen'),
                    className: 'btn-success'
                },
                cancel: {
                    label: Translate.t('Mégse'),
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                callback.apply(this, [result]);
            }
        });
    }
    /**
     * @param message
     * @param callback
     */
    , alert : function(message, callback) {
        bootbox.alert({
            message: message,
            buttons: {
                ok: {
                    label: Translate.t('Ok'),
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                if (callback) {
                    callback.apply(this, [result]);
                }
            }
        });
    }
};