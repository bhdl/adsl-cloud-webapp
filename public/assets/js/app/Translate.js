
/**
 * Javascript fordító nyelvi kifejezések megjelenítéséhez
 * JS context-ben. Az osztály paraméterezését a Translate 
 * plugin végzi. Az osztály csak egy locale fordításait
 * tárolja.
 */

var Translate = {
		
	/**
	 * @var string
	 */	
	_locale : "hu_HU",
	
	/**
	 * Kifejezések és fordításaik
	 * @var JSON
	 */
	_messages: {},
	
	/**
	 * Kifejezés fordítása
	 * @param string message
	 * @param JSON   replacements
	 * @param string locale
	 * @returns string
	 */
	_: function (message, replacements, locale) {
		
		if (undefined == locale) {
			var locale = Translate._locale;
		}

		var translated = null;

        if (typeof Translate._messages[locale] == "undefined"
			|| typeof Translate._messages[locale][message] == "undefined"
		) {
        	// nincs ilyen locale, vagy message
            translated = message;
        } else {
        	// van ilyen message
            translated = Translate._messages[locale][message];
        }

		if (replacements != undefined) {
			// vannak változók is
			for (key in replacements) {
				var value = replacements[key];
				var regexp = new RegExp("%" + key + "%", "g");
				translated = translated.replace(regexp, value);
			}
		}
				
		return translated;
	},
	
	/**
	 * Proxy a "_" függvényre
	 * @param string message
	 * @param JSON   replacements
	 * @param string locale
	 * @returns string
	 */
	translate: function(message, replacements, locale) {
		return Translate._(message, replacements, locale);
	},

    /**
	 * Proxy a "_" függvényre
     * @param message
     * @param replacements
     * @param locale
     * @returns {*|string}
     */
    t: function(message, replacements, locale) {
        return Translate._(message, replacements, locale);
    },
	
	/**
	 * Kifejezések settere
	 * @param messages
	 */
	setMessages: function (messages) {
		Translate._messages = messages;
	},
	
	/**
	 * Locale setter
	 * 
	 * Szükséges a megfelelő üzenetek kiolvasásához a messages 
	 * JSON tömbből
	 * @param string locale
	 */
	setLocale: function (locale) {
		Translate._locale = locale;
	},
	
	/**
	 * Locale getter
	 * @return string 
	 */
	getLocale: function () {
		return Translate._locale;
	}
};