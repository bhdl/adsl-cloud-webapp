$(document).on("click", ".Product__list-item", function() {
    let selectedClass = "--yellow-list-item";
    $(".Search__list").find("." + selectedClass).removeClass(selectedClass);
    $(this).addClass(selectedClass);

    $(".Detail__list-item-value-count").html($(this).find(".Product__list-item-value").html())

    $.ajax({
        type: 'POST',
        url: '/admin/product-positioning/ajax',
        data: {
            'operation': 'product-list'
            , 'productId' : $(this).attr('data-productid')
        },
        success: function (response) {
            $(".Position__list").html("");
            if (response.length > 0) {
                for(let i in response) {
                    $(".Position__list").append('<li class="Product__list-item">'
                        + '<span class="Product__list-item-name">x'+ response[i].currentX + ',y' + response[i].currentY + ',z' + response[i].currentZ +'</span>'
                        + '<span class="Product__list-item-value">x'+ response[i].newX + ',y' + response[i].newY + ',z' + response[i].newZ +'</span></li>');
                }
            }
        }
    });
});


$(function(){
    $(".Search__searchbar").keyup(function(){
        $.ajax({
            type: 'POST',
            url: '/admin/product-positioning/ajax',
            data: {
                'operation': 'search'
                , 'value': $(this).val()
            },
            success: function (response) {
                data = response.result;
                $(".Search__list").html("");
                if (data.length > 0) {
                    for(let i in data) {
                        $(".Search__list").append('<li class="Product__list-item" data-productid="'+ data[i].productId +'">'
                            + '<span class="Product__list-item-name">'+ data[i].name +'</span><span class="Product__list-item-value">'+ data[i].db +'</span>'
                            + '</li>');
                    }
                }
            }
        });
    })

    $("#startRepositioning").click(function(){
        $.ajax({
            type: 'POST',
            url: '/admin/product-positioning/ajax',
            data: {
                'operation': 'start-positioning'
            },
            success: function (response) {
                $("#startRepositioning").addClass("hide");
                $(".Repositioning__box .progress").removeClass("hide");
                $(".Repositioning__text").html(Translate.t('Számítás folyamatban.'))
            }
        });
    });
});
