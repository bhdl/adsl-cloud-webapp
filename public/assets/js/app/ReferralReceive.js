/**
 * ReferralReceive
 */
var ReferralReceive = {

    itemDiv: '.element-itemName',
    itemNameId: '#itemName',
    itemPanelElementClass: '.itemPanelElement',
    selectedItem: 'selectedItem',
    selectedItemClass: '.selectedItem',
    itemsClass: '.items',
    selectedRequestClass: '.selectedRequest',
    dropItemClass: '.dropItem',
    changeItemClass: '.changeItem',
    itemChangeModalId: '#itemChangeModal',
    itemChangeId: '#itemChange',
    oldItemNameId: '#oldItemName',
    changeItemNameId: '#changeItemName',
    itemId: '#itemId',

    itemDeleteModalId: '#itemDeleteModal',
    itemDeleteId: '#itemDelete',

    addTubeReceivedId: '#addTubeReceived',

    fileUploadSaveId: '#fileUploadSave',
    scanSaveClass: '.scanSave',
    scanPreviewClass: '.scanPreview',

    /**
     * onClick-ek inicializalasa
     */
    init: function () {
        ReferralReceive.dropItems();
        ReferralReceive.changeItems();
    }
    /**
     * onClick-ek inicializalasa
     */
    , initTubeReceived: function () {
        ReferralReceive.addTubeReceivedClick();
        ReferralReceive.requiredItems();
        ReferralReceive.checkSelection();
        ReferralReceive.addHasError();
    }
    , initFileUpload: function () {
        ReferralReceive.fileUploadSave();
        setInterval(function () {
            ReferralReceive.showNewUploadedFile();
        }, 5000);
    }
    /**
     * Kérés / panel kiválasztása
     */
    , selectedItems: function (item) {
        $(ReferralReceive.itemDiv).after(ReferralReceive.getItemsDiv(item));
    }
    , setSelectedItems: function (items) {
        $.each(items, function(index, item){
            $(ReferralReceive.itemDiv).after(ReferralReceive.getItemsDiv(item));
        });
    }
    , getSelectedItems: function () {
        var ids = [];
        $('.item').each(function (index, value) {
            ids.push($(value).val());
        });
        return ids.join(',');
    }
    , getItemsDiv: function (item) {
        return '<div class="row itemPanelElement">' +
                    '<div class="col-lg-offset-4 col-lg-8">' +
                        '<div class="panel border-top border-top-primary">' +
                            '<div class="panel-body">' +
                                '<div class="row selectedItem items pb-5">' +
                                    '<input class="item" type="hidden" name="selectedItem[]" value="' + item.itemId + '">' +
                                    '<div class="col-lg-8 name">'+ (item.allRequests.length == 0 ? '<strong>' : '') + item.name + (item.allRequests.length == 0 ? '</strong>' : '') + '</div>' +
                                    '<div class="col-lg-4">' +
                                        (item.allRequests.length == 0 ? (
                                           '<button type="button" title="' + Translate.translate('Kérés cseréje') + '" ' +
                                                'class="changeItem button bg-defaul" ' +
                                           'data-toggle="modal" data-target="' + ReferralReceive.itemChangeModalId + '"' +
                                           'data-item_id="' + item.itemId + '">' +
                                                    '<i class="icon-sync"></i>' +
                                            '</button>'
                                            ) : ''
                                        ) +
                                       '<button type="button" title="' + Translate.translate((item.allRequests.length > 0 ? 'Panel' : 'Kérés') + ' törlése') + '" ' +
                                            'class="dropItem button bg-defaul ' + (item.allRequests.length > 0 ? 'border border-orange':'') + '" ' +
                                            'data-toggle="modal" data-target="' + ReferralReceive.itemDeleteModalId + '"' +
                                            'data-item_id="' + item.itemId + '">' +
                                                '<i class="icon-trash"></i>' +
                                        '</button>' +
                                    '</div>' +
                                '</div>' +
                                ReferralReceive.getPanelsRequests(item.allRequests, item.itemId) +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';
    }
    , getPanelsRequests: function (allRequests, itemId) {
        var requests = '';
        $.each(allRequests, function(key, value){
            requests += '<div class="items row pb-5">' +
                            '<input type="hidden" class="item" name="selectedRequests[' + itemId + '][]" value="' + value.itemId + '">' +
                            '<div class="col-lg-8 name">' + value.itemName + '</div>' +
                            '<div class="col-lg-4">' +
                                '<button type="button" title="' + Translate.translate('Kérés cseréje') + '" ' +
                                    'data-toggle="modal" data-target="' + ReferralReceive.itemChangeModalId + '"' +
                                    'class="changeItem button bg-default" data-item_id="' + value.itemId + '">' +
                                        '<i class="icon-sync"></i>' +
                                '</button>&nbsp;' +
                                '<button type="button" title="' + Translate.translate('Kérés törlése') + '" ' +
                                    'data-toggle="modal" data-target="' + ReferralReceive.itemDeleteModalId + '"' +
                                    'class="dropItem button bg-default" data-item_id="' + value.itemId + '">' +
                                        '<i class="icon-trash"></i>' +
                                '</button>' +
                            '</div>' +
                        '</div>';
        });
        return requests;
    }
    /**
     * Kiválasztott kérés / panel törlése
     */
    , dropItems: function () {
        $(ReferralReceive.itemDeleteModalId).on('shown.bs.modal', function (e) {
            var item = $(e.relatedTarget)
                .parents(ReferralReceive.itemsClass)
            ;
            $(ReferralReceive.itemDeleteId).off().on('click', function(){
                if (item.hasClass(ReferralReceive.selectedItem)) {
                    item.parents(ReferralReceive.itemPanelElementClass).remove();
                } else {
                    item.remove();
                }
                $(ReferralReceive.itemDeleteModalId).modal('hide');
            });
        });
    }
    /**
     * Kiválasztott kérés cseréje
     */
    , changeItems: function () {
        $('body').on('click', ReferralReceive.changeItemClass, function () {
            $(ReferralReceive.itemChangeModalId).on('shown.bs.modal', function (e) {
                var item = $(e.relatedTarget)
                    .parents(ReferralReceive.itemsClass)
                ;
                /**
                 * Modal adatok kipucolása megnyitáskor
                 */
                $(ReferralReceive.itemChangeModalId + ' ' + ReferralReceive.oldItemNameId).html('');
                $(ReferralReceive.changeItemNameId).val('');
                $(ReferralReceive.itemId).val('');

                /**
                 * Cserélendő kérés nevének beírása
                 */
                $(ReferralReceive.itemChangeModalId + ' ' + ReferralReceive.oldItemNameId).append(item.find('.name').html());

                $(ReferralReceive.itemChangeId).off().on('click', function(){
                    if ($('#itemId').val().length > 0) {
                        item.find('.name').html($(ReferralReceive.changeItemNameId).val());
                        item.find('input').val($(ReferralReceive.itemId).val());
                    }
                    $(ReferralReceive.itemChangeModalId).modal('hide');
                });

            });

        });
    }
    , addTubeReceivedClick: function () {
        $('body').on('click', ReferralReceive.addTubeReceivedId, function () {
            BlockUi.block($(ReferralReceive.addTubeReceivedId));
            $.ajax({
                url: '/admin/referral-receive/ajax?operation=addTubeReceivedForm',
                type: 'POST',
                dataType: 'json',
                async: false,
                success: function (response) {
                    if (response.errors == false) {
                        $(ReferralReceive.addTubeReceivedId).parent().before(response.form);
                    }
                },
                complete: function () {
                    BlockUi.unblock($(ReferralReceive.addTubeReceivedId));
                }
            });
        });
    }
    , requiredItems:  function () {
        $('body').on('change', 'select', function () {
            ReferralReceive.checkSelection();
        });
    }
    , checkSelection: function () {
        $('.required').removeClass('required');
        $('select').each(function (index, value) {
            var parent = $(value).parents('.tube');
            if ($(value).val().length > 0) {
                parent.find('label').addClass('required');
                parent.find('input').addClass('required');
                parent.find('select').addClass('required');
            }
        });
    }
    , addHasError: function () {
        $('.form-control.required').each(function (index,value) {
            if ($(value).val().length == 0) {
                $(value).closest('.form-group').addClass('has-error');
            }
        })
    }
    , fileUploadSave: function () {
        $(ReferralReceive.fileUploadSaveId).on('click', ReferralReceive.scanSaveClass, function () {
            var button = $(this);
            var referralId = $('.documents').data('referralid');
            BlockUi.block(button);
            $.ajax({
                url: '/admin/referral-receive/ajax?operation=scanSave',
                data: {fileHash: button.data('hash'), referralId: referralId},
                type: 'POST',
                dataType: 'json',
                async: false,
                success: function (response) {
                    button.parents(ReferralReceive.scanPreviewClass).remove();
                    if (response.errors == false) {
                        $('#savedFiles').html(response.savedFiles);
                    }
                    else {
                        BootBox.alert(response.message);
                    }
                },
                complete: function () {
                    BlockUi.unblock(button);
                }
            });
        });
    }
    , showNewUploadedFile: function () {
        var hashs = [];
        $(ReferralReceive.scanSaveClass).each(function (index, value) {
            hashs.push($(value).data('hash'));
        });
        $.ajax({
            url: '/admin/referral-receive/ajax?operation=showUploadedFile',
            data: {fileHashs: hashs},
            type: 'POST',
            dataType: 'json',
            async: false,
            success: function (response) {
                if (response.errors == false) {
                    if (response.newUploadedFiles.length > 0) {
                        $('.documents').append(response.newUploadedFiles);
                    }
                    $.each(response.dropFilesPreview,function (index, value) {
                        $('button[data-hash="' + value + '"]').parents(ReferralReceive.scanPreviewClass).remove();
                    });
                }
            },
            complete: function () {
            }
        });
    }
};