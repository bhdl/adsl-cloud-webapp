$.fn.serializeObject = function()
{
    // ckeditor
    for (var i in CKEDITOR.instances) {
        var instance = CKEDITOR.instances[i];
        instance.updateElement();
    }

    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};