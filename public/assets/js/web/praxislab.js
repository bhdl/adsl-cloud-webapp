$(function() {
	var animationIn ="";
	var animationOut ="";
	var slider = $('#carousel-praxislab');
	
	$(slider).carousel({
		interval: 5000
	});

	
	$(slider).swiperight(function() {
		$(slider).carousel('prev');
	});

	$(slider).swipeleft(function() {  
		$(slider).carousel('next');  
	});
	
	$(slider).on('slide.bs.carousel', function (event) {
		// Animation starts
		var current = $(this).find(".item.active").find('.slideImg');
		var target = event.relatedTarget;
		var tagerImg = $(target).find('.slideImg');
		$(current).removeClass(animationIn + ' ' + animationOut).addClass(animationOut);
		$(tagerImg).removeClass(animationIn + ' ' + animationOut).addClass(animationIn);
		
	});
	$(slider).on('slid.bs.carousel', function (event) {
		// Animation complete
	});

	$( ".animated-input" ).each(function(){
		if ($(this).val()!="" || $(this).text()!=""){
			$(this).addClass('filled-input');
		}
		else {
			$(this).removeClass('filled-input');
		}
	});

	$( ".animated-input" ).focusout(function() {
		if ($(this).val()!="" || $(this).text()!=""){
			$(this).addClass('filled-input');
		}
		else {
			$(this).removeClass('filled-input');
		}
	});

  	$('[data-toggle="tooltip"]').tooltip();

  	
  	$(document).on('click', '.comment-input-box-trigger', function(e){
		e.stopPropagation();
		e.preventDefault();

		$(this).next('.comment-input-box').slideDown("slow", function() {
			$(this).find('.comment-name').delay(500).focus();
		});
	});
});

function validateEmail(email) { 
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}