/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

    // ACS custom config
    config.autoParagraph = false;
    config.enterMode = CKEDITOR.ENTER_P;
    config.fillEmptyBlocks = true;
    config.entities = false;
    config.entities_latin = false;
    config.pasteFromWordPromptCleanup = true;

    // Toolbar
    // ------------------------------

    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'document',	   groups: [ 'mode' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'align' ] }
    ];


    // Extra config
    // ------------------------------

    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'Subscript,Superscript,Scayt,Anchor,Image,Table,HorizontalRule,SpecialChar,Maximize,Styles,Format,About,Blockquote,Strike,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField';

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';

    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';

    // Allow content rules
    config.allowedContent = true;


    // Extra plugins
    // ------------------------------

    // CKEDITOR PLUGINS LOADING
    config.extraPlugins = ''; // add other plugins here (comma separated)
};
