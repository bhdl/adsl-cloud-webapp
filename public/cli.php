<?php

/*
 * CLI entry point
 */

include_once 'required.php';

(new App\Cli\Bootstrap())->setArgc($argc)->setArgv($argv)->run(true);