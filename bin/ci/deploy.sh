#!/bin/bash

echo "Yeah, we're happy."

# simply copy the files to webroot
# TODO: use rsync
rm -R /var/www/adsl.arteries.hu
cp -R . /var/www/adsl.arteries.hu

# permissions
chmod -R 0777 /var/www/adsl.arteries.hu/public/workspace /var/www/adsl.arteries.hu/workspace

# create the env file
echo "APP_ENV=dev" > /var/www/adsl.arteries.hu/.env

# restart queue
systemctl daemon-reload
systemctl restart adsl-queue-worker.service

# run migrations (dev)
/var/www/adsl.arteries.hu/vendor/bin/migrate migrate:up dev
