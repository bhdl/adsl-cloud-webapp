#!/bin/bash

echo "Vincent, are we happy?"

# composer
composer update

# directory permissions
chmod -R 0777 vendor public/workspace workspace

# create the env file
echo "APP_ENV=ci" > .env

# set up db
mysql -u$DB_USER -h$DB_HOST -p$DB_PASSWORD --execute="DROP DATABASE IF EXISTS $DB_SCHEMA;CREATE DATABASE $DB_SCHEMA COLLATE 'utf8_general_ci'"
mysql -u$DB_USER -h$DB_HOST -p$DB_PASSWORD $DB_SCHEMA < schemas/initial_schema.sql

# init&run migrations (ci)
vendor/bin/migrate migrate:init ci
vendor/bin/migrate migrate:up ci

# run tests
vendor/bin/codecept run unit --coverage
