/* ------------------------------------------------------------------------------
 *
 *  # Gulp file
 *
 *  Basic Gulp tasks for Limitless template
 *
 *  Version: 1.1
 *  Latest update: Aug 20, 2016
 *
 * ---------------------------------------------------------------------------- */


// Include gulp
var gulp = require('gulp'); 


// Include our plugins
var jshint = require('gulp-jshint');
var less = require('gulp-less');
var minifyCss = require('gulp-clean-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();


// Lint task
/*gulp.task('lint', function() {
    return gulp
        .src('public/assets/js/core/app.js')                 // lint core JS file. Or specify another path
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});*/

// Compile less files of a full version
gulp.task('lessLab', function() {
    return gulp
        .src('public/assets/less/_main_full/*.less')         // locate /less/ folder root to grab 4 main files
        .pipe(less())                                 // compile
        .pipe(gulp.dest('public/assets/css'))                // destination path for normal CSS
        .pipe(minifyCss({                             // minify CSS
            keepSpecialComments: 0                    // remove all comments
        }))
        .pipe(rename({                                // rename file
            suffix: ".min"                            // add *.min suffix
        }))
        .pipe(gulp.dest('public/assets/css'));               // destination path for minified CSS
});

// web less files
/*gulp.task('lessWeb', function() {
    return gulp
        .src('public/assets/less/web/*.less')
        .pipe(less())                                 
        .pipe(gulp.dest('public/assets/css/web'))
        .pipe(minifyCss({                             
            keepSpecialComments: 0                    
        }))
        .pipe(rename({                                
            suffix: ".min"                            
        }))
        .pipe(gulp.dest('public/assets/css/web'))
});*/


// Compile less files of starter kit
/*gulp.task('less_starters', function() {
    return gulp
        .src('assets/less/_main_starters/*.less')     // locate /less/ folder root to grab 4 main files
        .pipe(less())                                 // compile
        .pipe(gulp.dest('starters/assets/css'))       // destination path for normal CSS
        .pipe(minifyCss({                             // minify CSS
            keepSpecialComments: 0                    // remove all comments
        }))
        .pipe(rename({                                // rename file
            suffix: ".min"                            // add *.min suffix
        }))
        .pipe(gulp.dest('starters/assets/css'));      // destination path for minified CSS
});*/


// Concatenate & minify JS (uncomment if you want to use)
/*gulp.task('minifyLab', function() {
    return gulp
        .src('assets/js/app/*.js')                    // path to js files you want to concat
        .pipe(concat('lab.js'))                       // output file name
        .pipe(gulp.dest('assets/js'))                 // destination path for normal JS
        .pipe(rename({                                // rename file
            suffix: ".min"                            // add *.min suffix
        }))
        .pipe(uglify())                               // compress JS
        .pipe(gulp.dest('assets/js'));                // destination path for minified JS
});*/


// Minify template's core JS file
/*gulp.task('minify_core', function() {
    return gulp
        .src('public/assets/js/core/app.js')                 // path to app.js file
        .pipe(uglify())                               // compress JS
        .pipe(rename({                                // rename file
            suffix: ".min"                            // add *.min suffix
        }))
        .pipe(gulp.dest('public/assets/js/core/'));          // destination path for minified file
});*/


// Watch files for changes
gulp.task('watch', function() {
    /*gulp.watch('public/assets/js/core/app.js', [             // listen for changes in app.js file and automatically compress
        'lint',                                       // lint
        //'concatenate',                              // concatenate & minify JS files (uncomment if in use)
        'minify_core'                                 // compress app.js
    ]);*/

    gulp.watch('public/assets/less/**/*.less', [/*'lessWeb',*/ 'lessLab']);    // listen for changes in all LESS files and automatically re-compile
});

// Default task
gulp.task('default', [                                // list of default tasks
    'lessLab'
//    , 'lessWeb'
//    , 'minifyLab'
    , 'watch'
]);
