<?php

namespace App\Rest;

use App\Module as AbstractModule;

/**
 * Class Module
 * @package App\Rest
 */
class Module
    extends AbstractModule
{
    /**
     * @return array
     */
    public function getDefaultServices()
    {
        return [
            // default szolgáltatások
            'errorHandler'                   => '\\App\\ServiceProvider\\Service\\Rest\\ErrorHandler'

            // module szolgáltatások
            , 'db'                           => '\\App\\ServiceProvider\\Service\\Db'
            , 'session'                      => '\\App\\ServiceProvider\\Service\\Session'
            , 'modelsMetaDataCache'          => '\\App\\ServiceProvider\\Service\\ModelsMetaDataCache'
            , 'modelsCache'                  => '\\App\\ServiceProvider\\Service\\ModelsCache'
            , 'modelsManager'                => '\\App\\ServiceProvider\\Service\\ModelsManager'
            , 'auth'                         => '\\App\\ServiceProvider\\Service\\Auth'
            , 'logCache'                     => '\\App\\ServiceProvider\\Service\\LogCache'
            , 'mailer'                       => '\\App\\ServiceProvider\\Service\\Mailer'
            , 'logger'                       => '\\App\\ServiceProvider\\Service\\Logger'
//            , 'url'                          => '\\App\\ServiceProvider\\Service\\Url'
            , 'dispatcher'                   => '\\App\\ServiceProvider\\Service\\Dispatcher'
            , 'translate'                    => '\\App\\ServiceProvider\\Service\\Translate'
            , 'eventsManager'                => '\\App\\ServiceProvider\\Service\\EventsManager'
            , 'userTokenListener'            => '\\App\\ServiceProvider\\Service\\Admin\\Listener\\UserToken'
            , 'acl'                          => '\\App\\ServiceProvider\\Service\\Acl'
            , 'tag'                          => '\\App\\ServiceProvider\\Service\\Admin\\Tag'
            , 'view'                         => '\\App\\ServiceProvider\\Service\\Rest\\View'
            , 'security'                     => '\\App\\ServiceProvider\\Service\\Security'
            , 'locale'                       => '\\App\\ServiceProvider\\Service\\Locale'
            , 'filter'                       => '\\App\\ServiceProvider\\Service\\Filter'
        ];
    }

    /**
     * @param \Phalcon\DiInterface|null $dependencyInjector
     */
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null)
    {
        $loader = $dependencyInjector->get('loader');

        $version = $dependencyInjector->get('router')->getParams();

        if(isset($version["version"])) {
            $version = $version["version"];
        }

        $loader->registerDirs([
            $dependencyInjector->get('app')->getAppPath('modules/rest/controllers/v'.$version)
        ]);
    }
}
