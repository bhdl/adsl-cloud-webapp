<?php

use App\Controller\AbstractRestController;
use App\Model\Site;
use Phalcon\Validation;
use App\Validation\Validator\PresenceOf;
use App\Validation\Validator\Db\Uniqueness;
use Phalcon\Validation\Validator\Json as JsonValidator;

/**
 * Class SiteController
 */
class SiteController extends AbstractRestController
{
    protected $setup = [
        'model' => 'App\Model\Site'
        , 'dataKey' => [
            'singular' => 'site'
            , 'plural' => 'sites'
        ]
    ];


    protected $validator;

    protected static $excluded = [
        'name'
    ];

    public function initialize()
    {
        parent::initialize();
        $this->validator = new Validation();
        $this->validator->add(
            'partnerId',
            new PresenceOf([
                'message' => $this->translate->t('The partner is required!')
            ])
        );
        $this->validator->add(
            'gatewayId',
            new PresenceOf([
                'message' => $this->translate->t('The gateway id is required!')
            ])
        );
        $this->validator->add(
            'gatewayId',
            new Uniqueness([
                "table"       => "site"
                , "column"    => "gatewayId"
                , 'message'   => $this->translate->t('Ez a Gateway ID már tartozik telephelyhez!')
            ])
            );
        $this->validator->add(
            'address',
            new PresenceOf([
                'message' => $this->translate->t('The address is required!')
            ])
        );
        $this->validator->add(
            'mapSvg',
            new JsonValidator([
                'message' => $this->translate->t('Wrong JSON format')
            ])
        );
    }
}