<?php

use App\Controller\AbstractRestController;
use App\Model\Tag;
use Phalcon\Validation;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\InclusionIn;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class TagController
 */
class TagController
    extends AbstractRestController
{

    protected $setup = [
        'model' => 'App\Model\Tag'
        , 'dataKey' => [
            'singular' => 'tag'
            , 'plural' => 'tags'
        ]
    ];

    protected $validator;

    public function initialize()
    {
        parent::initialize();

        $this->validator = new Validation();
        $this->validator->add(
            'type'
            , new PresenceOf(array(
                'message' => $this->translate->t('The type is required!')
            ))
        );

        $this->validator->add(
            "type",
            new InclusionIn([
              "message" => "The type must be in this: ".implode(", ", App\Model\Tag::$types),
              "domain"  => App\Model\Tag::$types,
          ])
        );

        $this->validator->add(
            'name'
            , new PresenceOf(array(
            'message' => $this->translate->t('The name is required!')
        )));

        $this->validator->add(
            'tagToken'
            , new Uniqueness([
                "table"       => "tag"
                , "column"    => "tagToken"
                , 'message'   => $this->translate->t('The tagToken is required!')
            ])
        );

        $this->validator->add(
            'description'
            ,new PresenceOf(array(
                'message' => $this->translate->t('The description is required!')
            ))
        );
    }
}
