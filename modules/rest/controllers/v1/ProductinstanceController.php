<?php

use App\Controller\AbstractRestController;
use App\Model\ProductInstance;
use Phalcon\Validation;
use App\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\InclusionIn;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class ProductinstanceController
 */
class ProductinstanceController
    extends AbstractRestController
{

    protected $setup = [
        'model' => 'App\Model\ProductInstance'
        , 'dataKey' => [
            'singular' => 'ProductInstance'
            , 'plural' => 'ProductInstances'
        ]
    ];

    protected $validator;

    public function initialize()
    {
        parent::initialize();

        $this->validator = new Validation();

    }
}
