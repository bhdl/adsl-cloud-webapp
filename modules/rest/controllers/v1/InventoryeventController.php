<?php

use App\Controller\AbstractRestController;
use App\Model\InventoryEvent;
use App\Model\Tag;
use App\Model\Site;
use App\Model\ProductInstance;
use Phalcon\Validation;
use App\Validation\Validator\PresenceOf;
use App\Validation\Validator\InventoryEventDate;
use App\Model\InventorySnapshot;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

/**
 * Class InventoryeventController
 */
class InventoryeventController extends AbstractRestController
{
    protected $setup = [
        'model' => 'App\Model\InventoryEvent'
        , 'dataKey' => [
            'singular' => 'inventoryEvent'
            , 'plural' => 'inventoryEvents'
        ]
    ];

    public function initialize()
    {
        parent::initialize();
        $this->validator = new Validation();

        if ($this->request->isPost()) {
            $this->validator->add(
                'siteId',
                new PresenceOf([
                    'message' => $this->translate->t('The site is required!')
                ])
            );
            $this->validator->add(
                'startTime',
                new PresenceOf([
                    'message' => $this->translate->t('The start time is required!')
                ])
            );

            $this->validator->add(
                'endTime',
                new PresenceOf([
                    'message' => $this->translate->t('The end time is required!')
                    , 'cancelOfFail' => true
                ])
            );

            $this->validator->add(
                'endTime',
                new InventoryEventDate([
                    'startTime' => 'startTime'
                ])
            );
            /* $this->validator-> */
        }
    }

    public function createAction()
    {
        try {
            if ($this->request->isPost()) {
                $this->db->beginLogged();

                $model = new $this->model;

                $postData = (array)$this->request->getJsonRawBody();
                $postData["endTime"]= $postData["startTime"];

                if(isset($this->validator)) {
                    $messages = $this->validator->validate($postData);
                    if(count($messages) > 0) {
                        return $this->generalResponse(400, $messages[0]);
                    }
                }

                $model->assign($postData);

                $this->beforeModelCreate();

                $model->create();

                $this->afterModelCreate();

                $this->db->commitLogged();

                $data = $this->model::find([
                    'columns' => 'inventoryEventId, siteId, startTime, endTime',
                    'conditions' => 'inventoryEventId = ?1',
                    'bind' => [
                        1 => $model->getPrimaryKey()
                    ]
                ]);

                $this->createInventorySnapshot($model->getPrimaryKey(), $postData["siteId"]);
                

                return $this->generalResponse(
                    201
                    , ""
                    , $data
                );
            }

        } catch ( \PDOException $b ) {
            $this->db->rollbackLogged();

            return $this->generalResponse(
                400
                , $b->getMessage()
            );
        } catch (Exception $e) {
            $this->db->rollbackLogged();

            $message = $e->getMessage();

            if ($model->validationHasFailed()) {
                $message = $model->getMessagesAsString();
            }

            $this->logger->error($message, ['exception' => $e]);

            return $this->generalResponse(
                400
                , $message
            );
        }
    }

    public function modifyAction()
    {
        try {
            if ($this->request->isPut()) {
                $this->db->beginLogged();

                $postData = (array)$this->request->getJsonRawBody();

                $id = $postData["inventoryEventId"];

                $model = $this->model::findFirst($id);

                if (!$model || $model->count() == 0) {
                    return $this->generalResponse(404);
                }

                if(isset($this->validator)) {
                    $messages = $this->validator->validate($postData);
                    if(count($messages) > 0) {
                        return $this->generalResponse(406, $messages[0]);
                    }
                }

                $this->beforeModelModify();

                $model->update($postData);

                $this->afterModelModify();

                $this->db->commitLogged();
               

                $data = $this->model::find([
                    'columns' => 'inventoryEventId, siteId, startTime, endTime',
                    'conditions' => 'inventoryEventId = ?1',
                    'bind' => [
                        1 => $model->getPrimaryKey()
                    ]
                ]);

                $site = Site::find([
                    'columns' => 'siteId, gatewayId',
                    'conditions' => 'siteId = ?1',
                    'bind' => [
                        1 => $postData["siteId"]
                    ]
                ]);
                $tag = Tag::find([
                    'conditions' => 'siteId = ?1 AND clientId IS NOT NULL',
                    'bind' => [
                        1 => $site[0]->siteId,
                    ],
                    'columns' => 'tagId, siteId, tagToken, clientId, type, name, description'
                ]);
                $temp = $data->toArray()[0];
                $temp['startTime'] = (new DateTime($temp['startTime']))->getTimestamp();
                $temp['endTime'] = (new DateTime($temp['endTime']))->getTimestamp();
                $temp["gatewayId"] = $site[0]->gatewayId;
                $temp["tag"] = $tag;

                $data = $temp;

                return $this->generalResponse(
                    200
                    , ""
                    , $data
                );
            }

        } catch ( \PDOException $b ) {
            $this->db->rollbackLogged();

            return $this->generalResponse(
                400
                , $b->getMessage()
            );
        } catch (Exception $e) {
            $this->db->rollbackLogged();

            $message = $e->getMessage();

            if ($model->validationHasFailed()) {
                $message = $model->getMessagesAsString();
            }

            $this->logger->error($message, ['exception' => $e]);

            return $this->generalResponse(
                400
                , $message
            );
        }
    }

    private function createInventorySnapshot($inventoryEventId, $siteId)
    {
        $productInstance = ProductInstance::find([
            'conditions' => 'siteId = :siteId:',
            'bind' => [
                'siteId' => $siteId
            ] 
        ]);

        $transactionManager = new TransactionManager();
        $transaction = $transactionManager->get();
        
        foreach ($productInstance->toArray() as $key => $value) {
            $inventorySnapshotInstance = new InventorySnapshot();
            $value["inventoryEventId"] = $inventoryEventId;
            $inventorySnapshotInstance->setTransaction($transaction);
            $inventorySnapshotInstance->assign((array) $value);
            $inventorySnapshotInstance->create();
        }

        try {
            $transaction->commit();
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
}