<?php

use App\Controller\AbstractRestController;
use App\Model\Product;
use Phalcon\Validation;
use App\Validation\Validator\PresenceOf;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class ProductController
 */
class ProductController extends AbstractRestController
{
    protected $setup = [
        'model' => 'App\Model\Product'
        , 'dataKey' => [
            'singular' => 'product'
            , 'plural' => 'products'
        ]
    ];

    protected $validator;

    public function initialize()
    {
        parent::initialize();
        $this->validator = new Validation();
        $this->validator->add(
            'partnerId',
            new PresenceOf([
                'message' => $this->translate->t('The partner is required!')
            ])
        );
        $this->validator->add(
            'name',
            new PresenceOf([
                'message' => $this->translate->t('The name is required!')
            ])
        );

        $id = $this->di->get('router')->getParams();

        $options = [];
        $options[] = [
            'column' => 'deleted',
            'value' => \App\Model\AbstractModel::YES
        ];

        if (isset($id['id']) && (int)$id['id'] > 0) {
            $options[] = [
                'column' => 'productId',
                'value'  => $id['id']
            ];
        }

        $this->validator->add(
            'name'
            , new Uniqueness([
                'table'       => 'product'
                , 'column'    => [ 'name', 'partnerId' ]
                , 'message'   => $this->translate->t('The name you entered is currently being used by another product!')
                , 'exclude'   => $options
            ])
        );

    }

    public function viewAction()
    {
        $data = parent::viewAction();

        $dataToModify = json_decode($data->getContent());

        if(isset($dataToModify->data->products)) {
            foreach($dataToModify->data->products as $product) {
                $product->width = 100 * $product->productId % 120;
                $product->height = ceil(100 * $product->productId % 120 / 3);
                $product->length = ceil(100 * $product->productId % 120 * 1.5);
                $product->mass = 100 * $product->productId % 150;
            }
        }

        $data->setContent(json_encode($dataToModify));

        return $data;
    }
}