<?php

use App\Controller\AbstractRestController;
use App\Model\ProductPositionEvent;
use App\Model\ProductPosition;
use Phalcon\Validation;
use App\Validation\Validator\PresenceOf;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class ProductPositionController
 */
class ProductPositionController extends AbstractRestController
{
    protected $setup = [
        'model' => 'App\Model\ProductPositionEvent'
        , 'dataKey' => [
            'singular' => 'productPosition'
            , 'plural' => 'productsPositions'
        ]
    ];

    protected $validator;

    public function initialize()
    {
        parent::initialize();

        $this->validator = new Validation();
    }

    /**
     *
     */
    private function searchProduct($needle, $racksAfterOptimising)
    {
        foreach($racksAfterOptimising as $rack) {
            foreach($rack->containedProduct as $product) {
                if ($needle['productId'] == $product->product->ID
                   && $needle['count'] == $product->numberOfProduct)
                {
                    return $rack->position;
                }
            }
        }
        return false;
    }

    /**
     *
     */
    public function createAction()
    {
        try {
            if ($this->request->isPost()) {
                $this->db->beginLogged();

                $postData = (array)$this->request->getJsonRawBody();
                $lastProduction = ProductPositionEvent::maximum(["column" => "productPositionEventId"]);

                $productPositionEvent                    = ProductPositionEvent::findFirst("productPositionEventId = $lastProduction");
                $productPositionEvent->positionEventData = json_encode($postData);
                $productPositionEvent->end               = date("Y-m-d H:i:s");
                $productPositionEvent->save();

                $productList = [];
                if($postData["racksBeforeOptimising"]) {
                    $postData["racksBeforeOptimising"] = json_decode($postData["racksBeforeOptimising"]);
                }
                foreach($postData["racksBeforeOptimising"] as $element) {
                    foreach($element->containedProduct as $product) {
                        $productList[] = [
                            "productId"  => $product->product->ID
                            , "currentX" => $element->position->x
                            , "currentY" => $element->position->y
                            , "currentZ" => $element->position->z
                            , "newX"     => 0
                            , "newY"     => 0
                            , "newZ"     => 0
                            , "count"    => $product->numberOfProduct
                        ];
                    }
                }
                if($postData["racksAfterOptimised"]) {
                    $postData["racksAfterOptimised"] = json_decode($postData["racksAfterOptimised"]);
                }
                // Todo jelezze hogyha az afterban több vagy kevesebb elem van
                if (!empty($productList)) {
                    foreach($productList as $key => $product) {
                        if ($temp = $this->searchProduct($product, $postData["racksAfterOptimised"])) {
                            $productList[$key]['newX'] = $temp->x;
                            $productList[$key]['newY'] = $temp->y;
                            $productList[$key]['newZ'] = $temp->z;
                        }

                        for($i = 0;$i < $productList[$key]['count']; $i++) {
                            $productPosition = new ProductPosition();
                            $productPosition->productPositionEventId = $productPositionEvent->productPositionEventId;
                            $productPosition->productId = $productList[$key]['productId'];
                            $productPosition->currentX= $productList[$key]['currentX'];
                            $productPosition->currentY= $productList[$key]['currentY'];
                            $productPosition->currentZ= $productList[$key]['currentZ'];
                            $productPosition->newX= $productList[$key]['newX'];
                            $productPosition->newY= $productList[$key]['newY'];
                            $productPosition->newZ= $productList[$key]['newZ'];
                            $productPosition->save();
                        }
                    }
                }

                $this->db->commitLogged();

                return $this->generalResponse(
                    201
                    , ""
                    , ["productPositionEventId" => $productPositionEvent->productPositionEventId]
                );
            }

        } catch ( \PDOException $b ) {
            $this->db->rollbackLogged();

            return $this->generalResponse(
                400
                , $b->getMessage()
            );
        } catch (Exception $e) {
            $this->db->rollbackLogged();

            $message = $e->getMessage();

            if ($model->validationHasFailed()) {
                $message = $model->getMessagesAsString();
            }

//            $this->logger->error($message, ['exception' => $e]);

            return $this->generalResponse(
                400
                , $message
            );
        }
    }
}
