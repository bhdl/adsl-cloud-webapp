<?php

use App\Controller\AbstractRestController;

/**
 * Class CurrentdatetimeController
 */
class CurrentdatetimeController extends AbstractRestController
{
    protected $validator;

    public function initialize()
    {
        parent::initialize();
    }

    public function viewAction()
    {
        return date('Y-m-d H:i:s');
    }

    public function createAction(){}
    public function deleteAction(){}
    public function modifyAction(){}
}