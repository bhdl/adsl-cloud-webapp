<?php

use App\Controller\AbstractRestController;
use App\Model\Partner;
use Phalcon\Validation;
use App\Validation\Validator\PresenceOf;
use App\Validation\Validator\Db\Uniqueness;

/**
 * Class PartnerController
 */
class PartnerController extends AbstractRestController
{
    protected $setup = [
        'model' => 'App\Model\Partner'
        , 'dataKey' => [
            'singular' => 'partner'
            , 'plural' => 'partners'
        ]
    ];

    protected $validator;

    public function initialize()
    {
        parent::initialize();

        $this->validator = new Validation();
        $this->validator->add(
            'name'
            , new PresenceOf([
                'message' => $this->translate->t('The name is required!')
            ])
        );

        $this->validator->add(
            'name'
            , new Uniqueness([
                "table"       => "partner"
                , "column"    => "name"
                , 'message'   => $this->translate->t('The name you entered is currently being used by another partner')
            ])
        );
    }
}