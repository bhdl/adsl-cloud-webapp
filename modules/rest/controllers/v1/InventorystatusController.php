<?php

use App\Controller\AbstractRestController;
use App\Model\InventoryStatus;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use Phalcon\Validation;
use App\Validation\Validator\PresenceOf;

/**
 * Class InventorystatusController
 */
class InventorystatusController extends AbstractRestController
{
    protected $setup = [
        'model' => 'App\Model\InventoryStatus'
        , 'dataKey' => [
            'singular' => 'inventoryStatus'
            , 'plural' => 'inventoryStatuses'
        ]
    ];

    protected $validator;

    public function initialize()
    {
        parent::initialize();
        $this->validator = new Validation();
        if ($this->request->isPost()) {
            $this->validator->add(
                'inventoryEventId',
                new PresenceOf([
                    'message' => $this->translate->t('Inventory event ID is required!')
                ])
            );
            $this->validator->add(
                'rfid',
                new PresenceOf([
                    'message' => $this->translate->t('RFID is required!')
                ])
            );
            $this->validator->add(
                'x',
                new PresenceOf([
                    'message' => $this->translate->t('X coordinate is required!')
                ])
            );
            $this->validator->add(
                'y',
                new PresenceOf([
                    'message' => $this->translate->t('Y coordinate is required!')
                ])
            );
            $this->validator->add(
                'z',
                new PresenceOf([
                    'message' => $this->translate->t('Z coordinate is required!')
                ])
            );
            $this->validator->add(
                'accuracy',
                new PresenceOf([
                    'message' => $this->translate->t('Accuracy is required!')
                ])
            );
            $this->validator->add(
                'latitude',
                new PresenceOf([
                    'message' => $this->translate->t('Latitude is required!')
                ])
            );
            $this->validator->add(
                'longitude',
                new PresenceOf([
                    'message' => $this->translate->t('Longitude is required!')
                ])
            );
        }
    }

    public function createAction()
    {
        try {
            if ($this->request->isPost()) {
                $this->db->beginLogged();

                $postData = (array)$this->request->getJsonRawBody();

                //$transactionManager = new TransactionManager();
                //$transaction = $transactionManager->get();
  
                if (isset($postData['inventoryEvents']) && !empty($postData['inventoryEvents'])) {
                    foreach ($postData['inventoryEvents'] as $key => $value) {
                        $value->inventoryEventId = $postData['inventoryEventId'];
                        if(isset($this->validator)) {
                            $messages = $this->validator->validate($value);
                            if(count($messages) > 0) {
                                if ($messages[0]->getType() != "PresenceOf") {
                                    return $this->generalResponse(406, $messages[0]);
                                }
                                return $this->generalResponse(400, $messages[0]);
                            }
                        }
                        $model = new $this->model;
                        //$model->setTransaction($transaction);
                        $model->assign((array) $value);
                        $this->beforeModelCreate();

                        $model->create();

                        $this->afterModelCreate();
                        $ids[] = $model->getPrimaryKey();
                    }
                    
                    //$transaction->commit();

                    $this->db->commitLogged();

                    $data = $this->model::find([
                        'conditions' => 'inventoryStatusId IN ({ids:array})',
                        'bind' => [
                            'ids' => $ids
                        ]
                    ]);

                    $returnData = [];

                    foreach($data as $val) {
                        $returnData[] = $this->transformData($val);
                    }
                } else {
                    $returnData = [];
                }
                
                return $this->generalResponse(
                    201
                    , ""
                    , $returnData
                );
            }

        } catch ( \PDOException $b ) {
            $this->db->rollbackLogged();

            return $this->generalResponse(
                400
                , $b->getMessage()
            );
        } catch (Exception $e) {
            $this->db->rollbackLogged();

            $message = $e->getMessage();

            if ($model->validationHasFailed()) {
                $message = $model->getMessagesAsString();
            }

            $this->logger->error($message, ['exception' => $e]);

            return $this->generalResponse(
                400
                , $message
            );
        }
    }
}