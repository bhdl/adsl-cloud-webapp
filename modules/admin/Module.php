<?php

namespace App\Admin;

use App\Module as AbstractModule;

/**
 * Class Module
 * @package App\Admin
 */
class Module
    extends AbstractModule
{
    /**
     * @return array
     */
    public function getDefaultServices()
    {
        return [
            'db'                             => '\\App\\ServiceProvider\\Service\\Db'
            , 'session'                      => '\\App\\ServiceProvider\\Service\\Session'
            , 'modelsMetaDataCache'          => '\\App\\ServiceProvider\\Service\\ModelsMetaDataCache'
            , 'modelsCache'                  => '\\App\\ServiceProvider\\Service\\ModelsCache'
            , 'modelsManager'                => '\\App\\ServiceProvider\\Service\\ModelsManager'
            , 'auth'                         => '\\App\\ServiceProvider\\Service\\Auth'
            , 'logCache'                     => '\\App\\ServiceProvider\\Service\\LogCache'
            , 'mailer'                       => '\\App\\ServiceProvider\\Service\\Mailer'
            , 'logger'                       => '\\App\\ServiceProvider\\Service\\Logger'
            , 'url'                          => '\\App\\ServiceProvider\\Service\\Url'
            , 'dispatcher'                   => '\\App\\ServiceProvider\\Service\\Dispatcher'
            , 'flash'                        => '\\App\\ServiceProvider\\Service\\Flash'
            , 'flashSession'                 => '\\App\\ServiceProvider\\Service\\FlashSession'
            , 'assets'                       => '\\App\\ServiceProvider\\Service\\Admin\\Assets'
            , 'translate'                    => '\\App\\ServiceProvider\\Service\\Translate'
            , 'aclCache'                     => '\\App\\ServiceProvider\\Service\\AclCache'
            , 'eventsManager'                => '\\App\\ServiceProvider\\Service\\EventsManager'
            , 'userTokenListener'            => '\\App\\ServiceProvider\\Service\\Admin\\Listener\\UserToken'
            , 'acl'                          => '\\App\\ServiceProvider\\Service\\Acl'
            , 'breadcrumb'                   => '\\App\\ServiceProvider\\Service\\Admin\\BreadCrumb'
            , 'navigationHistory'            => '\\App\\ServiceProvider\\Service\\Admin\\NavigationHistory'
            , 'mainNavigation'               => '\\App\\ServiceProvider\\Service\\Admin\\MainNavigation'
            , 'headingButtons'               => '\\App\\ServiceProvider\\Service\\Admin\\HeadingButtons'
            , 'languageSwitch'               => '\\App\\ServiceProvider\\Service\\Admin\\LanguageSwitch'
            , 'tag'                          => '\\App\\ServiceProvider\\Service\\Admin\\Tag'
            , 'view'                         => '\\App\\ServiceProvider\\Service\\Admin\\View'
            , 'pdfResultSet'                 => '\\App\\ServiceProvider\\Service\\Admin\\PdfResultSet'
            , 'security'                     => '\\App\\ServiceProvider\\Service\\Security'
            , 'aclListener'                  => '\\App\\ServiceProvider\\Service\\Listener\\Acl'
            , 'userStatusCheckListener'      => '\\App\\ServiceProvider\\Service\\Admin\\Listener\\UserStatusCheck'
            , 'sessionDetectListener'        => '\\App\\ServiceProvider\\Service\\Admin\\Listener\\SessionDetect'
            , 'userPasswordOutdatedListener' => '\\App\\ServiceProvider\\Service\\Admin\\Listener\\UserPasswordOutdated'
            , 'keepAliveListener'            => '\\App\\ServiceProvider\\Service\\Admin\\Listener\\KeepAlive'
            , 'modelInjectorListener'        => '\\App\\ServiceProvider\\Service\\Admin\\Listener\\ModelInjector'
            , 'modelAclListener'             => '\\App\\ServiceProvider\\Service\\Admin\\Listener\\ModelAcl'
            , 'navigationHistoryListener'    => '\\App\\ServiceProvider\\Service\\Admin\\Listener\\NavigationHistory'
            , 'locale'                       => '\\App\\ServiceProvider\\Service\\Locale'
            , 'filter'                       => '\\App\\ServiceProvider\\Service\\Filter'
            , 'queue'                        => '\\App\\ServiceProvider\\Service\\Queue'
            , 'hotKeysListener'              => '\\App\\ServiceProvider\\Service\\Admin\\Listener\\HotKeys'
            , 'searchBar'                    => '\\App\\ServiceProvider\\Service\\Admin\\SearchBar'
            , 'scanCache'                    => '\\App\\ServiceProvider\\Service\\ScanCache'
//            'profiler'=> '\\App\\ServiceProvider\\Service\\Profiler'
        ];
    }

    /**
     * @param \Phalcon\DiInterface|null $dependencyInjector
     */
    public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector = null)
    {
        $loader = $dependencyInjector->get('loader');

        $loader->registerDirs([
            $dependencyInjector->get('app')->getAppPath('modules/admin/controllers')
        ]);
    }
}