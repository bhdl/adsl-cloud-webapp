<?php

use App\Auth\Adapter\Database;
use App\Form\Password\Email as EmailForm;
use App\Form\Password\Change as ChangeForm;
use Phalcon\Exception;
use App\Model\User;
use App\Model\UserResetPassword;

/**
 * Class ForgotPasswordController
 */
class PasswordController
    extends \App\Controller\AbstractController
{
    /**
     * Jelszó módosítás után jelentkeztessük-e be a felhasználót
     * @var bool
     */
    protected $automaticLogin = false;

    /**
     * Automatikus bejelentkezés és jelszó módosítás utáni oldalátirányítás URL-je
     * @var string
     */
    protected $redirectUrl;

    /**
     * @var string
     */
    protected $changePasswordUrl;

    /**
     * Email tárgya
     * @var string
     */
    protected $emailSubject = 'Elfelejtett jelszó';

    /**
     * initialize
     */
    public function initialize()
    {
        $this->view->setMainView('layouts/auth');
    }

    /**
     * Elfelejtett jelszó felület megjelenítése
     */
    public function indexAction()
    {
        $this->tag->prependTitle('Elfelejtett jelszó');

        $forgotPasswordForm = new EmailForm();

        /*
         * Mentés
         */
        if ($this->request->isPost()
            && $forgotPasswordForm->isValid($this->request->getPost())) {

            /*
             * Jelszó token generálása
             */

            try {

                $this->db->beginLogged();

                /*
                 * Felhasználó lekérdezése email cím alapján
                 */
                $user = User::findFirstByEmail(
                    $forgotPasswordForm->get('email')->getValue()
                );

                if ($user == false) {
                    throw new Exception($this->translate->_('A megadott e-mail cím nem létezik!'));
                }

                /*
                 * Jelszó modósítás kérvény létrehozása
                 */
                $resetPassword = new UserResetPassword();

                /*
                 * Felhasználó azonosítójának a beállítása
                 */
                $resetPassword->userId = $user->userId;

                /*
                 * Kérvény mentése
                 */
                $resetPassword->save();

                /*
                 * Ha ment ki email a felhasználónak csak akkor mentem el.
                 */
                if($this->sendEmail($user, $resetPassword->code)){
                    /*
                     * Mentés
                     */
                    $this->db->commitLogged();

                    /*
                     * Email cím törlése az input-ból
                     */
                    $forgotPasswordForm->get('email')->clear();

                    $this->flash->success($this->translate->_('Kérjük, ellenőrizze az e-mail fiókját!'));
                } else {
                    throw new Exception($this->translate->_('Az elfelejtett jelszóval kapcsolatos email-t nem sikerült kiküldeni. Kérjük, próbálja újra!'));
                }

            } catch (Exception $e) {

                $this->db->rollbackLogged();

                $this->logger->error($e->getMessage());

                $this->flash->error($e->getMessage());
            }
        }

        $this->view->form = $forgotPasswordForm;
    }

    /**
     * Jelszó módosítás felület megjelenítése
     */
    public function changeAction()
    {
        $this->tag->prependTitle('Jelszó változtatása');

        /*
         * Jelszó megadás űrlap generálása
         */
        $newPasswordForm = new ChangeForm();

        /*
         * Token alapján lekérdezzük, hogy van e ilyen jelszó modósítás kérelem
         */
        $model = UserResetPassword::findFirstByCode($this->request->getQuery('code'));

        try {
            if (empty($model)) {
                throw new Exception($this->translate->_('A jelszó módosítás nem lehetséges!'));
            }

            if ($model->reset == 'yes') {
                throw new Exception($this->translate->_('A jelszó változtatás már megtörtént!'));
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            $this->flashSession->error($e->getMessage());
            $this->response->redirect($this->getRedirectUrl());
        }

        /*
         * Mentés
         */
        if ($this->request->isPost()
            && $newPasswordForm->isValid($this->request->getPost())) {

            try {

                $this->db->beginLogged();

                /*
                 * Új jelszó
                 */
                $newPassword = $newPasswordForm->get('password')->getValue();

                /*
                 * Felhasználó lekérdezése
                 */
                $user = $model->user;

                /*
                 * Jelszó modósítása
                 */
                $user->setPassword($newPassword);

                /*
                 * Jelszó mentése
                 */
                $user->save();

                /*
                 * Új jelszó kérvény frissítése
                 */
                $model->save();

                /*
                 * Automatikus bejelenetkeztetéskor a felhasználót bejelentkeztetjük
                 */
                if ($this->isAutomaticLogin()) {
                    /*
                     * Felhasználó bejelentkeztetése
                     */
                    $adapter = new Database();

                    $adapter->setUserId($user->userId);

                    $this->auth->login($adapter);
                }

                $this->db->commitLogged();

                $this->logger->info('Sikeres jelszó módosítás!');

                $this->flashSession->success($this->translate->_('A jelszó módosítása sikerült!'));

                /*
                 * Oldal átirányítás
                 */
                $this->response->redirect($this->getRedirectUrl());

                $this->view->disable();

            } catch (Exception $e) {

                $this->db->rollbackLogged();

                $this->logger->error($e->getMessage());

                $this->flash->error($e->getMessage());
            }
        }

        $this->view->form = $newPasswordForm;
    }

    /**
     * Elavult jelszó oldal nézet
     */
    public function outdatedAction()
    {
        $form = new ChangeForm();

        if($this->request->isPost()
            && $form->isValid($this->request->getPost())) {

            try {
                /*
                 * @var \App\Model\User $user
                 */
                $user = $this->auth->getIdentity();

                /*
                 * Új jelszó
                 */
                $newPassword = $form->get('password')->getValue();

                $this->db->beginLogged();

                /*
                 * Új jelszó mentése
                 */
                $user->setPassword($newPassword);

                $user->save();

                $this->db->commitLogged();

                $this->logger->info('Sikeres jelszó módosítás!');

                $this->flashSession->success(
                    $this->translate->_('A jelszó módosítása sikerült!')
                );

                /*
                 * Oldal átírányítása
                 */
                $this->response->redirect($this->getRedirectUrl());
                $this->view->disable();
            } catch (Exception $e) {
                $this->db->rollbackLogged();

                $this->logger->error($e->getMessage());
                $this->flash->error($e->getMessage());
            }
        }

        $this->tag->prependTitle('Jelszó elévült');

        /*
         * Value értékek törlése
         */
        $form->get('password')->clear();
        $form->get('passwordRepeat')->clear();

        $this->view->form = $form;

        $this->view->pick('password/change');
    }

    /**
     * Email kiküldése a felhasználónak
     * @param \App\Model\User $user Felhasználó adatai
     * @param string $code Legenerált token azonosító
     * @return boolean
     */
    protected function sendEmail($user, $code)
    {
        $message = $this->mailer->createMessageFromView(
            'forgotpassword'
            , [
                'name'    => $user->getName()
                , 'email' => $user->email
                , 'url'   => $this->getChangePasswordUrl($code)
            ]
        );

        $message
            ->to($user->email, $user->getName())
            ->subject($this->emailSubject)
        ;

        return $message->send();
    }

    /**
     * @param string $code
     * @return string
     */
    public function getChangePasswordUrl($code)
    {
        if ($this->changePasswordUrl == null) {
            $this->changePasswordUrl =
                $this->app->getAppUrl(
                    '/' . $this->dispatcher->getModuleName()
                    . '/password/change'
                    . '?code=' . $code
                )
            ;
        }

        return $this->changePasswordUrl;
    }

    /**
     * @return boolean
     */
    public function isAutomaticLogin()
    {
        return $this->automaticLogin;
    }

    /**
     * @param boolean $automaticLogin
     */
    public function setAutomaticLogin($automaticLogin)
    {
        $this->automaticLogin = $automaticLogin;
    }

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        if ($this->redirectUrl == null) {
            $this->redirectUrl = '/' . $this->dispatcher->getModuleName();
        }

        return $this->redirectUrl;
    }

    /**
     * @param string $redirectUrl
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * @param string $emailSubject
     */
    public function setEmailSubject($emailSubject)
    {
        $this->emailSubject = $emailSubject;
    }
}