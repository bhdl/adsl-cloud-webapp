<?php

use App\Controller\AbstractCrudController;

/**
 * Class RequestController
 */
class RequestController extends ItemController
{
    /**
     * @return mixed|void
     * @throws \Phalcon\Exception
     */
    public function initialize()
    {
        $this->modifySetup();
        parent::initialize();

    }

    protected function modifySetup()
    {
        $this->setup['builder'] = 'App\Model\Builder\Request';
        $this->setup['export']  = [
            'spreadsheet' => 'App\Export\Spreadsheet\Rows\Request'
            , 'pdf'       => 'App\Export\Pdf\Template\Request'
        ];
        $this->setup['name'] = [
            'singular' => 'Kérés'
            , 'plural' => 'Kérések'
        ];
        $this->setup['hasManyToMany']['panel'] = [
            'model'           => 'App\Model\Item'
            , 'relationModel' => 'App\Model\ItemHasItem'
            , 'relationModelPrimaryKeys' => [
                'model' => 'requestId'
                , 'hasManyToManyModel' => 'panelId'
            ]
            , 'builder'       => 'App\Model\Builder\Panel'
            , 'presenter'     => 'App\Model\Presenter\Item'
            , 'forms' => [
                'create'   => 'App\Form\Item\Create'
                , 'modify' => 'App\Form\Item\Modify'
            ]
            , 'name' => [
                'singular' => 'Panel'
                , 'plural' => 'Panelek'
            ]
            , 'icon' => 'icon-list'
        ];
        $this->setup['hasManyToMany']['examination'] = [
            'model'           => 'App\Model\Examination'
            , 'relationModel' => 'App\Model\ExaminationHasRequest'
            , 'relationModelPrimaryKeys' => [
                'model' => 'requestId'
                , 'hasManyToManyModel' => 'examinationId'
            ]
            , 'builder'       => 'App\Model\Builder\Examination'
            , 'presenter'     => 'App\Model\Presenter\Examination'
            , 'forms' => [
                'create'   => 'App\Form\Examination\Create'
                , 'modify' => 'App\Form\Examination\Modify'
            ]
            , 'name' => [
                'singular' => 'Vizsgálat'
                , 'plural' => 'Vizsgálatok'
            ]
            , 'icon' => 'icon-lab'
        ];

    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface|void
     */
    public function dataTableAction()
    {
        $this->modifySetup();
        return parent::dataTableAction();
    }

    /**
     * beforeModelCreate
     */
    public function beforeModelCreate()
    {
        parent::beforeModelCreate();
        $this->model->itemType = \App\Model\Item::ITEM_TYPE_REQUEST;
    }

    /**
     * beforeHasManyToManyModelCreate
     */
    public function beforeHasManyToManyModelCreate()
    {
        $this->hasManyToManyModel->itemType = \App\Model\Item::ITEM_TYPE_REQUEST;
    }

    /**
     * @throws \Phalcon\Exception
     */
    public function addEntityHeadingButtons()
    {
        parent::addEntityHeadingButtons();

        $this->addHasManyToManyEntityHeadingButtons(
            $this->model->getPrimaryKey()
            , 'animalGroup'
        );

        $this->addHasManyToManyEntityHeadingButtons(
            $this->model->getPrimaryKey()
            , 'species'
        );

        $this->addHasManyToManyEntityHeadingButtons(
            $this->model->getPrimaryKey()
            , 'praxis'
        );

        $this->addHasManyToManyEntityHeadingButtons(
            $this->model->getPrimaryKey()
            , 'panel'
        );
    }
}