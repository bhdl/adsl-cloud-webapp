<?php

use Phalcon\Exception;
use Phalcon\Image;
use Phalcon\Image\Adapter\Gd;

/**
 * Class ProfileController
 */
class ProfileController
    extends \App\Controller\AbstractController
{
    /**
     * initialize
     */
    public function initialize()
    {
        $this->breadcrumb->add(
            $this->translate->_('Profil')
            , '/admin/profile'
        );

        $this->tag->prependTitle($this->translate->_('Profil'));

        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'password')) {
            $this->headingButtons->add(
                $this->translate->_('Jelszó')
                , '/admin/profile/password'
                , 'icon-key'
            );
        }

        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'avatar')) {
            $this->headingButtons->add(
                $this->translate->_('Avatár')
                , '/admin/profile/avatar'
                , 'icon-profile'
            );
        }

        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'config')) {
            $this->headingButtons->add(
                $this->translate->_('Beállítások')
                , '/admin/profile/config'
                , 'icon-hammer'
            );
        }
    }

    /**
     * indexAction
     */
    public function indexAction()
    {
        return $this->dispatcher->forward(['action' => 'config']);
    }

    /**
     * Jelszó módosítás nézet
     */
    public function passwordAction()
    {
        $this->breadcrumb->add(
            $this->translate->_('Jelszó változtatás')
        );

        /**
         * Jelszó módosítás űrlap
         */
        $newPasswordForm = new \App\Form\Profile\Password();

        if($this->request->isPost()
            && $newPasswordForm->isValid($this->request->getPost())) {
            try {
                $this->db->beginLogged();

                /**
                 * @var \Qs\Model\User $user
                 */
                $user = $this->auth->getIdentity();

                /*
                 * Jelenlegi jelszó ellenörzése
                 */
                $currentPasswordFromValue = $newPasswordForm->get('currentPassword')->getValue();

                if(!$this->security->checkHash($currentPasswordFromValue, $user->password)) {
                    throw new Exception('Jelenlegi jelszó nem egyezik meg!');
                }

                /*
                 * Új jelszó mentése
                 */
                $user->setPassword($newPasswordForm->get('newPassword')->getValue());

                $user->update();

                $this->logger->info('Sikeres jelszó módosítás!');

                $this->db->commitLogged();

                $this->flash->success(
                    $this->translate->_('A jelszó módosítása sikerült!')
                );
            } catch (Exception $e) {
                $this->db->rollbackLogged();

                $this->logger->error($e->getMessage());
                $this->flash->error($e->getMessage());
            }
        }

        /*
         * Value értékek törlése
         */
        $newPasswordForm->get('currentPassword')->clear();
        $newPasswordForm->get('newPassword')->clear();
        $newPasswordForm->get('newPasswordAgain')->clear();

        $this->view->setVar('newPasswordForm', $newPasswordForm);
    }

    /**
     * @param int $imageWidth
     * @param int $imageHeight
     * @param string $imagePath
     * @param int $quality
     * @param string $message
     */
    private function imageUpload($imageWidth, $imageHeight, $imagePath, $quality, $message)
    {
        try {
            $files = $this->request->getUploadedFiles();

            $file = array_shift($files);

            $imageFormat = '.' . strtolower($file->getExtension());

            /* Kép mentése */
            $file->moveTo($imagePath . $imageFormat);

            /* Kép méret módosítása */
            $image = new Gd($imagePath . $imageFormat);

            if($image->getWidth() > $image->getHeight()) {
                /* Fekvő kép esetén */
                $image
                    ->resize(null, $imageHeight, Image::HEIGHT)
//                    ->crop($imageWidth, $imageHeight, ($image->getWidth() - $imageWidth) / 2, 0)
                ;
            } elseif($image->getWidth() < $image->getHeight()) {
                /* Álló kép esetén */
                $image
                    ->resize($imageWidth, null, Image::WIDTH)
//                    ->crop($imageWidth, $imageHeight, 0, ($image->getHeight() - $imageHeight) / 2)
                ;
            } else {
                /* Szélesség és a magasság megegyezik */
                $image->resize($imageWidth, $imageHeight);
            }

            /* Méretre vágott kép mentése */
            $image->save($imagePath . '.jpg', $quality);

            /* Ha a feltöltött kép .png formátumú volt, akkor ezt a fájlt töröljük a mappából. */
            if($imageFormat == '.png') {
                @unlink($imagePath . $imageFormat);
            }

            $this->flash->success($message);

        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Profilkép feltöltés nézet
     */
    public function avatarAction()
    {
        $this->breadcrumb->add(
            $this->translate->_('Avatár')
        );

        $form = new \App\Form\Profile\Avatar();

        if($this->request->hasFiles()
            && $form->isValid($_FILES)) {
            $user = $this->auth->getIdentity();

            $this->imageUpload(
                $this->config->user->avatar->width,
                $this->config->user->avatar->height,
                $this->config->user->avatar->path . md5($user->userId),
                $this->config->user->avatar->quality,
                $this->translate->_('Az avatár frissítése sikerült!')
            );
        }

        $this->view->setVar('avatar', $this->auth->getIdentity()->getAvatarImageUrl(false));
        $this->view->setVar('form', $form);
    }

    /**
     * configAction
     */
    public function configAction()
    {
        $this->breadcrumb->add(
            $this->translate->_('Beállítások')
        );

        $user = $this->auth->getIdentity();

        if ($this->request->isPost()) {
            try {
                $this->db->beginLogged();

                if ($languageId = $this->request->getPost('languageId')) {
                    $user->languageId = (int)$languageId;
                }

                $user->update();

                $message = $this->translate->_('A beállítások módosítása sikerült!');

                $this->logger->info($message);

                $this->db->commitLogged();

                $this->flash->success($message);
            } catch (Exception $e) {
                $this->db->rollbackLogged();

                $this->logger->error($e->getMessage());
                $this->flash->error($e->getMessage());
            }
        }

        $this->view->form = new \App\Form\Profile\Config($user);
    }
}