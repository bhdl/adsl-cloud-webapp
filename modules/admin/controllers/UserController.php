<?php

use App\Controller\AbstractCrudController;
use App\Support\Str;

/**
 * Class UserController
 */
class UserController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\User'
        , 'builder'   => 'App\Model\Builder\User'
        , 'presenter' => 'App\Model\Presenter\User'
        , 'export'    => [
            'spreadsheet' => 'App\Export\Spreadsheet\Rows\User'
            , 'pdf'       => 'App\Export\Pdf\Template\User'
        ]
        , 'forms' => [
            'create'   => 'App\Form\User\Create'
            , 'modify' => 'App\Form\User\Modify'
            , 'search' => 'App\Form\User\Search'
        ]
        , 'name' => [
            'singular' => 'Felhasználó'
            , 'plural' => 'Felhasználók'
        ]
    ];


    public function beforeModelModify()
    {
        parent::beforeModelModify();

    }

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->view->urls = array_merge(
            $this->view->urls
            , [
                'send'    => $this->url('send')
            ]
        );
    }

    /**
     * @param $userId
     * @throws Exception
     */
    public function sendAction($userId)
    {
        try {
            $this->db->beginLogged();

            /*
             * User jelszó változtatás
             */
            $password = $this->model->password = Str::randomHash(8);
            $this->model->update();

            /*
             * Küldés levélben
             */
            $message = $this->mailer->createMessageFromView(
                'userCredentials'
                , [
                    'name'       => $this->model->getName()
                    , 'email'    => $this->model->email
                    , 'url'      => $this->app->getAppUrl('/admin')
                    , 'password' => $password
                ]
            );
            $message
                ->to($this->model->email, $this->model->getName())
                ->subject("Belépési adatok")
            ;

            if (!$message->send()) {
                throw new \Phalcon\Exception(
                    $this->translate->t('Nem sikerül a belépési adatok elküldése.')
                );
            }

            $message = $this->translate->t('A belépési adatokat elküldte az alábbi e-mail címre: %email%', [
                'email' => $this->model->email
            ]);

            $this->logger->info($message);

            $this->db->commitLogged();

            $this->flashSession->success($message);

            $this->redirect('index');

        } catch (\Phalcon\Exception $e) {
            $this->db->rollbackLogged();

            $this->logger->error($e->getMessage());

            $this->flashSession->error($e->getMessage());
        }
    }

    /**
     * @return void
     */
    protected function addEntityHeadingButtons()
    {
        parent::addEntityHeadingButtons();

        if ($this->acl->isAllowedByIdentity('lab.auth', 'change')) {
            $this->headingButtons->add(
                $this->translate->_('Felhasználóváltás')
                , $this->url('change', 'auth', null, $this->model->getPrimaryKey())
                , 'icon-users2'
            );
        }

        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'send')) {

            $url     = $this->url('send', null, null, $this->model->getPrimaryKey());
            $message = $this->translate->t('Biztosan újra el szeretné küldeni a felhasználónak a belépési adatait? A művelet során a rendszer új jelszót fog készíteni.');

            $this->headingButtons->add(
                $this->translate->_('Belépési adatok újraküldése')
                , 'javascript:BootBox.confirm(\'' . $message . '\', function(result){if(result)document.location.href=\'' . $url . '\';})'
                , 'icon-mail5'
            );
        }
    }
}