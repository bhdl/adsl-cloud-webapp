<?php
use App\Controller\AbstractCrudController;

/**
 * Class AclController
 */
class AclController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\AclRole'
        , 'builder'   => 'App\Model\Builder\AclRole'
        , 'forms' => [
            'create'   => 'App\Form\Acl\Create'
            , 'modify' => 'App\Form\Acl\Modify'
        ]
        , 'name' => [
            'singular' => 'Szerepkör'
            , 'plural' => 'Szerepkörök'
        ]
    ];

    /**
     * afterModelCreate
     */
    public function afterModelCreate()
    {
        $acl = $this->request->getPost("aclMatrix");

        $this->model->clearAclMatrix();

        if (is_array($acl)){
            $this->model->addAclRoles($acl);
        }

        if ($this->aclCache) {
            $this->aclCache->flush();
        }
    }

    /**
     * afterModelModify
     */
    public function afterModelModify()
    {
        $this->afterModelCreate();
    }

    /**
     * afterModelDelete
     */
    public function afterModelDelete()
    {
        if ($this->aclCache) {
            $this->aclCache->flush();
        }
    }
}