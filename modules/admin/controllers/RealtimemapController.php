<?php

use Phalcon\Exception;

/**
 * Class RealtimemapController
 */
class RealtimemapController
    extends \App\Controller\AbstractController
{
    /**
     * initialize
     */
    public function initialize()
    {

    }

    /**
     * indexAction
     */
    public function indexAction()
    {
        $mapData = \App\Model\Site::findFirst(1);

        $this->view->setVar("mapData", $mapData);
        
        // installing adsl-imap js project is required in public folder with yarn, and setting the gitlab config file to access the repo
        $this->assets
        ->addJs('/node_modules/adsl-imap/js/vendors.min.js')
        ->addJs('/node_modules/adsl-imap/js/imap.min.js')
        ->addCss('/node_modules/adsl-imap/css/imap.min.css')
        ;
        
        $tags = \App\Model\Tag::find();
        $this->view->setVar("tags", $tags);
    }
}