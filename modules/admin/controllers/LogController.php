<?php

use App\Controller\AbstractCrudController;

/**
 * Class LogController
 */
class LogController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\Log'
        , 'presenter' => 'App\Model\Presenter\Log'
        , 'builder'   => 'App\Model\Builder\Log'
        , 'forms' => [
            'search' => 'App\Form\Log\Search'
        ]
        , 'name' => [
            'singular' => 'Napló'
            , 'plural' => 'Naplók'
        ]
    ];

    /**
     * viewAction
     * @param int $logId
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface|void
     */
    public function viewAction($logId)
    {
        parent::viewAction($logId);

        $this->view->pick('log/view');
    }
}