<?php

use App\Controller\AbstractCrudController;
use App\Support\Str;

/**
 * Class ProductpositioningController
 */
class ProductpositioningController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\ProductPosition'
        , 'name' => [
            'singular' => 'Product Posistion'
            , 'plural' => 'Products Positions'
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     *
     */
    public function indexAction()
    {
        $this->view->setVar('productPositionEvent', \App\Model\ProductPositionEvent::find(["positionEventData IS NULL"]));
        $this->view->setVar('products', $this->getProductPosition());
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     * @throws Exception
     */
    public function ajaxAction()
    {
        if (!$this->request->isAjax()) {
            return $this->response->setJsonContent([]);
        }

        $json = null;

        switch ($this->request->get('operation')) {
            case 'search':
                $json = $this->ajaxSearch();
                break;
            case 'product-list':
                $json = $this->ajaxProductPositionList();
                break;
            case 'start-positioning':
                $json = $this->startRepositioning();
            default:
                break;
        }

        return $this->response->setJsonContent($json);
    }

    /**
     *
     */
    private function ajaxSearch()
    {
        return [
            'errors'   => false
            , 'result' => $this->getProductPosition($this->request->get('value'))
        ];
    }

    /**
     * @param bool $searchedText
     * @return array
     */
    private function getProductPosition($searchedText = false)
    {
        $products = [];
        $productPositionEventId = \App\Model\ProductPosition::maximum([
            "column" => "productPositionEventId"
        ]);

        if ($searchedText) {
            $text = " AND p.name LIKE '%".$searchedText."%' ";
        }

        if ($productPositionEventId) {
            $sql = "SELECT count(*) as db,pp.productId, p.name FROM product_position pp
                    JOIN product as p
                    ON p.productId = pp.productId
                    WHERE pp.productPositionEventId = ".$productPositionEventId.$text." GROUP BY pp.productId";
            $products = $this->getDi()->get('db')->fetchAll($sql);
        }

        return $products;
    }

    /**
     * @return mixed
     */
    private function ajaxProductPositionList()
    {
        $products = [];

        $productPositionEventId = \App\Model\ProductPosition::maximum([
            "column" => "productPositionEventId"
        ]);

        if ($productPositionEventId)  {
            $products = \App\Model\ProductPosition::find([
                " productPositionEventId = $productPositionEventId "
                ."AND productId = ".$this->request->get('productId')
            ]);
        }

        return $products;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function startRepositioning()
    {
        $url = $_ENV["CLOUD_DATA_MAPPER_URL"] . "rest/" . $_ENV["API_VERSION"] . "/startProductOrganisation";

        $client = new GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
            'body' => 'raw data'
        ]);
        if ($res->getStatusCode() == 200) {
            $productRepositioning = new \App\Model\ProductPositionEvent();
            $productRepositioning->start = date("Y-m-d H:i:s");
            $productRepositioning->save();
        }
    }
}
