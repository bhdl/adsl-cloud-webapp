<?php

use App\Controller\AbstractCrudController;

use App\Model\Page;

/**
 * Class class ProductinstanceController
 */
class ProductinstanceController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\ProductInstance'
        , 'forms' => [
            'create'   => 'App\Form\ProductInstance\Create'
            , 'modify' => 'App\Form\ProductInstance\Modify'
        ]
        , 'name' => [
            'singular' => 'Termék raktározása'
            , 'plural' => 'Termékek raktározása'
        ]
    ];

    public function indexAction()
    {
        $this->form = new \App\Form\ProductInstance\Create();
        
        $this->view->setVar('form', $this->form);
        $this->view->setVar('productStatuses', \App\Model\ProductStatus::find());
        $this->view->setVar("siteList",    \App\Model\Site::find());
        $this->view->setVar("productList", \App\Model\Product::find());
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function ajaxAction()
    {
        if (!$this->request->isAjax()) {
            return $this->response->setJsonContent([]);
        }

        $json = null;

        switch ($this->request->get('operation')) {

            case 'getProductInstances':
                $json = \App\Model\ProductInstance::find(
                    'productId = '.$this->request->get('productId')
                    .' AND siteId = '.$this->request->get('siteId')
                );
                break;
            case 'createProductInstances':
                $json = $this->createAction();
                break;
            case 'deleteProductInstance':
                $json = $this->deleteAction();
                break;
            /*
             * Session keep-alive
             */
            case 'keepAlive':
                $json = [];
                break;

            default:
                break;
        }

        return $this->response->setJsonContent($json);
    }

    /**
     * @return bool|\Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     * @throws \Phalcon\Exception
     */
    public function createAction() {
        $this->form = $this->getForm('create');

        $isValid = $this->form->isValid($this->request->getPost());

        if(!$isValid) {
            $message = $this->form->getMessages();
            $message = [
                "error_message" => $message[0]->getMessage()." (".$message[0]->getField().")"
                , "error_field" => $message[0]->getField()
            ];
        }

        if ($isValid && $this->request->isPost()) {
            try {

                $this->db->beginLogged();

                $rfids = explode("\n", $this->request->get("rfid"));
                
                if (!empty($rfids)) {
                    foreach($rfids as $rfid) {
                        $this->model = new \App\Model\ProductInstance();

                        $this->model->rfid = $rfid;
                        $this->model->productStatusId = $this->request->get("productStatusId");
                        $this->model->productId = $this->request->get("productId");
                        $this->model->siteId = $this->request->get("siteId");

                        $this->beforeModelCreate();
                        
                        $this->model->create();
                        
                        $this->afterModelCreate();
                    }        
                }
                
                $message = $this->translate->_(
                    'A(z) %name% létrehozása sikerült.'
                    , ['name' => $this->getSingularName(true)]
                );

                $this->logger->info($message);

                $this->db->commitLogged();

                return $message;

            } catch (Exception $e) {

                $this->db->rollbackLogged();

                $message = $this->translate->_(
                    'A(z) %name% létrehozása közben hiba történt: %message%'
                    , [
                        'name' => $this->getSingularName(true)
                        , 'message' => $e->getMessage()
                    ]
                );

                if ($this->model->validationHasFailed()) {
                    $message = $this->model->getMessagesAsString();
                }
                
                return $message;
            }
        }

        return $message;
    }

    public function deleteAction() {
        try {

            $this->db->beginLogged();

            $this->beforeModelDelete();

            $this->model = \App\Model\ProductInstance::find(
                $this->request->get("productInstanceId")
            );
            $result = $this->model->delete();
            if (!$result) {
                throw new Exception();
            }

            $this->afterModelDelete();

            $message = $this->translate->_(
                'A(z) %name% törlése sikerült.'
                , ['name' => $this->getSingularName(true)]
            );

            $this->logger->info($message);

            $this->db->commitLogged();

        } catch (Exception $e) {

            $this->db->rollbackLogged();

            $message = $this->translate->_(
                'A(z) %name% törlése közben hiba történt: %message%'
                , [
                    'name' => $this->getSingularName(true)
                    , 'message' => $e->getMessage()
                ]
            );

            if ($this->model->validationHasFailed()) {
                $message = $this->model->getMessagesAsString();
            }
        }

        return $message;
    }
}
