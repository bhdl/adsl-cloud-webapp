<?php

use App\Controller\AbstractCrudController;
use App\Support\Str;

/**
 * Class PartnerController
 */
class PartnerController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\Partner'
        , 'builder'   => 'App\Model\Builder\Partner'
        , 'presenter' => 'App\Model\Presenter\Partner'
        , 'forms' => [
            'create'   => 'App\Form\Partner\Create'
            , 'modify' => 'App\Form\Partner\Modify'
        ]
        , 'name' => [
            'singular' => 'Partner'
            , 'plural' => 'Partners'
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();
    }
}