<?php

use App\Controller\AbstractController;
use App\Auth\Adapter\Database;
use App\Form\Auth\Login as LoginForm;
use Phalcon\Exception;

/**
 * Class Auth
 * @package Qs\Controller
 */
class AuthController
    extends AbstractController
{
    /**
     * Kijelentkezés utáni oldalátirányítás url-je
     * @var string
     */
    protected $logoutRedirectUrl = '/admin';

    /**
     * Bejelentkezés utáni oldalátirányítás url-je
     * @var string
     */
    protected $loginRedirectUrl = '/admin';

    /**
     * @var string
     */
    protected $modelClass = 'App\Model\User';

    /**
     * initialize
     */
    public function initialize()
    {
        $this->view->setMainView('layouts/auth');
    }

    /**
     * Bejelentkezés action
     * @throws \Phalcon\Exception
     */
    public function indexAction()
    {
        $loginForm = new LoginForm();

        if ($this->request->isPost()
            && $loginForm->isValid($this->request->getPost())) {

            try {
                $email    = $loginForm->get('email')->getValue();
                $password = $loginForm->get('password')->getValue();

                /*
                 * Felhasználó bejelentkeztetése
                 */
                $adapter = new Database();
                $adapter->setEmail($email);
                $adapter->setPassword($password);
                $adapter->setModel($this->modelClass);

                /**
                 * @var \App\Model\User $user
                 */
                $user = $this->auth->login($adapter);

                $redirectUrl = $this->userLogin($user);

                $this->response->redirect($redirectUrl);
                $this->view->disable();
            } catch (Exception $e) {
                $message = $this->translate->_($e->getMessage());
                $this->logger->error($message, ['exception' => $e]);
                $this->flash->error($message);
            }
        }

        $this->view->form = $loginForm;

        /* Title beállítása */
        $this->tag->prependTitle('Bejelentkezés');
    }

    /**
     * Kijelentkezés action
     * @throws \Phalcon\Exception
     */
    public function logoutAction()
    {
        try {

            // naplózás, még mielőtt visszaállítja az auth identity-t vendégre
            $this->logger->info(
                $this->translate->_('Kijelentkezés sikeres.')
            );

            /*
             * Felhasználó kijelentkeztetése
             */
            $this->auth->logout();

            /*
             * Oldal átirányítás
             */
            $this->response->redirect(
                $this->getRedirectUrl($this->getLogoutRedirectUrl())
            );

            $this->view->disable();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            $this->flash->error($e->getMessage());
        }
    }

    /**
     * Belépés más felhasználó nevében
     *
     * @param $userId
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     * @throws Exception
     */
    public function changeAction($userId = null)
    {
        if (is_numeric($userId)) {
            // váltás
            $user = \App\Model\User::findFirst($userId);
            if (!$user) {
                throw new Exception(
                    'Nem létező felhasználó.'
                );
            }

            // aktuális user mentése
            $this->auth->setIdentity(
                $this->auth->getIdentity()
                , true
            );
        } else {
            // visszaállítás
            $user = $this->auth->getIdentity(true);
            $this->auth->logout(true);
        }

        // aktuális user felülírása
        $this->userLogin($user);

        // redirect
        return $this->response->redirect(
            $this->userLogin($user)
        );
    }

    /**
     * Felhasználó bejelentkeztetése
     * @param \App\Model\User $user
     * @return string $redirectUrl
     */
    protected function userLogin($user)
    {
        /*
         * Felhasznáűló adatok mentése Session-be
         */
        $this->auth->setIdentity($user);

        /*
         * Átirányítási url
         */
        $redirectUrl = $this->getRedirectUrl($this->getLoginRedirectUrl());

        // naplózás, itt már nem vendég az identity
        $this->logger->info($this->translate->_('Bejelentkezés sikeres.'));

        return $redirectUrl;
    }

    /**
     * @param string $fallbackRedirectUrl
     * @return string
     */
    protected function getRedirectUrl(string $fallbackRedirectUrl): string
    {
        /* ha kaptunk returnUrl paramétert, akkor oda kell továbbítani */
        $url = base64_decode($this->request->get('redirectUrl', null, ''));

        if (empty($url)) {
            $url = $fallbackRedirectUrl;
        }

        return $url;
    }

    /**
     * @return string
     */
    public function getLogoutRedirectUrl(): string
    {
        return $this->logoutRedirectUrl;
    }

    /**
     * @return string
     */
    public function getLoginRedirectUrl(): string
    {
        return $this->loginRedirectUrl;
    }

    /**
     * @param string $logoutRedirectUrl
     */
    public function setLogoutRedirectUrl(string $logoutRedirectUrl)
    {
        $this->logoutRedirectUrl = $logoutRedirectUrl;
    }

    /**
     * @param string $loginRedirectUrl
     */
    public function setLoginRedirectUrl(string $loginRedirectUrl)
    {
        $this->loginRedirectUrl = $loginRedirectUrl;
    }
}