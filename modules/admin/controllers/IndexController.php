<?php

use App\Controller\AbstractController;

/**
 * Class IndexController
 */
class IndexController
    extends AbstractController
{
    /**
     * indexAction
     */
    public function indexAction()
    {
        $this->tag->prependTitle('Nyitólap');

        $language = $this
            ->auth
            ->getIdentity()
            ->getInterfaceLanguage()
        ;

        $this->view->language = $language;
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function ajaxAction()
    {
        if (!$this->request->isAjax()) {
            return $this->response->setJsonContent([]);
        }

        $json = null;

        switch ($this->request->get('operation')) {

            case 'dataTableStateSave':
                $json = $this->ajaxDataTableStateSaveAction(
                    $this->request->get('id')
                    , $this->request->get('data')
                );
                break;

            case 'dataTableStateLoad':
                $json = $this->ajaxDataTableStateLoadAction(
                    $this->request->get('id')
                );
                break;

            /*
             * Session keep-alive
             */
            case 'keepAlive':
                $json = [];
                break;

            default:
                break;
        }

        return $this->response->setJsonContent($json);
    }

    /**
     * @param string  $id
     * @param array $data
     * @return array
     */
    protected function ajaxDataTableStateSaveAction($id, $data)
    {
        try {
            $model = $this->getDataTableStateModel($id);
            $model->data = $data;
            $model->save();
        } catch (\Phalcon\Exception $e) {
            $this->logger->error(
                $e->getMessage()
                , [
                    'exception' => $e
                ]
            );

            return [
                'errors'   => true
                , 'result' => null
            ];
        }

        return [
            'errors'   => false
            , 'result' => null
        ];
    }

    /**
     * @param string $id
     * @return array
     */
    protected function ajaxDataTableStateLoadAction($id)
    {
        $model = $this->getDataTableStateModel($id);

        return [
            'errors'   => false
            , 'result' => $model->data
        ];
    }

    /**
     * @param string $id
     * @return \App\Model\DataTableState|mixed
     */
    protected function getDataTableStateModel($id)
    {
        $model = \App\Model\DataTableState::findFirst(
            'dataTableStateId = \'' . $id . '\''
        );

        if (!$model) {
            $model = new \App\Model\DataTableState([
                'dataTableStateId' => $id
            ]);
        }

        return $model;
    }
}
