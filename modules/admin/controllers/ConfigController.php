<?php

use App\Controller\AbstractCrudController;

use App\Form\Config\Modify as ConfigForm;
use App\Model\Config as ConfigModel;

/**
 * Class ConfigController
 */
class ConfigController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\Config'
        , 'name' => [
            'singular' => 'Beállítás'
            , 'plural' => 'Beállítások'
        ]
    ];

    /**
     * Index action
     */
    public function indexAction()
    {
        $model = ConfigModel::findFirst();

        $form = new ConfigForm($model);

        if ($this->request->isPost()
            && $form->isValid($this->request->getPost())) {

            try {

                $this->db->beginLogged();

                $form->bind(
                    $this->request->getPost()
                    , $model
                );

                $model->update();

                $message = $this->translate->_(
                    'A(z) %name% módosítása sikerült.'
                    , ['name' => $this->getPluralName(true)]
                );

                $this->logger->info($message);

                $this->db->commitLogged();

                $this->flash->success(
                    $message
                );

            } catch (Exception $e) {
                $this->db->rollbackLogged();

                $message = $this->translate->_(
                    'Sajnáljuk, a(z) %name% módosítása közben hiba történt (%message%).'
                    , [
                        'name' => $this->getPluralName(true)
                        , 'message' => $e->getMessage()
                    ]
                );

                if ($model->validationHasFailed()) {
                    $message = $model->getMessagesAsString();
                }

                $this->flash->error($message);

                $this->logger->error($message, ['exception' => $e]);
            }
        }

        $this->view->form = $form;
    }
}