<?php

use App\Controller\AbstractCrudController;
use App\Support\Str;

/**
 * Class InventoryeventController
 */
class InventoryeventController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\InventoryEvent'
        , 'builder'   => 'App\Model\Builder\InventoryEvent'
        , 'presenter' => 'App\Model\Presenter\InventoryEvent'
        , 'forms' => [
            'create'   => 'App\Form\InventoryEvent\Create'
            , 'modify' => 'App\Form\InventoryEvent\Modify'
        ]
        , 'name' => [
            'singular' => 'Inventory Event'
            , 'plural' => 'Inventory Events'
        ]
    ];

    /**
     * Dummy data
     */
    public $rfids;
    public $inventoryStatuses;
    public $inventorySnapshots;

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();
    }

    public function dummyData()
    {
        for($i=0; $i<60; $i++) {
            $this->rfids[] = uniqid();
        }

        /**
         * statusElement
         */
        for ($i=1; $i<50; $i++) {
            $statusElement = new stdClass();
            $statusElement->inventoryStatusId = $i;
            $statusElement->rfid = $this->rfids[$i];
            $statusElement->x = rand();
            $statusElement->y = rand();
            $statusElement->z = rand();
            $statusElement->accuracy = rand();
            $statusElement->latitude = rand();
            $statusElement->longitude = rand();
            $statusElement->inventoryEventId = 1;
            $this->inventoryStatuses[] = (array) $statusElement;
        }

        /**
         * snapshotElement
         */
        for ($i=1; $i<21; $i++) {
            $snapshotElement = new stdClass();
            $snapshotElement->inventorySnapshotId = $i;
            $snapshotElement->snapshotTime = "2018-09-01";
            $snapshotElement->rfid = $this->rfids[$i];
            $snapshotElement->status = "success";
            $snapshotElement->productId = $i;
            $snapshotElement->productInstanceId = rand();
            $snapshotElement->inventoryEvenetId = rand();
            $this->inventorySnapshots[] = $snapshotElement;
        }

        for ($i=31; $i<41; $i++) {
            $snapshotElement = new stdClass();
            $snapshotElement->inventorySnapshotId = $i;
            $snapshotElement->snapshotTime = "2018-09-01";
            $snapshotElement->rfid = $this->rfids[$i];
            $snapshotElement->status = "success";
            $snapshotElement->productId = $i;
            $snapshotElement->productInstanceId = rand();
            $snapshotElement->inventoryEvenetId = rand();
            $this->inventorySnapshots[] = $snapshotElement;
        }

        for ($i=41; $i<50; $i++) {
            for ($t=1;$t<40;$t++) {
                $snapshotElement = new stdClass();
                $snapshotElement->inventorySnapshotId = $i;
                $snapshotElement->snapshotTime = "2018-09-01";
                $snapshotElement->rfid = $this->rfids[$t];
                $snapshotElement->status = "success";
                $snapshotElement->productId = $i;
                $snapshotElement->productInstanceId = rand();
                $snapshotElement->inventoryEvenetId = rand();
                $this->inventorySnapshots[] = $snapshotElement;
            }
        }
    }

    public function viewAction($id)
    {
        if($this->request->get("dummy")) {
            $this->dummyData();
            $inventoryStatuses  = $this->inventoryStatuses;
            $inventorySnapshots = $this->inventorySnapshots;
        } else {
            $inventoryStatuses  = \App\Model\InventoryStatus::find("inventoryEventId =".$id)->toArray();
            $inventorySnapshots = \App\Model\InventorySnapshot::find("inventoryEventId =".$id);
        }

        $matchesCount = 0;
        $excessCount  = 0; //Többlet
        $deficitCount = 0; //Hiány
        $productList  = [];
        $excessList   = [];

        $productId = $this->request->get("productId") ? $this->request->get("productId") : false;

        foreach ($inventorySnapshots as $snapshotElement) {
            $snapshotElement = $snapshotElement;
            if ($productId && $snapshotElement->productId != $productId) {
                continue;
            }

            $temp = \App\Model\Product::find($snapshotElement->productId)->getFirst();
            $temp->matches = true;
            $temp->rfid = $snapshotElement->rfid;

            if ($statusElement   = $this->findInInventoryStatus($inventoryStatuses, $snapshotElement)) {
                $temp->matches   = true;
                $statusElement   = (object) $statusElement;
                $temp->latitude  = $statusElement->latitude;
                $temp->longitude = $statusElement->longitude;
                $matchesCount++;
            } else {
                $temp->matches = false;
                $deficitCount++;
            }


            $productList[] = $temp;
        }

        foreach ($inventoryStatuses as $statusElement) {
            if ($productId && $snapshotElement->productId != $productId) {
                continue;
            }
            $excessList[] = (object) $statusElement;
            $excessCount++;
        }

        if ($this->request->get("productId")) {
            $this->view->pick("inventoryEvent/viewByProductId");
            $this->view->setVar('inventoryEvent',   \App\Model\InventoryEvent::find($id)->getFirst());
            $this->view->setVar('product', \App\Model\Product::find($productId)->getFirst());
        } else {
            $this->view->setVar('inventoryEventId', $id);
        }

        $this->view->setVar('matchesCount', $matchesCount);
        $this->view->setVar('excessCount', $excessCount);
        $this->view->setVar('deficitCount', $deficitCount);
        $this->view->setVar('productList', $productList);
        $this->view->setVar('excessList', $excessList);
    }

    /**
     * @param array $inventoryStatuses
     * @param $snapshotElement
     * @return bool
     */
    public function findInInventoryStatus(&$inventoryStatuses, $snapshotElement)
    {
        foreach($inventoryStatuses as $key => $statusElement) {
            if($statusElement['rfid'] == $snapshotElement->rfid) {
                $temp = $inventoryStatuses[$key];
                unset($inventoryStatuses[$key]);
                return $temp;
            }
        }

        return false;
    }

    public function createAction()
    {
        parent::createAction();
        $this->view->pick("inventoryEvent/partials/crud/create");
    }
}
