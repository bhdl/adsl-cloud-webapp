<?php

use App\Controller\AbstractCrudController;
use App\Support\Str;

/**
 * Class TagController
 */
class TagController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\Tag'
        , 'builder'   => 'App\Model\Builder\Tag'
        , 'presenter' => 'App\Model\Presenter\Tag'
        , 'forms' => [
            'create'   => 'App\Form\Tag\Create'
            , 'modify' => 'App\Form\Tag\Modify'
        ]
        , 'name' => [
            'singular' => 'Tag'
            , 'plural' => 'Tags'
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();
    }
}