<?php

use App\Controller\AbstractCrudController;

/**
 * Class CronController
 */
class CronController extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\Cron'
        , 'presenter' => 'App\Model\Presenter\Cron'
        , 'builder'   => 'App\Model\Builder\Cron'
        , 'name' => [
            'singular' => 'Ütemezett feladat'
            , 'plural' => 'Ütemezett feladatok'
        ]
    ];
}
