<?php

use App\Controller\AbstractCrudController;

use DataTables\DataTable;
use Qs\Model\AbstractModel;

/**
 * Class GroupController
 */
class GroupController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\Group'
        , 'builder'   => 'App\Model\Builder\Group'
        , 'presenter' => 'App\Model\Presenter\Group'
        , 'forms' => [
            'create'   => 'App\Form\Group\Create'
            , 'modify' => 'App\Form\Group\Modify'
        ]
        , 'name' => [
            'singular' => 'Csoport'
            , 'plural' => 'Csoportok'
        ]
    ];

    /**
     *
     */
    public function afterModelCreate()
    {
        $roleIds = $this->request->getPost("roleId");
        $this->model->clearGroupHasAclRole();
        foreach ($roleIds as $roleId) {
            $this->model->addGroupHasAclRoles([$roleId]);
        }
    }

    /**
     *
     */
    public function afterModelModify()
    {
        $this->afterModelCreate();
    }
}