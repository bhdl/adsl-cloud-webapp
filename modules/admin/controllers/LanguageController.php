<?php

use App\Controller\AbstractCrudController;
use App\Model\Translation;
use DataTables\DataTable;

/**
 * Class LanguageController
 */
class LanguageController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\Language'
        , 'builder'   => 'App\Model\Builder\Language'
        , 'presenter' => 'App\Model\Presenter\Language'
        , 'forms' => [
            'create'   => 'App\Form\Language\Create'
            , 'modify' => 'App\Form\Language\Modify'
        ]
        , 'name' => [
            'singular' => 'Nyelv'
            , 'plural' => 'Nyelvek'
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->view->urls = array_merge(
            $this->view->urls
            , [
                'translate' => $this->url('translate')
            ]
        );
    }

    /**
     * @param int $languageId
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function changeAction($languageId)
    {
        if (!is_numeric($languageId)) {
            return $this->redirect('index', 'index');
        }

        $user = $this->auth->getIdentity();
        $user->setInterfaceLanguage(\App\Model\Language::findFirst($languageId));

        if ($redirectUrl = $this->request->get('redirectUrl')) {
            return $this->response->redirect(
                base64_decode($redirectUrl)
            );
        }

        return $this->redirect('index', 'index');
    }

    /**
     * translateAction
     *
     * @param int|null $languageId
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function translateAction($languageId)
    {
        if ($this->request->isAjax()) {

            $key = $this->request->getPost('key');
            $value = $this->request->getPost('value');

            if ($languageId
                && $key
                && $value
            ) {
                return $this->updateTranslation(
                    $languageId
                    , $key
                    , $value
                );
            }

            return $this->translateDataTable($languageId);
        }

        $this->view->languageId = $languageId;

        $this->addEntityBreadCrumbs();

        $this->breadcrumb->add(
            $this->translate->t('Fordítás')
            , $this->url('translate', null, null, $languageId)
            , ''
        );

        $this->addEntityHeadingButtons();
    }

    /**
     * @param $languageId
     * @param $key
     * @param $value
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     * @throws Exception
     */
    protected function updateTranslation($languageId, $key, $value)
    {
        $json = [
            'errors' => false
        ];

        try {

            $this->db->begin();

            $data = [
                'key'          => $key
                , 'languageId' => $languageId
            ];

            $translation = Translation::findFirst([
                'conditions' => 'key = :key: AND languageId = :languageId:'
                , 'bind'     => $data
            ]);

            if (!$translation) {
                throw new Exception(
                    $this->transate->t('Nem létező kifejezés: %key%', [
                        'key' => $key
                    ])
                );
            }

            $translation->value = trim($value);

            $translation->update();

            $this->db->commit();

        } catch (\Phalcon\Exception $e) {

            $this->db->rollback();

            $this->logger->error(
                $this->translate->t(
                    'Az alábbi kulcs-érték pár mentése nem sikerült: %key%, %value%'
                    , [
                        'key'     => $key
                        , 'value' => $value
                    ]
                )
            );

            $json['errors'] = true;
        }

        return $this->response->setJsonContent(
            $json
        );
    }

    /**
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    protected function translateDataTable($languageId)
    {
        $builder = (new \App\Model\Builder\Translation())
            ->setParams(['languageId' => $languageId])
            ->build()
        ;

        $dataTables = new DataTable();
        $dataTables->fromBuilder($builder);

        return $this->response->setJsonContent(
            $dataTables->getResponse()
        );
    }

    /**
     * @return AbstractCrudController|void
     */
    protected function addEntityHeadingButtons()
    {
        parent::addEntityHeadingButtons();

        if ($this->acl->isAllowedByIdentity($this->getResourceId(), 'translate')) {
            $this->headingButtons->add(
                $this->translate->_('Fordítás')
                , $this->url('translate', null, null, $this->model->getPrimaryKey())
                , 'icon-transmission'
            );
        }
    }
}