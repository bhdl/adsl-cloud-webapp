<?php

use App\Controller\AbstractCrudController;
use App\Support\Str;

/**
 * Class ProductController
 */
class ProductController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\Product'
        , 'builder'   => 'App\Model\Builder\Product'
        , 'presenter' => 'App\Model\Presenter\Product'
        , 'forms' => [
            'create'   => 'App\Form\Product\Create'
            , 'modify' => 'App\Form\Product\Modify'
        ]
        , 'name' => [
            'singular' => 'Product'
            , 'plural' => 'Products'
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();
    }
}