<?php

use App\Controller\AbstractCrudController;
use App\Support\Str;

/**
 * Class SiteController
 */
class SiteController
    extends AbstractCrudController
{
    /**
     * @var array
     */
    protected $setup = [
        'model' => 'App\Model\Site'
        , 'builder'   => 'App\Model\Builder\Site'
        , 'presenter' => 'App\Model\Presenter\Site'
        , 'forms' => [
            'create'   => 'App\Form\Site\Create'
            , 'modify' => 'App\Form\Site\Modify'
        ]
        , 'name' => [
            'singular' => 'Site'
            , 'plural' => 'Sites'
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();
    }
}